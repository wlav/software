__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import lmfit
import numpy as np


#...!...!....................
# input, amplitude, decay parameter, angular freq, offset phase, constant offset
def linear_f(x,A,C): 
    return  x* A +C 


###########################################################
# The standard practice of defining an ``lmfit`` model

class ModelLinear(lmfit.model.Model):
    __doc__ = "linear model" + lmfit.models.COMMON_DOC

#...!...!..................
    def __init__(self, *args, **kwargs):
        super().__init__(linear_f, *args, **kwargs)
        #self.set_param_hint('L', min=1e-9)        

#...!...!..................        
    def guess(self, data, x=None, **kwargs):

        verbose = kwargs.pop('verbose', None)
        if x is None:   return

        C=data.mean()
        A=0        
        if verbose:
            print('Guess: C=',C,'A=',A)
        params = self.make_params(A=A, C=C)
        #params['F0'].set(min=fmin, max=fmax)        
        # fix  some params to test sensitivity
        #params['C1'].vary = False
        return lmfit.models.update_param_vals(params, self.prefix, **kwargs)

