import numpy as np
from matplotlib import cm as cmap
from toolbox.PlotterBackbone import PlotterBackbone
#from QubicUtil import repack1D_cent2edge
import qutip

#............................
#............................
#............................
class Plotter(PlotterBackbone):
    def __init__(self, exper):
        PlotterBackbone.__init__(self,exper)
        self.cL7=['b', 'r','g', 'c', 'm', 'y', 'k']  # 7 distinct colors
        self.mL7=['*','^','x','h','o','x','D']

#...!...!..................
    def rabi_fit(self,dataTime, dataPop,plDD,figId=5):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(10,4))
        nrow,ncol=1,1
        rdQ=plDD['read_qubit']
        drQ=plDD['drive_qubit']
        QL=[rdQ,drQ]
        yLab='read %s counts'%rdQ
        tit='Rabi osc, drive amp=%.1f FS  %s %s  meas=%s'%(plDD['drive_rabi_amp'],plDD['chip_name'],plDD['fridge_name'],plDD['meas_date'])
        xLab='Rabi drive width (ns)'
        ax = self.plt.subplot(nrow,ncol,1)
        
        for it in range(2):
            dLab='%s'%(QL[it])
            dCol=self.cL7[it]
            jt=it*3
            yV=dataPop[:,jt+2]
            dFmt=self.mL7[it]
            ax.errorbar(dataTime,yV[0],yerr=yV[1],fmt=dFmt,color=dCol,markersize=2,linewidth=0.8,label=dLab)

            # add fit to ax=Z
            ftag="fit_axZ_dr%s"%(QL[it])
            ax.plot(dataTime,plDD[ftag+'_pred'],color=dCol)
            fitMD=plDD[ftag]
            FV=fitMD['fit_result']['fitPar']['F']
            redchi=fitMD['fit_result']['fitQA']['redchi'][0]
            #print(ftag,'freq:',str(freqV))
            txt1='drive %s -->  freq/MHz: %.3f +/- %.3f  chi2/ndf=%.1f'%(QL[it],FV[0]*1e3, FV[1]*1e3,redchi)
            print(ftag,txt1)#,'freq:',str(freqV))
            ax.text(0.04,0.9+0.06*it,txt1,transform=ax.transAxes,color=dCol,fontsize=10)

        # beautification...
        betaVal, betaErr=plDD['xtalk_strength']
        txt2='beta(%s,%s): %.3f +/-%.3f '%(rdQ,drQ,betaVal,betaErr)
        print(txt2)
        ax.text(0.04,0.02,txt2,transform=ax.transAxes,color='b',fontsize=12)
        
        ax.set_ylim(0,1.)
        ax.set_xlim(-5.,)
        ax.grid()
        ax.axhline(0.5,linestyle='--',linewidth=0.8,color='k')
        ax.set(xlabel=xLab, ylabel=yLab, title=tit)
        ax.legend(loc='lower right',title='drive qubit')
 
#...!...!..................
    def tomo_probs(self,dataTime, dataPop,plDD,figId=5):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(12,8))
        nrow,ncol=2,1
        nxyz=3
        paxL=['X','Y','Z']
        rdQ=plDD['read_qubit']
        drQ=plDD['drive_qubit']
        QL=[rdQ,drQ]
        yLab='read %s counts'%rdQ
        tit='tomography  Rabi amp=%.1f FS  %s %s  meas=%s'%(plDD['drive_rabi_amp'],plDD['chip_name'],plDD['fridge_name'],plDD['meas_date'])
        print('dataPop',dataPop.shape,dataTime.shape)
        for it in range(nrow):
            ax = self.plt.subplot(nrow,ncol,it+1)
            dataA=dataPop[:,it*3:it*3+3]
            xLab='%s Rabi drive width (ns)'%(QL[it])
            for k in range(nxyz):
                #if k!=0: continue
                data1=dataA[:,k]
                dLab='meas-%s'%(paxL[k])
                dFmt='o-'
                if k==2: dFmt='*'
                ax.errorbar(dataTime,data1[0],yerr=data1[1],fmt=dFmt,markersize=2,linewidth=0.8,label=dLab)
                ax.set_ylim(0,1.)
                ax.set_xlim(-5.,)
                ax.grid()
                ax.axhline(0.5,linestyle='--',linewidth=0.8,color='k')
                ax.set(xlabel=xLab, ylabel=yLab)
                if it==0: ax.set_title(tit)

            #continue #tmp
            # add fit to ax=Z
            ftag="fit_axZ_dr%s"%(QL[it])        
            ax.plot(dataTime,plDD[ftag+'_pred'],label='Z-fit')
            fitMD=plDD[ftag]
            freqV=fitMD['fit_result']['fitPar']['F']
            redchi=fitMD['fit_result']['fitQA']['redchi'][0]
            #print(ftag,'freq:',str(freqV))
            txt1='fit freq/MHz: %.3f +/- %.3f  chi2/ndf=%.1f'%(freqV[0]*1e3, freqV[1]*1e3,redchi)
            print(ftag,txt1)#,'freq:',str(freqV))
            ax.text(0.04,0.02,txt1,transform=ax.transAxes,color='r',fontsize=10)
            ax.legend(loc='best',title='meas axis')
        
#...!...!..................
    def expectation_xy(self,dataEV,plDD,figId=6):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(8,5))
        nrow,ncol=1,1
        rdQ=plDD['read_qubit']
        drQ=plDD['drive_qubit']
        QL=[rdQ,drQ]
        yLab=rdQ+' expected value <Y>';  xLab=rdQ+' expected value <X>'
        tit1='read %s X-Y state, Rabi amp=%.1f FS %s meas=%s'%(rdQ,plDD['drive_rabi_amp'],plDD['chip_name'],plDD['meas_date'])
        tit2='%s X-Y state projection  rabiAmp=%.1f FS  %s %s  %s'%(rdQ,plDD['drive_rabi_amp'],plDD['chip_name'],plDD['fridge_name'],plDD['meas_date'])

        ax = self.plt.subplot(nrow,ncol,1)
               
        for it in range(2):
            dLab='%s'%(QL[it])
            dCol=self.cL7[it]
            jt=it*3
            xV=dataEV[0,jt]  # use EV ignore errEV
            yV=dataEV[0,jt+1]
            ax.scatter(xV,yV,alpha=0.7,color=dCol,label=dLab)

            # add fit...
            ftag="fit_xy_dr%s"%(QL[it])
            dLab='fit %s'%(QL[it])
            fV=plDD[ftag+'_pred']
            ax.plot(xV,fV,color=dCol)
            #m=6;  ax.plot(xV[:m],fV[:m],color=dCol)
            fitMD=plDD[ftag]
            AV=fitMD['fit_result']['fitPar']['A']
            CV=fitMD['fit_result']['fitPar']['C']
            redchi=fitMD['fit_result']['fitQA']['redchi'][0]
            if it==1:
                m=4;dx=0.3
                ax.arrow(xV[0],fV[0],dx,dx*AV[0],color='k',head_length=0.06,head_width=0.08,width=0.025,alpha=0.6)
      
            txt1='%s fit=(%.2f +/-%.2f)*X + (%.2f +/-%.2f); chi2/ndf=%.1f'%(QL[it],AV[0],AV[1],CV[0],CV[1],redchi)
            print(ftag,txt1)
            ax.text(0.05,0.03+it*.08,txt1,transform=ax.transAxes,color=dCol,fontsize=12)
        # beautification...

        angVal,angErr,auni=plDD['xtalk_phase']
        degVal=angVal*57.296
        degErr=angErr*57.296
        txt2='tilt angle: %.2f +/-%.2f (%s) \n   = %.1f +-%.1f (deg)'%(angVal,angErr,auni,degVal,degErr)
        print(txt2)
        ax.text(0.04,0.89,txt2,transform=ax.transAxes,color=dCol,fontsize=12)
        ax.legend(loc='best',title='Rabi drive')
        ax.grid()
        ax.set_aspect(1.0)
        ax.set(xlabel=xLab, ylabel=yLab)
        dd=1.05
        ax.set_ylim(-.6,.6)
        ax.set_xlim(-dd,+dd)
        ax.axhline(0.,linestyle='--',linewidth=0.8,color='k')
        ax.axvline(0.,linestyle='--',linewidth=0.8,color='k')
        ax.set_title(tit1)

#...!...!..................
    def bloch(self,dataEV,plDD,figId=7, viewAngs=None):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(6,6))
        nrow,ncol=1,1

        print('dataEV:',dataEV.shape,sorted(plDD),'view=',viewAngs)
        
        ax = self.plt.subplot(nrow, ncol, 1)

        b=qutip.Bloch(view=viewAngs)
        '''
        view : list {[-60,30]}
        Azimuthal and Elevation viewing angles in deg, list
        '''
            
        b.add_vectors([1,0,0])
        
        up = qutip.basis(2,0)
        b.add_states(up)

        # input data order is : [X,Y,Z]*2
        # qutip expects: X,Y,Z
        b.add_points(dataEV[0,:3])
        b.add_points(dataEV[0,3:])
        
        b.show()

  
