import numpy as np
import hashlib
#import lmfit
import time

#...!...!....................
def do_yield_stats(dataY,dataVar=None): # vectorized version
    '''
    WARNING: this code works for M>1 qubits measurement 
    INPUT:  yield[NY,NX,NB], where
         NB: number of N-qubit bit-strings
         NX,NY: user defined (preserved) dimensions

    OUTPUT: prob[PE,NY,NX] , where
         PE=4 for: prob, probErr, yield, varYield 
    '''

    # assumes input np array [...,NY,NX,NB] with last index 'NB' enumarating all possible bit-strings. E.g. for 2 qubit --> NB=2^2=4

    # short: variance of nj-base yield given Ns-shots: var nj = Ns*var <xj> = nj*(Ns-nj)/(Ns-1).
    
    ''' Steve V: The way I think about this problem is that you have a total of Ns trials.  On any one trial, the probability of the throw ending up in state j is 1 or 0.  The mean probability <xj> of ending up in state j, over Ns tries, is nj/Ns and the uncertainty in that mean probability is given by

var <xj> = Sum(xj-<xj>)^2/[Ns(Ns-1)] = [nj(1-<xj>)^2 + (Ns-nj)<xj>^2]/[Ns(Ns-1)]

This reduces to:

var nj = Ns*var <xj> = nj*(Ns-nj)/(Ns-1).

I think this works for any number of substates and any large number of trials.
    '''
    
    assert dataY.ndim>=1  # works for arbitrary data dimensionality, just the bit-strings index must be the last one 
    NB=dataY.shape[-1]  # number of bit strings
    shots=np.sum(dataY,axis=-1)
    #print('DYS: dataY:',dataY.shape, dataY)
    
    # count empty labels
    nZero=np.sum(shots<=0)
    if nZero>0:
        print('\ndo_yield_stats:WARN, %d measurements have 0 shots ?!?, assume 1\n'%nZero)
    shots=np.clip(shots,1,None)  # assume std of yield==0 is 1

    #print('Stv:shots',shots.shape)
    shotsY=np.stack([shots]*NB,axis=-1)
    prob=dataY/shotsY
    print('Stv:prob',prob.shape)

    # assume # of shots >>1, and skip the '-1' from 1/(shots-1)

    if type(dataVar) is np.ndarray:
        bbb1
        # for derivation of the formula below
        # see https://www.overleaf.com/project/5bbc581a71604d62994dc350

        assert NB==dataVar.shape[-1]        
        Vs=np.sum(dataVar,axis=-1) # sum of variances
 
        var_nj=np.zeros(dataY.shape) # placeholder for results
        print('varSpam dim',var_nj.shape,Vs.shape)
        for j in range(NB):
            vari=prob[...,j]**2 * (Vs - dataVar[...,j]) + (1-prob[...,j])**2 * dataVar[...,j]
            var_nj[...,j]=vari
        probErr=np.sqrt(var_nj)/shotsY 

    else:
        # assume var(yiled)=yield, i.e. no SPAM correction was applied
        dataVar=dataY    
        #var_nj=dataY * ( shotsY-dataY) / shotsY
        #probErr1=np.sqrt(var_nj)/ shotsY
        var_nj=prob*(1-prob)/shotsY  # more computationally efficient 
        probErr=np.sqrt(var_nj)        
        
    dataProb=np.stack( [prob,probErr,dataY,dataVar], axis=0 ).astype('float32')
    return dataProb
    #where:  PE=4 for: prob, probErr, yield, varYield

#...!...!..................
def md5hash(text):
    hao = hashlib.md5(text.encode())
    hastr=hao.hexdigest()
    return hastr[-8:]  # use just last 8 characters from 32-long hash

#...!...!..................
def dateT2Str(xT):  # --> string
    nowStr=time.strftime("%Y%m%d_%H%M%S_%Z",xT)
    return nowStr


#...!...!..................
def prob_to_pauliExpectedValue(dataP):
    print('P2PEV:',dataP.shape)
    prob=dataP[0]
    probEr=dataP[1]
    dataEV=np.copy(dataP)  # keep yileds and varYields as-is
    dataEV[0]= 2*prob -1.
    dataEV[1]=2*probEr
    return dataEV
