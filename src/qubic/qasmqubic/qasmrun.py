import os
import json
import pickle
import bz2
import imp
import sys
sys.path.append('../../')
from qubic.qubic.squbic import c_qchip
from qubic.qubic.qubicrun import c_qubicrun
from qubic.qubic.chassis import c_chassis
from qubic.qasmqubic.qasmcircuit import qasmcircuits

def qasmrun(nsample,qasms,qubitsmap,heraldingqasm,**kwargs):
    ops={}
    ops['cfgpath'] = './submodules/qchip/' + os.environ['QUBIC_CHIP_NAME'] + '/'
    ops['ip'] = os.environ['QUBICIP']
    ops['sim']=False#True
    ops['phunit']='deg'
    ops['debug']=0
    ops['combineorder']=None
    ops['runpickle']=False
    ops['pk']=''
    ops['snapshotfilename']=None
    ops['heraldingsymbol']=None
    ops.update(**kwargs)
    ops['gmixs']=ops['cfgpath']
    ops['qubitcfgfile'] = ops['cfgpath'] + os.environ['QUBICQUBITCFG']
    ops['wiremapfile']= ops['cfgpath'] + os.environ['QUBICWIREMAP'] + '.py'
#    if ops['gmixspath'] is None:
#        ops['gmixspath']=ops['cfgpath']
    chassis=c_chassis(ops['ip'],sim=ops['sim'],debug=ops['debug'])
    if ops['runpickle']:
        pk=ops['pk']
        phyqubit=[]#[i for i in pk['readmaprev'].values()]
    else:
        print(ops['cfgpath'],ops['qubitcfgfile'])
        with open(ops['qubitcfgfile']) as jfile:
            qubitcfg=json.load(jfile)
        wiremap = imp.load_source("wiremap",ops['wiremapfile'])
        phyqubit=[i for l in qubitsmap.values() for i in l]
        qchip=c_qchip(qubitcfg)
    if ops['combineorder'] is None:
        ops['combineorder']=phyqubit

    if not ops['runpickle']:
        qasmcmds=qasmcircuits(qubitsmap=qubitsmap,qasms=qasms,**ops)#,para=[dict(phi=phi) for phi in range(0,360,5)])
        if heraldingqasm is not None:
            heraldcmds=qasmcircuits(qubitsmap=qubitsmap,qasms=heraldingqasm,**ops)#,para=[dict(phi=phi) for phi in range(0,360,5)])
            if ops['heraldingsymbol'] is None:
                ops['heraldingsymbol']='0' 
        else:
            heraldcmds=None
    else:
        qasmcmds=[]
        heraldcmds=[]
        qchip=None
        wiremap=None
        pass

    qr=c_qubicrun(chassis=chassis,qchip=qchip,wiremap=wiremap)
    qr.compile(seqs=qasmcmds,heraldcmds=heraldcmds,**ops)
    qr.accbufrun(nsample=nsample)
    qasmresult=qr.gmmcount()
    if ops['debug']>2:
        print('adc min max',chassis.adcminmax())

    return qasmresult

if __name__=="__main__":
    nsample=500
    qasms=[]
    heraldingqasm=None#[]
    for fname in [sys.argv[1]]:
        f=open(fname)
        qasms.append(f.read())
        f.close()
#    for fname in ['heralding01.qasm']:#,'gmm1.qasm']:
#        f=open(fname)
#        heraldingqasm.append(f.read())
#        f.close()
    qubitsmap={'q':['Q6','Q5']}#,'gamma':'QQQ'}

    qasmresult=qasmrun(nsample=nsample,qasms=qasms,qubitsmap=qubitsmap,heraldingqasm=heraldingqasm,cfgpath='../../../../qchip/chip57/')
    print(qasmresult)
#	qasmresult=qasmrun(nsample,qasms,qubitsmap,heraldingqasm,sim=False,gmixs=None,phunit='deg',debug=True,labels=['1','0'])
