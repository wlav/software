# Generated from qasm3.g4 by ANTLR 4.7.2
from antlr4 import *
#if __name__ is not None and "." in __name__:
#    from .antlr4qasm3.qasm3Parser import qasm3Parser
#    from .commVisitor import commVisitor
#else:
import sys
sys.path.append('../../')
from qubic.qasmqubic.antlr4qasm3.qasm3Parser import qasm3Parser
from qubic.qasmqubic.commVisitor import commVisitor

# This class defines a complete generic visitor for a parse tree produced by qasm3Parser.
import inspect
import numpy
class quanVisitor(commVisitor):
    def __init__(self):
        commVisitor.__init__(self)
        self.gatedefinition=False
        self.gates=[]
        self.qubits=[]
        self.variables=[]


    # Visit a parse tree produced by qasm3Parser#quantumDeclarationStatement.
    def visitQuantumDeclarationStatement(self, ctx:qasm3Parser.QuantumDeclarationStatementContext):
        return self.visitChildren(ctx)

    # Visit a parse tree produced by qasm3Parser#quantumDeclaration.
    def visitQuantumDeclaration(self, ctx:qasm3Parser.QuantumDeclarationContext):
        start=ctx.start.text
        Identifier=ctx.Identifier().getText()
        if ctx.designator():
            designator=self.visit(ctx.designator())
        else:
            designator=None
        value={'qdec':start,'Identifier':Identifier,'designator':designator}
        self.qubits.append(value)
        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#quantumInstruction.
    def visitQuantumInstruction(self, ctx:qasm3Parser.QuantumInstructionContext):

        return self.visitChildren(ctx)



    # Visit a parse tree produced by qasm3Parser#quantumGateDefinition.
    def visitQuantumGateDefinition(self, ctx:qasm3Parser.QuantumGateDefinitionContext):
        #        print(inspect.stack()[0][3],inspect.stack()[2][3])
        self.gatedefinition=True
        self.visitChildren(ctx)
        self.gatedefinition=False
#        return

    # Visit a parse tree produced by qasm3Parser#quantumGateSignature.
    def visitQuantumGateSignature(self, ctx:qasm3Parser.QuantumGateSignatureContext):
        #print(self.visit(ctx.quantumGateName()))
        identifierList=[]
        for iden in ctx.identifierList():
            identifierList.append(self.visit(iden))
        #print('debug signature',identifierList)

        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#quantumBlock.
    def visitQuantumBlock(self, ctx:qasm3Parser.QuantumBlockContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#quantumLoop.
    def visitQuantumLoop(self, ctx:qasm3Parser.QuantumLoopContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#quantumLoopBlock.
    def visitQuantumLoopBlock(self, ctx:qasm3Parser.QuantumLoopBlockContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#quantumStatement.
    def visitQuantumStatement(self, ctx:qasm3Parser.QuantumStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#quantumGateName.
    def visitQuantumGateName(self, ctx:qasm3Parser.QuantumGateNameContext):
        return ctx.getText()


    # Visit a parse tree produced by qasm3Parser#quantumGateCall.
    def visitQuantumGateCall(self, ctx:qasm3Parser.QuantumGateCallContext):
        quantumGateModifier=None
        if ctx.quantumGateModifier():
            quantumGateModifier=self.visit(ctx.quantumGateModifier())
        expressionList=None
        if ctx.expressionList():
            expressionList=self.visit(ctx.expressionList())
        indexIdentifierList=None
        if ctx.indexIdentifierList():
            indexIdentifierList=self.visit(ctx.indexIdentifierList())

        quantumGateName=self.visit(ctx.quantumGateName())

        if self.gatedefinition:
            #            print('debug defgate',{'quantumGateModifier':quantumGateModifier,'expressionList':expressionList,'quantumGateName':quantumGateName,'indexIdentifierList':indexIdentifierList})
            pass
        else:
            value={'quantumGateModifier':quantumGateModifier,'expressionList':expressionList,'quantumGateName':quantumGateName,'indexIdentifierList':indexIdentifierList}
            #print('debug qgate',value)
            self.gates.append(value)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by qasm3Parser#quantumPhase.
    def visitQuantumPhase(self, ctx:qasm3Parser.QuantumPhaseContext):
        quantumGateModifier=None
        if ctx.quantumGateModifier():
            quantumGateModifier=self.visit(ctx.quantumGateModifier())
        expression=None
        if ctx.expression():
            expression=self.visit(ctx.expression())
        indexIdentifierList=None
        if ctx.indexIdentifierList():
            indexIdentifierList=self.visit(ctx.indexIdentifierList())

        if self.gatedefinition:
            #            print('debug defphase',{'quantumGateModifier':quantumGateModifier,'expression':expression,'quantumGateName':'gphase','indexIdentifierList':indexIdentifierList})
            pass
        else:
            value={'quantumGateModifier':quantumGateModifier,'expression':expression,'quantumGateName':'gphase','indexIdentifierList':indexIdentifierList}
            #print('debug gphase',value)
            self.gates.append(value)
        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#quantumReset.
    def visitQuantumReset(self, ctx:qasm3Parser.QuantumResetContext):
        indexIdentifierList=None
        if ctx.indexIdentifierList():
            indexIdentifierList=self.visit(ctx.indexIdentifierList())

        if self.gatedefinition:
            #            print('debug defreset',{'quantumGateName':'reset','indexIdentifierList':indexIdentifierList})
            pass
        else:
            #print('debug reset',value)
            value={'quantumGateName':'reset','indexIdentifierList':indexIdentifierList}
            self.gates.append(value)
        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#quantumMeasurement.
    def visitQuantumMeasurement(self, ctx:qasm3Parser.QuantumMeasurementContext):
        indexIdentifierList=None
        if ctx.indexIdentifierList():
            indexIdentifierList=self.visit(ctx.indexIdentifierList())

        if self.gatedefinition:
#            print('debug defmeas',{'quantumGateName':'meas','indexIdentifierList':indexIdentifierList})
            pass
        else:
            value={'quantumGateName':'meas','indexIdentifierList':indexIdentifierList}
#            print('debug meas',value)
            self.gates.append(value)
        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#quantumMeasurementAssignment.
    def visitQuantumMeasurementAssignment(self, ctx:qasm3Parser.QuantumMeasurementAssignmentContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#quantumBarrier.
    def visitQuantumBarrier(self, ctx:qasm3Parser.QuantumBarrierContext):
        indexIdentifierList=None
        if ctx.indexIdentifierList():
            indexIdentifierList=self.visit(ctx.indexIdentifierList())

        if self.gatedefinition:
#            print('debug defbarrier',{'quantumGateName':'barrier','indexIdentifierList':indexIdentifierList})
            pass
        else:
            value={'quantumGateName':'barrier','indexIdentifierList':indexIdentifierList}
#            print('debug barrier',value)
            self.gates.append(value)
        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#quantumGateModifier.
    def visitQuantumGateModifier(self, ctx:qasm3Parser.QuantumGateModifierContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#powModifier.
    def visitPowModifier(self, ctx:qasm3Parser.PowModifierContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#ctrlModifier.
    def visitCtrlModifier(self, ctx:qasm3Parser.CtrlModifierContext):
        return self.visitChildren(ctx)

    # Visit a parse tree produced by qasm3Parser#timingInstruction.
    def visitTimingInstruction(self, ctx:qasm3Parser.TimingInstructionContext):
        expressionList=None
        if ctx.expressionList():
            expressionList=self.visit(ctx.expressionList())
        indexIdentifierList=None
        if ctx.indexIdentifierList():
            indexIdentifierList=self.visit(ctx.indexIdentifierList())
        if ctx.timingInstructionName():
            timingInstructionName=ctx.timingInstructionName().getText()
        if ctx.designator():
            designator=self.visit(ctx.designator())
        value={'timingInstructionName':timingInstructionName,'expressionList':expressionList,'designator':designator,'indexIdentifierList':indexIdentifierList}
#        print('debug gphase',value)
        self.gates.append(value)
        return self.visitChildren(ctx)


        return self.visitChildren(ctx)
#: timingInstructionName ( LPAREN expressionList? RPAREN )? designator indexIdentifierList
#: quantumGateModifier* 'gphase' LPAREN expression RPAREN indexIdentifierList?
del qasm3Parser
