"""
QubiC interface to QASM
"""
import sys
sys.path.append("../")
from qubic.qasmqubic import qasmqubic #c_qasmqubic
import datetime
import numpy

#def qasmcircuits(qubitsmap,qasms ,**kwargs):
#    """
#localgate: local gate pulse dict
#qubitsmap:  dictionary, mapping the qubit in qasm to physical qubit in the memcfg. Key should match the qreg declaration in the qasm, Value should match the gate name style in the memcfg
#qasms:  List of QASM strings, each of them representing a circuit
#nsample: int,  number of sample to average
#gmixs:  None or Dictionary of Gaussian mixture model, if None, will generate from the data. Dictionary key should match the physical qubit name
#
#phunit='rad' |'deg'
#heraldingqasm=None
#delaybetweenelement=600e-6
#delayafterheralding=12e-6
#preprocess=True
#"""
#    ops=dict(phunit='rad',heraldingqasm=None,delaybetweenelement=600e-6,delayafterheralding=12e-6,preprocess=True)
#    ops.update(kwargs)
##    print('qasmcircuits qasms',qasms)
#    circcmdlist=qasmcircuit(qubitsmap=qubitsmap,qasms=qasms,**ops)#phunit=ops['phunit'])
#    if ops['heraldingqasm'] is not None:
#        hrldcmdlist=qasmcircuit(qubitsmap=qubitsmap,qasms=ops['heraldingqasm'],**ops)#,phunit=ops['phunit'])
#    else:
#        hrldcmdlist=None
#    circcmds=commands.circuitgaps(circcmdlist=circcmdlist,hrldcmdlist=hrldcmdlist,**ops)#delaybetweenelement=ops['delaybetweenelement'],delayafterheralding=ops['delayafterheralding'])
#
#    return circcmds


def qasmcircuits(qasms,qubitsmap,preprocess=True,phunit='rad',**kwargs):
    ops=dict(para=[{}],debug=0)
    ops.update(kwargs)
    if ops['debug']>2:
        print('qasm',qasms)
    circcmdlist=[]
#    print('here 00',datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
    qubiccircuits=[]
    for index,qasmstr in enumerate(qasms):
        #        print('here 10',datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f'))
        #print('here 11',datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f'))
#        print('debug qasmcircuit',index,qasmstr)
#        cmdlistfun,lastpoint=qasmq.qasmqubic()

#        qasmq=qasmqubic.c_qasmqubic(qasmstr,qubitsmap,localgate,preprocess=preprocess,phunit=phunit,**ops)
        ops['circindex']=index        
        qasmqubiccir=qasmqubic.c_qasmqubic(qasmstr,qubitsmap,preprocess=preprocess,phunit=phunit,**ops)
        circuitgates=qasmqubiccir.qasmqubic()
        qubiccircuits.append(circuitgates)

#        qasmq=qasmqubic.c_qasmqubic(qasmstr,qubitsmap,localgate,preprocess=preprocess,phunit=phunit,**ops)
#        cmdlistfun,lastpoint=qasmq.qubictime(qasmq.qasmqubic())
        #for para in ops['para']:
            #            print('debug cmdlistfun',cmdlistfun)
        #    circcmdlist.append((cmdlistfun(**para),lastpoint))
        #    print('loading %d out of %d qasm circuit'%(index,len(qasms)))
    return qubiccircuits






if __name__=="__main__":
    from qubic.qubic.chassis import c_chassis
    from qubic.qubic.squbic import c_qchip
    import os
    import json
    from matplotlib import pyplot
    import imp
    memcfg=imp.load_source("memcfg","./submodules/qchip/X6Y8/wiremap_X6Y8_20210629.py")
    qubitcfgfile="submodules/qchip/X6Y8/qubitcfg_X6Y8.json"
    ip=os.environ['QUBICIP']
    delaybetweenelement=20e-6
    qyaml={}
    qyaml['qubitsmap']={'q':['Q1','Q5','Q6']}#,'gamma':'QQQ'}
    qyaml['qasms']=[]
    qyaml['qasms']=['''OPENQASM 3.0;
include "qelib1.inc";
qreg q[3];
creg meas[3];
measure q[0] -> meas[0];
measure q[1] -> meas[1];
measure q[2] -> meas[2];
barrier q[0],q[1],q[2];''',
'''OPENQASM 3.0;
include "qelib1.inc";
qreg q[3];
creg meas[3];
sx q[0];
sx q[0];
sx q[1];
sx q[1];
sx q[2];
sx q[2];
barrier q[0],q[1],q[2];
measure q[0] -> meas[0];
measure q[1] -> meas[1];
measure q[2] -> meas[2];
barrier q[0],q[1],q[2];''']
    with open(qubitcfgfile) as jfile:
        qubitcfg=json.load(jfile)
    qchip=c_qchip(qubitcfg)
    chassis=c_chassis(ip)
    accval,elemcmdcnt=qasmcircuit(qchip=qchip,chassis=chassis,memcfg=memcfg,qyaml=qyaml,delaybetweenelement=delaybetweenelement,nget=2)


    #print(accval.shape,elemcmdcnt)
    #print('adc min max',chassis.adcminmax())
    #pyplot.figure(2)
    if 1:
        pyplot.figure(figsize=(30,20))
        for isub in range(3):
            vr=accval[:,isub,:].real.reshape((-1,1))
            vi=accval[:,isub,:].imag.reshape((-1,1))
            print('%3d'%isub,'dr/r','%10.0f'%vr.std(),'%10.0f'%vr.mean(),'%8.5f'%(0 if vr.mean()==0 else vr.std()/vr.mean()),'di/i','%10.0f'%vi.std(),'%10.0f'%vi.mean(), '%8.5f'%(0 if vi.mean()==0 else vi.std()/vi.mean()))
            pyplot.subplot(2,3,isub+1)
            pyplot.title(isub)
            pyplot.plot(vr,vi,'.')
#        pyplot.xlim([-4000000,4000000])
#        pyplot.ylim([-4000000,4000000])
            pyplot.subplot(2,3,isub+4)
            pyplot.plot(vr,'r.')
            pyplot.plot(vi,'b.')
        pyplot.show()


