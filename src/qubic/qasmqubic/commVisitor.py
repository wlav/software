# Generated from qasm3.g4 by ANTLR 4.7.2
from antlr4 import *
#if __name__ is not None and "." in __name__:
#    from .antlr4qasm3.qasm3Parser import qasm3Parser
#    from ..qubic.expression import c_expression
#else:
import sys
sys.path.append('../..')
from qubic.qasmqubic.antlr4qasm3.qasm3Parser import qasm3Parser
from qubic.qubic.expression import c_expression

# This class defines a complete generic visitor for a parse tree produced by qasm3Parser.
import inspect
import numpy
import re
class commVisitor(ParseTreeVisitor):
    def __init__(self):
        self.Constant={'pi':numpy.pi,'π':numpy.pi,'tau':numpy.pi*2,'τ':numpy.pi*2,'euler':numpy.e,'ℇ':numpy.e}
        self.TimeUnit={'dt':c_expression('dt','dt'),'ns':1.0e-9,'us':1.0e-6,'µs':1.0e-6,'ms':1.0e-3,'s':1.0}
    def intorfloatorcexpre(self,var):
        return isinstance(var,int) or isinstance(var,float) or isinstance(var,c_expression)

#    # Visit a parse tree produced by qasm3Parser#program.
#    def visitProgram(self, ctx:qasm3Parser.ProgramContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#header.
#    def visitHeader(self, ctx:qasm3Parser.HeaderContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#version.
#    def visitVersion(self, ctx:qasm3Parser.VersionContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#include.
#    def visitInclude(self, ctx:qasm3Parser.IncludeContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#ioIdentifier.
#    def visitIoIdentifier(self, ctx:qasm3Parser.IoIdentifierContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#io.
#    def visitIo(self, ctx:qasm3Parser.IoContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#globalStatement.
#    def visitGlobalStatement(self, ctx:qasm3Parser.GlobalStatementContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#statement.
#    def visitStatement(self, ctx:qasm3Parser.StatementContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#quantumDeclarationStatement.
#    def visitQuantumDeclarationStatement(self, ctx:qasm3Parser.QuantumDeclarationStatementContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#classicalDeclarationStatement.
#    def visitClassicalDeclarationStatement(self, ctx:qasm3Parser.ClassicalDeclarationStatementContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#classicalAssignment.
#    def visitClassicalAssignment(self, ctx:qasm3Parser.ClassicalAssignmentContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#assignmentStatement.
#    def visitAssignmentStatement(self, ctx:qasm3Parser.AssignmentStatementContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#print(inspect.stack()[0][3],inspect.stack()[2][3])
#    def visitReturnSignature(self, ctx:qasm3Parser.ReturnSignatureContext):
#        return self.visitChildren(ctx)
#

    # Visit a parse tree produced by qasm3Parser#designator.
    def visitDesignator(self, ctx:qasm3Parser.DesignatorContext):
        value=self.visit(ctx.expression())
        return value


#    # Visit a parse tree produced by qasm3Parser#doubleDesignator.
#    def visitDoubleDesignator(self, ctx:qasm3Parser.DoubleDesignatorContext):
#        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#identifierList.
    def visitIdentifierList(self, ctx:qasm3Parser.IdentifierListContext):
        idenlist=[]
        for iden in ctx.Identifier():
            idenlist.append(iden.getText())
        return idenlist


#    # Visit a parse tree produced by qasm3Parser#quantumDeclaration.
#    def visitQuantumDeclaration(self, ctx:qasm3Parser.QuantumDeclarationContext):
#        return self.visitChildren(ctx)


#    # Visit a parse tree produced by qasm3Parser#quantumArgument.
#    def visitQuantumArgument(self, ctx:qasm3Parser.QuantumArgumentContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#quantumArgumentList.
#    def visitQuantumArgumentList(self, ctx:qasm3Parser.QuantumArgumentListContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#bitType.
#    def visitBitType(self, ctx:qasm3Parser.BitTypeContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#singleDesignatorType.
#    def visitSingleDesignatorType(self, ctx:qasm3Parser.SingleDesignatorTypeContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#doubleDesignatorType.
#    def visitDoubleDesignatorType(self, ctx:qasm3Parser.DoubleDesignatorTypeContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#noDesignatorType.
#    def visitNoDesignatorType(self, ctx:qasm3Parser.NoDesignatorTypeContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#classicalType.
#    def visitClassicalType(self, ctx:qasm3Parser.ClassicalTypeContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#constantDeclaration.
#    def visitConstantDeclaration(self, ctx:qasm3Parser.ConstantDeclarationContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#singleDesignatorDeclaration.
#    def visitSingleDesignatorDeclaration(self, ctx:qasm3Parser.SingleDesignatorDeclarationContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#doubleDesignatorDeclaration.
#    def visitDoubleDesignatorDeclaration(self, ctx:qasm3Parser.DoubleDesignatorDeclarationContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#noDesignatorDeclaration.
#    def visitNoDesignatorDeclaration(self, ctx:qasm3Parser.NoDesignatorDeclarationContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#bitDeclaration.
#    def visitBitDeclaration(self, ctx:qasm3Parser.BitDeclarationContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#classicalDeclaration.
#    def visitClassicalDeclaration(self, ctx:qasm3Parser.ClassicalDeclarationContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#classicalTypeList.
#    def visitClassicalTypeList(self, ctx:qasm3Parser.ClassicalTypeListContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#classicalArgument.
#    def visitClassicalArgument(self, ctx:qasm3Parser.ClassicalArgumentContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#classicalArgumentList.
#    def visitClassicalArgumentList(self, ctx:qasm3Parser.ClassicalArgumentListContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#aliasStatement.
#    def visitAliasStatement(self, ctx:qasm3Parser.AliasStatementContext):
#        return self.visitChildren(ctx)
#

    # Visit a parse tree produced by qasm3Parser#indexIdentifier.
    def visitIndexIdentifier(self, ctx:qasm3Parser.IndexIdentifierContext):
        if ctx.Identifier():
            identifier=ctx.Identifier().getText()
            if ctx.rangeDefinition():
                rangelist=self.visit(ctx.rangeDefinition())
            elif ctx.expressionList():
                rangelist=self.visit(ctx.expressionList())
                op=2
            else:
                rangelist=None
                op=3
            value={'Identifier':identifier,'rangelist':rangelist}
        elif ctx.indexIdentifier():
            indexid=[self.visit(index) for index in ctx.indexIdentifier()]
        else:
            print('in indexidentifier,else',ctx.toStringTree())
        return value


    # Visit a parse tree produced by qasm3Parser#indexIdentifierList.
    def visitIndexIdentifierList(self, ctx:qasm3Parser.IndexIdentifierListContext):
        indexIdentifierlist=[]
        for identifier in ctx.indexIdentifier():
            indexIdentifierlist.append(self.visit(identifier))
#        if len(indexIdentifierlist)==1:
#            value=indexIdentifierlist[0]
#        else:
        value=indexIdentifierlist
#        print(value)
        return value


    # Visit a parse tree produced by qasm3Parser#rangeDefinition.
    def visitRangeDefinition(self, ctx:qasm3Parser.RangeDefinitionContext):
        exlist=[self.visit(ex) for ex in ctx.expression()]
        value=range(exlist[0],exlist[1], 1 if len(exlist)==2 else exlist[2])
        print('debug range',value,type(value))
        return value


#    # Visit a parse tree produced by qasm3Parser#quantumGateDefinition.
#    def visitQuantumGateDefinition(self, ctx:qasm3Parser.QuantumGateDefinitionContext):
#        return self.visitChildren(ctx)

#
#    # Visit a parse tree produced by qasm3Parser#quantumGateSignature.
#    def visitQuantumGateSignature(self, ctx:qasm3Parser.QuantumGateSignatureContext):
#        return self.visitChildren(ctx)


#    # Visit a parse tree produced by qasm3Parser#quantumGateName.
#    def visitQuantumGateName(self, ctx:qasm3Parser.QuantumGateNameContext):
#        return self.visitChildren(ctx)
    #print(inspect.stack()[0][3],inspect.stack()[2][3])
    #    return ctx.getText()


#    # Visit a parse tree produced by qasm3Parser#quantumBlock.
#    def visitQuantumBlock(self, ctx:qasm3Parser.QuantumBlockContext):
#        return self.visitChildren(ctx)


#    # Visit a parse tree produced by qasm3Parser#quantumLoop.
#    def visitQuantumLoop(self, ctx:qasm3Parser.QuantumLoopContext):
#        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#quantumLoopBlock.
#    def visitQuantumLoopBlock(self, ctx:qasm3Parser.QuantumLoopBlockContext):
#        return self.visitChildren(ctx)


#    # Visit a parse tree produced by qasm3Parser#quantumStatement.
#    def visitQuantumStatement(self, ctx:qasm3Parser.QuantumStatementContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#quantumInstruction.
#    def visitQuantumInstruction(self, ctx:qasm3Parser.QuantumInstructionContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#quantumPhase.
#    def visitQuantumPhase(self, ctx:qasm3Parser.QuantumPhaseContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#quantumReset.
#    def visitQuantumReset(self, ctx:qasm3Parser.QuantumResetContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#quantumMeasurement.
#    def visitQuantumMeasurement(self, ctx:qasm3Parser.QuantumMeasurementContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#quantumMeasurementAssignment.
#    def visitQuantumMeasurementAssignment(self, ctx:qasm3Parser.QuantumMeasurementAssignmentContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#quantumBarrier.
#    def visitQuantumBarrier(self, ctx:qasm3Parser.QuantumBarrierContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#quantumGateModifier.
#    def visitQuantumGateModifier(self, ctx:qasm3Parser.QuantumGateModifierContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#powModifier.
#    def visitPowModifier(self, ctx:qasm3Parser.PowModifierContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#ctrlModifier.
#    def visitCtrlModifier(self, ctx:qasm3Parser.CtrlModifierContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#quantumGateCall.
#    def visitQuantumGateCall(self, ctx:qasm3Parser.QuantumGateCallContext):
#        return self.visitChildren(ctx)
#

    # Visit a parse tree produced by qasm3Parser#unaryOperator.
    def visitUnaryOperator(self, ctx:qasm3Parser.UnaryOperatorContext):
        return ctx.getText()


    # Visit a parse tree produced by qasm3Parser#comparisonOperator.
    def visitComparisonOperator(self, ctx:qasm3Parser.ComparisonOperatorContext):
        return ctx.getText()


    # Visit a parse tree produced by qasm3Parser#equalityOperator.
    def visitEqualityOperator(self, ctx:qasm3Parser.EqualityOperatorContext):
        return ctx.getText()


#    # Visit a parse tree produced by qasm3Parser#logicalOperator.
#    def visitLogicalOperator(self, ctx:qasm3Parser.LogicalOperatorContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#expressionStatement.
#    def visitExpressionStatement(self, ctx:qasm3Parser.ExpressionStatementContext):
#        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#expression.
    def visitExpression(self, ctx:qasm3Parser.ExpressionContext):
        op=0
        if ctx.expressionTerminator():
            value=self.visit(ctx.expressionTerminator())
            op=1
        elif ctx.unaryExpression():
            value=self.visit(ctx.unaryExpression())
            op=2
        elif ctx.logicalAndExpression():
            rvalue=self.visit(ctx.logicalAndExpression())
            if ctx.expression():
                lvalue=self.visit(ctx.expression())
                if self.intorfloatorcexpre(lvalue) and self.intorfloatorcexpre(rvalue):
                    value=lvalue or rvalue
                else:
                    value='%s%s%s'%(str(lvalue),'||',str(rvalue))
                op=3
            else:
                value=rvalue
                op=4
        return value


    # Visit a parse tree produced by qasm3Parser#logicalAndExpression.
    def visitLogicalAndExpression(self, ctx:qasm3Parser.LogicalAndExpressionContext):
        rvalue=self.visit(ctx.bitOrExpression())
        if ctx.logicalAndExpression():
            lvalue=self.visit(ctx.logicalAndExpression())
            if self.intorfloatorcexpre(lvalue) and self.intorfloatorcexpre(rvalue):
                value=bool(lvalue) and bool(rvalue)
            else:
                value='%s%s%s'%(str(lvalue),'&&',str(rvalue))
            db=1
        else:
            value=rvalue
            db=2
        return value


    # Visit a parse tree produced by qasm3Parser#bitOrExpression.
    def visitBitOrExpression(self, ctx:qasm3Parser.BitOrExpressionContext):
        rvalue=self.visit(ctx.xOrExpression())
        if ctx.bitOrExpression():
            lvalue=self.visit(ctx.bitOrExpression())
            value=lvalue|rvalue
            db=1
        else:
            value=rvalue
            db=2
        return value


    # Visit a parse tree produced by qasm3Parser#xOrExpression.
    def visitXOrExpression(self, ctx:qasm3Parser.XOrExpressionContext):
        rvalue=self.visit(ctx.bitAndExpression())
        if ctx.xOrExpression():
            lvalue=self.visit(ctx.xOrExpression())
            if (isinstance(lvalue,int) and isinstance(rvalue,int)):
                value=lvalue^rvalue
            else:
                value='%s%s%s'%(str(lvalue),'^',str(rvalue))
            db=1
        else:
            value=rvalue
            db=2
        return value


    # Visit a parse tree produced by qasm3Parser#bitAndExpression.
    def visitBitAndExpression(self, ctx:qasm3Parser.BitAndExpressionContext):
        rvalue=self.visit(ctx.equalityExpression())
        if ctx.bitAndExpression():
            lvalue=self.visit(ctx.bitAndExpression())
            if (isinstance(lvalue,int) and isinstance(rvalue,int)):
                value=lvalue&rvalue
            else:
                value='%s%s%s'%(str(lvalue),'&',str(rvalue))
            db=1
        else:
            value=rvalue
            db=2
        return value


    # Visit a parse tree produced by qasm3Parser#equalityExpression.
    def visitEqualityExpression(self, ctx:qasm3Parser.EqualityExpressionContext):
        rvalue=self.visit(ctx.comparisonExpression())
        if ctx.equalityOperator():
            lvalue=self.visit(equalityExpression())
            op=self.visit(equalityOperator())
            if self.intorfloatorcexpre(lvalue) and self.intorfloatorcexpre(rvalue):
                if op=='==':
                    value=rvalue==lvalue
                    db=1
                elif op=='!=':
                    value=rvalue!=lvalue
                    db=2
                else:
                    print('wrong equality operator')
            else:
                value='%s%s%s'%(str(lvalue),op,str(rvalue))
        else:
            value=rvalue
            db=3
        return value


    # Visit a parse tree produced by qasm3Parser#comparisonExpression.
    def visitComparisonExpression(self, ctx:qasm3Parser.ComparisonExpressionContext):
        rvalue=self.visit(ctx.bitShiftExpression())
        if ctx.comparisonExpression():
            op=self.visit(ctx.comparisonOperator())
            lvalue=self.visit(ctx.comparisonExpression())
            if (isinstance(lvalue) and isinstance(rvalue)):
                if (op=='<'):
                    value=lvalue<rvalue;
                    db=1
                elif op=='>':
                    value=lvalue>rvalue;
                    db=2
                elif op=='<=':
                    value=lvalue<=rvalue;
                    db=3
                elif op=='>=':
                    value=lvalue>=rvalue;
                    db=4
                else:
                    print('wrong comparison Operator')
            else:
                value='%s%s%s'%(str(lvalue),op,str(rvalue))
        else:
            value=rvalue
            db=5
        return value


    # Visit a parse tree produced by qasm3Parser#bitShiftExpression.
    def visitBitShiftExpression(self, ctx:qasm3Parser.BitShiftExpressionContext):
        additiveExpression=self.visit(ctx.additiveExpression())
        if ctx.bitShiftExpression():
            op=ctx.getChild(1).getText()
            bitShiftExpression=self.visit(ctx.bitShiftExpression())
            if (isinstance(bitShiftExpression,int) and isinstance(additiveExpression,int)):
                if op=='<<':
                    value=additiveExpression<<bitShiftExpression
                    db=1
                elif op=='>>':
                    value=additiveExpression>>bitShiftExpression
                    db=2
                else:
                    print('operator << or >>')
            else:
                value='%s%s%s'%(str(additiveExpression),op,str(bitShiftExpression))
        else:
            value=additiveExpression
            db=3
        return value


    # Visit a parse tree produced by qasm3Parser#additiveExpression.
    def visitAdditiveExpression(self, ctx:qasm3Parser.AdditiveExpressionContext):
        rvalue=self.visit(ctx.multiplicativeExpression())
        if ctx.additiveExpression():
            lvalue=self.visit(ctx.additiveExpression())
            if self.intorfloatorcexpre(lvalue) and self.intorfloatorcexpre(rvalue):
                if ctx.PLUS():
                    value=lvalue+rvalue
                    db=1
                elif ctx.MINUS():
                    value=lvalue-rvalue
                    db=2
                else:
                    print('wrong operator +/-')
            else:
                op='+' if ctx.PLUS() else '-' if ctx.MINUS() else '???+-'
                value='%s%s%s'%(str(lvalue),op,str(rvalue))
        else:
            value=rvalue
            db=3
        return value



    # Visit a parse tree produced by qasm3Parser#multiplicativeExpression.
    def visitMultiplicativeExpression(self, ctx:qasm3Parser.MultiplicativeExpressionContext):
        rvalue=None
        if ctx.powerExpression():
            rvalue=self.visit(ctx.powerExpression())
        if ctx.unaryExpression():
            rvalue=self.visit(ctx.unaryExpression())
        if ctx.multiplicativeExpression():
            lvalue=self.visit(ctx.multiplicativeExpression())
            if self.intorfloatorcexpre(lvalue) and self.intorfloatorcexpre(rvalue):
                if ctx.MUL():
                    value=lvalue*rvalue
                elif ctx.DIV():
                    value=lvalue/rvalue
                elif ctx.MOD():
                    value=lvalue%rvalue
                else:
                    print('which operator in multiplication?')
            else:
                op='*' if ctx.MUL() else '/' if ctx.DIV() else '%' if ctx.MOD() else '???'
                value='%s%s%s'%(str(lvalue),op,str(rvalue))
        else:
            value=rvalue
        return value


    # Visit a parse tree produced by qasm3Parser#unaryExpression.
    def visitUnaryExpression(self, ctx:qasm3Parser.UnaryExpressionContext):
        op=self.visit(ctx.unaryOperator())
        powerExpression=self.visit(ctx.powerExpression())
        if self.intorfloatorcexpre(powerExpression):
            if op=='-':
                value=-1*powerExpression
            elif op=='!':
                value=not (powerExpression)
            elif op=='~':
                value=~(powerExpression)
            else:
                print('error',op,inspect.stack()[0][3],inspect.stack()[2][3])
                value=None
        else:
            value='%s%s'%(op,str(powerExpression))
        return value


    # Visit a parse tree produced by qasm3Parser#powerExpression.
    def visitPowerExpression(self, ctx:qasm3Parser.PowerExpressionContext):
        expressionTerminator=(self.visit(ctx.expressionTerminator()))
        if ctx.powerExpression():
            powerExpression=self.visit(ctx.powerExpression())
            if self.intorfloatorcexpre(expressionTerminator) and self.intorfloatorcexpre(powerExpression):
                value=expressionTerminator**powerExpression
            else:
                value='%s**%s'%(str(expressionTerminator),str(powerExpression))
        else:
            powerExpression=None
            value=expressionTerminator
        return value


    # Visit a parse tree produced by qasm3Parser#expressionTerminator.
    def visitExpressionTerminator(self, ctx:qasm3Parser.ExpressionTerminatorContext):
        #prop=['Constant','Identifier','Integer','LBRACKET','LPAREN','RBRACKET','RPAREN','RealNumber','StringLiteral','booleanLiteral','builtInCall','expression','expressionTerminator','incrementor','externOrSubroutineCall','timingIdentifier']
#        print(dir(ctx))
        #print(ctx.__getattribute__('Constant')())
        #print([a for a in prop if ctx.__getattribute__(a)()],ctx.getText())
        if ctx.Constant():
            value= self.Constant[ctx.Constant().getText()]
        elif ctx.Integer():
            value= int(ctx.Integer().getText())
        elif ctx.RealNumber():
            value= float(ctx.RealNumber().getText())
        elif ctx.booleanLiteral():
            value= bool(ctx.booleanLiteral().getText())
        elif ctx.expression():
            expression= self.visit(ctx.expression())
            if ctx.expressionTerminator():
                expressionTerminator=self.visit(ctx.expressionTerminator())
#                value={'expressionterminator':expressionTerminator,'expression':expression}
#                print('debug array','term',expressionTerminator,'expre',expression,'%s[%s]'%(expressionTerminator,expression))
                stre1=expressionTerminator.expression if isinstance(expressionTerminator,c_expression) else expressionTerminator
#                stre0=expressionTerminator.expr0() if isinstance(expressionTerminator,c_expression) else expressionTerminator
                value=c_expression(expression='%s[%s]'%(stre1,expression),variable=[expressionTerminator])
            else:
                value=expression
        elif ctx.expressionTerminator():
            expressionTerminator=self.visit(ctx.expressionTerminator())
            if ctx.incrementor():
                value= expressionTerminator+(+1 if self.visit(ctx.incrementor())=='++' else -1)
            else:
                print('expressionTerminator unknown',self.getText())
                value= 0;
        elif ctx.externOrSubroutineCall():
            value= self.visit(ctx.externOrSubroutineCall())
        elif ctx.Identifier():
            iden=ctx.Identifier().getText()
#            print(ctx.Identifier().getText())
            value=c_expression(expression=iden,variable=[iden])
        elif ctx.timingIdentifier():
            value=self.visit(ctx.timingIdentifier())
        else:
            self.visitChildren(ctx)
            print('ExpressionTerminator other',ctx.getText())
            value= ctx.getText();#987;
        return value


    # Visit a parse tree produced by qasm3Parser#booleanLiteral.
#    def visitBooleanLiteral(self, ctx:qasm3Parser.BooleanLiteralContext):
#        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#incrementor.
#    def visitIncrementor(self, ctx:qasm3Parser.IncrementorContext):
#        return ctx.getText()


    # Visit a parse tree produced by qasm3Parser#builtInCall.
#    def visitBuiltInCall(self, ctx:qasm3Parser.BuiltInCallContext):
#        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#builtInMath.
#    def visitBuiltInMath(self, ctx:qasm3Parser.BuiltInMathContext):
#        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#castOperator.
#    def visitCastOperator(self, ctx:qasm3Parser.CastOperatorContext):
#        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#expressionList.
    def visitExpressionList(self, ctx:qasm3Parser.ExpressionListContext):
        expressionlist=[]
        for e in ctx.expression():
            expressionlist.append(self.visit(e))
        return expressionlist


    # Visit a parse tree produced by qasm3Parser#equalsExpression.
#    def visitEqualsExpression(self, ctx:qasm3Parser.EqualsExpressionContext):
#        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#assignmentOperator.
#    def visitAssignmentOperator(self, ctx:qasm3Parser.AssignmentOperatorContext):
#        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#setDeclaration.
#    def visitSetDeclaration(self, ctx:qasm3Parser.SetDeclarationContext):
#        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#programBlock.
#    def visitProgramBlock(self, ctx:qasm3Parser.ProgramBlockContext):
#        return self.visitChildren(ctx)


#    # Visit a parse tree produced by qasm3Parser#branchingStatement.
#    def visitBranchingStatement(self, ctx:qasm3Parser.BranchingStatementContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#loopSignature.
#    def visitLoopSignature(self, ctx:qasm3Parser.LoopSignatureContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#loopStatement.
#    def visitLoopStatement(self, ctx:qasm3Parser.LoopStatementContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#endStatement.
#    def visitEndStatement(self, ctx:qasm3Parser.EndStatementContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#print(inspect.stack()[0][3],inspect.stack()[2][3])
#    def visitReturnStatement(self, ctx:qasm3Parser.ReturnStatementContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#controlDirective.
#    def visitControlDirective(self, ctx:qasm3Parser.ControlDirectiveContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#externDeclaration.
#    def visitExternDeclaration(self, ctx:qasm3Parser.ExternDeclarationContext):
#        return self.visitChildren(ctx)
#
#
#
#    # Visit a parse tree produced by qasm3Parser#subroutineDefinition.
#    def visitSubroutineDefinition(self, ctx:qasm3Parser.SubroutineDefinitionContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#subroutineBlock.
#    def visitSubroutineBlock(self, ctx:qasm3Parser.SubroutineBlockContext):
#        return self.visitChildren(ctx)
#

    # Visit a parse tree produced by qasm3Parser#subroutineCall.
    def visitExternOrSubroutineCall(self, ctx:qasm3Parser.ExternOrSubroutineCallContext):
        Identifier=ctx.Identifier().getText()
        if ctx.expressionList():
            expressionList=self.visit(ctx.expressionList())
        else:
            expressionList=[]
        indexIdentifierList=self.visit(ctx.indexIdentifierList())
        value={'Identifier':Identifier,'expressionList':expressionList,'indexIdentifierList':indexIdentifierList}
        return value


#    # Visit a parse tree produced by qasm3Parser#pragma.
#    def visitPragma(self, ctx:qasm3Parser.PragmaContext):
#        return self.visitChildren(ctx)


#    # Visit a parse tree produced by qasm3Parser#timingType.
#    def visitTimingType(self, ctx:qasm3Parser.TimingTypeContext):
#        return self.visitChildren(ctx)


#    # Visit a parse tree produced by qasm3Parser#timingBox.
#    def visitTimingBox(self, ctx:qasm3Parser.TimingBoxContext):
#        return self.visitChildren(ctx)


    # Visit a parse tree produced by qasm3Parser#timingIdentifier.
    def visitTimingIdentifier(self, ctx:qasm3Parser.TimingIdentifierContext):
        if ctx.TimingLiteral():
            timingLiteral=ctx.TimingLiteral().getText()
            m=re.match('([\d\.Ee+-]+)(%s)'%('|'.join(self.TimeUnit.keys())),timingLiteral)
            if m:
                val,unit=m.groups()
                unit=self.TimeUnit[unit]
                if '.' in val:
                    value=float(val)*unit
                else:
                    value=int(val)*unit
            else:
                print('timingLiteral looks strange, should be number followed by unit')
        else:
            exit("I don't know how to process duration of yet")
        return value


#    # Visit a parse tree produced by qasm3Parser#timingInstructionName.
#    def visitTimingInstructionName(self, ctx:qasm3Parser.TimingInstructionNameContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#timingInstruction.
#    def visitTimingInstruction(self, ctx:qasm3Parser.TimingInstructionContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#timingStatement.
#    def visitTimingStatement(self, ctx:qasm3Parser.TimingStatementContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#calibration.
#    def visitCalibration(self, ctx:qasm3Parser.CalibrationContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#calibrationGrammarDeclaration.
#    def visitCalibrationGrammarDeclaration(self, ctx:qasm3Parser.CalibrationGrammarDeclarationContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#calibrationDefinition.
#    def visitCalibrationDefinition(self, ctx:qasm3Parser.CalibrationDefinitionContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#calibrationGrammar.
#    def visitCalibrationGrammar(self, ctx:qasm3Parser.CalibrationGrammarContext):
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by qasm3Parser#calibrationArgumentList.
#    def visitCalibrationArgumentList(self, ctx:qasm3Parser.CalibrationArgumentListContext):
#        return self.visitChildren(ctx)
#


del qasm3Parser
