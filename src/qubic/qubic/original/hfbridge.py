import sys
from qubic.qubic.envelop_pulse import c_envelop_pulse
from qubic.bids.ether import c_ether
from qubic.bids.regmap import c_regmap
from qubic.qubic.cmdsim1 import c_cmdsim
from qubic.qubic.commands import cmdgen128
import inspect
import decimal
import time
import copy
import json
import numpy
import re
from matplotlib import pyplot
import datetime
import importlib
import imp
#from squbic import c_qchip, c_seqs
#	def c
import pkgutil
class adc:
	def __init__(self):
		pass
class dac:
	def __init__(self):
		pass
class c_hfbridge:
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',wiremapmodule='wiremap',min3=True,memgatewaybug=False,run=True,sim=False):
		if regmappath:
			with open(regmappath) as jfile:
				registers=json.load(jfile)
		else:
			registers=json.loads(pkgutil.get_data(__package__,'regmap.json'))
		if wavegrppath:
			with open(wavegrppath) as jfile:
				wavegrp=json.load(jfile)
		else:
			wavegrp=json.loads(pkgutil.get_data(__package__,'wavegrp.json'))
			self.regmappath=regmappath
		self.regmap=c_regmap(interface=c_ether(ip=ip,port=port),registers=registers,wavegrp=wavegrp,min3=min3,memgatewaybug=memgatewaybug,run=run)
		wiremap=imp.load_source(name='wiremap',pathname=wiremapmodule)#importlib.import_module(wiremapmodule)
		self.dacfs=32768/1.647
		self.adcfs=32768/1.647
		self.dacchans=8
		self.adcchans=8
		self.sim=sim
		self.dt=dt
		self.dtdec=decimal.Decimal(str(dt))
		self.ndws=[]
		self.cmd128all=[]
		self.compiled=False
		self.nelem=8
		self.elements=[]
		self.dacelements=[]
		self.bufread=None
		self.bufreadwidth=None
		self.bufwidthdict={}
		self.bufwidth_dict={}
#		for startaddr in range(0x38000,0x38000+self.nelem*0x1000,0x1000):
		size=0x1000
		for elemindex in range(self.nelem):
			startaddr=0x38000+elemindex*size
			#startaddr=0x000+elemindex*size
			self.dacelements.append(c_element(start=startaddr,index=elemindex,memreg=self.regmap.regs['elementmem_%x'%elemindex],size=0x1000))
		self.elements.extend(self.dacelements)
		self.nelemread=3
#		self.lo0elements=[]
#		for startaddr in range(0x41000,0x41000+self.nelemread*0x1000,0x1000):
#		for elemindex in range(self.nelemread):
#			startaddr=0x41000+elemindex*size
#			self.adcelements.append(c_element(start=startaddr,index=elemindex+8,size=0x1000))
		self.lo0elements=[c_element(start=0xc0000,index=8,memreg=self.regmap.regs['elementmem_8'],size=0x4000)]
		self.lo1elements=[c_element(start=0xc4000,index=9,memreg=self.regmap.regs['elementmem_9'],size=0x4000)]
		self.lo2elements=[c_element(start=0xc8000,index=10,memreg=self.regmap.regs['elementmem_a'],size=0x4000)]
		self.loelements=[self.lo0elements[0],self.lo1elements[0],self.lo2elements[0]]
		self.elements.extend(self.loelements)
		self.digielement0=[c_markelem(index=12)]
		self.digielement1=[c_markelem(index=13)]
		self.digielement2=[c_markelem(index=14)]
		self.digielement3=[c_markelem(index=15)]
		self.digielements=[self.digielement0[0],self.digielement1[0],self.digielement2[0],self.digielement3[0]]
		self.elements.extend(self.digielements)


		self.dac=self.dacchans*[dac()]
		self.adc=self.adcchans*[adc()]
		self.chanmapqubit=wiremap.chanmapqubit
		self.lofreq=wiremap.lofreq
		self.dacelementsdest=wiremap.dacelementsdest
		self.lo0elementsdest=wiremap.lo0elementsdest
		self.lo1elementsdest=wiremap.lo1elementsdest
		self.lo2elementsdest=wiremap.lo2elementsdest
		self.digielem0dest=wiremap.digielem0dest
		self.digielem1dest=wiremap.digielem1dest
		self.digielem2dest=wiremap.digielem2dest
		self.digielem3dest=wiremap.digielem3dest
	def initelem(self):
		for elem in self.elements:
			elem.initelem()
		self.compiled=False
		self.cmd128all=[]
	def cmdgen0(self,seqs,bypass=False):
		if not self.compiled:
			nvlist=self.step1(seqs=seqs,dt=self.dt,npatch=2,npatchmod=4)
			#print 'cmdgen',nvlist,[nv.len() for nv in nvlist]
			dacnv,lo0,lo1,lo2,dg0,dg1,dg2,dg3=self.step2(nvlist)
			self.step3(lo0,self.lo0elements)
			self.step3(lo1,self.lo1elements)
			self.step3(lo2,self.lo2elements)
			if not bypass:
				self.step3(dacnv,self.dacelements)
				self.step3(dg0,self.digielement0)
				self.compiled=True
#		self.step3(dg1,self.digielement1)
#		self.step3(dg2,self.digielement2)
#		self.step3(dg3,self.digielement3)
	def cmdgen1(self,bypass=False,destamp=None):
		if not bypass:
			memcmds=[]
			for elem in self.elements:
				memcmds.append(elem.memcmd())
	def cmdgen2(self,bypass=False,destfqdrv={},destphini={},destfrdrv={},destfread={}):
		if not bypass:
			opcmds=[]
			for elem in self.elements:
				opcmds.append(elem.opcmd())
			opcmdall={}
			for opcmd in opcmds:
				opcmdall.update(opcmd)
			for t in sorted(opcmdall):
			#print '\n'.join([str(hex(i)) for i in self.cmd128all])
				dest=opcmdall[t]['dest']
#				print 'dest1',dest,opcmdall[t]['freq']
				if dest in destfqdrv.keys():
					opcmdall[t].update({'freq':destfqdrv[dest]})
					#print 'update fqdrv',dest,destfqdrv[dest]
				if dest in destfrdrv.keys():
					opcmdall[t].update({'freq':destfrdrv[dest]})
					#print 'update frdrv',dest,destfrdrv[dest]
				if dest in destfread.keys():
					opcmdall[t].update({'freq':destfread[dest]})
					#print 'update fread',dest,destfread[dest]
				#print 'dest',dest
				opcmdall[t].update({'dest':self.chanmapqubit[opcmdall[t]['dest']]})
#				print 'dest2',dest,opcmdall[t]['freq']
#				print 'opcmdall[t]',opcmdall[t]
				self.cmd128all.append(cmdgen128(**opcmdall[t],debug=True))
#				print 'cmd128all',self.cmd128all

	#		print t,opcmdall[t]
	def simpmemcmd(self,gates):
		self.gateinfo={}
		self.destmem={}
		for gate in gates:
			if gate.name not in list(self.gateinfo.keys()):
				self.gateinfo[gate.name]=[]
			for pulse in gate.pulses:
				if self.elementlist[pulse.dest] not in list(self.destmem.keys()):
					self.destmem[self.elementlist[pulse.dest]]=numpy.empty(0)
				newpulse={}
				# link element to dest, every dest correspoonding to an elem.
				# or say each dac have its own element/memory
				# dest 0: elem 0
				# dest 1: elem 1
				# dest 2: elem 2
				# dest 3: elem 3
				# dest 0: elem 4 // 2nd readout drv
				# elem 4,5,6,7 are not used (wasting)
				# dest 8: elem 8
				# dest 9: elem 9
				# dest a: elem a
				# we don't have to do that, but for small amount of gate
				# this simplified the memory allocation
				nvpulse=c_nvpulse(pulse=pulse,dt=self.dt,flo=0,mod=False)
				patch=decimal.Decimal(str(self.patchlist[pulse.dest]))
				nvpulse.addpatch0(npatch=int(patch/self.dtdec),patchtomod=4)
				val=nvpulse.vall()
				#print('gatename',gate.name,'dest',pulse.dest,'memlen',self.elementlist[pulse.dest],self.destmem[self.elementlist[pulse.dest]])
				newpulse['start']=len(self.destmem[self.elementlist[pulse.dest]])
				self.destmem[self.elementlist[pulse.dest]]=numpy.append(self.destmem[self.elementlist[pulse.dest]],val)
#				newpulse['element']=pulse.dest
#				newpulse['t0']=pulse.t0
				twidth=decimal.Decimal(str(pulse.twidth))
				length_t=twidth+patch
				newpulse['length']=length_t/self.dtdec
				if 'pulse' not in list(newpulse.keys()):
					newpulse['pulse']=[]
				newpulse['pulse'].append(pulse)
#				newpulse['dest']=pulse.dest
#				newpulse['fcarrier']=pulse.fcarrier
#				newpulse['pcarrier']=pulse.pcarrier
				#self.gates.pulses
				self.gateinfo[gate.name].append(newpulse)
#		print('gateinfo',self.gateinfo)
#		numpy.set_printoptions(threshold=sys.maxsize)
		for k in self.destmem.keys():
			for elem in self.elements:
				if elem.index==k:#self.elementlist[k]:
					elem.ramdata=list(self.destmem[k])
#			print(k)
#			print(self.destmem[k])
#			print(len(self.destmem[k]))
	def setmap(self,elementlist,destlist,patchlist):
		self.elementlist=elementlist
		self.destlist=destlist
		self.patchlist=patchlist
	def simpcmdgen(self,cmdarray,delaybetweenelement,delayafterheralding=0):
		#		print('\n,'.join([str(c) for c in cmdarray]))
		self.slice=decimal.Decimal(4)
		r=len(cmdarray)
		self.destcnt={}
		self.freqphase={}
		cmd128_64k=numpy.zeros((2**16,2),dtype='O')
		cmdindex=0;
		for gatename in list(self.gateinfo.keys()):
			for pulseinfo in self.gateinfo[gatename]:
				for pulse in pulseinfo['pulse']:
					if pulse.dest not in list(self.destcnt.keys()):
						self.destcnt[pulse.dest]=0
						self.freqphase[pulse.fcarriername]=0

		self.cmd128all_t=numpy.empty((0,2),dtype=int)
		tini=decimal.Decimal(str(32e-9))#gave space for patch at beginning.
		period=0
		nextgatetrig=tini #decimal.Decimal(str(tini))
		cond= 0
		index=0
		lastline0=0
		for line in cmdarray:
			gatetrig=nextgatetrig #if line[1] is None else decimal.Decimal(str(line[1]))
			if (lastline0 ) and delayafterheralding>0:
				#				print('before heralding time',gatetrig)
				if index==1:
					pass
				else:
					gatetrig+=decimal.Decimal(str(delayafterheralding))
				tini=gatetrig
				#print('after  heralding time',gatetrig,tini)
			if line[0] or index==1:
				tini=gatetrig
#				if index!=1:
				if line[0]:
					gatetrig+=decimal.Decimal(str(delaybetweenelement))
				#print('first in circuit')
			index=index+1
			lastline0=line[0]
			#print('simpcmdgen',line)
			maxpatch=0
			gates=line[1]
			for gatetuple in gates:
				for pulseinfo in self.gateinfo[gatetuple[0]]:
					for pulse in pulseinfo['pulse']:
						patch=decimal.Decimal(str(self.patchlist[pulse.dest]))
						maxpatch=max(maxpatch,patch)
						#print('maxpatch,patch',maxpatch,patch)

			gatelength=0
			for gatetuple in gates:
				#print('gate gatetrig',gatetrig)
				gatename=gatetuple[0]
				phgatein=gatetuple[2]
				freqin=gatetuple[1]
				gate=self.gateinfo[gatename]
				pulseindex=0
				for pulseinfo in gate:
					
					#print(pulseinfo,[p.t0 for p in pulseinfo['pulse']])
					for pulse in pulseinfo['pulse']:
						#print('freq',freq)
						t0=decimal.Decimal(str(pulse.t0))
						twidth=decimal.Decimal(str(pulse.twidth))
						patch=decimal.Decimal(str(self.patchlist[pulse.dest]))
						trig_t=(gatetrig+t0)+(maxpatch-patch)#-patch
#				print('line',line,pulse['t0'],pulse['dest'],self.patchlist[pulse['dest']],trig_t)
						trig_tn=int(trig_t/self.dtdec/self.slice)
						element=self.elementlist[pulse.dest]
						dest=self.destlist[pulse.dest]
						start=pulseinfo['start']/self.slice
						length=pulseinfo['length']/self.slice
#				print(pulse.keys())
						if isinstance(phgatein,tuple):
							if len(phgatein)>pulseindex:
								phgate=None if phgatein[pulseindex] is None else float(phgatein[pulseindex]/180.0*numpy.pi)
						else:
							phgate=None if phgatein is None else float(phgatein/180.0*numpy.pi)
						if isinstance(freqin,tuple):
							if len(freqin)>pulseindex:
								freq=pulse.fcarrier if freqin[pulseindex] is None else float(freqin[pulseindex])
						else:
							freq=pulse.fcarrier if freqin is None else float(freqin)
						phtime=2*numpy.pi*freq*float(trig_t-tini)
						phpatch=2*numpy.pi*freq*float(-maxpatch-0*patch)
	#					phpatch=2*numpy.pi*freq*float(-patch)
						self.freqphase[pulse.fcarriername]+=pulse.pcarrier
#						self.freqphase[pulse.fcarriername]=pulse.pcarrier
						pcarrieraccumlated=self.freqphase[pulse.fcarriername]
#						print('pcarrieraccumlated',pcarrieraccumlated)
						phadd=0 if phgate is  None else (pcarrieraccumlated+phgate)
						#phadd=0 if phgate is  None else (pulse.pcarrier+phgate)
						#phrad=0 if phgate is  None else (phtime+phadd)
						phrad=phpatch+(0 if phgate is  None else (phtime+phadd))
						#print('gate pulse time',trig_t)
						#print('phase calc: ','phgate',phgate,'phrad',phrad,'phtime',phtime,'freq',freq,'trigt-tini',float(trig_t-tini),'tini',tini,'phadd',phadd,'pcarrieraccumlated',pcarrieraccumlated,'phpatch',phpatch)
						newcmd=cmdgen128(trig_t=trig_tn,element=element,dest=dest,start=start,length=length,phrad=phrad,freq=freq,cond=cond)
						gatelength=max((patch+t0 if t0<patch else t0)     +twidth,gatelength)
			#			self.cmd128all_t=numpy.append(self.cmd128all_t,numpy.array([[trig_tn,newcmd]]),0)
						cmd128_64k[cmdindex]=[trig_tn,newcmd];
						cmdindex=cmdindex+1;
						self.destcnt[pulse.dest]+=1;
					pulseindex+=1
#			print(gatelength)
			nextgatetrig=gatetrig+gatelength;#int(trig_t)+pulse['length']*self.dt
#		print(self.cmd128all_t,cmd128_64k[0:cmdindex],self.cmd128all_t.dtype);
#		print('diff',self.cmd128all_t-cmd128_64k[0:cmdindex]);
		self.cmd128all_t=cmd128_64k[0:cmdindex];
		sortindex=numpy.argsort(self.cmd128all_t[:,0])
		self.cmd128all=self.cmd128all_t[sortindex,1]
		bufs=['accout_0','accout_1','accout_2']
		bufdest0=[d for d in list(self.destlist.keys()) if self.destlist[d]==4]
		bufdest1=[d for d in list(self.destlist.keys()) if self.destlist[d]==5]
		bufdest2=[d for d in list(self.destlist.keys()) if self.destlist[d]==6]
		bufdestfull=[bufdest0,bufdest1,bufdest2]
		bufdest  ={bufs[i]:bufdestfull[i] for i,e in enumerate(bufdestfull) if len(bufdestfull[i])>=1}
		destbuf={}
		self.accout_dict={}
		for k,v in bufdest.items():
			for vi in v:
				destbuf[vi]=k
		self.bufwidthdict={i:0 for i in bufs};#numpy.zeros(len(bufs),dtype=numpy.int)
		self.bufwidth_dict={}
		for k,v in destbuf.items():
			self.bufwidthdict[v]+=self.destcnt[k]
			self.bufwidth_dict[k.split('.')[0]]=self.destcnt[k]
			self.accout_dict[k.split('.')[0]]=v
		self.bufreadwidth=[self.bufwidthdict[k] for k in bufs if self.bufwidthdict[k]>0]
		self.bufread     = [k for k in bufs if self.bufwidthdict[k]>0]

#		print('bufread',self.bufread)
#		print('bufwidthdict',self.bufwidthdict)
#		print('bufwidth_dict',self.bufwidth_dict)
#		print('accout_dict',self.accout_dict)
#
#		print('bufdest',bufdest)
#		print('destbuf',destbuf)
#		print('bufdestfull',bufdestfull)
#		print('debug destlist',self.destlist.keys(),self.destlist)
#		bufdest  ={bufs[i]:bufdestfull[i][0] for i,e in enumerate(bufdestfull)}# if len(bufdestfull[i])==1}
  #		self.bufread     = [] #bufdest.keys()
  #		self.bufreadwidthall=numpy.zeros(len(bufs),dtype=numpy.int)
  #		self.bufwidthdict={}
  #		self.bufwidth_dict={}
  #		self.accout_dict={}
  #		print('destcnt',self.destcnt)
  #		for i in range(len(bufs)):
  #			print('bufs[i]',bufs[i])
  #			if bufs[i] in list(bufdest.keys()):
  #
  #				if bufdest[bufs[i]] in  list(self.destcnt.keys()):
  #					self.bufreadwidthall[i]=int(self.destcnt[bufdest[bufs[i]]])
  #					if self.bufreadwidthall[i]>0:
  #						self.bufread.append(bufs[i])
  #						print('dicts',bufdest[bufs[i]],self.destcnt[bufdest[bufs[i]]])
  #						self.bufwidth_dict[(bufdest[bufs[i]].split('.')[0])]=self.destcnt[bufdest[bufs[i]]]
  #						self.accout_dict[(bufdest[bufs[i]].split('.')[0])]=bufs[i]
  #						#self.bufwidth_dict[bufdestfull[bufs[i]].split('.')[0]]=int(self.bufreadwidth[i])
  #			self.bufwidthdict[bufs[i]]=int(self.bufreadwidthall[i])
  #		self.bufreadwidth=[k for k in self.bufreadwidthall if k>0]
#			if self.bufreadwidth[i]>0:
#				print('bufsdestfull[i]',bufdestfull[i][0].split('.')[0])
			#self.bufwidth_dict[bufdestfull[i][0].split('.')[0]]=int(self.bufreadwidth[i])
#			print('bufs[i]',bufs[i])
#			print('bufreadwidth[i]',self.bufreadwidth[i])
#			print(bufdestfull[bufs[i]])
#		print('bufwidthdict',self.bufwidthdict)
#		print('self.bufreadwidth',self.bufreadwidth,type(self.bufreadwidth[0]))
		period=float(nextgatetrig)
#		print('hfbridge cmd128all simp',self.cmd128all,[type(i) for i in self.cmd128all])
		return (len(self.cmd128all),period)
	def countgate(self,gatename):
		if gatename  in self.destcnt.keys():
			count=self.destcnt[gatename]
		else:
			count=0
		return count
		#self.cmd128all=cmdarray[:,5]
#	def cmdgen(self,seqs):
#		memcmds=[]
#		opcmds=[]
#		nvlist=self.step1(seqs=seqs,dt=self.dt,npatch=2,npatchmod=4)
#		dacnv,lo0,lo1,lo2=self.step2(nvlist)
#		if self.step3(dacnv,self.dacelements):
#			for ele in self.dacelements:
#				memcmds.append(ele.memcmd())
#				opcmds.append(ele.opcmd())
#		if self.step3(lo0,self.lo0elements):
#			for ele in self.lo0elements:
#				memcmds.append(ele.memcmd())
#				opcmds.append(ele.opcmd())
#		if self.step3(lo1,self.lo1elements):
#			for ele in self.lo1elements:
#				memcmds.append(ele.memcmd())
#				opcmds.append(ele.opcmd())
#		if self.step3(lo2,self.lo2elements):
#			for ele in self.lo2elements:
#				memcmds.append(ele.memcmd())
#				opcmds.append(ele.opcmd())
#		mema=[]
#		memv=[]
#		for memcmd in memcmds:
#			mema.extend(memcmd[0])
#			memv.extend(memcmd[1])
#		#	print len(mema),len(memv)
#	#	for i in range(len(mema)):
#	#		print '0x%x'%mema[i],memv[i]
#		opcmdall={}
#		for opcmd in opcmds:
#			#print opcmd
#			opcmdall.update(opcmd)
#		for t in sorted(opcmdall):
#			opcmdall[t].update({'dest':self.chanmapqubit[opcmdall[t]['dest']]})
#	#		print t,opcmdall[t]
#		return ((mema,memv),opcmdall)
#	sys.path.append('../laser_stack/gui/fmc120/qubic')
#	def memad(self,mema,memv):
#		alist=list(cmda)
#		dlist=[((int(v.real*19894)<<16)+int(v.imag*19894)) for v in cmdv]
#def opad1(self,trig_t,element,dest,start,length,phini,freq):
#	ph_binary = int(phini/360.0*2**14)
#	freq_binary=int(1e-9*freq*2**24)
#	cmd={trig_t:((trig_t&0xffffff)<<72)+((element&0xff)<<64)+((dest&0x3)<<62)+((start&0xfff)<<50)+((length&0xfff)<<38)+((ph_binary&0x3fff)<<24)+(freq_binary&0xffffff)}
##	print trig_t,element,dest,start,length,ph_binary,freq_binary,hex(cmd[trig_t])
#	return cmd
#def opad(self,
#def cmdadd(cmddict,newcmddict):
#	commonkeys=set(cmddict.keys())&set(newcmddict.keys())
#	if not commonkeys:
#		cmddict.update(newcmddict)
#	else:
#		print
#		print "ERROR: commonkeys ",commonkeys
#		print
#	return cmddict

	def step1(self,seqs,npatch=2,npatchmod=4,dt=1.0e-9):
		seqs.allocate()
		seqs.calcperiod()
		nvtdict={}
		#print 'step1',[[p.tlength() for p in s.gate.pulses] for s in seqs.seqlist]
		nv=[[c_nvpulse(p,dt=dt,flo=self.lofreq[p.dest],mod=False,cond=s.cond) for p in s.gate.pulses] for s in seqs.seqlist]
		print('debug step1',len(nv))
#		nvlist=[c_nvpulse(p,dt=1e-9,flo=5.5e9,mod=False) for seqlist in seqs.seqlist for p in seqlist]
		nvlist=[item for sublist in nv for item in sublist]
		lnvlist=len(nvlist)
		for nv in nvlist:
			n0=nv.n0()
			i=int(numpy.floor(n0/npatchmod))*npatchmod
			n0avail=None
			while n0avail is None:
				if len(set(range(i-npatch*npatchmod+1*npatchmod,i+npatch*npatchmod,npatchmod)).intersection(nvtdict))>0:
					i=i-npatchmod
		#			print i
				else:
					n0avail=i
			#n0avail=max(set(range(n0+1))-set(nvtdict))
		#	print 'n0avail',n0avail
			nv.addpatch0(n0-n0avail)
			nvtdict[n0avail]=nv
			#print nv.pulse.dest,nv.patch0,nv.patch1
		n0list=[i.n0() for i in nvlist]
		minn0=min(n0list)
		if minn0 <0:
			for nv in nvlist:
				nv.shiftt(-minn0)
		n0list=[i.n0() for i in nvlist]
#		print 'n0list',n0list
		return nvlist
	def step2(self,nvs):
		dacnv=[]
		lo0nv=[]
		lo1nv=[]
		lo2nv=[]
		dg0nv=[]
		dg1nv=[]
		dg2nv=[]
		dg3nv=[]
		for nv in nvs:
			if nv.dest in self.dacelementsdest:
				dacnv.append(nv)
			elif nv.dest in self.lo0elementsdest:
				lo0nv.append(nv)
			elif nv.dest in self.lo1elementsdest:
				lo1nv.append(nv)
			elif nv.dest in self.lo2elementsdest:
				lo2nv.append(nv)
			elif nv.dest in self.digielem0dest:
				dg0nv.append(nv)
			elif nv.dest in self.digielem1dest:
				dg1nv.append(nv)
			elif nv.dest in self.digielem2dest:
				dg2nv.append(nv)
			elif nv.dest in self.digielem3dest:
				dg3nv.append(nv)
		return (dacnv,lo0nv,lo1nv,lo2nv,dg0nv,dg1nv,dg2nv,dg3nv)
	def step3(self,nvlist,elements):
		nveledict={}
		theele=None
		for nv in sorted(nvlist,key=lambda x:x.len()):
			#			print 'step3 debug nv type',type(nv),nv.__class__
			theele=None
			if nv in nveledict:
				#print 1
				for ele in nveledict[nv]:
#					print 'sort',len(ele.mem),'%x'%ele.start
					if theele is None:
						#						if nv.dest in self.adcelementsdest:
						#	print 'adc step3','overlap',ele.overlap(nv),'havespace',ele.havespace(nv)
						if not ele.overlap(nv):# and ele.havespace(nv):
							theele=ele
				if theele is None:
					for ele in sorted(list(set(elements)-set(nveledict[nv])),key=lambda x:(-len(x.mem),x.start),reverse=False):
#						print 'sort',len(ele.mem),'%x'%ele.start
						if theele is None:
							if not ele.overlap(nv) and ele.havespace(nv):
								theele=ele
			else:
				#	print elements
				for ele in sorted(elements,key=lambda x:(-len(x.mem),x.start),reverse=False):
					#	print 'sort',len(ele.mem),'%x'%ele.start
					if theele is None:
						#						print 'element step3','len ele.mem',len(ele.mem),'len element',len(elements),'not overlap',(not ele.overlap(nv)),'allocated',(theele is None),'havespace',(ele.havespace(nv)),(not ele.overlap(nv)) and (theele is None) and (ele.havespace(nv))
						if (not ele.overlap(nv)) and (theele is None) and (ele.havespace(nv)):
							theele=ele
						#	print theele
			if theele is None:
				print('!!!!!!!!!!!!!!!!!!!!!ERROR!!!!!!!!!!!!! not enough elements')
				for ele in sorted(elements,key=lambda x:(-len(x.mem),x.start),reverse=False):
					print('element index',ele.index,'len(ele.mem)',len(ele.mem),'len(elements)',len(elements),'not overlap',(not ele.overlap(nv)),'havespace',(ele.havespace(nv)))#,'ramdata size',len(ele.ramdata)
			else:
				theele.addnv(nv)
				#print 'nv ',nv.len()
				if nv not in nveledict:
					nveledict[nv]=[]
				if theele not in nveledict[nv]:
					nveledict[nv].append(theele)
		return False if theele is None else True
	def step4(self,ele):
		ele.allocate()

	def elementmemclear(self):
		for elem in self.elements:
			d=elem.size*[0]
			if hasattr(elem,'memreg'):
				self.write(((elem.memreg.name,d),))


	def elementmemwrite(self,fullscale=19894,memscale=None):
		if memscale:
			memscale=fullscale*memscale
		for elem in self.elements:
			scale=memscale if elem in self.dacelements and memscale else fullscale
			if hasattr(elem,'memreg'):
				if (elem.ramdata):
					d=[((int(v.real*scale)<<16)+int(v.imag*scale)) for v in elem.ramdata]
					self.write(((elem.memreg.name,d),))
		#			print('elementmemwrite length',elem.memreg.name,len(d))
		#			print('d',d)

#	def cmdgen128(self,trig_t,element,dest,start,length,phrad,freq,cond=0):
#		trig_t=int(trig_t)
#		assert trig_t<2**24, 'trig_t exceeds 2**24'
#		element=int(element)
#		dest=int(dest)
#		start=int(start)
#		length=int(length)
#		phdeg=phrad/2/numpy.pi*360
#		freq=freq
#		ph_binary = int(phdeg/360.0*2**14)
#		freq_binary=int(1e-9*freq*2**24)
##		cmd=((trig_t&0xffffff)<<72)+((element&0xff)<<64)+((dest&0x3)<<62)+((start&0xfff)<<50)+((length&0xfff)<<38)+((ph_binary&0x3fff)<<24)+(freq_binary&0xffffff)
##		cmd1=((cmd&0xffffffff)<<64)+(((cmd>>32)&0xffffffff)<<32)+((cmd>>64)&0xffffffff)
#		cmd=((cond&0x1)<<96)+((trig_t&0xffffff)<<72)+((element&0xff)<<64)+((dest&0x3)<<62)+((start&0xfff)<<50)+((length&0xfff)<<38)+((ph_binary&0x3fff)<<24)+(freq_binary&0xffffff)
#		cmd1=(((cmd>>96)&0xffffffff)<<96)+((cmd&0xffffffff)<<64)+(((cmd>>32)&0xffffffff)<<32)+((cmd>>64)&0xffffffff)
#		#print('trig_t',trig_t,'element',element,'dest',dest,'start',start&0xfff,hex(start&0xfff),'length',length,'ph_binary',ph_binary,phdeg,'deg','freq',freq,'freq_binary',freq_binary,format(cmd1,'032x'))
#		#print('***v2.0 printing format***','trig_t',trig_t,'element',element,'dest',dest,'start',start&0xfff,hex(start&0xfff),'length',length,'ph_rad:%.3f'%(phrad%(2*numpy.pi)),'freq_MHz:%.3f'%(freq/1e6))
#		return cmd1
#	def cmd128decode(self,cmd):
#		trig_t=(cmd>>8)&0xffffff;
#		element=(cmd>>0)&0xff;
#		ph_binary=(((cmd>>32)&0x3f)<<8)+((cmd>>88)&0xff)
#		length=(cmd>>38)&0xfff
#		start=(cmd>>50)&0xfff
#		dest=(cmd>>62)&0x3
#		freq_binary=(cmd>>64)&0xffffff
#		cond=(cmd>>96)&0x1
#		print('trig_t',trig_t,'element',element,'dest',dest,'start',start&0xfff,hex(start&0xfff),'length',length,'ph_binary',ph_binary,'freq_binary',freq_binary)
	def cmdwrite(self,cmdreg='command'):
		#for i in self.cmd128all:
		#	print(format(i,'024x'))
		#	self.cmd128decode(i)
		self.write(((cmdreg,self.cmd128all),))
		return len(self.cmd128all)
	def cmdclear(self,cmdreg='command',length=0):
		#self.write(((self.regmap.regs[cmdreg],numpy.zeros(655).astype('int')),))
		reg=self.regmap.regs[cmdreg]
		d=((1<<reg.addr_width) if length==0 else length)*[0]
		self.write(((reg.name,d),))
	def signvalue(self,vin,width=16):
		return vin-2**width if vin>>(width-1) else vin
	def adcminmax(self):
		#print 'adcminmax'
		bufs=['adc0_minmax','adc1_minmax']
		self.read(bufs)
		return ([(self.signvalue(int(i[0])>>16),self.signvalue(int(i[0])&0xffff)) for i in [self.getregval(r) for r in bufs]])
	def getbufreadwidth(self):
		self.calcbufreadwidth()
		return self.bufreadwidth
	def getbufwidthdict(self):
		self.calcbufreadwidth()
		return self.bufwidthdict

	def calcbufreadwidth(self):
		bufs=['accout_0','accout_1','accout_2']
		if self.bufread is None:
			self.bufread     =[bufs[i] for i,e in enumerate(self.loelements) if len(e.pulses)>0]
			self.bufreadwidth=[len(e.pulses) for i,e in enumerate(self.loelements) if len(e.pulses)>0]
			self.bufwidthdict=dict([(bufs[i],len(e.pulses)) for i,e in enumerate(self.loelements)])
#			print('self.loelements',self.loelements)
			print('bufread',self.bufread)
			print('bufreadwidth',self.bufreadwidth)
			print('bufreadwidthdict',self.bufwidthdict)
	def accbuf(self,chan=[0,1,2]):
		#print 'accbuf'
		#print 'accbuf chan',[len(i.pulses) for i in self.loelements]
		self.getbufreadwidth()
		#print('accbuf read',self.bufread,self.bufreadwidth)
		#print(self.bufread)
		self.read(self.bufread,resetafter=False)
		#return [{r:self.getregval(r)} for r in self.bufread]
		result = numpy.array([self.getregval(r) for r in self.bufread]).transpose()
		#print result.shape
		return result

	def buf_monout(self,delay=0.1,opsel=0,mon_navr=0,mon_dt=0,mon_slice=0,mon_sel0=0,mon_sel1=0,panzoom_reset=0):
		# opsel 0 for navg; 1 for min; 2 for max
		# mon_sel: 
#			0 : xmeasin_d2,
#			1 : ymeasin_d2,
#			2 : xlo_w[0],
#			3 : ylo_w[0],
#			4 : xlo_w[1],
#			5 : ylo_w[1],
#			6 : xlo_w[2],
#			7 : ylo_w[2],
#			8 : dac0_in,
#			9 : dac1_in,
#			10 : dac2_in,
#			11 : dac3_in,
#			12 : dac4_in,
#			13 : dac5_in,
#			14 : dac6_in,
#			15 : dac7_in,
#		print 'opsel,mon_navr,mon_dt,mon_slice,mon_sel0,mon_sel1'
#		print opsel,mon_navr,mon_dt,mon_slice,mon_sel0,mon_sel1
		self.write((
			('opsel',opsel)
			,('mon_navr',mon_navr)
			,('mon_dt',mon_dt)
			,('mon_slice',mon_slice)
			,('mon_sel0',mon_sel0)
			,('mon_sel1',mon_sel1)
			,('panzoom_reset',panzoom_reset)
			))
		time.sleep(delay)
		regs=['buf_monout_1','buf_monout_0']
		self.read(regs)
		return [numpy.array(self.getregval(r)).reshape((-1,1)) for r in regs]
	def getregval(self,name):
		return self.regmap.getregval(name)
	def periodwrite(self,period):
		print('periodwrite',period)
		return self.write((('period_adc',period),('period_dac0',period),('period_stream',period)))
	def write(self,namedatalist):
		if self.sim:
			self.ndws.extend([(n,d,1) for (n,d) in namedatalist])
		return self.regmap.write(namedatalist)

	def read(self,names,offsets=None,resetafter=True):
		if self.sim:
			self.ndws.extend([(n,0,0) for (n) in names])
		return self.regmap.read(names,offsets,resetafter=resetafter)
	def run(self,seqs,init=None,period=None,preread=True,memclear=True,cmdclear=True,memscale=None,memwrite=True,periodwrite=True,cmdwrite=True,cmdclearlength=0):
		'''clear=False
		init=False
		memwrite=True
		periodwrite=True
		cmdwrite=True
		preread=True'''
		print('loading.....')

		#datetimeformat='%Y%m%d_%H%M%S_%f'
		#starttime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		if cmdclear:
			self.cmdclear(length=cmdclearlength)
			print('cmdclear length',cmdclearlength)
		#cmdclearstoptime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		if memclear:
			self.elementmemclear()
		#memclearstoptime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		if init:
			self.write(init)
		#initstoptime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		if memwrite:
			self.elementmemwrite(memscale=memscale)
		#memwritestoptime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		if periodwrite:
			period=int(numpy.ceil(seqs.getperiod()/self.dt/4.0))
			self.periodwrite(period)
			print('c_seqs','seqs period %f second'%seqs.period)
			print('period %d FPGA clk cycles'%period)
		#periodwritestoptime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		if cmdwrite:
			cmdwritelength=self.cmdwrite()
			print('cmdwrite length',cmdwritelength)
		#cmdwritestoptime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		self.write((('start',0),('start',1),('start',0)))
#		self.write((('start',0),('start',1),('start',0)))
#		print('seqs.tend() %f second'%seqs.tend())
		if preread:
			print('prereading')
			time.sleep(1)
			for i in range(2):
				self.accbuf()
				self.buf_monout(delay=0.0)
		#prereadstoptime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		#cmdcleartime=(datetime.datetime.strptime(cmdclearstoptime,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).total_seconds()
		#print('cmdcleartime',cmdcleartime)
		#memcleartime=(datetime.datetime.strptime(memclearstoptime,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).total_seconds()
		#print('memcleartime',memcleartime)
		#inittime=(datetime.datetime.strptime(initstoptime,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).total_seconds()
		#print('inittime',inittime)
		#memwritetime=(datetime.datetime.strptime(memwritestoptime,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).total_seconds()
		#print('memwritetime',memwritetime)
		#periodwritetime=(datetime.datetime.strptime(periodwritestoptime,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).total_seconds()
		#print('periodwritetime',periodwritetime)
		#cmdwritetime=(datetime.datetime.strptime(cmdwritestoptime,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).total_seconds()
		#print('cmdwritetime',cmdwritetime)
		#prereadtime=(datetime.datetime.strptime(prereadstoptime,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).total_seconds()
		#print('prereadtime',prereadtime)
	def sim1(self,mod):
		self.sim=True
		sim1=c_cmdsim(regmappath=self.regmappath,ndws=self.ndws,mod=mod)
#	def sim(self,seqs):
#		((cmda,cmdv),ops)=self.cmdgen(seqs)
#		sim1=c_cmdsim((cmda,cmdv),ops)
#		for chan in range(7):
#			tv=sim1.getiqmod(chan)
##       tv=sim1.getiq(chan)
#			pyplot.subplot(7,2,chan*2+1)
#			pyplot.plot(tv[:,0],tv[:,1].real,'.')
##		pyplot.xlim([tstart/1e-9,tend/1e-9])
#			pyplot.subplot(7,2,chan*2+2)
#			pyplot.plot(tv[:,0],tv[:,1].imag,'.')
##		pyplot.xlim([tstart/1e-9,tend/1e-9])
##print 'chan %d max %8.2f'%(chan,max(abs(tv[:,1])))
#		pyplot.show()

#	def c
# 0 for navg; 1 for min; 2 for max;
#   alist.append(0x5000f)
#   dlist.append(opsel)
#   mon_navr=3
#   alist.append(0x50006)
#   dlist.append(mon_navr)
#   mon_dt=500
#   alist.append(0x50007)
#   dlist.append(mon_dt)
#   mon_slice=0
#   alist.append(0x50008)
#   dlist.append(mon_slice)
#   mon_sel0=0
#   alist.append(0x50009)
#   dlist.append(mon_sel0)
#   mon_sel1=17
#   alist.append(0x5000a)
#   dlist.append(mon_sel1)
#   alist.append(0x5000b)
#   dlist.append(1)
#   alist.append(0x5000c)
#   dlist.append(0)

		#linepulse=seqs.linepulsedict()
		#mc={}
#
#		for line in linepulse:
#			for pulse in line:
#				memmap,cmd=pulsememcmd(linepulse[line],dt)
	def pulsememcmd(self,pulses,dt):
		elements=[]
		element=pulses
		pulselibdict={}
		npulse=len(pulse)
		for p in range(npulse):
			if p not in pulselibdict:
				pulselibdict[p]=[]
			pulselibdict[p].append(p)



		for p in range(npulse):
			for q in range(p,npulse):
				if pulses[p].timeoverlap(pulses[q]):
					#element.
					pass
class c_markelem:
	def __init__(self,index):
		self.index=index
		self.initelem()
	def initelem(self):
		self.start=None
		self.mem={}
		self.size=0
		self.pulses=[]
		self.usedspace=0
		self.nall=numpy.array([])
		self.vall=[]
	def addnv(self,nv):
		self.pulses.append(nv)
		if nv not in self.mem:
			self.mem[nv]=[]
			self.usedspace=self.usedspace+nv.len()
		self.mem[nv].append(nv)
		self.nall=numpy.append(self.nall,(nv.nall()))
#		self.nallset=set(self.nall)
		self.vall.extend(nv.vall())
	def memcmd(self):
		return []
	def overlap(self,pulse):
		ov4=False
		for nv in self.pulses:
			ov=ov4 or ((nv.nallmin<=pulse.nallmin and nv.nallmax>pulse.nallmin) or (pulse.nallmin<=nv.nallmin and pulse.nallmax>nv.nallmin))
		#ov3=numpy.intersect1d(self.nall, pulse.nall(), assume_unique=True)
		#return len(ov3)>0
		#if (len(ov3)>0)^ov4:
		#	print('overlap wrong',self.nallmin,self.nallmax,pulse.nallmin,pulse.nallmax,len(ov3))
		return ov4
	def havespace(self,nv):
		return True
	def opcmd(self):
		cmddict={}
		for nvs in self.mem:
			for nv in self.mem[nvs]:
				#print 'opcmd','trig',nv.n0(),'astart','0x%x %dx4'%(nvs.astart,nvs.astart/4),'len',nvs.len(),nvs.patch0,nvs.patch1,nv.len(),nv.patch0,nv.patch1
				trig_t=nv.n0()
				start=nv.patch0
				length=nv.len()
				#print 'opcmd length',length
				phrad=0
				freq=0
				if trig_t in cmddict:
					print('conflit, one commend per 4ns')
				else:
					cmddict[trig_t]={'trig_t':trig_t/4.0
							,'start':start/4.0
							,'length':length/4.0
							,'phrad':phrad
							,'freq':freq
							,'element':self.index
							,'dest':nv.dest
							,'cond':0
							}
                                #if self.index==9:
                                #print 'opcmd','startaddr',start,'0x%x'%start,cmddict[trig_t]['start'],'length',length,cmddict[trig_t]['length'],'trig_t',trig_t,cmddict[trig_t]['trig_t'],nv.pulse.tstart/1e-9,nv.pulse.twidth,nv.patch0,nv.patch1
#		for k in sorted(cmddict):
#			print cmddict[k]
		#print 'markelem opcmd',cmddict
		return cmddict

class c_element:
	def __init__(self,start,index,memreg,size=0x1000):
		#print '%x'%start
		self.index=index
		self.memreg=memreg
		self.start=start
		self.size=(1<<memreg.addr_width)
		#print memreg
		self.initelem()
		#print size, memreg.addr_length
	def initelem(self):
		self.usedspace=0
		self.ramdata=[]
		self.mem={}
		self.pulses=[]
		self.nall=numpy.array([])
		self.vall=[]
	def overlap(self,pulse):
		#return
		#ov1=True if len(set(self.nall).intersection(pulse.nall()))>0 else False
		ov4=False
		for nv in self.pulses:
			ov=ov4 or ((nv.nallmin<=pulse.nallmin and nv.nallmax>pulse.nallmin) or (pulse.nallmin<=nv.nallmin and pulse.nallmax>nv.nallmin))
		#ov3=numpy.intersect1d(self.nall, pulse.nall(), assume_unique=True)
		#ov2=len(self.nall.intersection(set(pulse.nall())))>0
		#if ov1!=ov2:
		#	print 'ov1 ov2',ov1,ov2
		#return len(ov3)>0
		#if (len(ov3)>0)^ov4:
		#	print('overlap wrong',self.nallmin,self.nallmax,pulse.nallmin,pulse.nallmax,len(ov3))
		return ov4
	def havespace(self,pulse):
		#		usedspace=0
		#for nv in self.mem:
		#	usedspace=usedspace+max([i.len() for i in self.mem[nv]])
		#if usedspace!=self.usedspace:
		#	print 'usedspace %d != self.usedspace %d'%(usedspace,self.usedspace)
		#print 'usedspace',usedspace,'size',self.size,'len',pulse.len()
		#return self.size > (usedspace+pulse.len())
		return self.size >(self.usedspace+pulse.len())

	def allocate(self):
		pass
	def addnv(self,nv):
		self.pulses.append(nv)
		if nv not in self.mem:
			self.mem[nv]=[]
			self.usedspace=self.usedspace+nv.len()
		self.mem[nv].append(nv)
		self.nall=numpy.append(self.nall,(nv.nall()))
#		self.nallset=set(self.nall)
		self.vall.extend(nv.vall())
	def memcmd(self):
		self.ramdata=[]
		addr=self.start
		for nvs in self.mem:
			lnv=sorted(self.mem[nvs],key=lambda x:x.len())[-1]
			#print 'c_element memcmd()','%x'%self.start, nvs,max([nv.patch0 for nv in self.mem[nvs]]),max([nv.patch1 for nv in self.mem[nvs]]),nvs.len(),min(nvs.n),len(nvs.n),nvs.n0()
			self.ramdata.extend(list(lnv.vall()))
			for nv in self.mem[nvs]:
				nv.astart=addr+lnv.patch0-nv.patch0
				#print 'astart',nv.astart
				#print 'c_element memcmd()','addr',addr,'lnv len',lnv.len(),'lnv patch0',lnv.patch0,'nv patch0',nv.patch0,'len ram',len(self.ramdata),'nv astart',nv.astart
			addr=addr+lnv.len()
			#print addr,len(self.ramdata)
		addrlist=range(self.start,self.start+len(self.ramdata))
		#print addrlist,self.ramdata
		return (addrlist,self.ramdata)
	def memcmd2(self):
		self.ramdata=[]
		for nvs in self.mem:
			lnv=sorted(self.mem[nvs],key=lambda x:x.len())[-1]
			#print 'c_element memcmd()','%x'%self.start, nvs,max([nv.patch0 for nv in self.mem[nvs]]),max([nv.patch1 for nv in self.mem[nvs]]),nvs.len(),min(nvs.n),len(nvs.n),nvs.n0()
			self.ramdata.extend(list(lnv.vall()))
		return self.ramdata
	def check(self):
		(alist,dlist)=self.memcmd()
		for nvs in self.mem:
			for nv in self.mem[nvs]:
				print(nv.astart+nv.len()<=len(alist))


	def opcmd(self):
		cmddict={}
		for nvs in self.mem:
			for nv in self.mem[nvs]:
				#print 'opcmd','trig',nv.n0(),'astart','0x%x %dx4'%(nvs.astart,nvs.astart/4),'len',nvs.len(),nvs.patch0,nvs.patch1,nv.len(),nv.patch0,nv.patch1
				trig_t=nv.n0()
				start=nv.astart
				length=nv.len()
				phrad=nv.pcarrier
				freq=nv.fcarrier
				if trig_t in cmddict:
					print('conflit, one commend per 4ns')
				else:
					cmddict[trig_t]={'trig_t':trig_t/4.0
							,'start':start/4.0
							,'length':length/4.0
							,'phrad':phrad
							,'freq':freq
							,'element':self.index
							,'dest':nv.dest
							,'cond':nv.cond
							}
                                #if self.index==9:
                                #print 'opcmd','startaddr',start,'0x%x'%start,cmddict[trig_t]['start'],'length',length,cmddict[trig_t]['length'],'trig_t',trig_t,cmddict[trig_t]['trig_t'],nv.pulse.tstart/1e-9,nv.pulse.twidth,nv.patch0,nv.patch1
#		for k in sorted(cmddict):
#			print cmddict[k]
		return cmddict
class c_nvpulse:
	def __init__(self,pulse,dt,flo,mod,cond=0):
		tv=pulse.val(dt=dt,flo=flo,mod=mod)
		self.n,self.v=tv.nvval(dt)
		self.pulse=pulse
		self.patch0=0
		self.patch1=0
		self.cond=cond
		self.pcarrier=pulse.pcarrier if hasattr(pulse,'pcarrier') else 0
		self.fcarrier=(0 if pulse.fcarrier==0 else pulse.fcarrier-flo ) if hasattr(pulse,'fcarrier') else 0
#		print self.fcarrier,pulse.fcarrier,flo
		self.dest=pulse.dest
		self.dt=dt
		self.astart=0
		self.nallmax=0
		self.nallmin=0

	def shiftt(self,nshift):
		self.n=self.n+nshift
	def addpatch1(self,patchtomod=4):
		currentlen=len(self.n)+self.patch0
		self.patch1=(patchtomod-currentlen%patchtomod)%patchtomod
                #print 'addpatch1',self.n[0],self.n[-1],self.pulse.tstart/1e-9,self.patch0,currentlen,self.patch1,self.pulse.twidth

	def addpatch0(self,npatch,patchtomod=4):
		self.patch0+=npatch
#		print 'nvtype',type(self.n),type(self.v)
#		print 'before',self.n0(),npatch,range(self.n0()-npatch,self.n0())
#		self.n=numpy.append(self.n,range(self.n0()-npatch,self.n0()))
#		self.v=numpy.append(self.v,numpy.zeros(npatch))
#		print 'after',self.n0()
		self.pcarrier=self.pcarrier-(2*numpy.pi*self.fcarrier*self.dt*npatch)
                #print 'patch phase',self.fcarrier,npatch,self.dt,self.pcarrier
	def __eq__(self,other):
		#		print 'c_nvpulse eq'
		return self.pulse==other.pulse and self.patch0%4==other.patch0%4
	def __hash__(self):
		#	print 'c_nvpulse hash'
		return hash(self.pulse)
	def n0(self):
		return min(self.n)-self.patch0
	def len(self):
		return len(self.n)+self.patch0+self.patch1
	def nall(self):
		self.addpatch1()
		nallran=numpy.arange(self.n0(),self.n0()+self.len())
		self.nallmax=max(nallran)
		self.nallmin=min(nallran)
		return nallran
	def vall(self):
		#		v1=numpy.append(numpy.ones(self.patch0),self.v)
		#vall=numpy.append(v1,numpy.ones(self.patch1))
		v1=numpy.append(numpy.zeros(self.patch0),self.v)
		vall=numpy.append(v1,numpy.zeros(self.patch1))
		return vall


if __name__=="__main__":
	with open('qubitcfg.json') as jfile:
		chipcfg=json.load(jfile)
	qchip=squbic.c_qchip(chipcfg)
	seq=[]
	trun=numpy.arange(0,50*3000e-9,3000e-9)
	#for irun in range(len(trun)):
	for irun in range(5):
		run=trun[irun]+1e-9
		wrun=7e-9*(irun+1)
		arun=0.2*(irun+1)
		#print 'run',run
		seq.extend([(run+0,qchip.gates['Q1readout'])
				,(run+0,qchip.gates['Q0readoutdrv'].modify({"amp":0.5}))
				,(run+0,qchip.gates['Q1readoutdrv'].modify({"amp":0.5}))
				,(run+1e-6,qchip.gates['Q7readout'])
				,(run+2e-6,qchip.gates['Q1rabi'].modify({"twidth":wrun,"amp":0.5}))
				,(None,qchip.gates['Q1readoutdrv'].modify({"amp":0.5}))
			,(None,qchip.gates['Q0rabi_ef'])
			,(None,qchip.gates['Q1rabi_gh'].modify({"amp":0.5}))
			,(None,qchip.gates['Q0readoutdrv'].modify({"amp":0.5}))
			])
		#		seq.extend([(run+0,qchip.gates['Q1rabi_gh'])
		#	,(None,qchip.gates['Q1rabi_gh'].modify({"twidth":wrun}))
		#	,(None,qchip.gates['Q1rabi_gh1'])
		#	,(None,qchip.gates['Q1rabi_gh2'])
		#	])
		#g1=qchip.gates['Q1rabi'].modify({"twidth":wrun,"amp":arun})
		#print 'g1',g1
		#seq.extend([(run+20e-6,g1)])#,(None,qchip.gates['Q1readoutdrv'].modify({"twidth":wrun})),(None,qchip.gates['Q1rabi_ef'].modify({"twidth":wrun})),(None,qchip.gates['Q0readoutdrv'])])
		#seq.extend([(run+20e-6,qchip.gates['Q1rabi'].modify({"twidth":wrun}))])
		#seq.extend([(run+20e-6,qchip.gates['Q1rabi'].modify({"twidth":wrun})),(None,qchip.gates['Q1readoutdrv'].modify({"twidth":wrun})),(None,qchip.gates['Q1rabi_ef'].modify({"twidth":wrun})),(None,qchip.gates['Q0readoutdrv'])])
		#g0=qchip.gates['Q1rabi']
		#print 'g1',[p.dest for p in g1.pulses],[p.dest for p in g0.pulses],g1.chip,g0.chip
	#seq=[(20e-6,qchip.gates['Q1readout'])]#,(20e-6,qchip.Q1.rabi),(None,qchip.Q1.readoutdrv),(None,qchip.Q1.readout)]
	seqs=squbic.c_seqs(seq)
	#print 'linepulsedict',seqs.linepulsedict()
	#print [[p for p in s.gate.pulses] for s in seqs.seqlist]
	#nvlist=[c_nvpulse(p,dt=1e-9,flo=5.5e9,mod=False) for p in s.gate.pulses for s in seqs.seqlist]
#	nvlist=[item for sublist in nv for item in sublist]
	#print 'len set nv',len(nvlist),len(set(nvlist))
	interface=c_ether('192.168.1.124',port=3000)
	hf=c_hfbridge(interface=interface)
	((cmda,cmdv),ops)=hf.cmdgen(seqs)
	hf.cmdgen0(seqs)
	hf.elementmemwrite()
	hf.cmdwrite()
	#print cmda,cmdv,ops
	tend=50*3000e-9
	from cmdsim import cmdsim
	sim1=cmdsim((cmda,cmdv),ops)
	for chan in range(7):
		#tv=sim1.getiqmod(chan)
		tv=sim1.getiq(chan)
		pyplot.subplot(7,2,chan*2+1)
		pyplot.plot(tv[:,0],tv[:,1].real,'.')
		pyplot.xlim([0,tend/1e-9])
		pyplot.subplot(7,2,chan*2+2)
		pyplot.plot(tv[:,0],tv[:,1].imag,'.')
		pyplot.xlim([0,tend/1e-9])
		print('chan %d max %8.2f'%(chan,max(abs(tv[:,1]))))
	pyplot.show()


	
	pyplot.figure(1)
#	tend=16e-9
	for i in range(8):
		print('dac element %d has %d different pulses, memory length %d'%(i,len(hf.dacelements[i].mem),len(hf.dacelements[i].ramdata)))
		ni=hf.dacelements[i].nall
		vi=numpy.array(hf.dacelements[i].vall)
		pyplot.subplot(8,2,2*(i+1)-1)
		pyplot.plot(ni,vi.real,'.')
		pyplot.xlim([0,tend/1e-9])
		pyplot.subplot(8,2,2*(i+1))
		pyplot.plot(ni,vi.imag,'.')
		pyplot.xlim([0,tend/1e-9])
	pyplot.figure(2)
	for i in range(3):
		print('adc element %d has %d different pulses,memory length %d'%(i,len((hf.loelements[i]).mem),len(hf.loelements[i].ramdata)))
		ni=hf.loelements[i].nall
		vi=numpy.array(hf.loelements[i].vall)
		pyplot.subplot(3,2,2*(i+1)-1)
		pyplot.plot(ni,vi.real,'.')
		pyplot.xlim([0,tend/1e-9])
		pyplot.subplot(3,2,2*(i+1))
		pyplot.plot(ni,vi.imag,'.')
		pyplot.xlim([0,tend/1e-9])
	pyplot.figure(3)
#	print hf.adcelements[0].mem[hf.adcelements[0].mem.keys()[0]][0]==hf.adcelements[2].mem[hf.adcelements[2].mem.keys()[0]][0]
#	n0list=[i.n0() for i in nvlist]
#	print n0list


#	print 'here',seqs.seqlist,seqs.seqlist[1].gate.pulses[0].twidth
	#for q in qchip.qubits:
	#	print q
	#	print 'qdrv',qchip.qubits[q].qdrv
	#	print 'rdrv',qchip.qubits[q].rdrv
	#	print 'read',qchip.qubits[q].read
	#	print
	#for q in qchip.qubits:
	#	for sig in ['qdrv','rdrv','read']:
	#		print q,sig
	#		plist=[p for p in getattr(qchip.qubits[q],sig)]
	#		print len(plist),len(set(plist)),[(p.t0,round(p.tstart/1e-9),round(p.tend()/1e-9)) for p in plist]
#	print 'seq check'
#	for seq in qchip.qubits['Q1'].qdrv:
#		print 'seq',seq.gate.pulses
#		for pulse in seq.gate.pulses:
#			print pulse.twidth
	(t,val)=(seqs.gettv(dt=1e-9,flo=5.5e9,mod=False)).tvval()
#	(t,val)=(seqs.gettv(dt=1e-9)).tvval()
	#plist=[[q.paradict for q in p.gate.pulses] for p in qchip.qubits['Q1'].qdrv]
#	for p in plist:
#		for q in plist:
#			print p==q
#	#print seqs
#	#print t,val
##	.val()
##	t,val=
#
#
##	tv=qchip.Q1.readout.getval(dt=1e-9)
##	print 'tv',tv
##	for pulse in qchip.Q1.rabi:
##		print 'loop',pulse,pulse.env
##		(t,v) =pulse.getval(dt=1e-9)
	pyplot.subplot(211)
	pyplot.plot(t,val.real,'.')
	pyplot.subplot(212)
	pyplot.plot(t,val.imag,'.')
