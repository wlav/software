import numpy
def cmdgen128(trig_t,element,dest,start,length,phrad,freq,cond=0,debug=False,dt=1e-9,fpgadacclkratio=4):
	trig_t=int(round(trig_t/dt/fpgadacclkratio))
	start=start/fpgadacclkratio
	length=length/fpgadacclkratio

	assert trig_t<2**24, 'trig_t %d exceeds 2**24'%trig_t
	element=int(element)
	dest=int(dest)
	start=int(start)
	length=int(length)
	phdeg=phrad/numpy.pi*180
	freq=freq
	ph_binary = int(phdeg/360.0*2**14)
	freq_binary=int(1e-9*freq*2**24)
#		cmd=((trig_t&0xffffff)<<72)+((element&0xff)<<64)+((dest&0x3)<<62)+((start&0xfff)<<50)+((length&0xfff)<<38)+((ph_binary&0x3fff)<<24)+(freq_binary&0xffffff)
#		cmd1=((cmd&0xffffffff)<<64)+(((cmd>>32)&0xffffffff)<<32)+((cmd>>64)&0xffffffff)
	cmd=((cond&0x1)<<96)+((trig_t&0xffffff)<<72)+((element&0xff)<<64)+((dest&0x3)<<62)+((start&0xfff)<<50)+((length&0xfff)<<38)+((ph_binary&0x3fff)<<24)+(freq_binary&0xffffff)
	cmd1=(((cmd>>96)&0xffffffff)<<96)+((cmd&0xffffffff)<<64)+(((cmd>>32)&0xffffffff)<<32)+((cmd>>64)&0xffffffff)
	if debug:
		#print('trig_t',trig_t,'deg','%8.1f'%(phdeg%360),'element',element,'dest',dest,'start',start&0xfff,hex(start&0xfff),'length',length,'ph_binary',ph_binary,'rad',phrad%(2*numpy.pi),'freq','%8.3e'%freq,'freq_binary',freq_binary,format(cmd1,'032x'))
		print('trig_t',trig_t,'deg','%8.1f'%(phdeg%360),'ph_binary',ph_binary,'rad',phrad%(2*numpy.pi),'dest',dest,'length',length,'freq','%8.3e'%freq,'freq_binary',freq_binary,format(cmd1,'032x'),'element',element,'start',start&0xfff,hex(start&0xfff))
	#print('***v2.0 printing format***','trig_t',trig_t,'element',element,'dest',dest,'start',start&0xfff,hex(start&0xfff),'length',length,'ph_rad:%.3f'%(phrad%(2*numpy.pi)),'freq_MHz:%.3f'%(freq/1e6))
	return cmd1
