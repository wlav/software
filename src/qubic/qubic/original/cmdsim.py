import numpy
from matplotlib import pyplot
class c_cmdsim:
	def __init__(self,mem,cmds,maxchan=8):
		mema,memv=mem
		self.mem=dict(zip(mema,memv))
		self.cmd=cmds
		self.dest={}
		for chan in range(maxchan):
			self.dest[chan]={}
		for cmd in sorted(cmds):
			self.runcmd(**cmds[cmd])
		self.sumdest()

	def runcmd(self,dest,phini,trig_t,element,start,length,freq,dt=1e-9):
		#print 'cmdsim runcmd',start,length
		for i in range(int(length)*4):
			t=trig_t*4+i
			iq=self.mem[4*start+i]
			iqmod=iq*numpy.cos(2*numpy.pi*freq*i*dt+phini)
			if t not in self.dest[dest]:
				self.dest[dest][t]={}
				self.dest[dest][t]['iq']=[]
				self.dest[dest][t]['iqmod']=[]
			self.dest[dest][t]['iq'].append(iq)
			self.dest[dest][t]['iqmod'].append(iqmod)
	def sumdest(self):
		for dest in self.dest:
			for t in self.dest[dest]:
				self.dest[dest][t]['iqsum']=sum(self.dest[dest][t]['iq'])
				self.dest[dest][t]['iqmodsum']=sum(self.dest[dest][t]['iqmod'])
	def getiq(self,chan=0):
		if self.dest[chan]:
			tv=numpy.array([(t,self.dest[chan][t]['iqsum']) for t in sorted(self.dest[chan])])
		else:
			tv=numpy.array([[0,0],[0,0]])
		#print 'cmdsim getiq tv shape',tv.shape
		return tv
	def getiqmod(self,chan):
		if self.dest[chan]:
			tv=numpy.array([(t,self.dest[chan][t]['iqmodsum']) for t in sorted(self.dest[chan])])
		else:
			tv=numpy.array([[0,0],[0,0]])
		#print 'cmdsim getiqmod tv shape',tv.shape
		return tv


if __name__=="__main__":
	mems=(range(0x38000,0x50000),numpy.ones(0x50000-0x38000))
	cmds={0:{'dest': 0, 'phini': -50.2654824574367, 'trig_t': 0.0, 'element': 0, 'start': 57515.0, 'length': 128, 'freq': 320000000.0}
		,8:{'dest': 0, 'phini': 17.090264035528477, 'trig_t': 2.0, 'element': 1, 'start': 58523.0, 'length': 120, 'freq': -160000000.0}
		}
	sim1=c_cmdsim(mems,cmds)
	tv=sim1.getiq()
	pyplot.plot(tv[:,0],tv[:,1])
	pyplot.show()

