import json
import sys
sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
from squbic import c_hfbridge,c_qchip,c_seq,c_seqs
import time
import argparse
def signvalue(vin,width=16):
	return vin-2**width if vin>>(width-1) else vin

if __name__=="__main__":
	reg=sys.argv[1]
	hf=c_hfbridge()
	hf.read([reg])
	print(hf.getregval(reg))
