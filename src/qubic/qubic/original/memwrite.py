print('memwrite',__name__,"." in __name__)
import sys
import numpy
import json

from qubic.qubic import squbic
from qubic.qubic.chassis import c_chassis
else:
	import squbic
	from chassis import c_chassis

if __name__=="__main__":
	#import memcfg
	import imp
	import experiment
	parser,cmdlinestr=experiment.cmdoptions()
	clargs=parser.parse_args()

	memcfg=imp.load_source("memcfg",clargs.wiremapmodule)#"./submodules/qchip/chip57/wiremap_chip57_20210629.py")
	with open(clargs.qubitcfg) as jfile:
		qubitcfg=json.load(jfile)
	qchip=squbic.c_qchip(qubitcfg)
	chassis=c_chassis(clargs.ip)
	membuf,gatecmd,usedsize=chassis.memgen(qchip,memcfg)
	val1=numpy.ones(4096)*(((19000)<<16)+0)
	val0=numpy.zeros(4096)*(((0000)<<16)+0)
	for elem,val in membuf.items():
		chassis.memwrite(((elem,val),))
		#chassis.memwrite(((elem,val1),))
		#chassis.memwrite(((elem,val0),))
