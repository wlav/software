import datetime
import argparse
import sys
from qubic.qubic.squbic import *
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import experiment
class c_cwdrv(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg_cwdrv.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		pass
	def cwdrvseqs(self,readoutdrvamp=0.95,fread=20e6,wcwdrv=1e-6):
		#self.seqs.add(360e-9,               self.qchip.gates['M0mark'])
		modcwdrv={}
		if readoutdrvamp is not None:
			modcwdrv.update(dict(amp=readoutdrvamp))
		if fread is not None:
			modcwdrv.update(dict(fcarrier=fread))
		run=0
		modcwdrv.update(dict(twidth=wcwdrv))
		#self.seqs.add(run,self.qchip.gates['readoutcwdrv'].modify(modcwdrv))
		self.seqs.add(run,self.qchip.gates['Q7cwdrv'].modify(modcwdrv))
		#self.seqs.add(run,self.qchip.gates['Q6cwdrv'].modify(modcwdrv))
		#self.seqs.add(run,self.qchip.gates['Q5cwdrv'].modify(modcwdrv))
		#self.seqs.add(run,self.qchip.gates['IP3cwdrv'])
		#self.seqs.add(run,self.qchip.gates['Q765cwdrv'])
		run=self.seqs.tend()
		self.seqs.setperiod(period=run)

if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(elementstep=4e-9)
	clargs=parser.parse_args()
	cwdrv=c_cwdrv(**clargs.__dict__)
	readoutdrvamp=1.0
	fread=50e6
	wcwdrv=1e-6
	cwdrv.cwdrvseqs(readoutdrvamp=readoutdrvamp,fread=fread,wcwdrv=wcwdrv)
	cwdrv.run(bypass=clargs.bypass)
	# IMPORTANT: Although qubitid is arbitrary, make sure the envelope should be set to "square"
	# readoutcwdrv: DAC0 and DAC1
	# Q7cwdrv: DAC2 and DAC3
	# Q6cwdrv: DAC4 and DAC5
	# Q5cwdrv: DAC6 and DAC7
	# Change parameter locally
	# Change frequency: fread
	# Change amplitude: readoutdrvamp
	# Execute: python cwdrv.py --qubitcfg qubitcfg_cwdrv.json
