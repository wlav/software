def heralding(qubits):
    circuits=[]
    circuit=[]
    if isinstance(qubits,list) or isinstance(qubits,tuple):
        for qubit in qubits:
            circuit.append({'name': 'read', 'qubit': [qubit]})
    elif isinstance(qubits,str):
        circuit.append({'name': 'read', 'qubit': [qubits]})
    circuits.append(circuit)
#    print('heralding circuits',circuits)
    return circuits
