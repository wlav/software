import numpy
import os
import sys
import bz2
sys.path.append('../..')
from qubic.qubic import commands
from qubic.qcvv.analysis import gmm
from qubic.qcvv.analysis import readcorr
from qubic.qubic.compile import c_compile,c_circbasegate#,elememory,circtocmds
from qubic.instrument.ttyclient import c_ttyclient
from matplotlib import pyplot
import pickle

class c_qubicrun():
    def __init__(self,chassis,wiremap,qchip,**kwargs):
        self.chassis=chassis
        self.wiremap=wiremap
        self.qchip=qchip
        self.ops=dict(heraldingsymbol=None,para=[{}],heraldcmds=None,runpickle=False,snapshotfilename=None,overlapcheck=True,gmixs=None,combineorder=None,n_components=2,labelmeasindex=0,labels=['1','0'],debug=0)
        self.ops.update(kwargs)
        sys.setrecursionlimit(100000)
        self.ttydev={}
        self.ttysocket={}
        if hasattr(wiremap,'ttydev'):
            self.debug(3,'Info: init ttydevice')
            for k,v in wiremap.ttydev.items():
                if v['hostname'] not in self.ttysocket:
                    self.ttysocket[v['hostname']]=c_ttyclient(v['hostname'],10000)
                self.ttydev[k]=v
                self.ttydev[k].update(dict(ttyclient=self.ttysocket[v['hostname']]))
        self.cmdlists=[]
    def debug(self,level,*args,**kwargs):
        if self.ops['debug']>=level:
            print(*args,**kwargs)
    def compile(self,seqs,**kwargs):
        self.ops.update(kwargs)
        #print('chassis debug',self.chassis.debug)
#    print('debug qubicrun run circcmds',circcmds)
#    print(ops['heraldingsymbol'],ops['heraldcmds'])
#    exit('herald')
#    print('run ops',ops)
#    print('chassis sim',chassis.sim)
#    print(circcmds)


        if not self.ops['runpickle']:
            circcmds,cmdlists=self.circtocmds(seqs,**self.ops)
            self.debug(4,'circcmds',circcmds)
            if self.ops['snapshotfilename'] is not None:
                snapshot=dict(cmdlists=cmdlists,elements=self.chassis.elements,ops=self.ops)
                self.debug(3,'Info: save snapshot file',self.ops['snapshotfilename'])
                with bz2.BZ2File(self.ops['snapshotfilename'],'wb') as f:
                    pickle.dump(snapshot,f)
        else:
            self.debug(4,'runpickle')
            pbz2filename=self.ops['snapshotfilename']
            if os.path.isfile(pbz2filename):
                with bz2.BZ2File(pbz2filename,'rb') as f:
                    pk=pickle.load(f)
                if 'cmdlists' in pk and 'elements' in pk and 'ops' in pk:
                    self.chassis.elements=pk['elements']
                    cmdlists=pk['cmdlists']
                    self.ops=pk['ops']
                else:
                    self.debug(1,'Warning: pickle have these keys',list(pk.keys()),'need cmdlists and elements, and combineorder')
            else:
                self.debug(0,'Error: no such file',pbz2filename)
        self.cmdlists=cmdlists
#        return cmdlists
    def readlo(self,freq=None):
        status=False
        if 'readlo' in self.ttydev:
            readlotty=self.ttydev['readlo']['ttyclient']
            rdrvdeviceid=self.ttydev['readlo']['deviceid']
            freq=self.ttydev['readlo']['default']['freq'] if freq is None else freq
            chan=self.ttydev['readlo']['default']['chan']
            readloret=readlotty.cmd(dict(cmd='write',deviceid=rdrvdeviceid,chan=chan,freq=freq))
            if not readloret['status']:
                exit('Error: read readlo failure')
            else:
                status=True
        else:
            if val!=0:
                self.debug(0,'Error: readlo setting error')
        return readloret['result']
    def qdrvlo(self,freq=None):
        status=False
        if 'qdrvlo' in self.ttydev:
            qdrvlotty=self.ttydev['qdrvlo']['ttyclient']
            rdrvdeviceid=self.ttydev['qdrvlo']['deviceid']
            freq=self.ttydev['qdrvlo']['default']['freq'] if freq is None else freq
            chan=self.ttydev['qdrvlo']['default']['chan']
            qdrvloret=qdrvlotty.cmd(dict(cmd='write',deviceid=rdrvdeviceid,chan=chan,freq=freq))
            if not qdrvloret['status']:
                exit('Error: qdrv qdrvlo failure')
            else:
                status=True
        else:
            if val!=0:
                self.debug(0,'Error: qdrvlo setting error')
        return qdrvloret['result']
    def setvat(self,vatdevice,val=None,finish=False):
        status=False
        if vatdevice in self.ttydev:
            vattty=self.ttydev[vatdevice]['ttyclient']
            vatdeviceid=self.ttydev[vatdevice]['deviceid']
            val=self.ttydev[vatdevice]['default'] if val is None else val
            vatret=vattty.cmd(dict(cmd='write',deviceid=vatdeviceid,data=val))
            if not vatret['status']:
                exit('Error: vat %s failure'%str(atdevice))
            else:
                status=True
        else:
            if val!=0:
                self.debug(0,'Error: can not change variable attenuator')
#        if finish:
#            switchtty.cmdfinish()
        return status
    def setrdrvvat(self,val=None,finish=False):
        return self.setvat(vatdevice='rdrvvat',val=val,finish=finish)
    def setswitch(self):
        switches=list(set([c['switch'] for cmdlist,p in self.cmdlists for c in cmdlist if 'switch' in c if c['switch'] is not None]))
        self.debug(4,'debug set switch',switches)
        if len(switches)==0:
            pass
        elif len(switches)==1:
            if 'qdrvswitch' in self.ttydev:
                switchtty=self.ttydev['qdrvswitch']['ttyclient']
                qdrvdeviceid=self.ttydev['qdrvswitch']['deviceid']
                switchret=switchtty.cmd(dict(cmd='write',deviceid=qdrvdeviceid,data=switches[0]))
                if not switchret['status']:
                    exit('Error: qdrv switch failure')
            else:
                self.debug(0,'Error: can not switch qdrv')
#    print(ttyclient.cmd(dict(cmd='write',deviceid='attn1',data=8)))
            #    from qubic.instrument import switch
            #switch.switch(switches[0],deviceid=self.wiremap.switchdev['id'])
        else:
            exit('Error:More than one qubit connected to the same switch position %s'%str(switches))
    def acqbufrun(self,nbuf,**kwargs):
        self.ops.update(kwargs)
        self.setswitch()
        acqval=self.chassis.acqbufrun(nbuf,cmdlists=self.cmdlists,**self.ops)
        return acqval

    def accbufrun(self,nsample,**kwargs):
        self.ops.update(kwargs)
        if self.ops['heraldcmds'] is not None:
            if self.ops['heraldingsymbol'] is None and 'heraldingsymbol' not in kwargs:
                self.ops['heraldingsymbol']=self.ops['labels'][-1]
        self.setswitch()
        self.ops.update(dict(nsample=nsample,cmdlists=self.cmdlists))
        self.accval=self.chassis.accbufrun(**self.ops)
        return self.accval
        #gmmresult=self.gmmcount(accval)
        #return gmmresult

    def gmmcount(self,accval=None,**kwargs):
        self.ops.update(kwargs)
        accval=self.accval if accval is None else accval
        
        self.debug(3,'Info using gmixs',self.ops['gmixs'])
        if self.ops['gmixs'] is None:
            self.debug(4,'gmix: generate from data')
            gmixs={}
            for isub in accval:
                self.debug(5,'debug gmix',self.ops['n_components'])
                gmixmodel=gmm.GaussianMixtureLabeled(n_components=self.ops['n_components'],covariance_type='full')
                gmixmodel.fit_complex(accval[isub].reshape((-1,1)))
                self.debug(5,'before setlabels',gmixmodel.labels)
                if isinstance(self.ops['labelmeasindex'],list) or isinstance(self.ops['labelmeasindex'],tuple):
                    for label,labelmeasindex in self.ops['labelmeasindex']:
                        self.debug(5,'label',label,labelmeasindex)
                        if labelmeasindex is None:
                            gmixmodel.setlabels(data=None,label=label)
                        else:
                            gmixmodel.setlabels(accval[isub][:,labelmeasindex],label,mode='max')

                else:
                    gmixmodel.setlabels(accval[isub][:,self.ops['labelmeasindex']],self.ops['labels'],mode='mintomax')
                gmixs[isub]=gmixmodel
                self.debug(3,'Info: generate gmm for %s '%isub, 'gmixmodel mean is',gmixmodel.means_,'angle',gmixmodel.angle())
        elif isinstance(self.ops['gmixs'],list):
            gmixspathdict={isub:'/'.join([self.ops['gmixs'][0],'%s_%s.npz'%(str(isub),self.ops['gmixs'][1])]) for isub in accval}
            self.debug(4,gmixspathdict)
            if all(os.path.exists(gmixspathdict[isub]) for isub in gmixspathdict):
                gmixs={isub:gmm.GaussianMixtureLabeled(fromdict=numpy.load(gmixspathdict[isub])) for isub in gmixspathdict}
                if self.chassis.debug:
                    self.debug(4,'gmix: use pre calibrated data from file:',gmixs)
            else:
                for isub in gmixspathdict:
                    if not os.path.exists(gmixspathdict[isub]):
                        self.debug(0,'Error: missing gmixs in path:'+gmixspathdict[isub])
                exit('gmixs path error')
        elif isinstance(self.ops['gmixs'],str):
            gmixspathdict={isub:'/'.join([self.ops['gmixs'],'%s_gmix.npz'%str(isub)]) for isub in accval}
            self.debug(4,gmixspathdict)
            if all(os.path.exists(gmixspathdict[isub]) for isub in gmixspathdict):
                gmixs={isub:gmm.GaussianMixtureLabeled(fromdict=numpy.load(gmixspathdict[isub])) for isub in gmixspathdict}
                if self.chassis.debug:
                    self.debug(4,'gmix: use pre calibrated data from file:',gmixs)
            else:
                for isub in gmixspathdict:
                    if not os.path.exists(gmixspathdict[isub]):
                        self.debug(0,'Error: missing gmixs in path:'+gmixspathdict[isub])
                exit('gmixs path error')
        elif isinstance(self.ops['gmixs'],dict):
            if all(isub in self.ops['gmixs'] for isub in accval):
                gmixsdict={isub:self.ops['gmixs'][isub] for isub in accval}
                gmixs={isub:gmm.GaussianMixtureLabeled(fromdict=numpy.load(gmixsdict[isub])) for isub in gmixsdict}
                if self.chassis.debug:
                    self.debug(4,'gmix: use pre calibrated data objects',gmixs)
            else:
                for isub in accval:
                    if not isub in self.ops['gmixs']:
                        self.debug(0,'Error: missing gmixs in dict:'+isub)
                exit('gmixs dict error')
        else:
            self.debug(0,'please specify which gmm model should be used, by a string to the path where the model is saved as a .npz file or by a dictionary of {qubitid:GaussianMIxtureLabeled object} or None to generate one')
        self.debug(3,'Info: predict using GMM model generated time')
        self.debug(4,[gmixs[i].labels for i in gmixs])
        self.debug(4,'isub',[isub for isub in accval])
        self.debug(4,'gmixs[isub]',[gmixs[isub].labels.values() for isub in accval])
        self.debug(3,'\n'.join(['%s: %s contain symbols: %s'%(isub,gmixs[isub].createtimestamp,' '.join(gmixs[isub].labels.values())) for isub in accval]))

        if self.ops['combineorder']:
            combineorder=[i for i in self.ops['combineorder'] if i in accval]
        else:
            combineorder=sorted(list(accval.keys()))#self.ops['qubitid']
            if len(combineorder)>1:
                self.debug(1,'Warning: No combine order given, using sorted accval order',combineorder)
#            if len(accval.keys())==1:
#                combineorder=list(accval.keys())#[i for i in readmaprev.values() if i in accval]
#            else:
#                if 'qubitid' in self.ops:
#                else:
#                    self.debug(0,'Error: No combine order given, using random order',self.ops['qubitid'])
#                    exit('Error:no combine order specified %s'%(str(list(accval.keys()))))
        self.debug(4,'gmixs',gmixs.keys(),'accval',accval.keys())

        predict={isub:gmixs[isub].predict_complex(accval[isub]) for isub in accval}
#    predict2={isub:gmixs2[isub].predict_complex(accval[isub]) for isub in accval}
#    print('debug combineorder',combineorder,ops['combineorder'],readmaprev.values(),accval.keys(),'readmaprev',readmaprev)
        countsum=gmm.countsum(predict,heraldingsymbol=self.ops['heraldingsymbol'],combineorder=combineorder)


        accvalmafterheralding={}
        accvalm={}
        accvalh={}
        if countsum['predicth'] is None:
            accvalh=None
            accvalm=accval
            accvalmafterheralding=None
        else:
            for p in combineorder:
                col,row2=accval[p].shape
                row=row2//2
                accval2=accval[p].reshape((col,row,2))
#                accvalmafterheralding[p]=accval2[:,:,1]*countsum['predicth'][p]
                accvalmafterheralding[p]=[numpy.extract(countsum['predicth'][p][:,m],accval2[:,m,1]) for m in range(row)]
#                accvalmafterheralding[p]=[accval2[:,c,1] if countsum['predicth'][p]
                accvalm[p]=accval2[:,:,1]
                accvalh[p]=accval2[:,:,0]
#                accvalmafterheralding={p:[numpy.extract(countsum['predicth'][p][:,m],accvalm[p][:,m]) for m in range(accvalm[p].shape[1])]  for p in combineorder}
#            accvalh={p:accval[p][:,0::2] for p in combineorder}
#            accvalm={p:accval[p][:,1::2] for p in combineorder}
#            accvalmafterheralding=None   # to be fixed
            #accvalmafterheralding={p:[numpy.extract(countsum['predicth'][p][:,m],accvalm[p][:,m]) for m in range(accvalm[p].shape[1])]  for p in combineorder}
            #accvalmafterheralding=[[numpy.extract(countsum['predicth'][m],mv) for m,mv in enumerate(accvalm[p])] for p in combineorder]
        countsum['combineorder']=combineorder
        self.rcal=readcorr.readcorr(countsum=countsum,rcalpath=self.ops['calipath'])
        pcombinecorr=self.rcal.corr(pmeas=countsum['pcombine'])
        #psinglecorr={q:None}#rcal.corr(pmeas=countsum['psingle'][q]) for q in countsum['psingle']}

        gmmresult={}
        gmmresult['countsum']=countsum
        gmmresult['gmixs']=gmixs
        gmmresult['accval']=accval
        gmmresult['pcombinecorr']=pcombinecorr
        #gmmresult['psinglecorr']=psinglecorr
        gmmresult['accvalmafterheralding']=accvalmafterheralding
        gmmresult['accvalm']=accvalm
        gmmresult['accvalh']=accvalh
        self.debug(3,'qubic run exit')
        self.gmmresult=gmmresult
        return self.gmmresult
    def    circtocmds(self,circcmds,**kwargs):
        ops=dict(starttime=20e-9)
        ops.update(kwargs)
        chipgatedictsmodi={}
        circseqs=[]
        hrldseqs=[]
        chipgatedict={}
        circbasegate={}
        allbasepulse=[]

        for index,circgates in enumerate(circcmds):
            circseq=c_compile(circuitgates=circgates,**ops)
            #print(circseq,circgates)
            circseqs.append(circseq)
            chipgatedictsmodi.update(circseq.chipgatedictsmodi)
        if ops['heraldcmds'] is not None:
            for index,circgates in enumerate(ops['heraldcmds']):
                hrldseq=c_compile(circuitgates=circgates)
                hrldseqs.append(hrldseq)
                chipgatedictsmodi.update(hrldseq.chipgatedictsmodi)
#        if ops['heraldingsymbol'] is None:
#            ops['heraldingsymbol'] = ops['labels'][-1]
        for gate,para in chipgatedictsmodi.items():
            if para:
                chipgatedict[gate]=self.qchip.gates[gate[0]].modify(para)
            else:
                chipgatedict[gate]=self.qchip.gates[gate].modify(None)
            circbasegate[gate]=c_circbasegate(name=gate,refgate=chipgatedict[gate],allbasepulse=allbasepulse)
            self.debug(4,'base gate name',gate)
        for index,circseq in enumerate(circseqs):
            circseq.maptobasepulse(circbasegate,allbasepulse,wiremap=self.wiremap)
            circseq.circtime()
            circseq.qubictime()
            circseq.circuittree.patchbase()
        if ops['heraldcmds'] is not None:
            for index,circseq in enumerate(hrldseqs):
                hrldseq.maptobasepulse(circbasegate,allbasepulse,wiremap=self.wiremap)
                hrldseq.circtime()
                hrldseq.qubictime()
                hrldseq.circuittree.patchbase()
        for p in allbasepulse:
            if p.circpulse:
                p.tlength=max([pp.tlength for pp in p.circpulse])
                for pp in p.circpulse:
                    pp.nlength=int(round(pp.tlength/self.chassis.dt,10))
        self.elememory(allbasepulse=allbasepulse,**ops)
        for p in allbasepulse:
            if p.circpulse:
                for pp in p.circpulse:
                    self.debug(5,pp.start,p.tlength,pp.tlength,self.chassis.dt)
                    if pp.dest is not None:
                        pp.start+=int(round((p.tlength-pp.tlength)/self.chassis.dt,10))
#        for index,circseq in enumerate(circseqs):
#            for pulse in circseq.pulselist:
#                pulse.calcnlengthtend(chassis.dt)
        circcmdlist=[]
        for index,circseq in enumerate(circseqs):
            cmdlistfun,lastpoint=circseq.qubiccmd()
            for para in ops['para']:
                self.debug(4,'debug para',para)
                circcmdlist.append((cmdlistfun(**para),lastpoint))

        for s in circcmdlist:
            self.debug(4,'debug circcmdlist',s)

        if ops['heraldcmds'] is not None:
            hrldcmdlist=[]
            for index,hrldseq in enumerate(hrldseqs):
                cmdlistfun,lastpoint=hrldseq.qubiccmd()
                for para in ops['para']:
                    hrldcmdlist.append((cmdlistfun(**para),lastpoint))
        else:
            hrldcmdlist=None
        circcmds=commands.circuitgaps(circcmdlist=circcmdlist,hrldcmdlist=hrldcmdlist,**ops)
        cmdlists=commands.cmdchop(circcmds=circcmds,readelem=self.chassis.dloelem,**ops)
        return circcmds,cmdlists

    def    elememory(self,allbasepulse,**kwargs):
        self.debug(4,'elemmemory wiremap',self.wiremap.elemdict)
        self.debug(4,'allbasepulse in elememory',allbasepulse)
        allbasepulse=[p for p in allbasepulse if p.dest is not None]
        for pulse in allbasepulse:
            pulse.calcnlengthtend(self.chassis.dt)
            pulse.nvall(self.chassis.dt)
        dacpulsearrange=[p for p in allbasepulse if 'read' not in p.dest]
        dlopulsearrange=[p for p in allbasepulse if 'read' in p.dest]
        if self.wiremap is not None:
            if hasattr(self.wiremap,'elemdict'):
                pulsedestdict={}
                for pulse in allbasepulse:
                    if pulse.dest not in pulsedestdict:
                        pulsedestdict[pulse.dest]=[]
                    if pulse not in pulsedestdict[pulse.dest]:
                        pulsedestdict[pulse.dest].append(pulse)
                        dacpulsearrange=[]
                        dlopulsearrange=[]
                self.debug(4,'pulsedestdict',pulsedestdict)
                for k,v in pulsedestdict.items():
                    if k in self.wiremap.elemdict:
                        preferedelem=self.wiremap.elemdict[k]
                        if preferedelem in self.chassis.dacelem:
                            self.debug(4,'dac elem')
                            for pulse in sorted(v,key=lambda n:n.nlength,reverse=True):
                                dacmemgood=self.chassis.dacelem[preferedelem].insertpulse(pulse,overlapcheck=kwargs['overlapcheck'])
                                self.debug(4,'dac mem allocation','good',dacmemgood,'lastpoint',self.chassis.dacelem[preferedelem].lastpoint,'gatename',pulse.gate.name,'preferedelem',preferedelem)
                                if not dacmemgood:
                                    dacpulsearrange.append(pulse)
                        elif preferedelem in self.chassis.dloelem:
                            self.debug(4,'dlo elem')
                            for pulse in sorted(v,key=lambda n:n.nlength,reverse=True):
                                self.debug(4,'preferedelem',preferedelem)
                                dlomemgood=self.chassis.dloelem[preferedelem].insertpulse(pulse,overlapcheck=True or kwargs['overlapcheck'])
                                self.debug(4,'dlo mem allocation','good',dlomemgood,'lastpoint',self.chassis.dloelem[preferedelem].lastpoint,'gatename',pulse.gate.name,'preferedelem',preferedelem)
                                if not dlomemgood:
                                    dlopulsearrange.append(pulse)
                        else:
                            self.debug(4,'preferedelem %s is not understood'%str(perferedelem))
                            exit('elememory')
                    else:
                        dacpulsearrange=[p for p in allbasepulse if 'read' not in p.dest]
                        dlopulsearrange=[p for p in allbasepulse if 'read' in p.dest]
#    print('debug arrange',chassis.debug,dacpulsearrange,dlopulsearrange,'nooverlap',kwargs['overlapcheck'])

        for pulselist,chassiselem in [(dacpulsearrange,self.chassis.dacelem),(dlopulsearrange,self.chassis.dloelem)]:
            for pulse in pulselist:
                self.debug(4,pulse,chassiselem,pulse.__class__)
                memgood=False
                for elemindex,elem in chassiselem.items():
                    self.debug(4,'elemindex',elemindex)
                    if not memgood:
                        memgood=elem.insertpulse(pulse,overlapcheck=kwargs['overlapcheck'])
                        self.debug(4,'Info: try to put ',pulse.gate.name,'in element',elemindex,'result is',memgood)
                if not memgood:
                    for elem in self.chassis.elements:
                        self.debug(4,'elem last point:',elem.elemindex,elem.lastpoint)
                    exit('Error: failed to allocate memory for pulse : %s'%str(pulse.gate.name))
        for elem in self.chassis.elements:
            elem.qubits=list(set([q for p in elem.pulses for q in p.gate.qubit]))
            elemdests=list(set([p.dest for p in elem.pulses]))
            elemgates=list(set([p.gate.name for p in elem.pulses]))
            self.debug(4,'elem',elem.elemindex,'qubit',elem.qubits,'dest',elemdests,'gate',elemgates)

#    for elem in chassis.elements:
#        print(elem,elem.elemindex)
#        for p in elem.pulses:
#            print(p.gate.name)
#        print(elem.basepulses,elem.lastpoint)
#    for bp in allbasepulse:
#        print(bp.gate.name,bp.start,bp.nlength)
#        for p in bp.circpulse:
#            print(p.element,p.start,p.length,p.tstart,p.tend)
    #return memgood
if __name__=="__main__":
    from squbic import c_qchip
    from chassis import c_chassis
    import imp
    import os
    import json
    ops={}
    ops['cfgpath'] = '../../../../../submodules/qchip/' + os.environ['QUBIC_CHIP_NAME'] + '/'
    ops['ip'] = os.environ['QUBICIP']
    ops['qubitcfgfile'] = ops['cfgpath'] + os.environ['QUBICQUBITCFG']
    ops['wiremapfile']= ops['cfgpath'] + os.environ['QUBICWIREMAP'] + '.py'
    ops['sim']=True
    ops['debug']=True
#    ops.update(**kwargs)
    chassis=c_chassis(ops['ip'],sqinit=False,sim=ops['sim'],debug=ops['debug'])
    with open(ops['qubitcfgfile']) as jfile:
        qubitcfg=json.load(jfile)
    wiremap = imp.load_source("wiremap",ops['wiremapfile'])
    qchip=c_qchip(qubitcfg)
    circuit2=[
            #{'name': 'delay', 'qubit': ['Q4', 'Q5', 'Q6', 'Q7'], 'para': {'delay': 2.9999999999999997e-05}}
            #,{'name': 'barrier', 'qubit': ['Q4', 'Q5', 'Q6', 'Q7']}
{'name': 'read', 'qubit': ['Q5']}
,{'name': 'read', 'qubit': ['Q4']}
,{'name': 'read', 'qubit': ['Q6']}
,{'name': 'read', 'qubit': ['Q5']}
#,{'name': 'read', 'qubit': ['Q4']}
,{'name': 'read', 'qubit': ['Q6']}
,{'name': 'barrier', 'qubit': ['Q5', 'Q6','Q4']}
,{'name': 'read', 'qubit': ['Q5']}
,{'name': 'read', 'qubit': ['Q4']}
,{'name': 'read', 'qubit': ['Q6']}
#,{'name': 'read', 'qubit': ['Q5']}
#,{'name': 'read', 'qubit': ['Q4']}
#,{'name': 'read', 'qubit': ['Q6']}
]
    circuit3=[[{'name': 'virtualz', 'qubit': ['Q1'], 'para': {'phase': -1.7894481504126447}},
{'name': 'virtualz', 'qubit': ['Q2'], 'para': {'phase': -0.2166870349553216}},
{'name': 'X90', 'qubit': ['Q1']},
{'name': 'X90', 'qubit': ['Q2']},
{'name': 'virtualz', 'qubit': ['Q1'], 'para': {'phase': 0.8328729079884275}},
{'name': 'virtualz', 'qubit': ['Q2'], 'para': {'phase': 1.069431891997904}},
{'name': 'X90', 'qubit': ['Q1']},
{'name': 'X90', 'qubit': ['Q2']},
{'name': 'virtualz', 'qubit': ['Q1'], 'para': {'phase': -0.8894232287467848}},
{'name': 'virtualz', 'qubit': ['Q2'], 'para': {'phase': -0.32917592244592914}},
{'name': 'barrier', 'qubit': ['Q1', 'Q2']},
{'name': 'read', 'qubit': ['Q1']},
{'name': 'read', 'qubit': ['Q2']}]]
    circuit4=[[
{'name': 'X90', 'qubit': ['Q1']},
{'name': 'barrier', 'qubit': ['Q1', 'Q2', 'Q3']},
{'name': 'X90', 'qubit': ['Q3']},
{'name': 'barrier', 'qubit': ['Q1', 'Q2', 'Q3']},
{'name': 'virtualz', 'qubit': ['Q1'], 'para': {'phase': -0.09550980968778036}},
{'name': 'virtualz', 'qubit': ['Q2'], 'para': {'phase': 0.8778018042682008}},
{'name': 'virtualz', 'qubit': ['Q3'], 'para': {'phase': -0.7294615055005516}},
{'name': 'X90', 'qubit': ['Q1']},
{'name': 'X90', 'qubit': ['Q2']},
{'name': 'X90', 'qubit': ['Q3']},
{'name': 'virtualz', 'qubit': ['Q1'], 'para': {'phase': 1.978452533476716}},
{'name': 'virtualz', 'qubit': ['Q2'], 'para': {'phase': 1.0731066421104303}},
{'name': 'virtualz', 'qubit': ['Q3'], 'para': {'phase': 2.3846378815165825}},
{'name': 'X90', 'qubit': ['Q1']},
{'name': 'X90', 'qubit': ['Q2']},
{'name': 'X90', 'qubit': ['Q3']},
{'name': 'virtualz', 'qubit': ['Q1'], 'para': {'phase': 1.3689471646842026}},
{'name': 'virtualz', 'qubit': ['Q2'], 'para': {'phase': 0.7902890495903311}},
{'name': 'virtualz', 'qubit': ['Q3'], 'para': {'phase': -1.9521414939332586}},
{'name': 'barrier', 'qubit': ['Q1', 'Q2', 'Q3']},
{'name': 'barrier', 'qubit': ['Q1', 'Q2', 'Q3']},
{'name': 'read', 'qubit': ['Q1']},
{'name': 'read', 'qubit': ['Q2']},
{'name': 'read', 'qubit': ['Q3']}]]
    circuit5=[[{'name': 'virtualz', 'qubit': ['Q2'], 'para': {'phase': 1}},
{'name': 'virtualz', 'qubit': ['Q3'], 'para': {'phase': 1}},
{'name': 'X90', 'qubit': ['Q2']},
{'name': 'X90', 'qubit': ['Q3']},
{'name': 'virtualz', 'qubit': ['Q2'], 'para': {'phase': 1}},
{'name': 'virtualz', 'qubit': ['Q3'], 'para': {'phase': 1}},
{'name': 'X90', 'qubit': ['Q2']},
{'name': 'X90', 'qubit': ['Q3']},
{'name': 'virtualz', 'qubit': ['Q2'], 'para': {'phase': 1}},
{'name': 'virtualz', 'qubit': ['Q3'], 'para': {'phase': 1}},
{'name': 'CNOT', 'qubit': ['Q3', 'Q2']},
{'name': 'virtualz', 'qubit': ['Q2'], 'para': {'phase': 1}},
{'name': 'virtualz', 'qubit': ['Q3'], 'para': {'phase': 1}},
{'name': 'X90', 'qubit': ['Q2']},
{'name': 'X90', 'qubit': ['Q3']},
{'name': 'virtualz', 'qubit': ['Q2'], 'para': {'phase': 1}},
{'name': 'virtualz', 'qubit': ['Q3'], 'para': {'phase': 1}},
{'name': 'X90', 'qubit': ['Q2']},
{'name': 'X90', 'qubit': ['Q3']},
{'name': 'virtualz', 'qubit': ['Q2'], 'para': {'phase': 1}},
{'name': 'virtualz', 'qubit': ['Q3'], 'para': {'phase': 1}},
{'name': 'read', 'qubit': ['Q2']},
{'name': 'read', 'qubit': ['Q3']}]]
    qubit='Q2'
    circuit6=[[
        {'name': 'X90', 'qubit': ['Q2']}
#                ,{'name': 'Y-90', 'qubit': ['Q2']}
,{'name': 'virtualz', 'qubit': qubit, 'para': {'phase': -90/180.*numpy.pi}},{'name': 'X90', 'qubit': qubit},{'name': 'virtualz', 'qubit': qubit, 'para': {'phase': 90/180.*numpy.pi}}
,{'name': 'read', 'qubit': ['Q2']},
        ]]
    circuit7=[[
{'name': 'CNOT', 'qubit': ['Q3', 'Q2']},
{'name': 'read', 'qubit': ['Q2']},
        ]]
    qr=c_qubicrun(chassis=chassis,qchip=qchip,wiremap=wiremap)#,tend=4e-6,**ops)
    qr.compile(circuit7)
    result=qr.accbufrun(nsample=20,combineorder=['Q2','Q1'])
#    result=run(chassis=chassis,circcmds=circuit5,qchip=qchip,nsample=20,wiremap=wiremap,tend=4e-6,**ops)
    print(result)


















