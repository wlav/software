from matplotlib import pyplot
import numpy
import sys
sys.path.append('../../')
from qubic.qubic.experiment import c_experiment

if __name__=="__main__":
    experiment=c_experiment(qubitid=None,calirepo='../../../../qchip',debug=False,bypass=True)
    experiment.opts['chassis'].setacqbuf(**dict(
        opsel=2,
        mon_navr=0,
        mon_dt=1,
        mon_slice=0,
        mon_sel0=0
        ,mon_sel1=1
        ,panzoom_reset=1
        ))
    
    n=100
    result=numpy.zeros((n,1024,2))
    experiment.acqbufrun(nbuf=n)
    result=experiment.acqresult
    rm0=result.mean(axis=0)
    print(experiment.acqresult.shape)
    print(result.shape)
    print(rm0.shape)
    fig=pyplot.figure()
    ax=fig.subplots(2,1)
    for i in range(2):
        ax[i].plot(rm0[:,i])
    pyplot.show()

