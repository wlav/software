import numpy
def zxzxz(qubitid,zprep):
    if isinstance(qubitid,str):
        qubitid=[qubitid]
    onequbit=len(qubitid)==1
    seq=[]
    for iz,z in enumerate(zprep):
        if iz!=0:
            seq.extend([{'name': 'X90', 'qubit': qubit} for qubit in qubitid])
        seq.extend([{'name': 'virtualz', 'qubit': qubit, 'para': {'phase': z if onequbit else z[iq]}} for iq,qubit in enumerate(qubitid)])
    return seq

if __name__=="__main__":
    print(zxzxz(['Q1','Q2'],[(1,4),(2,5),(3,6)]))
    print(zxzxz('Q4',[1,2,3]))
    print(zxzxz('Q4',[(1),(2),(3)]))
