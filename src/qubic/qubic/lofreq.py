import numpy
import os
import sys
import bz2
sys.path.append('../..')
from qubic.qubic import commands
from qubic.qcvv.analysis import gmm
from qubic.qubic.compile import c_compile,c_circbasegate#,elememory,circtocmds
from qubic.instrument.ttyclient import c_ttyclient
from matplotlib import pyplot
import pickle
from qubicrun import c_qubicrun

if __name__=="__main__":
    import argparse
    from squbic import c_qchip
    from chassis import c_chassis
    import imp
    import os
    import json
    parser = argparse.ArgumentParser()
    parser.add_argument('-qdrv','--qdrv',help='qubit drive LO frequency (Hz)',dest='qdrvfreq',type=float,nargs='?',default=-1)
    parser.add_argument('-read','--read',help='qubit read  LO frequency (Hz)',dest='readfreq',type=float,nargs='?',default=-1)
    clargs=parser.parse_args()
    print(clargs)
    ops={}
    ops['cfgpath'] = '../../../../qchip/' + os.environ['QUBIC_CHIP_NAME'] + '/'
    ops['ip'] = os.environ['QUBICIP']
    ops['qubitcfgfile'] = ops['cfgpath'] + os.environ['QUBICQUBITCFG']
    ops['wiremapfile']= ops['cfgpath'] + os.environ['QUBICWIREMAP'] + '.py'
    ops['sim']=True
    ops['debug']=True
#    ops.update(**kwargs)
    chassis=c_chassis(ops['ip'],sqinit=False,sim=ops['sim'],debug=ops['debug'])
    with open(ops['qubitcfgfile']) as jfile:
        qubitcfg=json.load(jfile)
    wiremap = imp.load_source("wiremap",ops['wiremapfile'])
    qchip=c_qchip(qubitcfg)
    qr=c_qubicrun(chassis=chassis,qchip=qchip,wiremap=wiremap)#,tend=4e-6,**ops)
    if clargs.qdrvfreq==-1:
        qdrvfreq=None
    elif clargs.qdrvfreq is None:
        qdrvfreq=qr.wiremap.loq
    else:
        qdrvfreq=clargs.qdrvfreq
    if clargs.readfreq==-1:
        readfreq=None
    elif clargs.readfreq is None:
        readfreq=qr.wiremap.lor
    else:
        readfreq=clargs.readfreq
    if qdrvfreq is not None:
        print(qr.qdrvlo(qdrvfreq))
    if readfreq is not None:
        print(qr.readlo(readfreq))
    #print(qr.wiremap.lor)
