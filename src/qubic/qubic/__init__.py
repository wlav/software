"""
QubiC software core

chassis.py: Define attribute and methods for a QubiC eval  chassis
cmdsim1.py: Simulation tool to interpret QubiC commands memory configuration, generate plot demonstrate pulse shape on each DAC channel
commands.py: Define operation on QubiC commands
commit.py: Readout the gateware commit number currently running on a FPGA 
compile.py: Compile QCREF format to the QubiC command and memory configuration
envelop_pulse.py: Define pulse shape to be used to define gate
envset.py: Read envrionment variables generate object of chassis, qchip  etc
experiment.py: Base class for upper level software to cover detail implementation of QubiC also provide basic common processing and plot functions
expression.py: Define a class can do basic expression, used to define variable in sequences
heralding.py: Generate heralding sequence for given qubit or qubits  list
__init__.py: Package init file
init.py: Chassis initialization parameters, this is optional loaded when connection to a chassis
qubicrun.py: Define core QubiC workflow to combine chassis, qchip, and sequence, generate command and memory buffer, read accumulated buffer and acquire buffer 
squbic.py: Define abstrace quantum Gate, gate pulse, quantum  chip
zxzxz.py: Generate QCREF format command for a ZXZXZ circuit
regmap.json: Gateware register map
wavegrp.json: Gateware wave group map
"""
