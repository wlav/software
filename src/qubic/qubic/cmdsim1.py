import json
import numpy
import sys
from qubic.bids.register import c_register
from matplotlib import pyplot
import pkgutil
def signvalue(vin,width=16):
    return vin-2**width if vin>>(width-1) else vin
class c_cmdsim:
    DWIDTH=32
    def __init__(self,regmappath,ndws,maxchan=8,mod=True,debug=False):
        self.regs={}
        self.regindex={}
        self.memw={}
        self.memr={}
        if isinstance(regmappath,dict):
            self.registers=regmappath
        else:
            if regmappath:
                with open(regmappath) as jfile:
                    self.registers=json.load(jfile)
            else:
                self.registers=json.loads(pkgutil.get_data(__package__,'regmap.json'))
        #jsonfile=open(regmappath)
        #self.registers=json.load(jsonfile)
        #jsonfile.close()
        self.vhcmds=[]
        for name in self.registers:
            #print name,self.registers[name]
            self.regs[name]=c_register(**dict({'name':name,'DWIDTH':self.DWIDTH},**self.registers[name]))
            if 'w' in self.regs[name].access:
                for a in self.regs[name].readaddr():
                    self.memw[a]=0
            if 'r' in self.regs[name].access:
                for a in self.regs[name].readaddr():
                    self.memr[a]=0
            #???#
            for addr in self.regs[name].readaddr():
                self.regindex[addr]=self.regs[name]
        for (n,d,w) in ndws:
            #print n,len(d) if isinstance(d,list) else 1,w
            if w:
                self.write(n=n,d=d)
            else:
                self.read(n)
        self.dest={}
        for chan in range(maxchan):
            self.dest[chan]={}
        self.runcmd(debug=debug)
        self.sumdest()
        self.mod=mod
#        self.ploteles()
#        f=open('cmdsimlist.vh','w')
#        f.write('\n'.join(self.vhcmds))
#        f.close()
    def write(self,n,d):
        for (a,da) in self.regs[n].write(d,offset=None):
            #print 'cmdsim1 write',n,hex(a),da,signvalue(((da>>16)&0xffff))+1j*(signvalue(da&0xffff))
            self.memw[a]=da
            self.vhcmds.append("lb_write_task(20'h%x,32'h%x);"%(a,da))
    def read(self,n):
        return [self.memr[a] for a in self.regs[n].readaddr()]
    def readw(self,n,offset=None):
        #print 'cmdsim1 readw',n
        return [self.memw[a] for a in self.regs[n].readaddr(offset)]
        #        adlist=[]
        #        for item in namedatalist:
        #            name=item[0]
        #            data=item[1]
        #            offset=(None if len(item)==2 else item[2])
        #    print n,w
        #mema,memv=mem
        #self.mem=dict(zip(mema,memv))
        #self.cmd=cmds
        #for cmd in sorted(cmds):
        #    self.runcmd(**cmds[cmd])
        #self.sumdest()

    def runcmd(self,debug=False):#,dest,phini,trig_t,element,start,length,freq,dt=1e-9):
        #print 'runcmd'
        period=self.readw('period_dac0')[0]
        #print period

        cmds=[]
        for [a0,a1,a2,a3] in numpy.array(self.regs['command'].readaddr()).reshape((-1,4)):
            cmdi=(self.memw[a0]<<64)+(self.memw[a1]<<32)+self.memw[a2]
            if cmdi:
                cmds.append(cmdi)
            #self.runcmd(**cmds[cmd])
                #print 'cmdline',hex(cmdi)

        for cmd in cmds:
            #cmd=((trig_t&0xffffff)<<72)+((element&0xff)<<64)+((dest&0x3)<<62)+((start&0xfff)<<50)+((length&0xfff)<<38)+((ph_binary&0x3fff)<<24)+(freq_binary&0xffffff)
            trig_t=(cmd>>72)&0xffffff
            element=(cmd>>64)&0xff
            dest=(cmd>>62)&0x3 if ((element&0xf8)==0) else ( 4 if element==8 else ( 5 if element ==9 else ( 6 if element ==10 else 7)))
            #print dest
            start=(cmd>>50)&0xfff
            length=(cmd>>38)&0xfff
            ph_binary=(cmd>>24)&0x3fff
            phini=ph_binary*1.0/2**14*2*numpy.pi
            freq_binary=(cmd&0xffffff)
            freq=freq_binary*1.0/2**24/1e-9
            tcmd=trig_t*4+numpy.arange(4*int(length))
            if debug:
                print('cmdsim1','trig_t',trig_t,'element',element,'dest',dest,'start',start,'length',length,'phini',phini,'freq',freq)
            #print 'cmdsim1 runcmd','start',start,'length',length,'element',element
            iqmem=self.readw('elementmem_%x'%element,offset=(start*4)%4096+(numpy.arange(4*length))) if element in range(11) else numpy.ones(length*4)
            #iq=numpy.array([signvalue((int(v)>>16)&0xffff)+1j*(signvalue(int(v)&0xffff)) for v in iqmem])
            iq=numpy.array([1j*(signvalue((int(v)>>16)&0xffff))+signvalue(int(v)&0xffff) for v in iqmem])
            #if element==8:
            #    print 'debug readout',iq
            iqmod=iq*numpy.exp(1j*(2*numpy.pi*freq*(tcmd-4*trig_t)*1e-9+phini))
        #print 'cmdsim runcmd',start,length
            for it in range(len(tcmd)):
                t=tcmd[it]

                #t=trig_t*4+i
                #iq=self.memw[4*start+i]
                #iqmod=iq*numpy.cos(2*numpy.pi*freq*i*dt+phini)
                if t not in self.dest[dest]:
                    self.dest[dest][t]={}
                    self.dest[dest][t]['iq']=[]
                    self.dest[dest][t]['iqmod']=[]
                self.dest[dest][t]['iq'].append(iq[it])
                self.dest[dest][t]['iqmod'].append(iqmod[it])
    def sumdest(self):
        for dest in self.dest:
            for t in self.dest[dest]:
                self.dest[dest][t]['iqsum']=sum(self.dest[dest][t]['iq'])
                self.dest[dest][t]['iqmodsum']=sum(self.dest[dest][t]['iqmod'])
    def getiq(self,chan=0):
        if self.dest[chan]:
            tv=numpy.array([(t,self.dest[chan][t]['iqsum']) for t in sorted(self.dest[chan])])
        else:
            tv=numpy.array([[0,0],[0,0]])
        #print 'cmdsim getiq tv shape',tv.shape
        with open('cmdsim_dumpwave_%s.npy'%str(chan),'wb') as f:
            numpy.save(f,tv)
        return tv
    def getiqmod(self,chan):
        if self.dest[chan]:
            tv=numpy.array([(t,self.dest[chan][t]['iqmodsum']) for t in sorted(self.dest[chan])])
        else:
            tv=numpy.array([[0,0],[0,0]])
        #print 'cmdsim getiqmod tv shape',tv.shape
        with open('cmdsim_dumpwave_mod_%s.npy'%str(chan),'wb') as f:
            numpy.save(f,tv)
        return tv
    def ploteles(self,plotxend=None,xlim=None):
        period=self.readw('period_dac0')[0]
        if plotxend:
            tend=plotxend
        else:
            tend=period*4*1e-9#*0.1
        if xlim is None:
            xlim=[0,tend/1e-9]
        else:
            xlim=xlim
        pyplot.figure('simulation',figsize=([16,8]))
        for chan in range(7):
            if self.mod:
                tv=self.getiqmod(chan)
            else:
                tv=self.getiq(chan)
            pyplot.subplot(7,2,chan*2+1)
            pyplot.plot(tv[:,0].real,tv[:,1].real,'.-')
            pyplot.xlim(xlim)
            pyplot.subplot(7,2,chan*2+2)
            pyplot.plot(tv[:,0].real,tv[:,1].imag,'.-')
            pyplot.xlim(xlim)
            #print 'chan %d max %8.2f'%(chan,max(abs(tv[:,1])))
        pyplot.show()


if __name__=="__main__":
    mems=(range(0x38000,0x50000),numpy.ones(0x50000-0x38000))
    cmds={0:{'dest': 0, 'phini': -50.2654824574367, 'trig_t': 0.0, 'element': 0, 'start': 57515.0, 'length': 128, 'freq': 320000000.0}
        ,8:{'dest': 0, 'phini': 17.090264035528477, 'trig_t': 2.0, 'element': 1, 'start': 58523.0, 'length': 120, 'freq': -160000000.0}
        }
    sim1=c_cmdsim(mems,cmds)
    tv=sim1.getiq()
    pyplot.plot(tv[:,0],tv[:,1])
    pyplot.show()

