import datetime
import os
import sys
sys.path.append("../..")
from matplotlib import pyplot
from qubic.qubic.expression import c_expression
from qubic.qubic import commands
import numpy
import pprint
def recuname(dl2):
    if isinstance(dl2, list) or isinstance(dl2,tuple):
        val=tuple([recuname(v) for v in dl2])
    elif isinstance(dl2,dict):
        val=tuple(sorted([(k,recuname(v)) for k,v in dl2.items()]))
    else:
        val=dl2
    return val
def r10(val):
    return round(val,10)
def pph(phin,formatstr='%8.3f'):
    if (isinstance(phin,int) or isinstance(phin,float)):
        #rstr=formatstr%((phin*180/numpy.pi)%360)
        rstr=formatstr%(phin)#(phin*180/numpy.pi)%360)
    elif (isinstance(phin,c_expression)):
        rstr=phin
    return rstr
class c_circuitnode():
    def __init__(self):
        self.required=[]
        self.name=None
        self.gatelength=0
        self.qubit=[]
        self.processed=False
        self.timed=False
        self.tstart=0
        self.tend=0
        self.tlength=0
        self.phase=0
        self.cnt=None
        self.cmded=False
        self.printed=False
        self.pulsed=False
        self.fullname=None
        self.basepatched=False
        self.opcmded=False
        self.patchgroupt0ed=False
        self.pulses=[]
#    def opcmd(self,opcmdlist,wiremap):
#        self.opcmded=True
#        for g in self.required:
#            if g.opcmded:
#                pass
#            else:
#                g.opcmd(opcmdlist,wiremap)
    def patchgroupt0(self,reset=False):
        self.patchgroupt0ed=True
        for g in self.required:
            if g.patchgroupt0ed:
                pass
            else:
                g.patchgroupt0()

    def patchbase(self):
        self.basepatched=True
        for g in self.required:
            if g.basepatched:
                pass
            else:
                g.patchbase()
    def print(self):
        self.printed=True
        for g in self.required:
            if g.printed:
                pass
            else:
                g.print()
        print('debug print ',self.name,r10(self.tlength),r10(self.tend),r10(self.tstart),[p.gate.circuit for p in self.pulses])
#        print('circuitnode print',self.name,self.qubit,self.cnt,r10(self.tlength))
    def process(self):
        self.processed=True
        for g in self.required:
            if g.processed:
                pass
            else:
                g.process()
    def maptobasepulse(self,circbasegate,allbasepulse,pulselist,wiremap):
        self.pulsed=True
        for g in self.required:
            if g.pulsed:
                pass
            else:
                #print('debug map',g,g.name)
                g.maptobasepulse(circbasegate,allbasepulse,pulselist,wiremap)

    def timegate(self,reset=False):
        self.timed=False if reset else True
        for g in self.required:
            if (g.timed and reset ) or (not g.timed and not reset):
                    g.timegate(reset=reset)
        if not reset:
            if self.required:
                self.tstart=max([g.tend for g in self.required])
            else:
                #print('gate start',self.tstart,[p['t0'] for p in self.pulses] if hasattr(self,'pulses') else '')
                pass
            for p in self.pulses:
                p.tstart=self.tstart+p.t0
                p.tend=p.tstart+p.tlength
            if self.pulses:
                self.tlength=max([p.tend for p in self.pulses])-self.tstart#-min([p.tstart for p in self.pulses])
#                print('update gate length',self.name,r10(self.tlength),self.pulses)
            self.tend=self.tstart+self.tlength
#            print('gate start/end',r10(self.tstart),r10(self.tend),[(r10(p.tstart) if p.tstart else None,r10(p.tend) if p.tend else None) for p in self.pulses] if self.pulses else 0)
    def calcgateph(self,gatephase,reset=False):
        self.cmded=False if reset else True
        for g in self.required:
            if (g.cmded and reset ) or (not g.cmded and not reset) :
                g.calcgateph(gatephase,reset)

class c_circgate():
    def __init__(self,name,refgate,circuit,allbasepulse=None):
        self.name=name
        self.refgate=refgate
        self.tlength=refgate.tlength
        self.pulses=[]
        self.circuit=circuit
        self.pulsesfromref(allbasepulse)
    def pulsesfromref(self,allbasepulse):
        for p in self.refgate.pulses:
            plocal=c_circpulse(refpulse=p,gate=self)
            allbasepulse[allbasepulse.index(p)].circpulse.append(plocal)
            self.pulses.append(plocal)
#    def calctlength(self):
#        self.tlength=max([p.t0+p.tlength for p in self.pulses])-min([p.t0 for p in self.pulses])
class c_circbasegate(c_circgate):
    def __init__(self,name,refgate,allbasepulse):
        c_circgate.__init__(self,name=name,refgate=refgate,circuit=None,allbasepulse=allbasepulse)
    def pulsesfromref(self,allbasepulse):
        #print('debug pulse in child')
        for p in self.refgate.pulses:
            #if p.dest is not None and p.tlength>0:
            plocal=c_circbasepulse(refpulse=p,gate=self)
            if plocal not in allbasepulse:
                allbasepulse.append(plocal)
            self.pulses.append(plocal)
            #elif p.tlength==0:
            #    print('Warning: zero length pulse in gate: ',p.gate.name)
            #elif p.dest is None:
            #    print(p.gate.name)
            #else:
            #    print('what?')


class c_circuitgate(c_circuitnode,c_circgate):
    def __init__(self,basename,qubit,circuit,**kwargs):#qubitphasedict,freqtype='freq',localgate={}):
        c_circuitnode.__init__(self)
        self.basename='%s%s'%(''.join(qubit),basename)
        self.qubit=qubit
        self.fullname=self.qubicfullname(**kwargs)
        self.circuit=circuit
        self.cmded=False
        cnt=0
        self.sametimegate=[]
        self.patchgroup=[self]
    def timegate(self,reset=False):
        c_circuitnode.timegate(self,reset=reset)
    def patchgroupt0(self):
        c_circuitnode.patchgroupt0(self)
#        print('debug patchgroupt0',self.patchgroup,[(gate.name,gate.pulses) for gate in self.patchgroup])
        pulseinpatchgroup=[p for gate in self.patchgroup for p in gate.pulses]
#        print('debug patchgroupt0,',pulseinpatchgroup)
        mint0=min([p.t0 for p in pulseinpatchgroup]) if len(pulseinpatchgroup)>0 else 0
#        print('patchgroup',self.patchgroup)
#        print('patchgroup pulse',[p for gate in self.patchgroup for p in gate.pulses])
        if mint0<0:
            for g in self.patchgroup:
                for p in g.pulses:
                    p.t0-=mint0
                    p.mint0=mint0
#                    print('patchgroupt0',p,'mint0',r10(mint0),'t0',r10(p.t0))
#        self.calctlength()

    def qubicfullname(self,**kwargs):
        self.memmodi=[{k:v for k,v in kw.items() if k in ['env','amp','twidth']} for kw in kwargs['modi']]
        if self.memmodi:
            if max([len(e) for e in self.memmodi])==0:
                self.memmodi=None

        self.cmdmodi=[{k:v for k,v in kw.items() if k in ['dest','pcarrier','t0','fcarrier']} for kw in kwargs['modi']]
        if self.memmodi:
            fullname=(self.basename,recuname(self.memmodi))#tuple([tuple(sorted(m.items())) for m in self.memmodi]))
        else:
            fullname=self.basename
        return fullname

    def maptobasepulse(self,circbasegate,allbasepulse,pulselist,wiremap):
        c_circuitnode.maptobasepulse(self,circbasegate,allbasepulse,pulselist,wiremap)
        c_circgate.__init__(self,name=self.fullname,refgate=circbasegate[self.fullname],allbasepulse=allbasepulse,circuit=self.circuit)
        for index,moddict in enumerate(self.cmdmodi):
            for k,v in moddict.items():
                if hasattr(self.pulses[index],k):
                    setattr(self.pulses[index],k,v)
                else:
                    print('debug localpulaes',index,moddict)
                    exit('%s not in pulse'%k)
        for p in self.pulses:
            if p.dest is not None:
                p.lo=wiremap.lofreq[p.dest]
                p.chassisif=p.fcarrier-p.lo
                if hasattr(wiremap,'switchmap'):
                    p.switch=wiremap.switchmap[p.dest]
                else:
                    p.switch=None
                p.dest=wiremap.destdict[p.dest]
        pulselist.extend([p for p in self.pulses if p.dest is not None and p.tlength>0])


    def qubitphase(self,qubitfreqname):
        qubit,freqname=qubitfreqname.split('.')
        if (qubit,freqname) in self.phase:
            value=self.phase[(qubit,freqname)]
        else:
            print('Error:qubit %s %s not in phyqubits'%(qubit,freqname))
            exit(1)
        return (qubit,freqname),value
#    def opcmd(self,opcmdlist,wiremap):
#        c_circuitnode.opcmd(self,opcmdlist,wiremap)
#        for pulse in self.pulses:
#            #print('debug opcmd',pulse.gate.name,pulse)
##            print('debug opcmd',pulse.trig_t,pulse.dest,'elem',pulse.refpulse.element,'start',pulse.start,'length',pulse.length,'npatch',pulse.npatch,'npatchingate',pulse.npatchingate)
#
#            opcmdlist.append(dict(trig_t=r10(pulse.trig_t),name=pulse.gate.name,dest=wiremap.destdict[pulse.dest],elem=pulse.element,patch=pulse.npatch,refpatch=pulse.refpulse.npatch,patchingate=pulse.npatchingate,refpatchingate=pulse.refpulse.npatchingate,lo=wiremap.lofreq[pulse.dest]))

    def patchbase(self):
        c_circuitnode.patchbase(self)
        for pulse in self.pulses:
            #print('pulse.npatch',pulse.npatch)
            #print('pulse.refpulse.npatch',pulse.refpulse.npatch)
            if pulse.npatch>pulse.refpulse.npatch:
                pulse.refpulse.npatch=pulse.npatch
                pulse.refpulse.tpatch=pulse.tpatch
                pulse.refpulse.tlength=pulse.tlength

    def calcgateph(self,gatephase,reset=False):
        #        print('debug calcgateph',self.phase)
        c_circuitnode.calcgateph(self,gatephase,reset)
        if not reset:
            for pulse in self.pulses:
                qubit,freqname=pulse.fcarriername.split('.')
                algophase=self.phase
                if pulse.fcarriername not in gatephase:
                    gatephase[pulse.fcarriername]=0
                gatephase[pulse.fcarriername]+=pulse.pcarrier
                pulse.update(dict(phrad=(algophase+gatephase[pulse.fcarriername])%(2*numpy.pi)))

class c_phase(c_circuitnode):
    def __init__(self,qubit,phase):
        c_circuitnode.__init__(self)
        self.gate='phase'
        self.name='phase'
        self.qubit=qubit
        self.phase=phase
        self.cmded=False
        self.tlength=0
        cnt=0
    def calcgateph(self,gatephase,reset=False):
        if not reset:
            super().calcgateph(gatephase,reset=False)
            k='%s.%s'%(self.qubit[0],'freq')
            if k not in gatephase:
                gatephase[k]=0
            gatephase[k]+=self.phase


class c_delay(c_circuitnode):
    def __init__(self,qubit,delay):
        c_circuitnode.__init__(self)
        self.gate='delay'
        self.name='delay'
        self.qubit=qubit
        self.delay=delay
        self.cmded=False
        self.tlength=delay
        cnt=0
class c_barrier(c_circuitnode):
    def __init__(self,qubit):
        c_circuitnode.__init__(self)
        self.gate='barrier'
        self.name='barrier'
        self.qubit=qubit
        self.cmded=False
        cnt=0
class c_circpulse():
    def __init__(self,refpulse,gate):
        self.refpulse=refpulse
        self.gate=gate
#        if not hasattr(refpulse,'circpulse'):
#            refpulse.circpulse=[]
#        refpulse.circpulse.append(self)
        self.t0=refpulse.t0
        self.mint0=0
        self.tlength=refpulse.tlength
        self.npatchingate=refpulse.npatch
        self.tpatchingate=refpulse.tpatch
        self.dest=refpulse.dest
        self.pcarrier=refpulse.pcarrier
        self.fcarrier=refpulse.fcarrier
        self.fcarriername=refpulse.fcarriername
        self.trig_t=None
        self.npatch=0
        self.tpatch=0
        self.nlength=None
        self.phrad=None
        self.start=None
        #self.length=None
        self.cond=None
        self.tend=None
        self.element=None
        self.switch=None
#        self.patchgroup=[self]

    def pulseval(self,**kwarg):
        return self.refpulse.pulseval(**kwarg)

    def nvall(self,dt):
        tv=self.pulseval(dt=dt,flo=0,mod=False)
        self.nv=numpy.zeros(self.nlength,dtype='complex64')
        nv=tv.nvval(dt)
#        print('nvall start debug',self.tpatch,self.nlength,self.length,self.refpulse.twidth)
        startindex=len(self.nv)-len(nv[0])#int(round(self.tpatch/dt,10))
#        print('debugnvall','self.nlength',self.nlength,len(nv[0]),startindex)
        #print(startindex,self.tpatch/dt,self.tlength,nv[1],nv[0])
        if len(nv[0])>0:
            self.nv[nv[0]+startindex]=nv[1]
#        print('debugnvall',self.nv[0:32])
        #print('patch',self.npatch,self.nv[0:10])
    def calcnlengthtend(self,dt):
        self.nlength=int(round(self.tlength/dt,10))
        #print('debug calcnlengthtend',self.gate.name,self.nlength)
        #self.tend=self.tstart+self.tlength
    def update(self,dictin):
        for k,v in dictin.items():
            if hasattr(self,k):
                setattr(self,k,v)
            else:
                print('c_circpulse does not have attr of %s'%(str(k)))
    def __ne__(self,other):
        return not self.__eq__(other)
    def __eq__(self,other):
        refpulseeq=False
        desteq=False
        if hasattr(self,'refpulse'):
            if hasattr(other,'refpulse'):
                if self.refpulse==other.refpulse:
                    refpulseeq=True
        if hasattr(self,'dest'):
            if hasattr(other,'dest'):
                if self.dest==other.dest:
                    desteq=True
        return all([refpulseeq,desteq])

class c_circbasepulse(c_circpulse):
    def __init__(self,refpulse,gate):
        c_circpulse.__init__(self,refpulse=refpulse,gate=gate)
        self.start={}
        self.circpulse=[]
        pass

class c_compile():
    def __init__(self,circuitgates,phmeas=False,patchstep=8e-9,dt=1e-9,**kwargs):
        #print(circuitgates)
        self.phmeas=phmeas
        localgatename=['X','X90','Y-90','CNOT','CR','Z','read','rabi','rabi_ef','X90_ef']
        if 'localgatename' in kwargs:
            localgatename.extend(kwargs['localgatename'])
        self.circuittree,self.qubits,self.chipgatedictsmodi=self.buildcircuittree(circuitgates,localgatename)
    #    print('chipgatedicts:',self.chipgatedicts)
        # generate locate gate by qchip later #
        self.timequbitgate={}
        self.dt=dt
        self.patchstep=patchstep
        self.pulselist=[]
#    def opcmd(self,wiremap):
#        opcmdlist=[]
#        self.circuittree.opcmd(opcmdlist,wiremap)
#        self.opcmdlist=sorted(opcmdlist,key=lambda x:x['trig_t'])
#        return self.opcmdlist
    def maptobasepulse(self,circbasegate,allbasepulse,wiremap):
        self.circuittree.maptobasepulse(circbasegate,allbasepulse,self.pulselist,wiremap)
    def buildcircuittree(self,circuitgates,localgatename):#=['X','X90','Y-90','CNOT','CR','Z','read','rabi','rabi_ef','X90_ef']):
        circuittree=[]
        circuitgatedict={}
        chipgatedictsmodi={}
        gatecnt=0
        for gate in circuitgates:
            qubit=gate['qubit']
            if isinstance(qubit,str):
                qubit=[qubit]
            elif isinstance(qubit,list) or isinstance(qubit,tuple):
                pass
            else:
                print(qubit)
                exit('qubit should be string for single qubit gate or list or tuple for multiple qubit gate')
            gatename=gate['name']
            if gatename=='barrier':
                nodegate=c_barrier(qubit=qubit)
            elif gatename=='phase' or gatename =='virtualz':
                nodegate=c_phase(qubit=qubit,**gate['para'])
            elif gatename=='delay':
                nodegate=c_delay(qubit=qubit,**gate['para'])
#            elif gatename=='X90' or gatename=='CNOT' or gatename=='read':
            elif gatename in localgatename:
                nodegate=c_circuitgate(basename=gatename,qubit=qubit,circuit=self,modi=gate['modi'] if 'modi' in gate else {})
            else:
                exit('compile: Gate name %s is not supported yet'%gatename)
            circuittree.append(nodegate)
            for q in qubit:
                if q not in circuitgatedict:
                    circuitgatedict[q]=[]
                else:
                    if circuitgatedict[q]:
                        nodegate.required.append(circuitgatedict[q][-1])
                circuitgatedict[q].append(nodegate)
            nodegate.cnt=gatecnt
            gatecnt+=1
            if nodegate:
                if nodegate.fullname is not None:
                    if nodegate.fullname not in chipgatedictsmodi:
                        chipgatedictsmodi[nodegate.fullname]=nodegate.memmodi

        entrypoint=c_circuitnode()
        entrypoint.required=[circuitgatedict[q][-1] for q in circuitgatedict if circuitgatedict[q]]
        qubits=list(circuitgatedict.keys())
        return entrypoint,qubits,chipgatedictsmodi

    def sametimepulse(self):
        tpulse=numpy.array([p.tstart for p in self.pulselist])
        difftlist=numpy.diff(tpulse)
        sametimes=[]
#        for i,p in enumerate(self.pulselist):
#            print('debug sametimepulse',i,'tpulse',r10(tpulse[i]),'diff',r10(difftlist[i-1]) if i>1 else None,p.gate.name)
        for index,pulse in enumerate(self.pulselist):
            if index==0:
                freepulse=pulse
                lastisfree=True
                sametime=[]
            else:
                if (difftlist[index-1]<=self.patchstep-self.dt/2.0):
                    sametime.append(pulse)
                    #print('s1',sametime,'p',pulse,'f',freepulse)
                    if lastisfree:
                        sametime.append(freepulse)
                        #print('s2',sametime,'p',pulse,'f',freepulse)
                    lastisfree=False
                else:
                    lastisfree=True
                    if len(sametime)>0:
                        sametimes.append(sametime)
                        #print('s3',sametime)
                    freepulse=pulse
                    sametime=[]
                    #print('s4',sametime)
            if index==len(self.pulselist)-1:
                if not lastisfree and len(sametime)>0:
                    sametimes.append(sametime)
                    #print('s5',sametime)

        for sametime in sametimes:
            npulse=len(sametime)
            for ipulse,pulse in enumerate(sametime):
                pulse.tpatch+=ipulse*self.patchstep
                pulse.t0=pulse.t0-pulse.tpatch
#                print('tpatch','tpatch',r10(pulse.tpatch),'t0',r10(pulse.t0),pulse)
                pulse.tlength=pulse.tlength+pulse.tpatch
                #print('debug ingate',pulse.tlength,pulse.npatchingate,pulse.tpatchingate)
#                pulse.patchgroup.extend(sametime)
        for sametime in sametimes:
#            print('sametime',sametime)
            gates=list(set([p.gate for p in sametime]))# for p0 in pulse.gate.pulses for p in p0.patchgroup]))
#            print('sametime gate',gates)
            #print('pulses',sametime)
            for pulse in sametime:
                pulse.gate.patchgroup.extend(gates)
                pulse.gate.patchgroup=list(set(pulse.gate.patchgroup))
                #print('gate patchgroup',pulse.gate.name,[g.name for g in pulse.gate.patchgroup])
        #print('sametimepulse out',[[(p.gate.name,p.t0) for p in ps] for ps in sametimes])
        self.circuittree.patchgroupt0()
#        print('sametimes',sametimes)
        return sametimes

    def circtime(self):
        gatephase={}
        self.circuittree.timegate()
        self.circuittree.calcgateph(gatephase)
#        print('debug circtime',gatephase)
        self.pulselist=sorted(self.pulselist,key=lambda x:x.tstart)
#        for p in self.pulselist:
#            print('debug circtime',r10(p.tstart),p.gate.name,p)
    def qubictime(self):
        analyspass=0
        sametimepulse=[]
        while ((analyspass==0 or len(sametimepulse)!=0) and analyspass<10):
            sametimepulse=self.sametimepulse()
#            print('debug sametimepulse',analyspass)
#            for pl in sametimepulse:
#                print()
#                for p in pl:
#                    print('debug qubictime',p,'t0',r10(p.t0),'tstart',r10(p.tstart),'tpatch',r10(p.tpatch),'tlength',r10(p.tlength),p.gate.name)
#            for p in self.pulselist:
#                print('debug qubictime pulselist',p,'t0',r10(p.t0),'tstart',r10(p.tstart),'tpatch',r10(p.tpatch),'tlength',r10(p.tlength),p.gate.name)

            if sametimepulse:
                self.circuittree.timegate(reset=True)
                self.circuittree.timegate()
                self.pulselist=sorted(self.pulselist,key=lambda x:x.tstart)
            analyspass+=1
#            print('while',len(sametimepulse),analyspass)
        if (analyspass>=10):
            exit('Failed patching pulse')
#        else:
#            print('Info: qubictime analyspass ',analyspass)
    def qubiccmd(self):
        self.pulselist=self.sortpulse(self.pulselist)
        self.pulselist,self.lastpoint=self.timephase(self.pulselist,phmeas=self.phmeas)
        def dictcmdfun(pulsefun=self.pulsefun,**kwargs):
            cmdfun=[]
            for pulse in self.pulselist:
                #                if pulse.length>0:
                #if 1:
                dictcmd=dict(trig_t=round(pulse.tstart,10),element=pulse.element,dest=pulse.dest,start=pulse.start,length=pulse.nlength,phrad=pulse.phrad,freq=pulse.chassisif,switch=pulse.switch)
#                print('debug qubiccmd',dictcmd)
                cmdfun.append(pulsefun(**dictcmd)(**kwargs))
                #else:
                #    print('zero length pulse, no command',pulse.tlength)
#,tlength=pulse.tlength
#,npatch=pulse.npatch
#,gate=pulse.gate.fullname
#,tgate=pulse.gate.tlength
#,pulse=pulse
            return cmdfun
        return dictcmdfun,self.lastpoint#cmdlist

    def pulsefun(self,**defkwargs):
        def _function(**callkwargs):
            #print('debug callkwargs',callkwargs)
            dictout={}
            for k,v in defkwargs.items():
                if isinstance(v,c_expression):
                    #    print('callkwargs',callkwargs)
                    #print('v',v)
                    val=v(**callkwargs)
                    dictout[k]=val
                else:
                    dictout[k]=v
            return dictout
        return _function
    def timephase(self,sortpulselist,phmeas=False):
        lastpoint=0
        for pulse in sortpulselist:
            if ('read' in pulse.fcarriername) & ~phmeas:
                #                print('timephase,debug pulse patch',pulse['patch'])
                pulse.phrad=pulse.pcarrier-(2*numpy.pi*pulse.chassisif*pulse.tpatch)%(2*numpy.pi)
                #print('read pulse, only pcarrier','pcarrier',pph(pulse['pcarrier']),'']))
            else:
                #print('regular gate before, timephase',pph(pulse.phrad),'f',pulse.fcarrier,'t',pulse.tstart)
                trigtphase=(2*numpy.pi*(pulse.chassisif)*(pulse.tstart+pulse.mint0))%(2*numpy.pi)
                pulse.phrad=pulse.phrad+trigtphase
                #print('regular gate after , timephase',pph(pulse.phrad),'= before +',pph(trigtphase))
            lastpoint=max([lastpoint,pulse.tstart+pulse.tlength])#*self.dt])
        return sortpulselist,lastpoint
    def sortpulse(self,sortpulselist):
        return sorted(sortpulselist,key=lambda i:i.tstart)


if __name__ == '__main__':
    from qubic.qubic.squbic import c_qchip
    from qubic.qubic.chassis import c_chassis
    import imp
    import os
    import json
    ops={}
    ops['cfgpath'] = '../../../../../submodules/qchip/' + os.environ['QUBIC_CHIP_NAME'] + '/'
    ops['ip'] = os.environ['QUBICIP']
    ops['qubitcfgfile'] = ops['cfgpath'] + os.environ['QUBICQUBITCFG']
    ops['wiremapfile']= ops['cfgpath'] + os.environ['QUBICWIREMAP'] + '.py'
    ops['sim']=False
    ops['debug']=True
#    ops.update(**kwargs)
    chassis=c_chassis(ops['ip'],sqinit=False,sim=ops['sim'])
    with open(ops['qubitcfgfile']) as jfile:
        qubitcfg=json.load(jfile)
    wiremap = imp.load_source("wiremap",ops['wiremapfile'])
    qchip=c_qchip(qubitcfg)
    circuit1=[{'name': 'CNOT', 'qubit': ['Q6', 'Q5']}
,{'name': 'delay', 'qubit': ['Q4', 'Q5', 'Q6', 'Q7'], 'para': {'delay': 3.e-5}}
,{'name': 'barrier', 'qubit': ['Q4', 'Q5', 'Q6', 'Q7']}
,{'name': 'read', 'qubit': ['Q5']}
,{'name': 'read', 'qubit': ['Q6']}
,{'name': 'read', 'qubit': ['Q6'],'modi' : [{'fcarrier':3e9},{'twidth':1e-6}]}
,{'name': 'read', 'qubit': ['Q5']}
,{'name': 'read', 'qubit': ['Q6'],'modi' : [{'fcarrier':3e9},{'twidth':13e-6}]}
,{'name': 'barrier', 'qubit': ['Q4', 'Q5', 'Q6', 'Q7']}
,{'name': 'virtualz', 'qubit': ['Q5'], 'para': {'phase': -2}}
,{'name': 'X90', 'qubit': ['Q5'],'modi' : [{'twidth':48e-9,'amp':0.1234,'phase':-5}]}
,{'name': 'X90', 'qubit': ['Q5'],'modi' : [{'amp':0.1234,'twidth':240e-9,'phase':-2}]}
,{'name': 'X90', 'qubit': ['Q6']}
,{'name': 'CNOT', 'qubit': ['Q6', 'Q5'],'modi':[{},{'env':[{'paradict':{'alpha':0.2}}]},{},{}]}
,{'name': 'X90', 'qubit': ['Q5']}
,{'name': 'X90', 'qubit': ['Q5']}
,{'name': 'X90', 'qubit': ['Q6']}
,{'name': 'barrier', 'qubit': ['Q4', 'Q5', 'Q6', 'Q7']}
,{'name': 'read', 'qubit': ['Q5']}
]
    '''    circuit2=[
{'name': 'X90', 'qubit': ['Q6']}
{'name': 'X90', 'qubit': ['Q5']}
,{'name': 'rabi', 'qubit': ['Q5'],'modi' : [{'twidth':48e-9,'amp':0.1234,'phase':-5}]}
,{'name': 'rabi', 'qubit': ['Q5'],'modi' : [{'twidth':32e-9,'amp':0.1234,'phase':-5}]}
,{'name': 'rabi', 'qubit': ['Q5'],'modi' : [{'twidth':28e-9,'amp':0.1234,'phase':-5}]}
,{'name': 'read', 'qubit': ['Q5']}
,{'name': 'barrier', 'qubit': ['Q4', 'Q5', 'Q6', 'Q7']}]
    circuit1=[
{'name': 'X90', 'qubit': ['Q5']}#,'modi' : [{'amp':0.1234,'twidth':40e-9,'phase':-2}]}
,{'name': 'X90', 'qubit': ['Q6']}
]'''
    circuit2=[
            #{'name': 'delay', 'qubit': ['Q4', 'Q5', 'Q6', 'Q7'], 'para': {'delay': 2.9999999999999997e-05}}
            #,{'name': 'barrier', 'qubit': ['Q4', 'Q5', 'Q6', 'Q7']}
{'name': 'read', 'qubit': ['Q5']}
,{'name': 'read', 'qubit': ['Q4']}
,{'name': 'read', 'qubit': ['Q6']}
,{'name': 'read', 'qubit': ['Q5']}
#,{'name': 'read', 'qubit': ['Q4']}
,{'name': 'read', 'qubit': ['Q6']}
,{'name': 'barrier', 'qubit': ['Q5', 'Q6','Q4']}
,{'name': 'read', 'qubit': ['Q5']}
,{'name': 'read', 'qubit': ['Q4']}
,{'name': 'read', 'qubit': ['Q6']}
#,{'name': 'read', 'qubit': ['Q5']}
#,{'name': 'read', 'qubit': ['Q4']}
#,{'name': 'read', 'qubit': ['Q6']}
]
    circuit3=[{'name': 'CNOT', 'qubit': ['Q6', 'Q5']}
            ,{'name': 'CNOT', 'qubit': ['Q6', 'Q5']}
,{'name': 'X90', 'qubit': ['Q5']}
,{'name': 'X90', 'qubit': ['Q6']}
            ]
    #import cir128
    #circuit4=cir128.circuit
    y=c_expression('y','y')
    rabi=[]
    for twidth in numpy.arange(0,320e-9,80e-9):
        rt=[]
        rt.append({'name': 'virtualz', 'qubit': ['Q5'], 'para': {'phase': -20}})
        rt.append({'name': 'X90', 'qubit': ['Q5']})
        rt.append({'name': 'X90', 'qubit': ['Q5']})
        rt.append({'name': 'rabi', 'qubit': ['Q5'],'modi' : [{'twidth':twidth,'fcarrier':y}]})
        rt.append({'name': 'rabi', 'qubit': ['Q5'],'modi' : [{'twidth':twidth,'fcarrier':y}]})
        rt.append({'name': 'read', 'qubit': ['Q5'],'modi' : [{},{}]})
        rabi.append(rt)
    chipgatedictsmodi={}
    circseqs=[]
    chipgatedict={}
    circbasegate={}
    allbasepulse=[]
    #for circgates in [circuit1,circuit2]:
    for index,circgates in enumerate([circuit3]):#[circuit3,rabi]):
        #        print('s1',index)
        circseq=c_compile(circuitgates=circgates)
        chipgatedictsmodi.update(circseq.chipgatedictsmodi)
        circseqs.append(circseq)
    for gate,para in chipgatedictsmodi.items():
        if para:
            chipgatedict[gate]=qchip.gates[gate[0]].modify(para)
        else:
            chipgatedict[gate]=qchip.gates[gate].modify(None)
        circbasegate[gate]=c_circbasegate(name=gate,refgate=chipgatedict[gate],allbasepulse=allbasepulse)
    #print('total # of localgate:',len(circbasegate),[n for n,g in circbasegate.items()])
    #print('tlength# of localgate:',len(circbasegate),[g.tlength for n,g in circbasegate.items()])
    #for name,gate in circbasegate.items():
    #    print('circbasegate',name,gate.pulses)
#    print('allbasepulse',allbasepulse)
    #for k,v in circbasegate.items():
    #    print(k,v.tstart,v.tlength,[(p.t0,p.twidth,p.fcarrier) for p in v.pulses])
    for index,circseq in enumerate(circseqs):
        #        print('s2',index)
        circseq.maptobasepulse(circbasegate,allbasepulse,wiremap=wiremap)
        circseq.circtime()
#        for i,p in enumerate(circseq.pulselist):
#            print(i,p,'tstart',round(p.tstart,10),'tend',round(p.tend,10),'tlength',round(p.tlength,10),'gate',p.gate.name,'circuit',p.gate.circuit,[p1.gate.circuit==p.gate.circuit for p1 in p.refpulse.circpulse])

    #for index,circseq in enumerate(circseqs):
#        print('s3',index)
        circseq.qubictime()
#        circseq.circuittree.patchbase()
#    for p in allbasepulse:
#        print('debug base',p.name,p.circpulse)
    elememory(qchip=qchip,chassis=chassis,allbasepulse=allbasepulse,wiremap=wiremap)

#    for index,circseq in enumerate(circseqs):
#        for pulse in circseq.pulselist:
#            pulse.calcnlengthtend(chassis.dt)
    circcmdlist=[]
    for index,circseq in enumerate(circseqs):
        cmdlistfun,lastpoint=circseq.qubiccmd()
        for yf in numpy.arange(142.857e6,142.857e6+1):

            circcmdlist.append((cmdlistfun(y=yf),lastpoint))
#        for cmd in cmdlistfun(y=10):
#            print(cmd)
#        print('circ index',index,'lastpoint',r10(lastpoint))
#    for p in allbasepulse:
#        print('all base pulse',p.gate.name)#,end='')
    chassis.memwrite(clear=False)
    hrldcmdlist=None
    circcmds=commands.circuitgaps(circcmdlist=circcmdlist,hrldcmdlist=hrldcmdlist,delaybetweenelement=600e-6)#delaybetweenelement=ops['delaybetweenelement'],delayafterheralding=ops['delayafterheralding'])
    cmdlists=commands.cmdchop(circcmds=circcmds,mark0=True)
    accval={}#numpy.empty((nsample,0))
    totalelemcmdcnt={}
    nsample=20
    for cmdlist,period in cmdlists:
        elemcmdcnt,readelemcnt=commands.cmdelemcnt(cmdlist,chassis.dloelem)
        totalelemcmdcnt={k:elemcmdcnt[k]+(totalelemcmdcnt[k] if k in totalelemcmdcnt else 0) for k in elemcmdcnt}
#        cmdh=[cmdlist[0]]
#        cmdh.extend(cmdlist[2:])
        chassis.cmdwrite(cmdlist,clear=True,write=True,debug=ops['debug'])
        print('setperiod %8.3e second'%period)
        chassis.setperiod(period)
        chassis.startacq()
#        print('elemcmdcnt',elemcmdcnt)
#    accval=chassis.accbuf(elemcmdcnt,nget)
        if ops['sim']:
            print('simulating')
            #chassis.sim1(mod=True,tend=20e-6)
            chassis.sim1(mod=False)#,tend=0.3e-6)#,tend=0.14e-6)#,tend=period)
        print('readelemcnt',readelemcnt)
        newval=chassis.accbufdata(readelemcnt,nsample)#,square=ops['square'],npreread=ops['npreread'])
        for v in newval:
            if v not in accval:
                accval[v]=newval[v]
            else:
                accval[v]=numpy.append(accval[v],newval[v],1)
#
    #    newval=chassis.accbufdata(readelemcnt,nsample,square=ops['square'],npreread=ops['npreread'])
    #chassis.cmdwrite(clear=False)
    #chassis.sim1()
    pyplot.show()
    #for elem in chassis.elements:
    #    print(elem.elemindex,elem.lastpoint)
    exit('end of compile')


