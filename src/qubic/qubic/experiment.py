import numpy
import sys
sys.path.append('../..')
from qubic.qubic import envset,qubicrun,heralding
from qubic.qcvv.plot import plot

class c_experiment():
    def __init__(self,qubitid,calirepo,chassis=None,wiremap=None,qchip=None,gmixs='default',**kwargs):
        if chassis is None or wiremap is None or qchip is None or gmixs=='default':
            #            _chassis,_wiremap,_qchip,_gmixs
            envsetopts=envset.envset(calirepo=calirepo,**kwargs)
        self.opts=dict(chassis=None,wiremap=None,qchip=None,gmixs='default',debug=0,plot=False)
        #self.opts.update(kwargs)
        self.opts.update(envsetopts)
        self.opts['qubitid']=[qubitid] if isinstance(qubitid,str) else qubitid if (isinstance(qubitid,list) or isinstance(qubitid,tuple)) else None
        
        self.opts['chassis'] = envsetopts['chassis'] if chassis  is None else chassis
        self.opts['wiremap'] = envsetopts['wiremap'] if wiremap  is None else wiremap
        self.opts['qchip']   = envsetopts['qchip']   if qchip    is None else qchip
        self.opts['gmixs']= envsetopts['gmixs'] if gmixs == 'default' else gmixs
        self.opts['calipath']= envsetopts['calipath']
#        print('debug gmixs',kwargs)
        if qubitid:
            self.opts['heraldcmds']=heralding.heralding(qubits=qubitid)

        self.qubicrun=qubicrun.c_qubicrun(**self.opts)
        self.result={}
        self.compiled=False
    def seqs(self,seqs):
        self.opts['seqs']=seqs
    def debug(self,level,*args,**kwargs):
        if self.opts['debug']>=level:
            print(*args,**kwargs)
    def accbufrun(self,nsample,includegmm=True,**kwargs):
        #        if not self.compiled:
        #    self.compile()
        self.opts.update(kwargs)
        self.opts.update(dict(nsample=nsample))
        self.accval=self.qubicrun.accbufrun(**self.opts)
        if includegmm:
            if 'accval' not in kwargs:
                kwargs.update(dict(accval=self.accval))
            result=self.gmmcount(**kwargs)
        else:
            result=self.accval
            gmmresult={}
            gmmresult['accval']=self.accval
            self.accresult=gmmresult
        return result
#        self.accresult=self.gmmcount()
#        return self.accresult
    def gmmcount(self,accval=None,**kwargs):
        #print('debug gmixs',self.opts['gmixs'],kwargs)
        self.accresult=self.qubicrun.gmmcount(accval,**kwargs)
        return self.accresult
    def acqbufrun(self,nbuf,**kwargs):
        self.opts.update(kwargs)
        self.acqresult=self.qubicrun.acqbufrun(nbuf=nbuf,**self.opts)
        return self.acqresult
    def compile(self,**kwargs):
        self.compiled=True
        self.opts.update(kwargs)
        self.opts['chassis'].membufreset()
        self.qubicrun.compile(**self.opts)

    def iqplot(self,fig,qubit=None,**kwargs):
        for iqubitid,qubitid in enumerate(self.opts['qubitid'] if qubit is None else [qubit]):
            if qubitid in self.accresult['accval']:
                accvaliq=self.accresult['accval'][qubitid]
                plot.iqplot(iq=accvaliq,fig=fig,**kwargs)
        #fig.title(self.opts['qubitid'])
        return  fig
    def gmmplot(self,fig,qubit=None,**kwargs):
        for iqubitid,qubitid in enumerate(self.opts['qubitid'] if qubit is None else [qubit]):
            if qubitid in self.accresult['accval']:
                plot.gmmplot(gmixs=self.accresult['gmixs'][qubitid],fig=fig)
        return fig
    def savegmm(self,filename=None):
        for q in self.accresult['gmixs']:
            numpy.savez('%s_gmix.npz'%q,**self.accresult['gmixs'][q].modelpara())
