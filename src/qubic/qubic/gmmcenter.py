import numpy
import sys
f=sys.argv[1] #'gmixfastreset_rabi_nget50_qubitidQ5_20200225_233211_851475.joblib'
gmix=numpy.load(f)
p01=gmix['means']
l01={int(k):v for k,v in gmix['labels']}
l01rev={v:int(k) for k,v in gmix['labels']}

##p01=numpy.array([[0,0],[0,0]])
print(l01)
p0=p01[l01rev['0']][0]+1j*p01[l01rev['0']][1]
p1=p01[l01rev['1']][0]+1j*p01[l01rev['1']][1]
angle=numpy.angle(p0-p1)%(2*numpy.pi)
center=p0+(p1-p0)*1.0/2.0
print('mean 0',p0,'mean 1',p1,'angle',angle,'center',center)

