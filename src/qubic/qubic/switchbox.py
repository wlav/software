import time
import urllib.request
def urlread(url):
    swread=urllib.request.urlopen(url).read()
    return swread
def urltoggle(url):
    swread=urlread(url+"/?buttonToggle")
    return swread

response={
        'AC':'Connected ports: A-C, B-D',
        'BD':'Connected ports: A-C, B-D',
        'AB':'Connected ports: A-B, C-D',
        'CD':'Connected ports: A-B, C-D'
        }

import argparse
if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-ip','--ip',help='ip address for the switchbox',dest='ip',type=str,default="192.168.1.208")
    parser.add_argument('-t','--toggle',help='toggle current sw and read the final status',dest='toggle',default=False,action='store_true')
    parser.add_argument('-d','--dest',help='destinate switch status, AB,AC,BD,CD. ',dest='dest', choices=['AB','AC', 'BD','CD'],type=str,default=None)
    clargs=parser.parse_args()
    swurl='http://'+clargs.ip
    if clargs.toggle:
        urltoggle(swurl)
    if clargs.dest is not None:
        swread=str(urlread(url=swurl))
        while response[clargs.dest] not in swread:
            urltoggle(swurl)
            time.sleep(1)
            swread=str(urlread(url=swurl))
    print(urlread(url=swurl))
