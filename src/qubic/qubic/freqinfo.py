import numpy
import json
import sys
from matplotlib import pyplot
def freqinfo(qubitcfgfilename,ef=False,predicted=False,qubits=None):
    with open(qubitcfgfilename) as jfile:
        cfg=json.load(jfile)
    if qubits is None:
        qubits={k:f for k,f in cfg['Qubits'].items() if 'Q' in k}
    else:
        qubits={k:f for k,f in cfg['Qubits'].items() if 'Q' in k and k in qubits}
    qname=sorted(qubits.keys())
    freq=numpy.array([qubits[q]['freq'] for q in qname])
#    freq=numpy.array([qubits[q]['freq'] for q in qname])
#    readfreq=numpy.array([qubits[q]['readfreq'] for q in qname])
    readfreq=numpy.array([qubits[q]['readfreq'] for q in qname])
    if ef:
        freq_ef=numpy.array([qubits[q]['freq_ef'] for q in qname])
    else:
        freq_ef=None
    if predicted:
        freq_predicted=numpy.arry([qubits[q]['freq_predicted'] for q in qname])
    else:
        freq_predicted=None
    return readfreq,freq,freq_ef,freq_predicted
def plotfreq(axread,axqdrv,readfreq,freq,freq_ef,freq_predicted):
    axqdrv.plot(freq/1e9,'*',label='ge')
    axqdrv.set_ylabel('Qubit drive frequency (GHz)',fontsize=fontsize)
    axqdrv.set_xlabel('Qubits',fontsize=fontsize)
    axread.plot(readfreq/1e9,'*')
    axread.set_ylabel('Qubit readout resonator frequency (GHz)',fontsize=fontsize)
    axread.set_xlabel('Qubits',fontsize=fontsize)
    if freq_ef is not None:
        ax[1].plot(freq_ef/1e9,'*',label='ef')
    if freq_predicted is not None:
        ax[1].plot(freq_predicted/1e9,'*',label='predicted')
    print('qubit,fread(GHz),fge(GHz),fef(GHz)')
    axread.legend()
    axqdrv.legend()
def lochassis(qubitcfgfilename,axread,axqdrv):
    sys.path.append('../..')
    from qubic.qubic.chassis import c_chassis
    import imp
    import os
    wiremap=imp.load_source("wiremap",os.path.join(os.path.dirname(os.path.abspath(qubitcfgfilename)),'wiremap.py'))
    chassis=c_chassis(None,sqinit=False,sim=False,debug=False)
    print(wiremap.lor,wiremap.loq,chassis.fsample)
    axread.axhspan((wiremap.lor-chassis.fsample/2)/1e9,(wiremap.lor+chassis.fsample/2)/1e9,alpha=0.05,color='blue')
    axqdrv.axhspan((wiremap.loq-chassis.fsample/2)/1e9,(wiremap.loq+chassis.fsample/2)/1e9,alpha=0.05,color='blue')
    axread.axhline(y=wiremap.lor/1e9)
    axqdrv.axhline(y=wiremap.loq/1e9)
def findlo(fge,fef,ax):
    f=numpy.concatenate((fge,fef),axis=0)
    l=numpy.arange(max(fge)-0.5,min(fge)+0.5,0.001)
    d=numpy.zeros(l.shape)
    d2=numpy.zeros(l.shape)
    for il,li in enumerate(l):
        d[il]=min(sorted(abs(f-li)))
        d2[il]=min(numpy.diff(sorted(sorted(abs(f-li)))))
    ax.plot(l,d,'r*',label="min abs(f-l)")
    ax.plot(l,d2,'g*',label="min +- diff")
    ax.legend()
if __name__=="__main__":
    ef=True
    qubitcfgfilename=sys.argv[1]
    fig=pyplot.figure(figsize=(15 if ef else 10,5))
    ax=fig.subplots(1,2)# if ef else 2)
    fontsize=15
    fig.suptitle(qubitcfgfilename,fontsize=fontsize)
    readfreq,freq,freq_ef,freq_predicted=freqinfo(qubitcfgfilename,ef=True)#,qubits=['Q0','Q5','Q6'])#,'Q1','Q2','Q5','Q6','Q7'])
    for i,fr in enumerate(readfreq):
        print(i,end=',')
        print('%8.3f'%(readfreq[i]/1e9),end=',')
        print('%8.3f'%(freq[i]/1e9),end=',')
        if freq_ef is not None:
            print('%8.3f'%(freq_ef[i]/1e9),end=',')
        print()
    plotfreq(axread=ax[0],axqdrv=ax[1],readfreq=readfreq,freq=freq,freq_ef=freq_ef,freq_predicted=freq_predicted)
    lochassis(qubitcfgfilename,axread=ax[0],axqdrv=ax[1])
    figlo=pyplot.figure('findlo')
    axlo=figlo.subplots()
    findlo(fge=freq/1e9,fef=freq_ef/1e9,ax=axlo)
    pyplot.show()
