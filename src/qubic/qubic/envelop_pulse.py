import inspect
import numpy
class c_envelop_pulse:
    def __init__(self):
        pass

    def mark(self,twidth,dt):
        t=numpy.arange(0,twidth,dt)
        width=len(t)
        return (t,numpy.ones(width).astype('int'))

    def square(self,twidth,dt,amplitude=1.0,phase=0.0):
        """A simple square pulse"""
        t=numpy.arange(0,twidth,dt)
        width=len(t)
        return (t,(numpy.ones(width)*amplitude*numpy.exp(1j*phase)).astype('complex64'))

    def sin(self,twidth, dt, frequency=0, phase0=0.0):
        """A simple sin pulse"""
        t=numpy.arange(0,twidth,dt)
        return (t,numpy.exp(1j * (2.0 * numpy.pi * frequency * t +phase0 )).astype('complex64'))

    def sinf1f2(self, twidth, dt, frequency1=0, frequency2=0, phase1=0.0, phase2=0.0):
        """Sin pulse with frequency f1 and f2"""
        t=numpy.arange(0,twidth,dt)
        value1=0.5*numpy.exp(1j * (2.0 * numpy.pi * frequency1 * t +phase1 )).astype('complex64')
        value2=0.5*numpy.exp(1j * (2.0 * numpy.pi * frequency2 * t +phase2 )).astype('complex64')
        return (t,value1+value2)

    def half_sin(self,twidth,dt, phase0=0.0):
        ' sin(x) for pi in the given width'
        t=numpy.arange(0,twidth,dt)
        step=1.0/(len(t)-1)
        return (t,numpy.sin(numpy.pi * numpy.arange(0,1.0+step/2.0,step) ).astype('complex64'))

    def gaussian(self,twidth,dt, sigmas=3):
        """
        Width is the exact width, not the width of the sigmas.
        sigmas is the number of sigma in the width in each side
        """
        #print 'gaussian',twidth, dt, sigmas
        t=numpy.arange(0,twidth,dt)
        #print len(t),len(numpy.arange(0,4e-6,1e-9))
        width=len(t)
        sigma = width / (2.0 * sigmas)  # (width - 1 )?
        val=numpy.exp(-(numpy.arange(0, width) - width / 2.) ** 2. / (2 * sigma ** 2)).astype('complex64')
        #print twidth,dt,width,len(t),len(val)
        #print t,val
        return (t,val)

    def DRAG(self,twidth, dt,sigmas=3, alpha=0.5, delta=-268e6,  df=0):
        """Standard DRAG pulse as defined in https://arxiv.org/pdf/0901.0534.pdf
        Derivative Removal by Adiabatic Gate (DRAG)
        Args:
            width: the total width of the pulse in points
            alpha: alpha parameter of the DRAG pulse 0-3e6 scan 
            sigmas: number of standard deviations
            delta: the anharmonicity of the qubit
            sample_rate: AWG sample rate (default 2.5 GHz), and not used in this method.
            df: additional detuning from target frequency
            """
        #  why use kargs pass that? try use the value directly
        #delta = kargs.get('sample_rate', delta)
        t=numpy.arange(0,twidth,dt)
        width=len(t)

        sigma = width / (2. * sigmas) #(width - 1)?
        delta = float(delta * 2 * numpy.pi *dt)
        x = numpy.arange(0, width)
        #gaus = numpy.exp(-(x - width / 2.) ** 2. / (2 * sigma ** 2))
        _,gaus = self.gaussian(twidth,dt,sigmas)
        dgaus = -(x - width / 2.) / (sigma ** 2) * gaus
        p1=numpy.exp(1j* x * 1.0*df * dt *  2 * numpy.pi) 
        p2=(gaus - 1j * alpha * dgaus / delta)
        return (t,(p1*p2).astype('complex64'))

    def cos_edge_square(self,twidth, dt, ramp_fraction=0.25, ramp_length=None):
        if ramp_length is None:
            tedge=numpy.arange(0,2*twidth*ramp_fraction,dt)
        else:
            if 2*ramp_length>twidth:
                exit('Error: cos_edge_square fixed ramp_length ramp_legnth*2 = %8.3e > twidth = %8.3e'%(2*ramp_length,twidth))
            tedge=numpy.arange(0,2*ramp_length,dt)
            ramp_fraction=0 if twidth==0 else 1.0*ramp_length/twidth
        t=numpy.arange(0,twidth,dt)
        width=len(t)
        if (ramp_fraction>0 and ramp_fraction<=0.5 and twidth>0):
            f=1.0/(2*ramp_fraction*twidth)
            edges=(numpy.cos(2*numpy.pi*f*tedge-numpy.pi)+1.0)/2.0
            #pyplot.plot(edges)
            #pyplot.show()
            nramp=int(len(edges)/2)
            nflat=width-len(edges)
            env=numpy.concatenate((edges[:nramp],numpy.ones(nflat),edges[nramp:]))
        else:
            #print('ramp_fraction (ramp_length/twidth) should be 0<ramp_function<=0.5, %s and twidth>0 %s'%(str(ramp_fraction),str(twidth)))
            env=numpy.ones(width)
        return (t,env.astype('complex64'))

    def sin_edge_square(self,twidth, dt, ramp_fraction=0.25):
        """Return a square pulse shape of the specified amplitude with cosine edges.

        Args:
            width: the total width of the pulse in points
            ramp_fraction: duration of the cosine ramp on either side of the pulse, expressed as a fraction of the total pulse length
            sample_rate: AWG sample rate (default 2.5 GHz), and not used in this method.
            df: additional detuning from target frequency
            """
        t=numpy.arange(0,twidth,dt)
        width=len(t)
        if (ramp_fraction>0 and ramp_fraction<=0.5):
            _,edges=self.half_sin(twidth=2*ramp_fraction*twidth,dt=dt, phase0=0.0)
            nramp=int(len(edges)/2)
            nflat=width-len(edges)
            env=numpy.concatenate((edges[:nramp],numpy.ones(nflat),edges[nramp:]))
        else:
            print('ramp_fraction should be 0<ramp_function<=0.5 %s'%(str(ramp_fraction)))
            env=numpy.ones(width)
        # 5.  multiply by a detuning df offset detuning...
      #  df = float(df / sample_rate)
      #  x = numpy.arange(0, width)
      #  env = env * numpy.exp(-x * df * 1j * 2 * numpy.pi)

        return (t,env.astype('complex64'))

    def gauss_edge_square(self,twidth,dt, ramp_fraction=0.1, sigmas=3):
        """Return a square pulse shape of the specified amplitude with gaussian edges.

        Args:
            width: the total width of the pulse in points
            amplitude: the amplitude of the pulse, clipped to between -1 and 1.  
            phase: the phase applied as an overall multipler to the pulse, i.e. pulse_out = pulse * numpy.exp(1j*phase)
            ramp_fraction: duration of the cosine ramp on either side of the pulse, expressed as a fraction of the total pulse length
        """
        t=numpy.arange(0,twidth,dt)
        width=len(t)

        if (ramp_fraction>0 and ramp_fraction<=0.5):
            _,edges=self.gaussian(twidth=2.0*ramp_fraction*twidth,dt=dt, sigmas=sigmas)
            nramp=int(len(edges)/2)
            nflat=width-len(edges)
            #print width,nramp,nflat,len(edges),len(edges[:nramp]),len(numpy.ones(nflat)),len(edges[nramp:])
            env=numpy.concatenate((edges[:nramp],numpy.ones(nflat),edges[nramp:]))
        else:
            print('ramp_fraction should be 0<ramp_function<=0.5 got %s'%(str(ramp_fraction)))
            env=numpy.ones(width)

        return (t,env.astype('complex64'))


    def hann(self,twidth,dt):
        """A Hann window function, see https://en.wikipedia.org/wiki/Window_function#Hann_window
        """
        t=numpy.arange(0,twidth,dt)
        width=len(t)
        return  (t,numpy.hanning(width).astype('complex64'))

    def arbcsvfile(self,twidth,dt, fcsvrealimag,delimiter=','):
        """An arbitrary pulse definition, allows for arbitrary pulses
        to be added to the sequencer without the creation of new functions.
        """
        realimag=numpy.loadtxt(fcsvrealimag,delimiter=delimiter)
        t=numpy.arange(0,twidth,dt)
        width=len(t)
        return (t,(realimag[:,0]+1j*realimag[:,1]).astype('complex64'))
    def arb(self,twidth,dt, awgpointsreal,awgpointsimag):
        """An arbitrary pulse definition, allows for arbitrary pulses
        to be added to the sequencer without the creation of new functions.
        """
        t=numpy.arange(0,twidth,dt)
        width=len(t)
        return (t,numpy.array(numpy.array(awgpointsreal[:width])+1j*numpy.array(awgpointsimag[:width])).astype('complex64'))

    def listpulses(self):
        funcs=inspect.getmembers(self, predicate=inspect.ismethod)
        funclist=[f for f in funcs if f[0] not in ['__init__','listpulses']]
#        funclist=[func for func in getmembers(self, predicate=inspect.ismethod) if func not in ['__init__','listpulses']]
        return funclist 

if __name__=="__main__":
    import inspect
    from matplotlib import pyplot
    pulses=c_envelop_pulse()
    funcs=inspect.getmembers(pulses, predicate=inspect.ismethod)
    allargs=[]
    testval={'phase0':0, 'df':0, 'frequency':3.5e9, 'width':100, 'awgpointsreal':numpy.random.random(100000), 'awgpointsimag':numpy.random.random(200000),'sample_rate':1.0e9, 'delta':268e6, 'alpha':1, 'ramp_fraction':0.2, 'sigmas':3,'amplitude':1.0,'phase':0.7854,'dt':1e-9,'twidth':10e-6,'ramp_length':32e-9,'frequency1':50e6,'frequency2':40e6,'phase1':0,'phase2':0}
    #testval={'ramp_fraction':0.25, 'dt':1e-9,'twidth':200e-9,'ramp_length':32e-9}
    for func in [f for f in funcs if f[0] not in ['__init__','listpulses']]:
        args=inspect.getargspec(func[1])
        #print(func[0],args.args,args.defaults)
        allargs.extend(args.args)
        (t,val)=getattr(pulses,func[0])(**dict((k,testval[k]) for k in args.args if k!='self'))
        #print len(t),len(val)
        pyplot.subplot(2,1,1)
        pyplot.plot(t,val.real)
        pyplot.subplot(2,1,2)
        pyplot.plot(t,numpy.angle(val))
        pyplot.suptitle(func[0])
        pyplot.show()
    print([f[0] for f in pulses.listpulses()])
