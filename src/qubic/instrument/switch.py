import serial
import sys
import time
import datetime
sys.path.append('../../')
from qubic.instrument import devcom
import os
def rewriteelog(newmsg,msg_id=None,url='http://192.168.1.24/',logbook='QubiC',port=8080):
    import elog
    log=elog.open(url,logbook,port=port)
    if msg_id:
        origmsg=log.read(msg_id)[0]
        msg='%s<br>%s'%(newmsg,origmsg)
    else:
        msg=newmsg
#    print(msg,log)
    log.post(msg,author='scriptupdate',type='Routine',msg_id=msg_id)


def switch(port,deviceid='switch8',sndev=devcom.sndev,logfilename='switchlog.log',elog=False):
    dev=devcom.devcom(sndev)
    print('devcom',dev)
    loginfo=switchport(port=port,serialport=dev[deviceid],device=deviceid)
    switchlog(loginfo,logfilename,elog=elog)

def switchport(port,serialport,device):
    a=serial.Serial(serialport,9600)
    time.sleep(2)
    a.write(str.encode('%d'%int(port)))
    timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
    log='%s change %s to : %s'%(timestamp,device,str(a.readline(),'utf-8'))
    
    return log
    print(log)
def switchlog(loginfo,logfilename,elog=False):
    f=open(logfilename,'a+')
    f.write(str(loginfo))
    f.close()

#    os.system(r"echo %s |elog -h 192.168.1.24 -p 8080  -l QubiC -a Author=scriptupdate -a type=Routine"%(log.rstrip('\r\n')))
    if elog:
        rewriteelog(log.rstrip('\r\n'),msg_id=1335)#None)

if __name__=="__main__":
    switch(sys.argv[1])
