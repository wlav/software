import serial.tools.list_ports
import serial
import sys
import time
import datetime
#import devcom
from qubic.instrument import devcom

def stepvat(device,atten,sndev=devcom.sndev,logfilename='stepvat.log'):
    dev=devcom.devcom(sndev)
    print('devcom',dev)
    device=device
    atten=atten
    print('device',device)
    serialport=dev[device]
    loginfo=stepvatatten(atten=atten,serialport=serialport,device=device)
    log(loginfo=loginfo,logfilename=logfilename)

def atten(atten,serialport,device):
    a=serial.Serial(serialport,9600)
    if (atten>=0 and atten<=31.5):
        seqint=int(round(255-atten*2))
        a.write(bytes([seqint]))
    else:
        print('atten setting between 0 to 31.5 dB')
    a.write(str.encode('i'))
    #print(a.readline())
    read_atten=(255-int(a.readline(),16))/2.0
    print('read_atten',read_atten)
    time.sleep(0.5)
    a.write(str.encode('v'))
    print('version',a.readline())
    timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
    loginfo='%s Step Attenuator: %s is %s dB \n'%(timestamp,device,read_atten)
    print('log',loginfo)
    return loginfo

def log(loginfo,logfilename):
    f=open(logfilename,'a+')
    f.write(loginfo)
    f.close()

if __name__=="__main__":
    stepvat(sys.argv[1],float(sys.argv[2]))
