import time
import struct
import socket
import sys
import json
class c_ttyclient:
    def __init__(self,ip,port=10000):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.settimeout(30)
        self.server_address = (ip, port)
    def cmd(self,cmddict):
        message = str.encode(json.dumps(cmddict)) #'This is the message.  It will be repeated.'
        self.sock.sendto(message,self.server_address)
        dataraw,server=self.sock.recvfrom(1024)
        data = json.loads(dataraw)
        return data
    def __del__(self):
        self.sock.close()

if __name__=="__main__":
    ttyclient=c_ttyclient(sys.argv[1],10000)
    deviceid=sys.argv[2]
#    data=int(sys.argv[3])
    print(ttyclient.cmd(dict(cmd='write',deviceid=deviceid,chan=3,freq=float(sys.argv[3]))))
#    print(ttyclient.cmd(dict(cmd='write',deviceid=deviceid,data=data)))
#    print(ttyclient.cmd(dict(cmd='write',deviceid='switch8',data=3)))
#    print('line 1 done')
#    print(ttyclient.cmd(dict(cmd='write',deviceid='switch8',data=4)))
#    print(ttyclient.cmd(dict(cmd='write',deviceid='attn1',data=8)))
#    print(ttyclient.cmd(dict(cmd='idn')))
#    print(ttyclient.cmd(dict(cmd='finished')))
