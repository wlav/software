import time
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment
from qubic.qubic.expression import c_expression
from qubic.qcvv.rabi import c_rabi
from qubic.qcvv.fit import fit
from scipy.optimize import fminbound



class c_rabioptimize(c_rabi):#experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        self.opts={}
        self.opts['overlapcheck']=False
        self.opts.update(kwargs)
        c_rabi.__init__(self,qubitid,calirepo,**self.opts)
        self.fread=None
        self.aread=None
        self.frabi=None
        self.arabi=None
        self.seqtype=None
        self.trabis=None
        self.freadtry=[]
        self.areadtry=[]
        self.arabitry=[]
        self.frabitry=[]
    def baseseqs(self,qubitid,aread,trabis,arabi=None,fread=None,frabi=None):
        seqs=[]
        frabipara=c_expression('frabipara',['frabipara']) if frabi is True else None if frabi is None else frabi
        freadpara=c_expression('freadpara',['freadpara']) if fread is True else None if fread is None else fread
        #print('debug seqs para',frabipara,freadpara)
        for trabi in trabis:
            seqs.append(self.rabiseq(qubitid=qubitid,frabi=frabipara,fread=freadpara,aread=aread,trabi=trabi,arabi=arabi))
        #print('seqs',type(seqs))
        return seqs
    def trabisetup(self,**kwargs):
        self.opts.update(kwargs)
        para=dict(qubitid=None,elementlength=80,elementstep=4e-9)
        para.update({k:v for k,v in self.opts.items() if k in para})
        self.trabis=numpy.arange(para['elementlength'])*para['elementstep']
        self.x=self.trabis
    #def seqsaread(self,aread):
#        self.opts.update(dict(para=dict(frabipara=self.frabi,freadpara=self.fread)))
    def funaread(self,aread):
        #self.seqsaread(aread=aread)
        if self.trabis is None:
            self.trabisetup()
        self.seqtype='aread'
        self.opts['seqs']=self.baseseqs(qubitid=self.opts['qubitid'],aread=aread,fread=self.fread,frabi=self.frabi,trabis=self.trabis)
        if self.seqtype != 'aread':
            print('aread')
        #self.compile()
        self.run()
        val=abs(0.5-self.result['amp'])
        self.areadtry.append((aread,val))
        if self.opts['plot']:
            pyplot.figure('aread')
            pyplot.plot(aread,val,'*')
            pyplot.draw()
            pyplot.pause(0.1)

        return val
#    def seqsfread(self):
    def funfread(self,fread):
        #self.seqsfread()
        if self.trabis is None:
            self.trabisetup()
        if self.seqtype != 'fread':
            self.opts['seqs']=self.baseseqs(trabis=self.trabis,qubitid=self.opts['qubitid'],aread=self.aread,fread=True,frabi=self.frabi)
            print('fread')
        self.seqtype='fread'
        self.opts.update(dict(para=[dict(freadpara=fread)]))
        #paradict=dict(para=[dict(freadpara=fread)])
        #self.compile()
        self.run()
        val=abs(0.5-self.result['amp'])
        self.freadtry.append((fread,val))
        if self.opts['plot']:
            pyplot.figure('fread')
            pyplot.plot(fread,val,'*')
            pyplot.draw()
            pyplot.pause(0.1)
            
        return val
    #def seqsfrabi(self):
    def funfrabi(self,frabi):
        #self.seqsfrabi()
        if self.trabis is None:
            self.trabisetup()
        if self.seqtype != 'frabi':
            self.opts['seqs']=self.baseseqs(trabis=self.trabis,qubitid=self.opts['qubitid'],aread=self.aread,frabi=True,fread=self.fread)
            print('frabi')
        self.seqtype='frabi'
        self.opts.update(dict(para=[dict(frabipara=frabi)]))
        #paradict=dict(para=[dict(frabipara=frabi)])
        self.run()
        val=abs(0.5-self.result['amp'])
        self.frabitry.append((frabi,val))
        if self.opts['plot']:
            pyplot.figure('frabi')
            pyplot.plot(frabi,val,'*')
            pyplot.draw()
            pyplot.pause(0.1)
        return val
    #def seqsarabi(self,arabi):
#        self.opts.update(dict(para=dict(frabipara=self.frabi,freadpara=self.fread)))
    def funarabi(self,arabi):
        #self.seqsarabi(arabi=arabi)
        if self.trabis is None:
            self.trabisetup()

        self.seqtype='arabi'
        if self.seqtype != 'aread':
            print('arabi')
        self.opts['seqs']=self.baseseqs(qubitid=self.opts['qubitid'],arabi=arabi,aread=self.aread,fread=self.fread,frabi=self.frabi,trabis=self.trabis)
        self.run()
        val=abs((self.period_target-self.result['period'])*1e9)
        self.arabitry.append((arabi,val))
        if self.opts['plot']:
            pyplot.figure('arabi')
            pyplot.plot(arabi,val,'*')
            pyplot.draw()
            pyplot.pause(0.1)
        return val
    def tryplot(self,prop):
        kv=getattr(self,prop)
        kvn=numpy.array(kv)#((len(kv),2))
        #print(kvn)
        pyplot.subplot(211)
        pyplot.plot(kvn[:,0])
        pyplot.subplot(212)
        pyplot.plot(kvn[:,1])



    def run(self):
        #        experiment.c_experiment.accbufrun(self)
        self.compile()
        self.accbufrun(nsample=self.opts['nsample'])#self.opts['nsample'])
        self.psingle=self.accresult['countsum']['psingle']
        #print(self.accresult['countsum']['totalsingle'])
        self.fit()
    #    return val
    def fminscan(self,func,x1,x2,**kwargs):
        #y=numpy.zeros(5)
        #for ix,x in enumerate(numpy.linspace(x1,x2,5)):
        #    y[ix]=func(x)
        return fminbound(func=func,x1=x1,x2=x2,**kwargs)
    def optimize(self,nsample,freadcenran=(None,8e6),frabicenran=(None,4e6),arabiminmax=(None,None),areadminmax=(None,None),period_target=128e-9,freadxtol=50e3,frabixtol=100e3,areadxtol=0.005,arabixtol=0.01,disp=0,**kwargs):
        self.opts.update(kwargs)
        self.opts['nsample']=nsample
        self.debug(4,'rabioptimize opts',self.opts,kwargs)
        print('opts plot',self.opts['plot'])
        if self.opts['plot']:
            for figname in ['fread','aread','frabi','arabi']:
                pyplot.figure(figname)
                pyplot.clf()
                pyplot.ion()
                pyplot.show()
                time.sleep(0.1)
        self.period_target=period_target
        qubitid=self.opts['qubitid'][0]

        fread0=self.opts['qchip'].getfreq(qubitid+'.readfreq')
        freadcen=fread0 if freadcenran[0] is None else freadcenran[0]
        freadran=freadcenran[1]
        frabi0=self.opts['qchip'].getfreq(qubitid+'.freq')
        frabicen=frabi0 if frabicenran[0] is None else frabicenran[0]
        frabiran=frabicenran[1]
        aread0=self.opts['qchip'].gates[qubitid+'read'].pulses[0].amp
        areadmin=0.001 if areadminmax[0] is None else areadminmax[0]
        #areadmax=1 if areadminmax[1] is None else areadminmax[1]
        areadmax=0.3 if areadminmax[1] is None else areadminmax[1]
        arabi0=self.opts['qchip'].gates[qubitid+'X90'].pulses[0].amp
        arabimin=0.001 if arabiminmax[0] is None else arabiminmax[0]
        arabimax=1 if arabiminmax[1] is None else arabiminmax[1]
        self.fread=self.fminscan(func=self.funfread,x1=freadcen-freadran/2,x2=freadcen+freadran/2,xtol=freadxtol,disp=disp)
        self.frabi=self.fminscan(func=self.funfrabi,x1=frabicen-frabiran/2,x2=frabicen+frabiran/2,xtol=frabixtol,disp=disp)
        self.aread=self.fminscan(func=self.funaread,x1=areadmin,x2=areadmax,xtol=areadxtol,disp=disp)
        #self.arabi=self.fminscan(func=self.funarabi,x1=arabimin,x2=arabimax,xtol=arabixtol,disp=disp)
        updatedict={}
        updatedict.update({('Qubits',qubitid,'readfreq'):self.fread
        ,('Qubits',qubitid,'freq'):self.frabi
        ,('Gates',qubitid+'read',0,'amp'):self.aread
        # ,('Gates',qubitid+'rabi',0,'amp'):self.arabi
        #,('Gates',qubitid+'X90',0,'amp'):self.arabi
        })
        updatedict.update({('Gates',qubitid+'X90',0,'twidth'):round(period_target/4,15)})
        
        self.seqs_w(elementlength=80,elementstep=4e-9,ef=False,frabi=self.frabi,fread=self.fread,arabi=self.arabi,aread=self.aread)
        #,ef=True,frabi=5.1675e9)#,arabi=0.1628)
        c_rabi.run(self,500)#,n_components=3,labels=['0','1','2'])#,labelmeasindex=(('0',0),('1',1),('2',None)))#,'0']))
#        updatedict.update(self.readphupdate(labels=['0','1'],updatedlophase=True))
#        c_rabi.run(self,500)#,n_components=3,labels=['0','1','2'])#,labelmeasindex=(('0',0),('1',1),('2',None)))#,'0']))
#        self.savegmm()
        if self.opts['gmixs'] is None:
            self.savegmm()
        if self.opts['plot']:
            pyplot.ioff()

        return updatedict#self.opts['qchip'].updatecfg(updatedict)

if __name__=="__main__":
    import datetime
    t0=datetime.datetime.now()
    rabioptimize=c_rabioptimize(qubitid=sys.argv[1],calirepo='../../../../qchip',debug=0,gmixs=None,plot=True)
    fread0=rabioptimize.opts['qchip'].getfreq(rabioptimize.opts['qubitid'][0]+'.readfreq')
    frabi0=rabioptimize.opts['qchip'].getfreq(rabioptimize.opts['qubitid'][0]+'.freq')
    aread0=rabioptimize.opts['qchip'].gates[rabioptimize.opts['qubitid'][0]+'read'].pulses[0].amp
    arabi0=rabioptimize.opts['qchip'].gates[rabioptimize.opts['qubitid'][0]+'X90'].pulses[0].amp
#    fread=fread0-1.23e6
#    freadbw=4e6
#    frabi=frabi0+2e6
#    frabibw=6e6
#    arabimin=0.001
#    arabimax=1
#    areadmin=0.001
#    areadmax=1
#    #print(rabioptimize.funfrabi(frabi))
#    #print(rabioptimize.funfread(fread))
#    #print(rabioptimize.funaread(aread))
#    rabioptimize.period_target=128e-9
#    rabioptimize.fread=fminbound(func=rabioptimize.funfread,x1=fread-freadbw/2,x2=fread+freadbw/2,xtol=50e3,disp=3)
#    t1=datetime.datetime.now()
#    print('fread optim takes',t1-t0)
#
#    rabioptimize.frabi=fminbound(func=rabioptimize.funfrabi,x1=frabi-frabibw/2,x2=frabi+frabibw/2,xtol=50e3,disp=3)
#    t2=datetime.datetime.now()
#    print('frabi optim takes',t2-t1)
#    rabioptimize.aread=fminbound(func=rabioptimize.funaread,x1=areadmin,x2=areadmax,xtol=0.005,disp=3)
#    t3=datetime.datetime.now()
#    print('aread optim takes',t3-t2)
#    rabioptimize.arabi=fminbound(func=rabioptimize.funarabi,x1=arabimin,x2=arabimax,xtol=0.005,disp=3)
    t4=datetime.datetime.now()
#    print('arabi optim takes',t4-t3)
    #rabioptimize.opts['qchip'].paradict['
    updatedict=rabioptimize.optimize(nsample=50,disp=3)
    print(updatedict)
    if 1:
        rabioptimize.opts['qchip'].updatecfg(updatedict,rabioptimize.opts['qubitcfgfile'])
    print('arabi',rabioptimize.arabi,arabi0)
    print('fread',rabioptimize.fread,fread0)
    print('aread',rabioptimize.aread,aread0)
    print('frabi',rabioptimize.frabi,frabi0)
    print('overall takes',t4-t0)

    fig,(sub1,sub2)=pyplot.subplots(2,1)
    fig.suptitle(sys.argv[1])
    rabioptimize.psingleplot(sub1)
    rabioptimize.plottyfit(sub1)
    rabioptimize.iqplot(sub2)
    rabioptimize.gmmplot(sub2)
#pyplot.savefig('fig1.pdf')
    pyplot.show()
    pyplot.figure('aread')
    rabioptimize.tryplot('areadtry')
    pyplot.figure('frabi')
    rabioptimize.tryplot('frabitry')
    pyplot.figure('fread')
    rabioptimize.tryplot('freadtry')
    pyplot.figure('arabi')
    rabioptimize.tryplot('frabitry')
    pyplot.show()
    #from scipy.optimize import fminbound,minimize,basinhopping
#    from noisyopt import minimizeCompass
#    rabioptimize.seqs(None,None,None)#elementlength=10)
    #rabioptimize.fread=fminbound(func=rabioptimize.funfread,x1=6.55e9,x2=6.57e9,xtol=10e4)
    #rabioptimize.aread=fminbound(func=rabioptimize.funaread,x1=0,x2=1,xtol=1e-4)
    #rabioptimize.fread=fminbound(func=rabioptimize.funfread,x1=6.55e9,x2=6.57e9,xtol=10e4)
    #xopt=fminbound(func=rabioptimize.opfun,x1=[[0],[0],[0]],x2=[[1],[1],[1]],xtol=1e-4)
    #xopt=basinhopping(func=rabioptimize.opfun,x0=[0.,0.,0.],bounds=((0,1),(0,1),(0,1)),niter=10)
    #xopt=minimize(fun=rabioptimize.opfun,x0=[0.,0.,0.],bounds=((0,1),(0,1),(0,1)),tol=1e-4)
#    xopt=minimizeCompass(rabioptimize.opfun,x0=[0.5,0.5,0.5],deltatol=1e-2,paired=False,bounds=((0,1),(0,1),(0,1)))
            #"freq": 5298741418.425535,
            #"readfreq": 6.55872e9,
            #"amp": 0.33819790929173177,
    #print(xopt)
    #rabioptimize.opfun([0,0,0])
#    for i in range(1):
#        rabioptimize.opfun(fread=6.5e9,aread=None,frabi=None)
#    rabioptimize.fit()
#    rabioptimize.savegmm()
#    print('result',rabioptimize.result)
#    fig=pyplot.figure()
#    ax=pyplot.subplots()
    #fig,(sub)=pyplot.subplots(1,1)
#    fig.suptitle(sys.argv[1])
#    print(rabioptimize.fread,rabioptimize.sept)
#    sub1.plot(rabioptimize.fread,rabioptimize.sept)
##    rabioptimize.psingleplot(sub1)
#    rabioptimize.plottyfit(sub1)
    #rabioptimize.iqplot(sub)
#pyplot.savefig('fig1.pdf')
    #pyplot.show()
