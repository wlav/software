import os
import datetime
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment
from qubic.qcvv.plot import plot
from qubic.qcvv.fit import fit
#from qubic.qcvv.rabi import rabiseq
from qubic.qcvv.analysis import blob
#from qubic.qubic.expression import c_expression
#def rabiseq(qubitid,**kwargs):
class c_blob3(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
    def baseseqs(self,qubitid,**kwargs):
        opts=dict(fread=None,tread=None,aread=None,pread=None)
        opts.update(kwargs)
        readmodi={}
        rdrvmodi={}
        readmodi.update({} if opts['tread'] is None else {'twidth':opts['tread']});    rdrvmodi.update({} if opts['tread'] is None else {'twidth':opts['tread']})
        readmodi.update({} if opts['fread'] is None else {'fcarrier':opts['fread']});    rdrvmodi.update({} if opts['fread'] is None else {'fcarrier':opts['fread']})
        readmodi.update({} if opts['pread'] is None else {'pcarrier':opts['pread']});    rdrvmodi.update({} if opts['pread'] is None else {})
        readmodi.update({} if opts['aread'] is None else {});    rdrvmodi.update({} if opts['aread'] is None else {'amp':opts['aread']})
        seqs=[]
        seq=[]
        seq.append({'name': 'read', 'qubit': qubitid,'modi' : [rdrvmodi,readmodi]})
        seqs.append(seq)
        seq=[]
        seq.append({'name': 'X90', 'qubit': qubitid})
        seq.append({'name': 'X90', 'qubit': qubitid})
        seq.append({'name': 'read', 'qubit': qubitid,'modi' : [rdrvmodi,readmodi]})
        seqs.append(seq)
        if 1:
            seq=[]
            seq.append({'name': 'X90', 'qubit': qubitid})
            seq.append({'name': 'X90', 'qubit': qubitid})
            seq.append({'name': 'X90_ef', 'qubit': qubitid})
            seq.append({'name': 'X90_ef', 'qubit': qubitid})
            seq.append({'name': 'X90', 'qubit': qubitid})
            seq.append({'name': 'X90', 'qubit': qubitid})
            seq.append({'name': 'read', 'qubit': qubitid,'modi' : [rdrvmodi,readmodi]})
            seqs.append(seq)
        return seqs

    def nonelist(self,vin):
        result=False
        if isinstance(vin,list) or isinstance(vin,tuple):
            if len(vin)==1:
                if vin[0] is None:
                    result=True
        return result

    def seqs(self,**kwargs):
        opts=dict(freads=[None],treads=[None],areads=[None],preads=[None])
        opts.update(kwargs)
        self.fread=[] if self.nonelist(opts['freads']) else opts['freads'] 
        self.tread=[] if self.nonelist(opts['treads']) else opts['treads'] 
        self.aread=[] if self.nonelist(opts['areads']) else opts['areads'] 
        self.pread=[] if self.nonelist(opts['preads']) else opts['preads'] 
        self.debug(4,'nfread in seqs %d'%len(opts['freads']))
        self.opts.update(kwargs)
        self.opts['seqs']=[]
        aread=None
        for fread  in opts['freads']:
            for aread  in opts['areads']:
                self.opts['seqs'].extend(self.baseseqs(qubitid=self.opts['qubitid'][0],fread=fread,aread=aread))
        self.compile(delaybetweenelement=600e-6,heraldcmds=None)

    def run(self,nsample,**kwargs):
        self.accbufrun(nsample=nsample,includegmm=False)
#        accbufval={k:v.reshape((-1,3)) for k,v in self.accval.items()}
#        self.gmmcount(accval=None,**kwargs)
        nfread=len(self.fread)
        self.debug(4,'nfread',nfread)
        self.accvals=[]
        self.gmmresults=[]
        for  k,v in self.accval.items():
            vs=v.reshape((-1,nfread,3))
            self.debug(4,v[0:10,:])
            self.debug(4,vs[0:10,:,:])
            self.debug(4,'blob3 k',k,'v.shape',v.shape,'vs.shape',vs.shape)
            for ifread  in range(nfread):
                self.accvals.append({k:vs[:,ifread,:]})
        self.opts['gmix']=None
        self.sept=[]
        self.dist={}
        for ifread,fread in enumerate(self.fread):
            gmmresult=self.gmmcount(self.accvals[ifread],n_components=3,labels=['0','1','2'],labelmeasindex=(('0',0),('1',1),('2',None)))
            self.gmmresults.append(gmmresult)
            separation=gmmresult['gmixs'][self.opts['qubitid'][0]].separation()
            self.sept.append(separation)
            for k,v in separation.items():#blob.gmixdist(gmmresult['gmixs'][self.opts['qubitid']]).items():
                if k not in self.dist:
                    self.dist[k]=[]
                self.dist[k].append(v)
            self.debug(4,gmmresult)
        self.debug(4,self.dist)

    def distplot(self,fig,x=None):
        x=self.fread if x is None else x
        for k,v in self.dist.items():
            self.debug(4,'distplot x',x)
            self.debug(4,'distplot v',v)
            fig.plot(x,v,label=str(k),marker='.',linewidth=0)
            fig.legend()
    def plot(self,fig):
#        for ifread,fread in enumerate(self.fread):
        if 1:
            ifread=len(self.fread)//2
            fread=self.fread[ifread]

            accvaliq=self.accvals[ifread]
            self.debug(4,'plot',list(accvaliq.keys()))
            for q in accvaliq:
                plot.iqplot(iq=accvaliq[q],fig=fig,log=False)
            plot.gmmplot(gmixs=self.gmmresults[ifread]['gmixs'][self.opts['qubitid'][0]],fig=fig)
            #fig.settitle('%8.3e'%fread)
    #def iqplot(self,fig):
    #    #        accvaliq=self.accresult['accval'][self.opts['qubitid']]
    #    accvaliq=self.accvals
    #    return plot.iqplot(iq=accvaliq,fig=fig)
#    def gmmplot(self,fig):
#        plot.gmmplot(gmixs=self.accresult['gmixs'],fig=fig)
#        for ielem in gmixs:#self.accresult['accval']:
#            mixmean=[]
#            for ilabel,label in gmixs[ielem].labels.items():
#                mixmean=gmixs[ielem].means_[ilabel].transpose()
#                fig.plot(mixmean[0],mixmean[1],color='b',marker='$%s$'%label,markersize=20)
#        return fig

if __name__=="__main__":
    blob3=c_blob3(qubitid=sys.argv[1],calirepo='../../../../qchip',debug=3,gmixs=None)
    areads=[1.0]#numpy.linspace(0,1,0.1)
    fr=blob3.opts['qchip'].getfreq(blob3.opts['qubitid'][0]+'.readfreq')
    ar=blob3.opts['qchip'].gates[blob3.opts['qubitid'][0]+'read'].paralist[0]['amp']
    fig=pyplot.figure()
    sub=fig.subplots(2,1)
    fig.suptitle(blob3.opts['qubitid'][0])
    bw=10e6
    freads=numpy.linspace(fr-bw/2.0,fr+bw/2.0,31)
    blob3.seqs(overlapcheck=False,freads=freads)#,areads=areads)
    blob3.run(1000,n_components=3,labels=['0','1','2'],labelmeasindex=(('0',0),('1',1),('2',None)))
    blob3.distplot(sub[0])
    sub[0].axvline(x=fr)
    freads=[fr]#numpy.linspace(fr-2e6,fr+2e6,50)
    blob3.seqs(overlapcheck=False,freads=freads)#,areads=areads)
    blob3.run(400,n_components=3,labels=['0','1','2'],labelmeasindex=(('0',0),('1',1),('2',None)))
#    sub.suptitle(sys.argv[1])
    blob3.plot(sub[1])
    blob3.savegmm()
#    print(blob3.accresult['countsum']['vsingle'])
#    blob3.fit()
#    blob3.savegmm()
#    print('result',blob3.result)
#    fig=pyplot.figure(figsize=(10,10))
#    sub=fig.subplots()#2,1)
#    fig.suptitle(sys.argv[1])
#    print(blob3.fread,blob3.sept)
#    sub[0].plot(blob3.fread,blob3.sept)
##    blob3.psingleplot(sub1)
#    blob3.plottyfit(sub1)
#    blob3.iqplot(sub)
#    blob3.gmmplot(sub)
    pyplot.savefig('blob3_%s.pdf'%blob3.opts['qubitid'][0])
    pyplot.show()
