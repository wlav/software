from matplotlib import pyplot
from scipy import signal
import numpy
import matplotlib
def getax(fig,shape=(1)):
    if isinstance(fig,matplotlib.figure.Figure):
        ax=fig.subplots(*shape)
    else:
        ax=fig
    return ax
def psingleplot(x,psingle,fig,**kwargs):
    #    ax1=fig.add_subplot(1,1,1)
#    ax1=fig.add_subplot(1,1,1)
    ax=getax(fig)
    for q,lv in psingle.items():
        for label in sorted(psingle[q]):
            ax.plot(x,psingle[q][label],label=label,**kwargs)
#        ax.plot(x,psingle[q]['1'],'b',label='1',**kwargs)
    ax.legend()
    return fig
def iqplot(iq,fig,hexbin=True,marker='.',color='r',log=True,cmap='Greys',colorbar=False,**kwargs):
    #ax1=fig.add_subplot(1,1,1)
    ax=getax(fig)
    if hexbin:
        imhex=ax.hexbin(iq.real,iq.imag,gridsize=30,cmap=cmap,bins='log' if log else None)
        if colorbar:
            pyplot.colorbar(imhex)
    else:
        ax.plot(iq.real,iq.imag,marker=marker,ls=None,lw=0,**kwargs)
    return fig

def iqapplot_a(x,iq,fig,diff=False,logy=False,**kwargs):
    if diff:
        fig.plot(x[:-1],abs(numpy.diff(abs(iq))),**kwargs)
    else:
        if logy:
            fig.plot(x,20*numpy.log10(abs(iq)),**kwargs)
        else:
            fig.plot(x,abs(iq),**kwargs)
    return fig
def iqapplot_p(x,iq,fig,unwrap=True,detrend=False,diff=False,**kwargs):
    p1=numpy.angle(iq)
    p2=numpy.unwrap(p1) if unwrap else p1
    p3=signal.detrend(p2) if detrend else p2
    if diff:
        fig.plot(x[:-1],numpy.diff(p3))
    else:
        fig.plot(x,p3,**kwargs)
    return fig

def iqapplot(x,iq,fig,**kwargs):
    #ax=getax(fig,(3,2))
    if fig.shape==(3,2):
        iqapplot_a(x,iq,fig=fig[0,0],diff=False,logy=True)
        iqapplot_a(x,iq,fig=fig[0,1],diff=True)
        iqapplot_p(x,iq,fig=fig[1,0],unwrap=True,detrend=True,diff=False)
        iqapplot_p(x,iq,fig=fig[1,1],unwrap=True,detrend=True,diff=True)
        iqapplot_p(x,iq,fig=fig[2,0],unwrap=True,detrend=False,diff=False)
        iqapplot_p(x,iq,fig=fig[2,1],unwrap=False,detrend=False,diff=False)
    else:
        print('iqapplot require 6 axes') 
    return fig
def gmmplot(gmixs,fig,**kwargs):
    markersize= kwargs['markersize'] if 'markersize' in kwargs else 20
    mixmean=[]
    blobs=gmixs.gmixblobs()
    for ilabel,label in gmixs.labels.items():
        mixmean=gmixs.means_[ilabel].transpose()
        fig.plot(mixmean[0],mixmean[1],color='r',marker='$%s$'%label,markersize=markersize)
        x,y=blobs[label].ellipsexy()
        fig.plot(x,y,'r')
    return fig
