import time
import os
import datetime
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment
from qubic.qcvv.plot import plot
from qubic.qcvv.analysis.s11peaks import s11peaks
from qubic.qcvv.fit import fit
from scipy import signal
#envset,qubicrun,heralding

class c_vna(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
        self.opts['qchip'].gates['vnaread'].calcnorm(1e-9,32767)
        self.norm=self.opts['qchip'].gates['vnaread'].normalize
        print('norm',self.norm)
    def baseseqs(self,t0=None,amp=None,twidth=4e-6):
        if 'c_expression' not in sys.modules:
            from qubic.qubic.expression import c_expression
        f=c_expression('f','f')
        rdrvmodi={}
        readmodi={}
        rdrvmodi.update({'fcarrier':f,'twidth':twidth})
        rdrvmodi.update(dict(amp=amp) if amp is not None else {})
        readmodi.update({'fcarrier':f,'twidth':twidth})
        readmodi.update(dict(t0=t0) if t0 is not None else {})
        seqs=[[
            #            {'name': 'X90', 'qubit': 'Q0'},
            #{'name': 'X90', 'qubit': 'Q0'},
            {'name': 'read', 'qubit': 'vna','modi' : [rdrvmodi,readmodi]}
            ]]
        return seqs


    def seqs(self,fx,**kwargs):
        self.opts.update(kwargs)
        self.opts['heraldcmds']=None
        self.opts['delaybetweenelement']=10e-6
        self.fx=fx
        self.opts['seqs']=self.baseseqs(**kwargs)#**self.opts)
        self.opts['gmixs']=None
        self.opts['para']=[dict(f=f) for f in fx]
        self.compile()#cmdlists=qubicrun.compile(**self.opts)
    def run(self,nsample,vat=None,**kwargs):
        self.debug(4,'debug c_vna run, gmixs',self.opts['gmixs'])
        if vat is not None:
            self.qubicrun.setrdrvvat(vat)
            time.sleep(1)
        experiment.c_experiment.accbufrun(self,nsample,includegmm=False,**kwargs)

        self.s11=(self.accresult['accval'][self.opts['qubitid'][0]].mean(axis=0))/self.norm
        if vat is not None:
            self.qubicrun.setrdrvvat(finish=True)


    def fitdt(self):
        up=numpy.unwrap(numpy.angle(self.s11))
        diffup=numpy.diff(numpy.unwrap(numpy.angle(self.s11)))
        df=numpy.diff(self.fx)
        dt=diffup.mean()/df.mean()/2/numpy.pi
        #print('overall estimate: delay=current','+' if dt<0 else '-', abs(dt))
        dtrup=signal.detrend(numpy.unwrap(numpy.angle(self.s11)))
        lup=len(dtrup)
        upflatcheck=numpy.array([max(abs(dtrup[iup]-dtrup[max(iup-20,0):min(lup,iup+20)]))<numpy.pi/2 for iup in range(lup)])
        upflatdiff=numpy.array([up[i+1]-up[i] for i in range(lup-1) if upflatcheck[i] and upflatcheck[i+1]])
        upflatdiffmean=upflatdiff.mean()
        dtu=upflatdiffmean/df.mean()/2/numpy.pi
        ltu=len([i for i in upflatcheck if i])
        #print('flatarea estimate based on %d point: '%ltu, 'delay=current','+' if dtu<0 else '-', abs(dtu))
        return dtu if ltu>100 else dt

    def ampplot(self,axis):
        axis.plot(self.fx,abs(self.s11))
        return axis
    def phplot(self,axis):
        axis.plot(self.fx,numpy.angle(self.s11))
        return axis

    def iqapplot(self,fig):
        #print(self.s11[0:10])
        return plot.iqapplot(x=self.fx,iq=self.s11,fig=fig)

    def offsetdiff(self,array,offset):
        a0=array[offset:]
        a1=array[:-offset]
        return a1-a0
    def s11peaks(self):
        numpy.savez('s11.npz',self.s11)
        phdiff=abs(numpy.diff(numpy.unwrap(numpy.angle(self.s11)),1))
        peakdict=s11peaks(self.fx,phdiff)
        self.peakindex=peakdict['peakindex']
        self.phdiff=peakdict['phdiff']
        return peakdict
    def fits11(self):
        p0=fit.s11est(self.fx,self.s11,f0est=self.fx.mean())
        return fit.fitcavs11(self.fx,self.s11,p0=p0)#,**kwargs):
#def s11peaks(fx,s11):
#    import scipy.signal
#    #p=abs(self.offsetdiff(numpy.unwrap(numpy.angle(self.s11)),4))
#    phdiff=abs(numpy.diff(numpy.unwrap(numpy.angle(s11)),4))
#    p1,p1p=scipy.signal.find_peaks(phdiff,prominence=2*phdiff.std())#height=p.mean()+3*p.std()))
#    pp1=sorted(p1p['prominences'],reverse=True)
#    npeak=30
#    if (len(pp1)<=npeak):
#        ppn=pp1[-1]
#        print('not enough peaks, only %d, need %d'%(len(pp1),npeak))
#    else:
#        ppn=pp1[npeak]
#    peakindex ,_=scipy.signal.find_peaks(phdiff,prominence=ppn)#height=p.mean()+3*p.std()))
#    #self.p=p
#    #self.p2=p2
#    fxc=fx[peakindex]
#    pw,pwh,lip,rip=scipy.signal.peak_widths(phdiff,peakindex)
#    pwint=numpy.array([numpy.ceil(i) for i in pw]).astype(int)
#    #print('pwint',pwint)
#    #print('p2',p2)
#    p2min=peakindex-pwint
#    p2min=[max(0,i) for i in p2min]
#    p2max=peakindex+pwint
#    p2max=[min(len(fx),i) for i in p2max]
#    #pwhz=[max(i,minbw/2) for i in pw*fxstep]
#    #print('fx 800-820',self.fx[800:820])
#    fxmin=fx[p2min]#fxc-pwhz
#    fxmax=fx[p2max]#fxc+pwhz
#    #print('pw,p2min,p2max',pw,p2min,p2max,fxmin,fxmax)
#    fscan=[]
#    fstart=fxmin[0]
#    fstop=fxmax[0]
#    ftot=0
#    for i in range(1,len(peakindex)):
#        fstartthis=fxmin[i]
#        fstopthis=fxmax[i]
#        if fxmin[i]<fstop:
#            fstop=fxmax[i]
#            print('overlap')
#            if i==len(peakindex)-1:
#                fscan.append((fstart,fstop))
#        else:
#            fscan.append((fstart,fstop))
#            print('diff','%8.3e'%(fstop-fstart))
#            ftot+=(fstop-fstart)
#            fstart=fxmin[i]
#            fstop=fxmax[i]
#            if i==len(peakindex)-1:
#                fscan.append((fxmin[i],fxmax[i]))
#                ftot+=(fxmax[i]-fxmin[i])
#    return dict(peakindex=peakindex,phdiff=phdiff,fxc=fxc,fscan=fscan,ftot=ftot)

if __name__=="__main__":
    import sys
    sys.path.append('../..')
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-l','--live',help='live update',dest='live',default=False,action='store_true')
    parser.add_argument('-q','--qubitid',help='qubitid list',dest='qubitid',default=None)
    parser.add_argument('-bw','--bandwidth',help='vna bandwidth',dest='bandwidth',default=None,type=float)
    parser.add_argument('-cf','--centerfreq',help='vna centerfreq',dest='centerfreq',default=None,type=float)
    parser.add_argument('-a','--amp',help='vna amp',dest='amp',default=None,type=float)
    parser.add_argument('-t0','--t0',help='read pulse delay',dest='t0',default=None,type=float)
    parser.add_argument('-n','--nshot',help='number of shot',dest='nshot',default=100,type=int)
    parser.add_argument('-np','--noplot',help='no plot',dest='noplot',default=False,action='store_true')
    parser.add_argument('-s','--savefig',help='save figure plot',dest='savefig',default=False,action='store_true')
    clargs=parser.parse_args()
    vna=c_vna(qubitid='vna',calirepo='../../../../qchip',debug=True)
    #print(norm)
#    exit('norm')
#    fx,fxstep=numpy.linspace(6.53e9,6.85e9,1000,retstep=True)
#    fx=numpy.linspace(6.69e9,6.72e9,1000)
#    fx,fxstep=numpy.linspace(6.02e9,7.02e9,1000,retstep=True)
#    fx=numpy.linspace(6.7e9,6.9e9,1000)
    lor=vna.opts['wiremap'].lor
    bw=vna.opts['chassis'].fsample
    if clargs.bandwidth is None:
        fbw=bw
    else:
        fbw=clargs.bandwidth
    if clargs.qubitid is None:
        if clargs.centerfreq is None:
            fcenter=lor
        else:
            fcenter=clargs.centerfreq
    else:
        fcenter=vna.opts['qchip'].getfreq(clargs.qubitid+'.readfreq')
        fbw=8e6
    if clargs.amp is None:
        if clargs.qubitid is None:
            amp=1.0
        else:
            amp=vna.opts['qchip'].gates[clargs.qubitid+'read'].pulses[0].amp
    else:
        amp=clargs.amp
    if clargs.t0 is None:
        if clargs.qubitid is None:
            t0=vna.opts['qchip'].gates['vna'+'read'].pulses[1].t0
        else:
            t0=vna.opts['qchip'].gates[clargs.qubitid+'read'].pulses[1].t0
    else:
        t0=clargs.t0
#        amp=vna.opts['qchip'].paradict['Gates'][sys.argv[1]+'read'][0]['amp']
#        fbw=3e6
    #fx=numpy.linspace(6.2e9,6.7e9,2000)
    fx=numpy.linspace(fcenter-fbw/2,fcenter+fbw/2,1000)
    #vna.seqs(fx,t0=608e-9,amp=0.5)
    #vna.seqs(fx,t0=540e-9,amp=amp)
    vna.seqs(fx,t0=t0,amp=amp)
    #vna.seqs(fx,t0=600e-9,amp=amp)
    print('amp',amp)
#    vna.qubicrun.setvat(vatdevice='readvat',val=20)
    if clargs.live:
        pyplot.ion()
    figs11=pyplot.figure('s11',figsize=(15,8))
    axs11=figs11.subplots(3,2)
    livecnt=0
    while clargs.live or livecnt==0:
        livecnt=livecnt+1
        vna.run(clargs.nshot)
        print('adcminmax',vna.opts['chassis'].adcminmax())
        if clargs.live:
            figs11=pyplot.figure('s11')
            pyplot.clf()
            axs11=figs11.subplots(3,2)
        vna.iqapplot(axs11)
        if clargs.live:
            pyplot.draw()
            pyplot.pause(0.1)
        else:
            if clargs.noplot:
                pass
            else:
                pyplot.show()

    dt=vna.fitdt()
    print('dt estimate: delay=current','+' if dt<0 else '-', abs(dt))
    #print(vna.accresult['accval'])
    if clargs.qubitid is not None:
        s11fit=vna.fits11()
#        print(s11fit)
        print(s11fit['popt'])
        (A,R,Q,f0,t0,a,b)=s11fit['popt']
        print(A,R,Q,f0,t0,a,b)
        figfit=pyplot.figure('fit')
        axfit=figfit.subplots(2,1)
        axfit[0].plot(vna.fx,abs(vna.s11))
        axfit[0].plot(vna.fx,abs(s11fit['yfit']))
        axfit[1].plot(vna.fx,numpy.angle(vna.s11))
        axfit[1].plot(vna.fx,numpy.angle(s11fit['yfit']))
    pyplot.ioff()
    if clargs.savefig:
        tstr=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S')
        #title=('%s_%s'%(clargs.qubitid,'vna_ef' if vna.ef else 'vna'))
        title=('%s_%s'%(clargs.qubitid,'vna'))
        pyplot.savefig(title+'.pdf')
        #pyplot.savefig('fig1.pdf')
    peaks=vna.s11peaks()
    #print([peaks[k] for k in ['fxc','fscan','ftot']])
    figpeak=pyplot.figure('peak',figsize=(15,8))
    axpeak=figpeak.subplots()
    axpeak.plot(vna.fx[0:len(vna.phdiff)],vna.phdiff)
    axpeak.plot(vna.fx[vna.peakindex],vna.phdiff[vna.peakindex],'x')
    for i,(left,right) in enumerate(peaks['fscan']):
        #print(left,right)
        pyplot.hlines(xmin=left,xmax=right,y=vna.phdiff.std(),color='r',linewidth=3)
#    fig3=pyplot.figure(figsize=(8,8))
#    ax3=fig3.subplots()
#    vna.iqplot(ax3,hexbin=False,marker='.')
    if clargs.noplot:
        pass
    else:
        pyplot.show()

