import re
import os
import datetime
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment
from qubic.qcvv.plot import plot
from qubic.qcvv.fit import fit
from qubic.qubic import heralding
class c_rabixtalk(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
    def baseseqs(self,qubitid,**kwargs):
        tomogatemap={'x':'Y-90','y':'X90','z':None}
        self.tomogatemaprev={v:k for k,v in tomogatemap.items()} 
        opts=dict(qdrvqubitid=None,qdrvdest=None,readqubitid=[],tomoaxis=['z'],ef=False,trabis=None,arabi=None,gatename='rabi')
        opts.update({k:v for k,v in kwargs.items() if k in opts})
        self.debug(4,'kwargs',kwargs)
        if 'heraldcmds' not in kwargs:
            self.opts['heraldcmds']=heralding.heralding(qubits=opts['readqubitid'])
        else:
            self.opts['heraldcmds']=kwargs['heraldcmds']
        self.debug(4,'rabixtalk baseseq opts',opts)
        self.opts['tomogate']=[tomogatemap[i] for i in opts['tomoaxis']]
        seqs=[]
        for trabi in opts['trabis']:
#        for trabi in len(opts['trabis'])*[8e-9]:#opts['trabis']:#debug 4 ns
            for tomogate in self.opts['tomogate']:
                rabimodi={}
                rabimodi.update({} if opts['qdrvdest'] is None else {'dest':opts['qdrvdest']+('' if re.match('.*\.qdrv',opts['qdrvdest']) else '.qdrv')})
                rabimodi.update({} if opts['arabi'] is None else {'amp':opts['arabi']})
                #print('trabi',trabi, opts['trabis'])
                rabimodi['twidth']=trabi
                seq=[]
                seq.append({'name': opts['gatename'], 'qubit': opts['qdrvqubitid'],'modi' : [rabimodi]})
                #print('readqubitid',opts['readqubitid'])
                seq.append({'name': 'barrier', 'qubit': opts['readqubitid']})
                for qubitid in opts['readqubitid']:
                    if tomogate is not None:
                        seq.append({'name': tomogate, 'qubit': qubitid})
                #seq.append({'name': 'barrier', 'qubit': opts['readqubitid']})
                for qubitid in opts['readqubitid']:
                    seq.append({'name': 'read', 'qubit': qubitid})
                seq.append({'name': 'barrier', 'qubit': opts['readqubitid']})
                seqs.append(seq)
                self.debug(4,'seq', seq)
        #print('seqs',seqs)
        return seqs

    def runseqs(self,nsample,**kwargs):
        self.debug(4,'runseqs kwargs',kwargs)
        self.opts.update(kwargs)
        para=dict(qubitid=None,elementlength=80,elementstep=4e-9,qdrvqubitid=None,qdrvdest=None,readqubitids=[],tomoaxis=['z'],ef=False,trabis=None,startlength=0)
        para.update({k:v for k,v in self.opts.items() if k in para})
        self.opts['trabis']=numpy.arange(para['startlength'],para['elementlength']+para['startlength'])*para['elementstep']
        self.opts['nsample']=nsample
        self.x=self.opts['trabis']
        self.accvals={}
        for readqubitid in [self.opts['readqubitids'][i:min(i+3,len(self.opts['readqubitids']))] for i in range(0,len(self.opts['readqubitids']),3)]:
            self.opts['chassis'].membufreset()
            self.opts['readqubitid']=readqubitid
            self.opts['seqs']=self.baseseqs(**self.opts)
#        self.cmdlists=qubicrun.compile(**self.opts)
#            print(self.opts['seqs'])
            self.compile(**kwargs)
            self.accbufrun(includegmm=False,**self.opts)
            self.accvals.update(self.accval)
        self.gmmcount(accval=self.accvals)
        tomocnt=len(self.opts['tomogate'])
        if self.opts['tomogate']==[]:
            self.opts['tomogate'].append('z')
        self.psingle={}
        #print('RBXT',self.accresult['psinglecorr'])
        #print('RBXT',self.accresult['pcombinecorr'])
        for q,pq in self.accresult['countsum']['psingle'].items():
            self.psingle[q]={}
            for l,pql in pq.items():
                self.psingle[q][l]={}
                pqlt=pql.reshape((-1,tomocnt if tomocnt > 0 else 1))
                for it,t in enumerate(self.opts['tomogate']):
                    self.psingle[q][l][t]=pqlt[:,it]
#        gmixs=self.accresult['gmixs'][self.opts['qubitid']]
#        self.debug(2,'mean',gmixs.means_)
#        self.debug(2,'covariances',gmixs.covariances_)
#        separation=gmixs.separation()#numpy.sqrt(numpy.sum(numpy.diff(gmixs.means_.T)**2)/numpy.sqrt(numpy.product(gmixs.covariances_)))
#        self.result['separation']= separation[list(separation.keys())[0]] if len(separation)==1 else separation


    def fit(self):
        try:
            est=fit.sinestimate(x=self.x,y=self.psingle[self.opts['qubitid']]['1'])
        except:
            est=None
        fitpara=fit.fitsin(x=self.x,y=self.psingle[self.opts['qubitid']]['1'],p0=est)
        self.yfit=fitpara['yfit']
        self.result.update(dict(amp=fitpara['popt'][0],period=1.0/fitpara['popt'][1],err=fitpara['pcov']))
        return self.result

    def psingleplot(self,qubitid,fig):
        return plot.psingleplot(x=self.x,psingle=dict(qubitid=self.psingle[qubitid]),fig=fig,marker='.',linestyle=None,linewidth=0)
    def plottyfit(self,fig):
        fig.plot(self.x,self.yfit)
        return fig
    #def gmmplot(self,fig):
    #    plot.gmmplot(gmixs=self.accresult['gmixs'],fig=fig)
    #    return fig

    #def iqplot(self,fig):
    #    accvaliq=self.accresult['accval'][self.opts['qubitid']]
    #    return plot.iqplot(iq=accvaliq,fig=fig)
    def savegmm(self):
        for q in self.accresult['gmixs']:
            numpy.savez('%s_gmix.npz'%q,**self.accresult['gmixs'][q].modelpara())

if __name__=="__main__":
    ef=False
    #rabi=c_rabi(qubitid=sys.argv[1],calirepo='../../../../qchip',debug=False,gmixs='.')
    #rabi.seqs_a(asteps=48,trabi=32e-9,ef=True)
    #rabi.run(50)
    qubits=['Q%d'%i for i in [1,2,3,4,5,7]]
    qubits=['Q%d'%i for i in range(8)]
    qubits=['Q5','Q2']#%i for i in range(8)]
    #qubits=['Q4','Q5']
    # readout xtalk
    readxtalk={}
    qdrvxtalk={}
    nsample=100
    t0=datetime.datetime.now()
    rabixtalk=c_rabixtalk(qubitid=qubits,calirepo='../../../../qchip',debug=3)#,gmixs=None)
    if 1:  # read out cross talk
        for qdrvqubitid in qubits:
            for readqubitid in qubits:
                #if 1:
                rabixtalk.runseqs(elementlength=80,elementstep=8e-9,qdrvqubitid=qdrvqubitid,qdrvdest=None,readqubitids=[readqubitid],tomoaxis=['z'],ef=ef,nsample=nsample,combineorder=qubits,overlapcheck=True,heraldcmds=None)
                readxtalk[(qdrvqubitid,readqubitid)]=rabixtalk.accvals
                t1=datetime.datetime.now()
                sys.stdout.flush()
        t0=datetime.datetime.now()
        numpy.savez('readxtalkaccvals.npz',readxtalk)
    # qdrv xtalk
    if 1:
        for qdrvqubitid in qubits:
            for qdrvdest in qubits:
                rabixtalk.runseqs(elementlength=80,elementstep=8e-9,qdrvqubitid=qdrvqubitid,qdrvdest=qdrvdest+'.qdrv',readqubitids=[qdrvqubitid],tomogate=[None],ef=ef,nsample=nsample,combineorder=[qdrvqubitid],heraldcmds=None)
                qdrvxtalk[(qdrvqubitid,qdrvdest)]=rabixtalk.accvals
                t1=datetime.datetime.now()
                print('qdrvxtalk',qdrvqubitid,qdrvdest,t1-t0)
                sys.stdout.flush()
        numpy.savez('qdrvxtalkaccvals.npz',qdrvxtalk)
#    rabixtalk.runseqs(elementlength=20,elementstep=4e-9,qdrvqubitid='Q2',qdrvdest='Q3.qdrv',readqubitids=qubits,tomogate=[None],ef=ef,nsample=50,combineorder=qubits)#,heraldcmds=None)
    #rabi.fit()
    #if rabi.opts['gmixs'] is None:
    #    rabi.savegmm()
    #print('result',rabi.result)
#    fig=pyplot.figure()
#    ax=fig.subplots(2,4)
#    for iqubit,qubit in enumerate(qubits):
#        rabixtalk.psingleplot(qubitid=qubit,fig=ax[iqubit//4,iqubit%4])
#        ax[iqubit//4,iqubit%4].set_ylim(-0.1,1.1)
#    fig,(sub1,sub2)=pyplot.subplots(2,1)
#    fig.suptitle(sys.argv[1])
#    rabi.psingleplot(sub1)
##    rabi.plottyfit(sub1)
#    rabi.iqplot(sub2)
#    rabi.gmmplot(sub2)
##pyplot.savefig('fig1.pdf')
    pyplot.show()
