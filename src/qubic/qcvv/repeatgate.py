import os
import datetime
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment 
from qubic.qcvv.plot import plot
from qubic.qcvv.fit import fit
import scipy.optimize

class c_repeatgate(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
        self.famptry=[]
    def baseseqs(self,qubitid,gate,amps,repeat,**kwargs):
        self.debug(4,'baseseqs',qubitid,gate,amps,repeat)
        seqs=[]
        for amp in amps:
            seq=[]
            for r in range(repeat):
                seq.append({'name': gate, 'qubit': qubitid,'modi':[{"amp":amp}]})
            seq.append({'name': 'read', 'qubit': qubitid})
            seqs.append(seq)
        return seqs

    def scanseqs(self,nsample,repeat,ampchangeratio=None,ampchange=None,**kwargs):
        self.opts.update(kwargs)
#        amp0=self.opts['qchip'].gates[self.opts['qubitid']+'X90'].pulses[0].amp
        amp0=self.opts['qchip'].gates[self.opts['qubitid'][0]+self.opts['gate']].pulses[0].amp
        self.opts['seqs']=[]
        if ampchange is None:
            self.ampchange=amp0*ampchangeratio
        else:
            self.ampchange=ampchange
        self.debug(4,'scanseqs ampchange',ampchange)
        self.opts['seqs'].extend(self.baseseqs(qubitid=self.opts['qubitid'][0],gate=self.opts['gate'],amps=self.ampchange,repeat=repeat))
        self.run(nsample)
        self.psingle1=self.accresult['countsum']['psingle'][self.opts['qubitid'][0]]['1']
        est=[(min(self.psingle1)-max(self.psingle1))/(max(self.ampchange)-min(self.ampchange))**2,self.ampchange[int(len(self.ampchange)/2)],1]
        fitpara=fit.fitquadratic(self.ampchange,self.psingle1,p0=est)
        self.debug(4,'repeatgate fitpara',fitpara)
        if self.opts['plot']:
            print('pulse3')
            pyplot.figure('repeatgate optimize each')
            pyplot.clf()
            print('pulse4')
            pyplot.plot(self.ampchange,self.psingle1,'*')
            pyplot.plot(self.ampchange,fitpara['yfit'])
            print('pulse5')
            pyplot.draw()
            print('pulse6')
            pyplot.figure('repeatgate optimize all %s'%self.opts['qubitid'][0])
            pyplot.plot(self.ampchange,self.psingle1,'*')
            pyplot.plot(self.ampchange,fitpara['yfit'])
            print('pulse7')
#            pyplot.draw()
            print('pulse8')
#            pyplot.show(block=False)
            print('pulse9')
            pyplot.pause(0.001)
            print('pulse10')

        print('sigma',numpy.sqrt(numpy.diag(fitpara['pcov'])) if fitpara['popt'] is not None else None)
        print('popt',fitpara['popt'])
        print('pcov',fitpara['pcov'])
        return fitpara['popt'][1] if fitpara['popt'] is not None else None


        

    def run(self,nsample):
        self.compile(overlapcheck=False)
        experiment.c_experiment.accbufrun(self,nsample=nsample)
#        self.x90y180=self.accresult['countsum']['psingle'][self.opts['qubitid']]['0'][0::2]
#        self.y90x180=self.accresult['countsum']['psingle'][self.opts['qubitid']]['0'][1::2]


    def optimizesetup(self,**kwargs):
        opts=dict(valkey='1',gate='X90',repeat=8,initspan=0.1,ngatemult=4,ngateadd=2,xtol=0.01,xsteps=50)
        opts.update(kwargs)
        self.opts.update(opts)
        self.ampchange=[]
        self.vals={}
    def optimize(self,nsample,nsteps=7,**kwargs):
        if self.opts['plot']:
            pyplot.figure('repeatgate optimize all %s'%self.opts['qubitid'][0])
            pyplot.title(self.opts['qubitid'][0])
            pyplot.ion()
            pyplot.clf()
#            pyplot.show()
            pyplot.figure('repeatgate optimize each')
            pyplot.clf()
#            pyplot.show()
            pyplot.pause(0.1)
            pyplot.show(block=False)
            print('pulse0')
        self.optimizesetup(**kwargs)
        self.nsample=nsample
        acenter=self.opts['qchip'].gates[self.opts['qubitid'][0]+self.opts['gate']].pulses[0].amp
        aspan=acenter*2.0/3.0#self.opts['initspan']
        self.opts['repeat']=self.opts['ngateadd']
        x1=acenter-aspan/2
        x2=acenter+aspan/2
        for istep,step in enumerate(range(nsteps)):
            #            acenter=self.optimizestep(nsample=nsample,acenter=acenter,aspan=aspan)
            self.debug(4,'optimize step',x1,x2,self.opts['xtol'],acenter)
            self.debug(4,'optimize val n,x1,x2=[',istep,',',x1,',',x2,']')#,self.opts['xtol'],acenter)
#            acenter=scipy.optimize.fminbound(func=self.funamp,x1=x1,x2=x2,xtol=self.opts['xtol'],disp=3)
            scan=True
            failcnt=0
            print('pulse1')
            while scan and failcnt<5:
                acenter=self.scanseqs(nsample=nsample,repeat=self.opts['repeat'],ampchange=numpy.linspace(x1,x2,self.opts['xsteps']))
                print('pulse2')
                if acenter is not None:
                    if acenter >x1 and acenter<x2:
                        self.debug(4,'optimize step:',istep,step,acenter)
                        aspan=aspan/2
                        x1=acenter-aspan/2
                        x2=acenter+aspan/2
                        self.opts['xtol']=self.opts['xtol']/2
                        self.opts['repeat']=self.opts['ngatemult']*2**istep+self.opts['ngateadd']
                        scan=False
                    else:
                        failcnt+=1
                        self.debug(0,'fit error cnt',failcnt)
                        scan=True
                else:
                    failcnt+=1
                    self.debug(0,'fit error cnt',failcnt)
                    scan=True

        updatedict={('Gates',self.opts['qubitid'][0]+'X90',0,'amp'):acenter}
        updatedict.update({('Gates',self.opts['qubitid'][0]+'rabi',0,'amp'):acenter})
        if self.opts['plot']:
            pyplot.ioff()
        return updatedict


    def plot(self,fig):
        for k,v in self.vals.items():
            fig.plot(self.ampchange,v,label=k)
            #fig.plot(v,label=k)
        fig.legend()





if __name__=="__main__":
    t0=datetime.datetime.now()
    repeatgate=c_repeatgate(qubitid=sys.argv[1],calirepo='../../../../qchip',debug=2,plot=True)
    repeatgate.optimizesetup()
    #repeatgate.seqs(ampchangeratio=numpy.arange(0.9,1.5,0.001))
    if 0:
        #repeatgate.scanseqs(nsample=50,ampchange=numpy.arange(0.1,0.4,0.005))
        
        #n,x0,x1=[6,0.21882286647268887, 0.2205549223277807]
        n,x1,x2=[ 5 , 0.21828315002655385 , 0.22174726173673753 ]
        n,x1,x2=[ 9 , 0.21874029503147158 , 0.22047235088656342 ]
        n,x1,x2=[ 8 , 0.22014166769149143 , 0.22057468165526437 ]
    #    n,x1,x2=[ 0 , 0.1662773620888161 , 0.2771289368146935 ]
    #    n,x1,x2=[ 1 , 0.20269209170408523 , 0.25811787906702394 ]
    #    n,x1,x2=[ 2 , 0.21500415191155525 , 0.2427170455930246 ]
    #    n,x1,x2=[ 3 , 0.2134385083112822 , 0.22729495515201686 ]
    #    n,x1,x2=[ 4 , 0.2161004281590826 , 0.22302865157944995 ]
    #    n,x1,x2=[ 5 , 0.21865024985943082 , 0.2221143615696145 ]
    #    n,x1,x2=[ 6 , 0.21910739486434855 , 0.2208394507194404 ]
        n,x1,x2=[ 7 , 0.2197448502894356 , 0.22061087821698153 ]
    #    n,x1,x2=[ 8 , 0.22014166769149143 , 0.22057468165526437 ]
        r=4*2**n+2
        repeatgate.scanseqs(nsample=1000,ampchange=numpy.linspace(x1, x2,50),repeat=r)
#    print(repeatgate.accresult['countsum']['vsingle'])
    
#    print(repeatgate.accresult['countsum'])
    #    print(repeatgate.fit())
    else:
        updatedict=repeatgate.optimize(nsample=10,nsteps=9)
        print(updatedict)
        repeatgate.opts['qchip'].updatecfg(updatedict,repeatgate.opts['qubitcfgfile'])
    sys.stdout.flush()
#    fig1=pyplot.figure(figsize=(15,15))
#    sub=fig1.subplots(1,1)
#    repeatgate.plot(fig=sub)
    ##repeatgate.plottyfit(fig=sub)
    t1=datetime.datetime.now()
    print('repeatgate time',t1-t0)
    pyplot.show()

