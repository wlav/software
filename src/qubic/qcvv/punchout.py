import itertools
import os
import datetime
from matplotlib import pyplot
import numpy
import sys
import time
sys.path.append('../..')
from qubic.qubic import experiment
from qubic.qcvv.plot import plot
#from qubic.instrument import stepvat
#envset,qubicrun,heralding

#def stepvat(atten,device,debug=True):
#    print(atten)

class c_punchout(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
    def baseseq(self,qubitid,ef=False,amp=None,**kwargs):#,repeat=0):
        if 'c_expression' not in sys.modules:
            from qubic.qubic.expression import c_expression
        f=c_expression('f','f')

        rdrvmodi={'fcarrier':f,'twidth':4e-6}
        rdrvmodi.update(dict(amp=amp) if amp is not None else dict(amp=1))
        readmodi={'fcarrier':f,'twidth':4e-6}
        #readmodi0={'fcarrier':f,'twidth':0e-6,'t0':608e-9}
        seq=[]
        #for r in range(repeat):
        #    seq.append({'name': 'read', 'qubit': qubitid,'modi' : [rdrvmodi,readmodi0]})
        if ef:
            seq.append({'name': 'X90', 'qubit': qubitid})
            seq.append({'name': 'X90', 'qubit': qubitid})
        seq.append({'name': 'read', 'qubit': qubitid,'modi' : [rdrvmodi,readmodi]})
        return seq

    def seqs(self,fx,amp,**kwargs):#,repeat=0,**kwargs):
        self.opts.update(kwargs)
        self.opts['heraldcmds']=None
        self.opts['delaybetweenelement']=4e-6
        self.fx=fx
        self.opts['gmixs']=None
        self.opts['para']=[dict(f=f) for f in fx]
        #self.opts['seqs']=[self.baseseq(qubitid=self.opts['qubitid'],amp=amp,**self.opts)]#,repeat=repeat)]
        self.opts['seqs']=[self.baseseq(amp=amp,**self.opts)]#,repeat=repeat)]
        self.compile()#cmdlists=qubicrun.compile(**self.opts)
        self.debug(4,'debug punchout seqs')
    def run(self,nshot,fx,ef=False,attens=None,maxvatatten=30):#amps=None):
        self.ef=ef
        self.debug(4,'debug c_punchout run, gmixs',self.opts['gmixs'])
        self.attens=attens
        #self.amps=amps
        self.fx=fx
        self.s11=numpy.zeros((len(self.attens),len(fx)),dtype='object')
        for iatten,atten in enumerate(attens):
            self.debug(4,'atten',atten)
            if atten>-maxvatatten:
                amp=1.0
                vatatten=-atten
#                self.seqs(fx,amp=None)
#                self.qubicrun.setrdrvvat(-atten)
                self.debug(4,'by stepvat',atten)
            else:
                vatatten=maxvatatten
                ampatten=atten+maxvatatten
                amp=10**(ampatten/20.0)
                self.debug(4,'by amp',ampatten,amp)
            self.qubicrun.setrdrvvat(vatatten)
            self.seqs(fx,amp=amp,ef=ef)#,repeat=10)

            time.sleep(0.1)
            experiment.c_experiment.accbufrun(self,nshot)
            self.debug(1,'adcminmax',self.opts['chassis'].adcminmax())
            s11=self.accresult['accval'][self.opts['qubitid'][0]].mean(axis=0)#.reshape((len(self.amps),-1))
            self.s11[iatten,:]=s11
        self.qubicrun.setrdrvvat(finish=True)
        ny=len(attens)
        ystep=numpy.diff(attens).mean()
        self.y=numpy.zeros(ny+1)
        self.y[0:-1]=attens-ystep/2
        self.y[-1]=attens[-1]+ystep/2
        self.yorig=attens
#        if attens is not None:
#            self.s11=numpy.zeros((len(self.attens),len(fx)),dtype='object')
#            self.seqs(fx,amp=None)
#            for iatten,atten in enumerate(attens):
#                #stepvat.stepvat(atten=atten,device=self.opts['wiremap'].stepvatdevid)
#                self.qubicrun.setrdrvvat(-atten)
#                time.sleep(0.1)
#                experiment.c_experiment.accbufrun(self,nshot)
#                s11=self.accresult['accval'][self.opts['qubitid']].mean(axis=0)#.reshape((len(self.amps),-1))
#                self.s11[iatten,:]=s11
#            self.qubicrun.setrdrvvat(finish=True)
#            ny=len(attens)
#            ystep=numpy.diff(attens).mean()
#            self.y=numpy.zeros(ny+1)
#            self.y[0:-1]=attens-ystep/2
#            self.y[-1]=attens[-1]+ystep/2
#            self.yorig=attens
#        elif amps is not None:
#            self.s11=numpy.zeros((len(amps),len(fx)),dtype='object')
#            for iamp,amp in enumerate(amps):
#                print('iamp',iamp,amp)
#                self.seqs(fx,amp=amp)
#                experiment.c_experiment.accbufrun(self,nshot)
#                s11=self.accresult['accval'][self.opts['qubitid']].mean(axis=0)#.reshape((len(self.amps),-1))
#                self.s11[iamp,:]=s11
##                seqs.append(seq(qubitid=self.opts['qubitid'],amp=amp))
#            ny=len(amps)
#            ystep=numpy.diff(amps).mean()
#            self.y=numpy.zeros(ny+1)
#            self.y[0:-1]=amps-ystep/2
#            self.y[-1]=amps[-1]+ystep/2
#            self.yorig=amps
        self.s11=self.s11.reshape((ny,len(self.fx))).astype('complex64')
        self.amp=abs(self.s11)
        self.ang=numpy.zeros(self.s11.shape,dtype=float)
        for l in range(ny):
            numpy.angle(self.s11[l,:].astype('complex64'))
            self.ang[l,:]=numpy.unwrap(numpy.angle(self.s11[l,:]))

        nshift=5
        self.ampdiffshift=self.offsetdiffaxis1(self.amp,offset=nshift)
        self.angdiffshift=self.offsetdiffaxis1(self.ang,offset=nshift)
        fxstep=numpy.diff(self.fx).mean()
        self.x=numpy.zeros(len(self.fx)+1)
        self.x[:-1]=self.fx-fxstep/2
        self.x[-1]=self.fx[-1]+fxstep/2
        self.xshift=self.x[nshift:]
        self.maxangdiffshiftx=self.xshift[numpy.argmax(self.angdiffshift,axis=1)]

#        m,n=self.s11.shape
#        for l in range(m):
#            print(self.s11[l,:])
#            print(type(self.s11[l,:]))
#            print(numpy.angle(self.s11[l,:]))
#            print(numpy.unwrap(numpy.angle(self.s11[l,:])))
#            self.ang[l]=numpy.unwrap(numpy.angle(numpy.array(self.s11[l,:])))


    def fit(self):
        pass

    def plotamp(self,fig):
        self.debug(4,'amp',self.x,self.y,self.amp,self.amp.shape)
        fig.pcolormesh(self.x,self.y,self.amp,cmap='RdBu_r')
        return fig
    def plotang(self,fig):
        self.debug(4,'ang',self.x,self.y,self.ang,self.ang.shape)
        fig.pcolormesh(self.x,self.y,self.ang,cmap='RdBu_r')
        return fig
    def plotampdiff(self,fig):
        self.debug(4,'ampdiff',self.x,self.y,self.amp,self.amp.shape)
        fig.pcolormesh(self.xshift,self.y,self.ampdiffshift,cmap='RdBu_r')
        return fig
    def plotangdiff(self,fig):
        self.debug(4,'angdiff',self.x,self.y,self.ang,self.ang.shape)
        fig.pcolormesh(self.xshift,self.y,self.angdiffshift,cmap='RdBu_r')

        return fig
    def plotmaxangdiff(self,fig):
        fig.plot(self.yorig,self.maxangdiffshiftx)
        return fig
    def offsetdiffaxis1(self,array,offset):
        a0=array[:,offset:]
        a1=array[:,:-offset]
        return a1-a0



if __name__=="__main__":
    import sys
    sys.path.append('../..')
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(help='qubitid list',dest='qubitid',default='vna')
    parser.add_argument('-ef','--ef',help='use ef',dest='ef',default=False,action='store_true')
    parser.add_argument('-c','--calipath',help='path to calibration repo',dest='cpath',default='../../../../qchip',type=str)
    parser.add_argument('-n','--nshot',help='number of shot',dest='nshot',default=100,type=int)
    parser.add_argument('-np','--noplot',help='no plot',dest='noplot',default=False,action='store_true')
    parser.add_argument('-s','--savefig',help='save figure plot',dest='savefig',default=False,action='store_true')
    clargs=parser.parse_args()
    punchout=c_punchout(qubitid=clargs.qubitid,calirepo=clargs.cpath,debug=2)
#    fcenter=6699598761.705519
    fcenter=6698758761.705519
    fcenter=punchout.opts['qchip'].getfreq(clargs.qubitid+'.readfreq')
    amp=punchout.opts['qchip'].gates[clargs.qubitid+'read'].pulses[0].amp
    fr=punchout.opts['qchip'].getfreq(clargs.qubitid+'.readfreq')
#    fcenter=6.52e9
    fbw=6e6
    fstart=fcenter-fbw/2
    fstop=fcenter+fbw/2
    fx=numpy.linspace(fstart,fstop,200)
    #amps=numpy.linspace(0.1,1,10)
    #punchout.run(20,fx=fx,amps=amps)#attens=range(0,30,3))
    punchout.run(clargs.nshot,fx=fx,attens=numpy.arange(-55,0.2,3.0),maxvatatten=0,ef=clargs.ef)
    #punchout.run(10,fx=fx,attens=numpy.arange(-30,0.1,15))
    #punchout.run(3,fx=fx,attens=numpy.arange(0,30,5.0))
    fig1=pyplot.figure(figsize=(10,10))
    sub=fig1.subplots(2,2)
    fig1.suptitle('%s_%s'%(clargs.qubitid,'punchout_ef' if clargs.ef else 'punchout'))
    print('sub',sub)
    punchout.plotamp(sub[0,0])
    sub[0,0].set_title('Amplitude')
#    sub[0,0].set_xlabel('Frequency(Hz)')
#    sub[0,0].set_ylabel('Readout drive power (dBFS)')
    punchout.plotang(sub[0,1])
    sub[0,1].set_title('Phase')
#    sub[0,1].set_xlabel('Frequency(Hz)')
#    sub[0,1].set_ylabel('Readout drive power (dBFS)')
    punchout.plotampdiff(sub[1,0])
    sub[1,0].set_title('diff(Amplitude)')
#    sub[1,0].set_xlabel('Frequency(Hz)')
#    sub[1,0].set_ylabel('Readout drive power (dBFS)')
    punchout.plotangdiff(sub[1,1])
    sub[1,1].set_title('diff(Phase)')
    vatattn=punchout.opts['wiremap'].ttydev['rdrvvat']['default'] if 'rdrvvat' in punchout.opts['wiremap'].ttydev else 0
    for subc in sub:
        for subi in subc:
            subi.set_xlabel('Frequency(Hz)')
            subi.set_ylabel('Readout drive power (dBFS)')
            subi.axhline(y=20*numpy.log10(amp)-vatattn,color='black')
            subi.axvline(x=fr,color='black')
#    sub[0,1].axhline(y=20*numpy.log10(amp)-vatattn)
#    sub[1,0].axhline(y=20*numpy.log10(amp)-vatattn)
#    sub[1,1].axhline(y=20*numpy.log10(amp)-vatattn)
    
    if clargs.savefig:
        tstr=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S')
        title=('%s_%s'%(clargs.qubitid,'punchout_ef' if punchout.ef else 'punchout'))
        pyplot.savefig(title+'.pdf')
    fig2=pyplot.figure()
    sub2=fig2.subplots()
    punchout.plotmaxangdiff(sub2)
    fig2.suptitle(clargs.qubitid)
    fig3=pyplot.figure()
    ax3=fig3.subplots(2,2)
    ax3[0,0].plot(punchout.s11.real.T,punchout.s11.imag.T)
    ax3[1,0].plot(abs(punchout.s11.T))
    ax3[1,1].plot(numpy.angle(punchout.s11.T))

    numpy.savez('punchoutdata_%s.npz'%clargs.qubitid,i=punchout.s11.real.T,q=punchout.s11.imag.T,attens=punchout.attens,fx=punchout.fx)

    #punchout.iqplot(ax3,marker='.',hexbin=False)
#            ,linestyle='-',linewidth='3'

#    pyplot.savefig('punchoutfig_%s.pdf'%sys.argv[1])
    if clargs.noplot:
        pass
    else:
        pyplot.show()

