import os
import datetime
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment,zxzxz
from qubic.qcvv.plot import plot
from scipy.optimize import curve_fit
from qubic.qcvv.gatesim import gatesim


class c_crfullxy(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
    def baseseqs(self,qubitid,xyrot,zprep,**kwargs):
        #    ops=dict(elementlength=80,elementstep=4e-9)
        #ops.update(**kwargs)
        seqs=[]
        for rot in xyrot:
            #        for tomo in [tomox,tomoy,tomoz]:
            for tomo in ['X90']:#,'Y-90',None]:
                seq=[]
                seq.extend(zxzxz.zxzxz(qubitid=qubitid,zprep=zprep))
                seq.append({'name': 'CNOT', 'qubit': qubitid})
                seq.extend([{'name': 'virtualz', 'qubit': qubit, 'para': {'phase': rot}} for qubit in qubitid])
                seq.append({'name': 'barrier', 'qubit': qubitid})
                if tomo is not None:
                    seq.extend([{'name': tomo, 'qubit': qubit} for qubit in qubitid])
                seq.extend([{'name': 'read', 'qubit': qubit} for qubit in qubitid])
                seqs.append(seq)
        return seqs
    def seqs(self,xyrot,zprep,**kwargs):
        self.opts.update(kwargs)
        self.opts['seqs']=self.baseseqs(xyrot=xyrot,zprep=zprep,**self.opts)
        self.compile(overlapcheck=False)
        self.code,self.sini=gatesim.sini()
        self.zprep=zprep
        self.xyrot=xyrot
        print('xyrot size',xyrot.shape)

    def run(self,nsample,**kwargs):
        self.opts.update(kwargs)
        experiment.c_experiment.accbufrun(self,nsample=nsample,**self.opts)
        self.v=self.accresult['countsum']['pcombine']
    def plot(self):
        ylim=(0,1)
        for scode,v in self.v.items():
            self.debug(4,scode)
            self.debug(4,v)
            pyplot.plot(self.xyrot,v,label=scode)
    
#  cnot =   xx(0,pxtgt)  zz(pzctl,0)  zz(0,-pztgt) cr zz(0,pztgt) @s
#  cr   =   zz(0,pztgt) zz(-pzctl,0) xx(0,-pxtgt) cnot zz(0,-pztgt)
    def cr(self,pzctl,pztgt,pxtgt):
        return gatesim.zz(0,pztgt)@gatesim.xx(0,-pxtgt)@gatesim.zz(-pzctl,0)@gatesim.cnot()@gatesim.zz(0,-pztgt)

    def crfullxyrot(self,init,xyrot,pzctl,pztgt,pxtgt):#=[[246,147],[314,234],[336,37]]):
        s=init
        for iz,(zz1,zz2) in enumerate(self.zprep):
            if iz!=0:
                s=gatesim.xx()@s
            s=gatesim.zz(zz1,zz2)@s
        state=self.cr(pztgt=pztgt,pzctl=pzctl,pxtgt=pxtgt)@s

        xyval=numpy.zeros((len(xyrot),len(state)))
        for i,p in enumerate(xyrot):
            xyval[i,:]=gatesim.tomoxy(state,p)[:,0]
        return xyval
    def fitfun(self,xyrot,pzctl,pztgt,pxtgt):
        y=self.crfullxyrot(xyrot=xyrot,init=self.sini,pzctl=pzctl,pztgt=pztgt,pxtgt=pxtgt)
#        print('debug fitfun',y.shape,xyrot.shape)
        return y.flatten('F')

    def fit(self):
        xymeas4=numpy.zeros((len(self.xyrot),len(self.v)))
        for icode,code in enumerate(self.code):
            print(icode,code)
            xymeas4[:,icode]=self.v[code]#[:,2]
        
        ydata=xymeas4.flatten('F')
        popt,pcov=curve_fit(self.fitfun,xdata=self.xyrot,ydata=ydata,p0=(0,0,0),bounds=(([-numpy.pi,-numpy.pi,-numpy.pi],[numpy.pi,numpy.pi,numpy.pi])))#([-360,-360,-360],[360,360,360])))
        print(popt,pcov)
        xyfit=self.fitfun(self.xyrot,*popt)

        lx=len(self.xyrot)
        pyplot.figure('ydata')
        pyplot.plot(ydata,'*')
        pyplot.plot(xyfit)
        pyplot.figure('xy')
        color=['r','g','b','k']
        for i in range(4):
            yi=ydata.reshape((4,-1))[i,:]
            pyplot.plot(self.xyrot[0:lx],yi,'*',color=color[i])
            fi=xyfit.reshape((4,-1))[i,:]
            pyplot.plot(self.xyrot[0:lx],fi,color=color[i])

#        pyplot.show()
        print('popt deg',popt*180/numpy.pi,'perr',numpy.sqrt(numpy.diag(pcov))*180/numpy.pi)
        print('popt rad',popt,'perr',numpy.sqrt(numpy.diag(pcov)))

        return popt,pcov
    def optimize(self):
        gatename=''.join(self.opts['qubitid'])+'CNOT'
        pztgt0=self.opts['qchip'].gates[gatename].pulses[0].pcarrier
        pztgt1=self.opts['qchip'].gates[gatename].pulses[2].pcarrier
        pzctl0=self.opts['qchip'].gates[gatename].pulses[3].pcarrier
        txtgt0=self.opts['qchip'].gates[gatename].pulses[4].twidth
        axtgt0=self.opts['qchip'].gates[gatename].pulses[4].amp
        txtgtX90=self.opts['qchip'].gates[self.opts['qubitid'][1]+'X90'].pulses[0].twidth
        axtgtX90=self.opts['qchip'].gates[self.opts['qubitid'][1]+'X90'].pulses[0].amp
        (pzctl,pztgt,pxtgt),pcov=self.fit()
        a90=((txtgt0*axtgt0+pxtgt/(numpy.pi/2)*txtgtX90*axtgtX90)%(txtgtX90*axtgtX90*4))/txtgtX90
        n90=numpy.ceil(a90)
        updatedict={}
        updatedict.update({('Gates',gatename,0,'pcarrier'):pztgt0+pztgt})
        updatedict.update({('Gates',gatename,2,'pcarrier'):pztgt1-pztgt})
        updatedict.update({('Gates',gatename,3,'pcarrier'):pzctl0+pzctl})
        updatedict.update({('Gates',gatename,4,'twidth'):txtgtX90*n90})
        updatedict.update({('Gates',gatename,4,'amp'):a90/n90})

        return updatedict




if __name__=="__main__":
    t0=datetime.datetime.now()
    qubitid=['Q1','Q2']
    qubitid=['Q3','Q2']
    qubitid=['Q0','Q1']
    qubitid=['Q6','Q5']
    qubitid=['Q5','Q4']
    qubitid=['Q4','Q3']
    qubitid=['Q5','Q4']
    qubitid=['Q2','Q1']
    qubitid=['Q6','Q5']
    qubitid=['Q6','Q0']
    qubitid=['Q1','Q0']
    crfullxy=c_crfullxy(qubitid=qubitid,calirepo='../../../../qchip',debug=3)
    xyrot=numpy.linspace(0,2*numpy.pi,100)
    zprepdeg=numpy.array([[246,147],[314,234],[336,37]])
    zprepdeg=numpy.array([[234,156],[312,24],[36,347]])
    zprepdeg=numpy.random.rand(3,2)*360#([[234,156],[312,24],[36,347]])
#    zprepdeg=numpy.array([[358.21822282,130.08020523],[236.60560769,134.53000841],[240.9194148,233.77343943]])
#    zprepdeg=numpy.array(
#            [[358.21822282,130.08020523],[236.60560769,134.53000841],[240.9194148,233.77343943]]
#[[164.30596038,93.71911097],[32.3704529,122.02870999],[94.1665104,283.254817]]
#            )
    print('zprepdeg',zprepdeg)
 #   zprepdeg=numpy.array([[0,150],[300,200],[36,73]])
    zprep=zprepdeg*numpy.pi/180.0
    crfullxy.seqs(zprep=zprep,xyrot=xyrot)
    crfullxy.run(400,combineorder=qubitid)
    crfullxy.plot()
#    popt,pcov=crfullxy.fit()
    updatedict=crfullxy.optimize()
    print(updatedict)
    #crfullxy.opts['qchip'].updatecfg(updatedict,'../../../../qchip/X6Y3/qubitcfg_X6Y3.json')
    crfullxy.opts['qchip'].updatecfg(updatedict,crfullxy.opts['qubitcfgfile'])
    from qubic.qcvv.cnot import c_cnot
    cnot=c_cnot(qubitid=qubitid,calirepo='../../../../qchip',debug=2)#,gmixs=None)
    cnot.runseqs(nsample=200,combineorder=qubitid)#,heraldcmds=None)
    for k,v in sorted(cnot.result.items()):
        print(k,''.join(['%8.3f'%i for i in  v]))
    numpy.savez('crfullxydata.npz',xyrot=xyrot,v=crfullxy.v)
    t1=datetime.datetime.now()
    print('crfullxy time: ', t1-t0)
    pyplot.show()
