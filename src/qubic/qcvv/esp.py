import os
import datetime
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment
from qubic.qcvv.plot import plot
from qubic.qcvv.fit import fit
from qubic.qcvv.analysis import gmm
class c_esp(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)

    def baseseqs(self,qubitid,**kwargs):
        seqs=[]
        seq=[]
        seq.append({'name': 'read', 'qubit': qubitid})
        seqs.append(seq)
        seq=[]
        seq.append({'name': 'X90', 'qubit': qubitid})
        seq.append({'name': 'X90', 'qubit': qubitid})
        seq.append({'name': 'read', 'qubit': qubitid})
        seqs.append(seq)
        seq=[]
        seq.append({'name': 'X90_ef', 'qubit': qubitid})
        seq.append({'name': 'X90_ef', 'qubit': qubitid})
        seq.append({'name': 'read', 'qubit': qubitid})
        #seq.append({'name': 'X90', 'qubit': qubitid})
        #seq.append({'name': 'X90', 'qubit': qubitid})
        #seq.append({'name': 'read', 'qubit': qubitid})
        seqs.append(seq)
        seq=[]
        seq.append({'name': 'X90', 'qubit': qubitid})
        seq.append({'name': 'X90', 'qubit': qubitid})
        seq.append({'name': 'X90_ef', 'qubit': qubitid})
        seq.append({'name': 'X90_ef', 'qubit': qubitid})
        seq.append({'name': 'read', 'qubit': qubitid})
        #seq.append({'name': 'X90', 'qubit': qubitid})
        #seq.append({'name': 'X90', 'qubit': qubitid})
        #seq.append({'name': 'read', 'qubit': qubitid})
        seqs.append(seq)
        #print('seqs',seqs)
        return seqs

    def seqs(self,**kwargs):
        self.opts.update(kwargs)
        self.opts['seqs']=self.baseseqs(**self.opts)
        #self.opts['heraldcmds'] =None
        self.compile()

    def run(self,nsample,**kwargs):
        self.accbufrun(nsample=nsample,**kwargs)
        self.psingle=self.accresult['countsum']['pcombine']
        self.psinglecorr=self.accresult['pcombinecorr']
    def plot(self,fig):
        pass

if __name__=="__main__":
    esp=c_esp(qubitid=sys.argv[1],calirepo='../../../../qchip',debug=1,gmixs='./')
    esp.seqs(elementlength=80,elementstep=4e-9,delayafterheralding=0.04e-6)#)
    esp.run(10000)
    fig=pyplot.figure()
    ax=fig.subplots()
    print("I R,X180 R,I X180ef,R, X180,X180ef,R")
    print('psingle',esp.psingle)
    print('after rcal',esp.psinglecorr)
#    pyplot.show()

