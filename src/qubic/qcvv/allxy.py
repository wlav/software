import os
import datetime
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment
from qubic.qcvv.plot import plot
from qubic.qcvv.fit import fit
from qubic.qcvv.analysis import gmm
class c_allxy(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)

    def baseseqs(self,qubitid,**kwargs):
        gate={}
        gate['Z90']={'name': 'virtualz', 'qubit': qubitid,'para':{'phase':numpy.pi/2}}
        gate['Z-90']={'name': 'virtualz', 'qubit': qubitid,'para':{'phase':-numpy.pi/2}}
        gate['X90']=[{'name': 'X90', 'qubit': qubitid}]
        gate['X180']=[{'name': 'X90', 'qubit': qubitid},{'name': 'X90', 'qubit': qubitid}]
        gate['Y90']=[gate['Z-90'],gate['X90'][0],gate['Z90']]
        gate['Y180']=[gate['Z-90'],gate['X180'][0],gate['X180'][1],gate['Z90']]
        gate['I']=[]
        self.allxygates=[['I','I'],['X180','X180']
            ,['Y180','Y180']
            ,['X180','Y180']
            ,['Y180','X180']
            ,['X90','I']
            ,['Y90','I']
            ,['X90','Y90']
            ,['Y90','X90']
            ,['X90','Y180']
            ,['Y90','X180']
            ,['X180','Y90']
            ,['Y180','X90']
            ,['X90','X180']
            ,['X180','X90']
            ,['Y90','Y180']
            ,['Y180','Y90']
            ,['X180','I']
            ,['Y180','I']
            ,['X90','X90']
            ,['Y90','Y90']
            ]
        seqs=[]
        for gates in self.allxygates:
            seq=[]
            for g in gates:
                seq.extend(gate[g])
            seq.append({'name': 'read', 'qubit': qubitid})
            seqs.append(seq)
        return seqs

    def seqs(self,**kwargs):
        self.opts.update(kwargs)
        self.opts['seqs']=self.baseseqs(**self.opts)
        self.compile()

    def run(self,nsample,**kwargs):
        self.accbufrun(nsample=nsample,**kwargs)
        self.psingle=self.accresult['countsum']['psingle']
    def plot(self,fig):
        plot.psingleplot(x=range(len(self.allxygates)),psingle=self.psingle,fig=fig,marker='.',linestyle=None,linewidth=0)
        pyplot.xticks(range(21), [' '.join(gate) for gate in self.allxygates], rotation=90)
        pyplot.ylim(-0.05,1.05)
        pyplot.ylabel('Population',fontsize=15)
        pyplot.tight_layout()
        pyplot.axhline(0.0, linestyle='--', color='k')
        pyplot.axhline(0.5, linestyle='--', color='k')
        pyplot.axhline(1.0, linestyle='--', color='k')

if __name__=="__main__":
    allxy=c_allxy(qubitid=sys.argv[1],calirepo='../../../../qchip',debug=1)
    allxy.seqs(elementlength=80,elementstep=4e-9)
    allxy.run(500)
    fig=pyplot.figure()
    ax=fig.subplots()
    allxy.plot(ax)
    print(allxy.psingle)
    pyplot.show()

