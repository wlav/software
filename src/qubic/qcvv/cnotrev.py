import re
import os
import datetime
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment
from qubic.qcvv.plot import plot
from qubic.qcvv.fit import fit
from qubic.qubic import heralding
from qubic.qcvv.gatesim import gatesim
import numpy.linalg
class c_cnotrev(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
        self.gatename=''.join(self.opts['qubitid'])+'CNOT'
    def baseseqs(self,qubitid,repeat=1,**kwargs):
        seqs=[]
        for tgtinit in [0,1]:
            for ctlinit in [0,1]:
                seq=[]
                if ctlinit==1:
                    seq.append({'name': 'X90', 'qubit': qubitid[1]})
                    seq.append({'name': 'X90', 'qubit': qubitid[1]})
                if tgtinit==1:
                    seq.append({'name': 'X90', 'qubit': qubitid[0]})
                    seq.append({'name': 'X90', 'qubit': qubitid[0]})
                for r in range(repeat):
                    seq.extend([{'name': 'virtualz', 'qubit': qubit, 'para': {'phase':-numpy.pi/2}} for qubit in qubitid])
                    seq.extend([{'name': 'X90', 'qubit': qubit} for qubit in qubitid])
                    seq.extend([{'name': 'virtualz', 'qubit': qubit, 'para': {'phase':numpy.pi/2}} for qubit in qubitid])
                    seq.extend([{'name': 'X90', 'qubit': qubit} for qubit in qubitid])
                    seq.extend([{'name': 'X90', 'qubit': qubit} for qubit in qubitid])
                    seq.append({'name': 'CNOT', 'qubit': qubitid})
                    seq.extend([{'name': 'virtualz', 'qubit': qubit, 'para': {'phase':-numpy.pi/2}} for qubit in qubitid])
                    seq.extend([{'name': 'X90', 'qubit': qubit} for qubit in qubitid])
                    seq.extend([{'name': 'virtualz', 'qubit': qubit, 'para': {'phase':numpy.pi/2}} for qubit in qubitid])
                    seq.extend([{'name': 'X90', 'qubit': qubit} for qubit in qubitid])
                    seq.extend([{'name': 'X90', 'qubit': qubit} for qubit in qubitid])
                seq.extend([{'name': 'read', 'qubit': qubit} for qubit in qubitid])
                seqs.append(seq)
        return seqs

    def runseqs(self,nsample,repeat=1,**kwargs):
        self.opts['seqs']=self.baseseqs(self.opts['qubitid'],repeat=repeat)
        self.compile()
        self.accbufrun(nsample=nsample)
        self.result=self.accresult['countsum']['pcombine']
    def error(self):
        resultmatrix=numpy.zeros((4,4))
        resultmatrix[:,0]=self.accresult['countsum']['pcombine']['00']
        resultmatrix[:,1]=self.accresult['countsum']['pcombine']['01']
        resultmatrix[:,2]=self.accresult['countsum']['pcombine']['10']
        resultmatrix[:,3]=self.accresult['countsum']['pcombine']['11']
        error=numpy.linalg.norm(resultmatrix-gatesim.cnotrev())
        return error


if __name__=="__main__":
    qubitid=['Q3','Q2']
    qubitid=['Q0','Q1']
    qubitid=['Q1','Q2']
    cnotrev=c_cnotrev(qubitid=qubitid,calirepo='../../../../qchip',debug=2)#,gmixs=None)
    if 1:
        cnotrev.runseqs(nsample=200,repeat=1)#,heraldcmds=None)
        for k,v in sorted(cnotrev.result.items()):
            print(k,''.join(['%8.3f'%i for i in  v]))
