import sys
sys.path.append('../../')
from qubic.qcvv.analysis import gmm
import numpy
from matplotlib import pyplot
readxtalkdata=numpy.load('readxtalkaccvals.npz',allow_pickle=True)['arr_0'].tolist()
qdrvxtalkdata=numpy.load('qdrvxtalkaccvals.npz',allow_pickle=True)['arr_0'].tolist()
print(list(qdrvxtalkdata.keys()))
qubits=['Q%d'%i for i in range(0,8)]
#qubits=['Q%d'%i for i in [1,2,3,4,5,7]]
#qubits=['Q4','Q5']
#for data in readxtalkdata:
labels=['1','0']
countsums={}
fig1=pyplot.figure(figsize=(16,16))
ax1=fig1.subplots(8,8)
fig2=pyplot.figure(figsize=(16,16))
ax2=fig2.subplots(8,8)
ax1y2={}
if 1:
    for iread,readqubitid in enumerate(qubits):
        #    print(readxtalkdata[(readqubitid,readqubitid )][readqubitid].shape)

        if (readqubitid,readqubitid) in readxtalkdata:
            gmix=gmm.GaussianMixtureLabeled(n_components=2)
            gmix.fit_complex(readxtalkdata[readqubitid,readqubitid][readqubitid].reshape((-1,1)))
            gmix.setlabels(readxtalkdata[readqubitid,readqubitid][readqubitid][:,0],labels,mode='mintomax')
            for iqdrv,qdrvqubitid in enumerate(qubits):
                #    print(readxtalkdata[(qdrvqubitid,readqubitid)][readqubitid].shape)
                if (qdrvqubitid,readqubitid) in readxtalkdata:
                    predict={(qdrvqubitid,readqubitid):gmix.predict_complex(readxtalkdata[qdrvqubitid,readqubitid][readqubitid])}
                    countsum=gmm.countsum(predict,heraldingsymbol='0')
                    countsums[(qdrvqubitid,readqubitid)]=countsum
                    for il, l in enumerate(labels):
                        if il==0:
                            ax1[iread,iqdrv].plot(countsum['pcombine'][l],'r')
#                    if iread==iqdrv:
#                        ax1[iread,iqdrv].set_ylim((-0.1,1.1))
#                        ax1[iread,iqdrv].set_ylabel(l,color='r')
                        else:
                            ax1y2[(iread,iqdrv)]=ax1[iread,iqdrv].twinx()
                            ax1y2[(iread,iqdrv)].plot(countsum['pcombine'][l],'b')
#                    if iread==iqdrv:
#                        ax1y2[(iread,iqdrv)].set_ylim((-0.1,1.1))
    for iax,ax in enumerate(ax1[0]):
        ax.set_title('Q%d'%iax,fontsize=16)
    for iax,ax in enumerate(ax1[:,0]):
        ax.set_ylabel('Q%d'%iax,rotation=90,fontsize=16)
    fig1.suptitle('read out cross talk',fontsize=16)
    fig1.tight_layout(rect=[0,0.03,1,0.95])
    pyplot.savefig('readxtalk.pdf')
            #print(countsum)#predict[(qdrvqubitid,readqubitid)])#[readqubitid])#['psingle'])
if 1 :
    for iqdrv,qdrvqubitid in enumerate(qubits):
        #    print(readxtalkdata[(readqubitid,readqubitid )][readqubitid].shape)
        print('0',iqdrv,qdrvqubitid)
        gmix=gmm.GaussianMixtureLabeled(n_components=2)
        wrongdest=qdrvqubitid#'Q7'
        if (qdrvqubitid,wrongdest) in qdrvxtalkdata:
            gmix.fit_complex(qdrvxtalkdata[qdrvqubitid,wrongdest][qdrvqubitid].reshape((-1,1)))
            gmix.setlabels(qdrvxtalkdata[qdrvqubitid,wrongdest][qdrvqubitid][:,0],labels,mode='mintomax')
#        for idest,destqubitid in enumerate(qubits):
            for idest,destqubitid in enumerate(qubits):
                if (qdrvqubitid,destqubitid) in qdrvxtalkdata:
                    print('1',iqdrv,qdrvqubitid,idest,destqubitid)
                    #    print(readxtalkdata[(qdrvqubitid,readqubitid)][readqubitid].shape)
                    predict={(qdrvqubitid,destqubitid):gmix.predict_complex(qdrvxtalkdata[qdrvqubitid,destqubitid][qdrvqubitid])}
                    countsum=gmm.countsum(predict,heraldingsymbol='0')
                    for l in labels:
                        ax2[iqdrv,idest].plot(countsum['pcombine'][l])
                        ax2[iqdrv,idest].set_ylim((-0.1,1.1))
                        ax2[iqdrv,idest].set_title('drive %s read %s'%(qdrvqubitid,destqubitid),fontsize=5)
            #print(countsum)#predict[(qdrvqubitid,readqubitid)])#[readqubitid])#['psingle'])
    for iax,ax in enumerate(ax2[0]):
        ax.set_title('Read Q%d'%iax,fontsize=16)
    for iax,ax in enumerate(ax2[:,0]):
        ax.set_ylabel('Drive Q%d'%iax,rotation=90,fontsize=16)
    fig2.suptitle('qubit drive cross talk',fontsize=16)
    fig2.tight_layout(rect=[0,0.03,1,0.95])
pyplot.show()
