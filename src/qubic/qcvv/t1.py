import os
import datetime
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment 
from qubic.qcvv.plot import plot
from qubic.qcvv.fit import fit

class c_t1(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
    def baseseqs(self,qubitid,**kwargs):
        opts=dict(elementlength=80,elementstep=4e-6,ef=False)
        opts.update(**{k:v for k,v in kwargs.items() if k in opts})
        self.ef=opts['ef']

        seqs=[]
        t=numpy.arange(opts['elementlength'])*opts['elementstep']
        for tdelay in t:
            seq=[]
            seq.append({'name': 'X90', 'qubit': qubitid})
            seq.append({'name': 'X90', 'qubit': qubitid})
            if self.ef:
                seq.append({'name': 'X90_ef', 'qubit': qubitid})
                seq.append({'name': 'X90_ef', 'qubit': qubitid})
            seq.append({'name': 'delay', 'qubit': qubitid, 'para': {'delay': tdelay}})
            seq.append({'name': 'read', 'qubit': qubitid})
            seqs.append(seq)
        return t,seqs


    def seqs(self,**kwargs):
        self.opts.update(kwargs)
        self.debug(4,'t1 opts',self.opts)
        self.t,self.opts['seqs']=self.baseseqs(**self.opts)
        self.compile()

    def run(self,nsample):
        experiment.c_experiment.accbufrun(self,nsample=nsample)
        self.psingle=self.accresult['countsum']['psingle']
    def plot(self,fig):
        plot.psingleplot(x=self.t,psingle=self.accresult['countsum']['psingle'],fig=fig)
        return fig
    def iqplot(self,fig):
        accvaliq=self.accresult['accval'][self.opts['qubitid'][0]]
        return plot.iqplot(iq=accvaliq,fig=fig)
    def iqplotafterheralding(self,fig):
        accvaliq=self.accresult['accvalmafterheralding'][self.opts['qubitid'][0]]
        for iq in accvaliq:
            plot.iqplot(iq=iq,fig=fig)
        return fig

    def fit(self):
        fitpara=fit.fitexp(x=self.t,y=self.psingle[self.opts['qubitid'][0]]['2' if self.ef else '1'], bounds=((0,numpy.inf)))#,p0=est)
        popt,pcov,self.yfit,rsqr=[fitpara['popt'],fitpara['pcov'],fitpara['yfit'],fitpara['rsqr']]
        err=numpy.sqrt(numpy.diag(pcov))
        self.result.update(dict(amp=popt[0],l=popt[1],c=popt[2],t1err=err[1],t1=popt[1]))
        return self.result

if __name__=="__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-ef','--ef',help='use ef',dest='ef',default=False,action='store_true')
    parser.add_argument('qubitid',help='qubitid id')
    parser.add_argument('-dt','--dt',help='tstep',dest='dt',default=0.1e-6,type=float)
    parser.add_argument('-c','--calipath',help='path to calibration repo',dest='cpath',default='../../../../qchip',type=str)
    parser.add_argument('-g','--gmixs',help='gmixs: None: use data\n default: use the cali path\ndict: use the dict from gmixdict\n or path to gmix folder',dest='gmixs',type=str,default='default')
    parser.add_argument('-n','--nshot',help='number of shot',dest='nshot',default=100,type=int)
    parser.add_argument('-np','--noplot',help='no plot',dest='noplot',default=False,action='store_true')
    parser.add_argument('-s','--savefig',help='save figure plot',dest='savefig',default=False,action='store_true')
    clargs=parser.parse_args()
    t1=c_t1(qubitid=clargs.qubitid,calirepo=clargs.cpath,ef=clargs.ef,gmixs=clargs.gmixs)
    t1.seqs(elementstep=clargs.dt)#elementlength=8)
    t1.run(clargs.nshot)
    t1.fit()
    print(list(t1.accresult.keys()))
    print(t1.result)

    fig1=pyplot.figure(figsize=(10,5))
    sub=fig1.subplots(1,2)
    t1.plot(fig=sub[0])
    t1.iqplot(sub[1])
    #t1.iqplotafterheralding(sub[1,1])
    if clargs.savefig:
        tstr=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S')
        title=('%s_%s'%(clargs.qubitid,'t1_ef' if t1.ef else 't1'))
        pyplot.savefig(title+'.pdf')
    if clargs.noplot:
        pass
    else:
        pyplot.show()
#
#
#    chassis,wiremap,qchip,gmixpath=envset.envset(calirepo='../../qchip')
#    qubitid=sys.argv[1]
#    t,t1=t1seqs(qubitid=qubitid,elementlength=80,elementstep=3e-6)
#    result=qubicrun.run(chassis=chassis,circcmds=t1,qchip=qchip,nsample=100,wiremap=wiremap,delaybetweenelement=600e-6,heraldcmds=t1hrld(qubitid=qubitid),debug=False,heraldingsymbol='0',gmixs=gmixpath)
#    print(list(result.keys()))
#    print(result['countsum']['psingle'])
#    pyplot.plot(t,result['countsum']['psingle']['0'][0],'r')
#    pyplot.plot(t,result['countsum']['psingle']['1'][0],'b')
#    pyplot.show()
