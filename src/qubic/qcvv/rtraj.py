import os
import datetime
from matplotlib import pyplot
import numpy
from scipy import signal
import sys
sys.path.append('../..')
from qubic.qubic import experiment
#envset,qubicrun,heralding


class c_rtraj(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
    def baseseqs0(self,qubitid):
        seqs=[[{'name': 'read', 'qubit': qubitid}]]
        return seqs
    def baseseqs1(self,qubitid):
        seq=[]
        seqs=[]
        seq.append({'name': 'X90', 'qubit': qubitid})
        seq.append({'name': 'X90', 'qubit': qubitid})
        seq.append({'name': 'read', 'qubit': qubitid})
        seqs.append(seq)
        return seqs

    def seqs(self,init=0,**kwargs):
        self.opts.update(kwargs)
        self.opts['heraldcmds']=None
        self.opts['seqs']=self.baseseqs0(self.opts['qubitid']) if init==0 else self.baseseqs1(self.opts['qubitid'])
        self.opts['gmixs']=None
        self.compile()#cmdlists=qubicrun.compile(**self.opts)
    def run(self,nbuf):
        self.acqbufrun(nbuf,**self.opts)
        self.acqvalmean=self.acqresult.mean(axis=0)
#        self.s11=(self.result['accval'][self.opts['qubitid']].mean(axis=0))


    def plot(self,fig):    
        ax1=fig.add_subplot(1,1,1)
        pyplot.subplot(211)
        pyplot.plot(self.acqvalmean[:,0])
        pyplot.subplot(212)
        pyplot.plot(self.acqvalmean[:,1])
        return fig

if __name__=="__main__":
    rtraj=c_rtraj(qubitid=sys.argv[1],calirepo='../../../../qchip',debug=False)
    path=sys.argv[2]
    for index in range(1):
        n=100
        print('index',index)
        resultinput=numpy.zeros((2*n,1024,2))
        resultoutput=numpy.zeros((2*n))
        rtraj.seqs(init=0,mon_sel0=0,mon_sel1=2,mon_dt=400)
        rtraj.run(n)
        resultinput[0:n,:,:]=rtraj.acqresult
        resultoutput[0:n]=numpy.zeros((n))
        rtraj.seqs(init=0,mon_sel0=0,mon_sel1=2)
        rtraj.run(n)
        resultinput[n:2*n,:,:]=rtraj.acqresult
        resultoutput[n:2*n]=numpy.ones((n))
        numpy.savez(path+'/ttraj_%d_input_%d.npz'%(n,index),resultinput)
        numpy.savez(path+'/ttraj_%d_output_%d.npz'%(n,index),resultoutput)
        print(rtraj.acqresult.shape)
    #fig1=pyplot.figure(1,figsize=(15,8))
    #rtraj.plot(fig1)
#
    #pyplot.show()

