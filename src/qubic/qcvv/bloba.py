import os
import datetime
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment
from qubic.qcvv.plot import plot
from qubic.qcvv.fit import fit
from qubic.qcvv.rabi import c_rabi
#from qubic.qubic.expression import c_expression
#def rabiseq(qubitid,**kwargs):
#    opts=dict(frabi=None,trabi=None,arabi=None,fread=None,tread=None,aread=None,pread=None)
class c_bloba(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
    def baseseqs(self,qubitid,freads,aread,tread):
        #frabipara=c_expression('frabipara','frabipara')
        #freadpara=c_expression('freadpara','freadpara')
        seqs=[]
        for fread in freads:
            readmodi={}
            rdrvmodi={}
            readmodi.update({} if tread is None else {'twidth':tread});    rdrvmodi.update({} if tread is None else {'twidth':tread})
            readmodi.update({} if fread is None else {'fcarrier':fread});    rdrvmodi.update({} if fread is None else {'fcarrier':fread})
            readmodi.update({} if aread is None else {});    rdrvmodi.update({} if aread is None else {'amp':aread})
            seq=[]
            seq.append({'name': 'read', 'qubit': qubitid,'modi' : [rdrvmodi,readmodi]})
            seqs.append(seq)
            seq=[]
            seq.append({'name': 'X90', 'qubit': qubitid})
            seq.append({'name': 'X90', 'qubit': qubitid})
            seq.append({'name': 'read', 'qubit': qubitid,'modi' : [rdrvmodi,readmodi]})
            seqs.append(seq)
#                    seq.append({'name': 'delay', 'qubit': qubitid, 'para': {'delay': 10e-6}})
        return seqs

    def run(self,nsample,areads=None):
        fr=self.opts['qchip'].getfreq(self.opts['qubitid'][0]+'.readfreq')
        ar=self.opts['qchip'].gates[self.opts['qubitid'][0]+'read'].paralist[0]['amp']
        tr=self.opts['qchip'].gates[self.opts['qubitid'][0]+'read'].paralist[0]['twidth']
        freadbw=5e6
        self.freads=[fr]
#        self.freads=numpy.linspace(fr-freadbw,fr+freadbw,6)
        self.treads=[tr]
#        self.treads=numpy.linspace(tr-2000e-9,tr,4)
#        self.areads=[ar]
        self.areads=numpy.linspace(0.1,1,50) if areads is None else areads
        ntread=len(self.treads)
        nfread=len(self.freads)
        naread=len(self.areads)
        self.p0180=numpy.zeros((naread,ntread,nfread))
        self.p00=numpy.zeros((naread,ntread,nfread))
        self.sep=numpy.zeros((naread,ntread,nfread))
        for iaread,aread in enumerate(self.areads):
            for itread,tread in enumerate(self.treads):
                self.opts['seqs']=(self.baseseqs(qubitid=self.opts['qubitid'][0],freads=self.freads,aread=aread,tread=tread))
                self.compile(delaybetweenelement=100e-6,heraldcmds=None)
#                self.opts['nsample']=nsample
                self.accvals=self.accbufrun(nsample,includegmm=False)
                print('aread',aread)
                for ifread,fread in enumerate(self.freads):
                    qubitid=self.opts['qubitid'][0]
                    gmmresult=self.gmmcount({qubitid:self.accvals[qubitid][:,2*ifread:2*ifread+2]})
                    self.p00[iaread,itread,ifread]=gmmresult['countsum']['pcombine'][('0',)][0]
                    self.p0180[iaread,itread,ifread]=gmmresult['countsum']['pcombine'][('0',)][1]
                    self.sep[iaread,itread,ifread]=gmmresult['gmixs'][qubitid].separation()[('0','1')]


    def fit(self):
        est=fit.sinestimate(x=self.x,y=self.psingle[self.opts['qubitid'][0]]['1'])
        popt,pcov,self.yfit=fit.fitsin(x=self.x,y=self.psingle[self.opts['qubitid'][0]]['1'],p0=est)
        self.result.update(dict(amp=popt[0],period=1.0/popt[1],err=pcov))
        return self.result

    def psingleplot(self,fig):
        return plot.psingleplot(x=self.x,psingle=self.psingle,fig=fig,marker='.',linestyle=None,linewidth=0)
    def plottyfit(self,fig):
        fig.plot(self.x,self.yfit)
        return fig

    def iqplot(self,fig):
        index=len(self.fread)//2#numpy.nanargmin(numpy.abs(self.fread-fr))
        accvaliq=self.accvals[index][self.opts['qubitid'][0]]
        #accvaliq=self.accresult['accval'][self.opts['qubitid'][0]]
        return plot.iqplot(iq=accvaliq,fig=fig)
    def savegmm(self):
        for q in self.accresult['gmixs']:
            numpy.savez('%s_gmix.npz'%q,**self.accresult['gmixs'][q].modelpara())

if __name__=="__main__":
    qubitid=sys.argv[1]
    bloba=c_bloba(qubitid=qubitid,calirepo='../../../../qchip',debug=False,gmixs=None,overlapcheck=False)#elementlength=10)
    areads=numpy.linspace(0.005,0.8,20)
    bloba.run(4000,areads=areads)
    print(bloba.p0180)
    print(bloba.sep)
    amp=bloba.opts['qchip'].gates[qubitid+'read'].pulses[0].amp
    fig=pyplot.figure()
    fig.suptitle(qubitid)
    ax1=fig.subplots()
    ax2=ax1.twinx()
    ax1.plot(areads,bloba.p0180[:,0,0],'r',label='prep1read0')
    ax1.plot(areads,bloba.p00[:,0,0],'b',label='prep0read0')
    ax1.plot(areads,bloba.p00[:,0,0]-bloba.p0180[:,0,0],'k',label='prep0read0-prep1read0')
    ax2.plot(areads,bloba.sep[:,0,0],'g',label='separation')
    ax1.set_ylabel('probability')
    ax2.set_ylabel('sep',color='g')
    ax1.set_xlabel('aread')
    ax1.axvline(x=amp)
    ax1.legend()
    ax2.legend()
    pyplot.show()
#    bloba.fit()
#    bloba.savegmm()
#    print('result',bloba.result)
#    fr=bloba.opts['qchip'].getfreq(bloba.opts['qubitid'][0]+'.readfreq')
#
#    fig,(sub1,sub2)=pyplot.subplots(2,1)
#    fig.suptitle(sys.argv[1])
#    print(bloba.fread,bloba.sept.values())
#    for k,v in bloba.sept.items():
#        sub1.plot(bloba.fread,v,label=k)
#    sub1.axvline(x=fr)
#    bloba.psingleplot(sub1)
#    bloba.plottyfit(sub1)
#    bloba.iqplot(sub2)
#pyplot.savefig('fig1.pdf')
#    pyplot.show()
