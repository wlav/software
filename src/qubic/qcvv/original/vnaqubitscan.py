import matplotlib
matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
from matplotlib import pyplot,patches
import datetime
import argparse
import sys
from squbic import *
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
from scipy import signal
from experiment import c_experiment
from vnaqubit import c_vnaqubit
import serial

if __name__=="__main__":
	#vnaqubit=c_vnaqubit()
	#parser,cmdlinestr=vnaqubit.cmdoptions()
	#parser.set_defaults(delaybetweenelement=2e-6)
	#parser.set_defaults(elementlength=1024)
	#clargs=parser.parse_args()
	#readoutdrvamp_map=numpy.array([0.2]*8)
	#fread_map=numpy.array([-181.4e6,-127.5e6,-71.4e6,-12.0e6,44.8e6,104.3e6,160.7e6,217.8e6])
	#readoutdrvamp_map=numpy.array([0.345]*8)
	#fread_map=numpy.array([-182.5e6,-130.6e6,-73.6e6,-12.7e6,44.3e6,103324180.18406232,160524828.70687464,217.5e6])
	readoutdrvamp_map=numpy.array([0.15,0.2,0.2,0.15,0.15,0.3,0.3,0.2])
	fread_map=numpy.array([-181.0e6,-127.5e6,-71.4e6,-11.9e6,45.0e6,104.1e6,160.5e6,217.8e6])
	qubitdrvamp_map=numpy.arange(0.1,0.6,0.1)
	switch_map=numpy.array([1,2,3,4])
	for ifread in range(len(fread_map)):
		for iqubitdrvamp in range(len(qubitdrvamp_map)):
			vnaqubit=c_vnaqubit()
			parser,cmdlinestr=vnaqubit.cmdoptions()
			parser.set_defaults(delaybetweenelement=2e-6)
			parser.set_defaults(elementlength=1024)
			clargs=parser.parse_args()
	
			#vnaqubit.initseqs()
			vnaqubit.vnaqubitseqs(delayread=clargs.delayread,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,readoutdrvamp=readoutdrvamp_map[ifread],readwidth=None,fstart=clargs.fstart,fstop=clargs.fstop,rdc=0,qubitdrvamp=qubitdrvamp_map[iqubitdrvamp],fread=fread_map[ifread])
			vnaqubit.run()
			for iswitch in range(len(switch_map)):
				a=serial.Serial('/dev/ttyUSB0',9600)
				time.sleep(2)
				a.write('%d'%int(switch_map[iswitch]))
				timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
				log='%s change switch to :%s'%(timestamp,a.readline())
				print(log)
				f=open('switchlog.log','aw+')
				f.write(log)
				f.close()
				time.sleep(2)

				cmdlinestr='rdrv'+str(readoutdrvamp_map[ifread])+'_qdrv'+str(qubitdrvamp_map[iqubitdrvamp])+'_fr'+str(fread_map[ifread])+'_sw'+str(switch_map[iswitch])
				data=vnaqubit.vnaqubitdata(clargs.nget)
				fprocess=vnaqubit.savejsondata(filename=clargs.filename,extype='vnaqubitscan',cmdlinestr=cmdlinestr,data=data)
				print('save data to ',fprocess)
				c=vnaqubit.processvnaqubit(fprocess)
				if clargs.plot:
					pyplot.clf()
					vnaqubit.vnaqubitplot(c)
					pyplot.grid()
					pyplot.title(fprocess[14:-27])
					pyplot.savefig(fprocess+'.png')
					#pyplot.show()
				numpy.savetxt('processed'+fprocess,(vnaqubit.freq,c))
