import datetime
import argparse
import sys
from matplotlib import pyplot,patches
import numpy
import time
import init
import experiment
class c_spinecho(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid=None
		pass
	def spinechoseqs(self,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,elementlength=80,elementstep=2e-6,framsey=0,qubitid='Q7',qubitidread=['Q5','Q4','Q3']):
		self.qubitid=qubitid
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')

		run=0;
		for irun in range(elementlength):
			therald=run
			self.seqs.add(therald,self.qchip.gates[qubitid+'read'])

			l180=self.qchip.gates[qubitid+'X180'].tlength()
			l90=self.qchip.gates[qubitid+'X90'].tlength()
			tspinecho=elementstep*(irun)
			t90_1=self.seqs.tend()+delay1
			t180 =t90_1+l90+tspinecho/2.0
			t90_2=t180+l180+tspinecho/2.0
			p90_1=0
			p180=2*numpy.pi*(self.qchip.getfreq(qubitid+".freq")+(framsey if framsey  else  0))*(tspinecho/2.0+l90)
			p90_2=2*numpy.pi*(self.qchip.getfreq(qubitid+".freq")+(framsey if framsey  else  0))*(tspinecho+l90+l180)
			self.seqs.add(t90_1,         self.qchip.gates[qubitid+'X90'].modify({"pcarrier":p90_1}))
			self.seqs.add(t180,          self.qchip.gates[qubitid+'X180'].modify({"pcarrier":p180}))
			self.seqs.add(t90_2,         self.qchip.gates[qubitid+'X90'].modify({"pcarrier":p90_2}))

			treaddrv=self.seqs.tend()
			self.seqs.add(treaddrv,self.qchip.gates[qubitid+'read'])
			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		#self.bufwidth=[self.seqs.countdest('Q5.read'),self.seqs.countdest('Q4.read'),self.seqs.countdest('Q3.read')]
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
	def acqspinecho(self,nget):
		data=self.acqdata(nget)
		return data
	def processspinecho(self,dt,filename,loaddataset,plot=True):
		c=self.loadjsondata(filename)
		print('c.keys()',c.keys())
		data=c[list(c.keys())[0]]
		#spinecho_result=self.process3(data,lengthperrow=max(self.bufwidth),training=False,loadname=loadname)
		spinecho_result=self.process3(data,qubitid=self.qubitid,lengthperrow=self.bufwidth_dict[self.qubitid],training=False,loaddataset=loaddataset)
		print('separation',spinecho_result['separation'])
		[t2e,fiterr]=self.fitspinecho(dt,spinecho_result['population_norm'],plot=plot,figname=filename)
		return [data,spinecho_result['separation'],spinecho_result['iqafterherald'],spinecho_result['population_norm'],t2e,fiterr]
if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(elementstep=2e-6)
	clargs=parser.parse_args()
	spinecho=c_spinecho(**clargs.__dict__)
	spinecho.spinechoseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,elementstep=clargs.elementstep,framsey=clargs.framsey,qubitid=clargs.qubitid,qubitidread=clargs.qubitidread)
	spinecho.run(bypass=clargs.bypass or not clargs.processfile=='')
	if clargs.processfile=='':
		if clargs.sim:
			spinecho.sim()
		data=spinecho.acqspinecho(clargs.nget)
		fprocess=spinecho.savejsondata(filename=clargs.filename,extype='spinecho',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
	else:
		fprocess=clargs.processfile
	[rawdata,separation,iqafterherald,population_norm,t2e,fiterr]=spinecho.processspinecho(dt=clargs.elementstep,filename=fprocess,loaddataset=clargs.dataset)
	print('rabi training dataset ',clargs.dataset)
	print('t2spinecho',t2e)
#	print 'period',period
	print('separation',separation)
#	print 'amp',amp
#	spinecho.plotrawdata(rawdata)
#	spinecho.plotafterheraldingtest(iqafterherald)
#	spinecho.plotpopulation_norm(population_norm)
	if clargs.plot:
		pyplot.grid()
		pyplot.show()
