import datetime
import argparse
import sys
from matplotlib import pyplot,patches
import numpy
import time
import init
import experiment
class c_t1meas(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid=None
		pass
	def t1measseqs(self,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,elementlength=80,elementstep=2e-6,qubitid='Q7',qubitidread=['Q5','Q4','Q3']):
		self.qubitid=qubitid
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		run=0
		gatemodi={}
		tresetdelay=180e-9
		for irun in range(elementlength):
			#self.readall(trdrv=run,delayread=delayread)
			therald=run
			self.seqs.add(therald,self.qchip.gates[qubitid+'readoutdrv'])
			self.seqs.add(therald+delayread,self.qchip.gates[qubitid+'readout'])
			#add sequence here
			t180=self.seqs.tend()+delay1
			self.seqs.add(t180,         	self.qchip.gates[qubitid+'X180'].modify({'amp':0.0}))

			treset=self.seqs.tend()
			self.seqs.add(treset,self.qchip.gates[qubitid+'readoutdrv'])#.modify({'twidth':20e-9}))
			self.seqs.add(treset+delayread,self.qchip.gates[qubitid+'readout'])#.modify({'twidth':20e-9}))
			treset180=self.seqs.tend()+tresetdelay
			pnew=self.qchip.gates[qubitid+'X180'].pcalc(dt=treset180-t180)[0]
			gatemodi.update(dict(pcarrier=pnew))
			gatemodi.update({'amp':0})
			self.seqs.add(treset180,         	self.qchip.gates[qubitid+'X180'].modify(gatemodi),cond=1)
			
#			treset_1=self.seqs.tend()
#			self.seqs.add(treset_1,self.qchip.gates[qubitid+'readoutdrv'])#.modify({'twidth':20e-9}))
#			self.seqs.add(treset_1+delayread,self.qchip.gates[qubitid+'readout'])#.modify({'twidth':20e-9}))
#			treset180_1=self.seqs.tend()+tresetdelay
#			pnew=self.qchip.gates[qubitid+'X180'].pcalc(dt=treset180_1-t180)[0]
#			gatemodi.update(dict(pcarrier=pnew))
#			gatemodi.update({'amp1':0})
#			self.seqs.add(treset180_1,         	self.qchip.gates[qubitid+'X180'].modify(gatemodi),cond=1)
#

			treaddrv=self.seqs.tend()+elementstep*(irun)
			#self.readall(trdrv=treaddrv,delayread=delayread)
			self.seqs.add(treaddrv,self.qchip.gates[qubitid+'readoutdrv'])
			self.seqs.add(treaddrv+delayread,self.qchip.gates[qubitid+'readout'])
			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
	def acqt1meas(self,nget):
		data=t1meas.acqdata(nget)
		return data
	def processt1meas(self,dt,filename,loaddataset,plot=True):
		c=self.loadjsondata(filename)
		data=c[list(c.keys())[0]]
		t1meas_result=self.process4(data,qubitid=self.qubitid,lengthperrow=self.bufwidth_dict[self.qubitid],training=False,loaddataset=loaddataset)
		[t1fit,fiterr]=self.fitt1meas(dt=dt,data=t1meas_result['population_norm'],plot=plot)
		return [data,t1meas_result['separation'],t1meas_result['iqafterherald'],t1meas_result['population_norm'],t1fit,fiterr]
if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(elementstep=2e-6)
	clargs=parser.parse_args()
	t1meas=c_t1meas(**clargs.__dict__)
	t1meas.t1measseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,elementstep=clargs.elementstep,qubitid=clargs.qubitid,qubitidread=clargs.qubitidread)
	t1meas.run(bypass=clargs.bypass or not clargs.processfile=='')
	if clargs.processfile=='':
		if clargs.sim:
			t1meas.sim()
		data=t1meas.acqt1meas(clargs.nget)
		fprocess=t1meas.savejsondata(filename=clargs.filename,extype='t1meas',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
	else:
		fprocess=clargs.processfile
	[rawdata,separation,iqafterherald,population_norm,t1fit,fiterr]=t1meas.processt1meas(dt=clargs.elementstep,filename=fprocess,loaddataset=clargs.dataset)
	print('rabi training dataset ',clargs.dataset)
	print('t1fit',t1fit)
	#print 'period',period
	print('separation',separation)
	#print 'amp',amp
	t1meas.plotrawdata(rawdata)
	t1meas.plotafterheraldingtest(iqafterherald)
	t1meas.plotpopulation_norm(population_norm)
	if clargs.plot:
		pyplot.grid()
		pyplot.show()
