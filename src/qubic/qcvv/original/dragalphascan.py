import datetime
import argparse
import sys
from squbic import *
sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import experiment
class c_dragalphascan(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid=None
		pass
	def dragalphascanseqs(self,elementlength=80,elementstep=4e-9,delayread=920e-9,delay1=12e-6,delaybetweenelement=600e-6,readoutdrvamp=None,qubitdrvamp=None,readwidth=None,rdc=0,draggate=None,alphastart=None,alphastop=None,alphastep=None,qubitid='Q7',qubitidread=['Q5','Q4','Q3']):
		self.qubitid=qubitid
		readoutdrvamp0,readoutdrvamp1,readoutdrvamp2=self.rdrvcalc(readoutdrvamp,dcoffset=rdc)
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		modrdrv={}
		modread={}
		modqdrv={}
		if readoutdrvamp:
			modrdrv.update(dict(amp=readoutdrvamp0))
		if readwidth:
			modrdrv.update(dict(twidth=readwidth))
			modread.update(dict(twidth=readwidth))
		run=0
		gates=['X90']+['Y180'] if draggate=='X90Y180' else ['Y90']+['X180']
		alphas=numpy.arange(alphastart,alphastop,alphastep)
		print('start loop')
		for alpha in alphas:
			therald=run
			self.seqs.add(therald,self.qchip.gates[qubitid+'read'].modify([modrdrv,modread]))
			pini=0
			tgate=self.seqs.tend()+delay1
			tini=tgate
			for gate in gates:
				modqdrv={}
				if qubitdrvamp:
					modqdrv.update(dict(amp=qubitdrvamp))
				gateobj=self.qchip.gates[qubitid+gate]
				pnew=self.qchip.gates[qubitid+gate].pcalc(dt=tgate-tini)[0]
				modqdrv.update(dict(pcarrier=pnew,env=[{"env_func": "DRAG", "paradict": {"sigmas": 3,"alpha":alpha,"delta":-268e6}}]))
				self.seqs.add(tgate,self.qchip.gates[qubitid+gate].modify(modqdrv))
				print(tgate,gate,modqdrv)
				tgate=self.seqs.tend()
			treaddrv=self.seqs.tend()
			self.seqs.add(treaddrv,self.qchip.gates[qubitid+'read'].modify([modrdrv,modread]))
			run=self.seqs.tend()+delaybetweenelement
		print('stop loop')
		print('period',run)
		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
	def acqdragalphascan(self,nget):
		data=self.acqdata(nget)
		return data
	def processdragalphascan(self,tdata,filename,loaddataset,draggate=''):
		c=self.loadjsondata(filename)
		print('c.keys()',c.keys())
		data=c[list(c.keys())[0]]
		dragalphascan_result=self.process3(data,qubitid=self.qubitid,lengthperrow=self.bufwidth_dict[self.qubitid],training=False,loaddataset=loaddataset)
		print('separation',dragalphascan_result['separation'])
		[slope,intercept]=self.fitdragalphascan(tdata,dragalphascan_result['population_norm'],draggate,plot=False)
		return [data,dragalphascan_result['separation'],dragalphascan_result['iqafterherald'],dragalphascan_result['population_norm'],slope,intercept]

if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	clargs=parser.parse_args()
	dragalphascan=c_dragalphascan(**clargs.__dict__)
	dragalphascan.dragalphascanseqs(elementlength=clargs.elementlength,elementstep=clargs.elementstep,delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,readwidth=clargs.readwidth,draggate=clargs.draggate,alphastart=clargs.alphastart,alphastop=clargs.alphastop,alphastep=clargs.alphastep,qubitid=clargs.qubitid,qubitidread=clargs.qubitidread)
	dragalphascan.run(bypass=clargs.bypass or not clargs.processfile=='')
	if clargs.processfile=='':
		if clargs.sim:
			dragalphascan.sim()
		data=dragalphascan.acqdragalphascan(clargs.nget)
		fprocess=dragalphascan.savejsondata(filename=clargs.filename,extype='dragalphascan',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
	else:
		fprocess=clargs.processfile
	[rawdata,separation,iqafterherald,population_norm,slope,intercept]=dragalphascan.processdragalphascan(tdata=numpy.arange(clargs.alphastart,clargs.alphastop,clargs.alphastep),filename=fprocess,loaddataset=clargs.dataset,draggate=clargs.draggate)
	dragalphascan.plotrawdata(rawdata)
	dragalphascan.plotafterheraldingtest(iqafterherald)
	dragalphascan.plotpopulation_norm(population_norm)
	pyplot.show()
	numpy.savetxt(clargs.draggate+'.dat',(numpy.arange(clargs.alphastart,clargs.alphastop,clargs.alphastep),population_norm))
	if clargs.draggate=='Y90X180':
		tdata0=numpy.loadtxt('X90Y180.dat')[0,:]
		data0=numpy.loadtxt('X90Y180.dat')[1,:]
		tdata1=numpy.loadtxt('Y90X180.dat')[0,:]
		data1=numpy.loadtxt('Y90X180.dat')[1,:]
		[slope0,intercept0]=dragalphascan.fitdragalphascan(tdata0,data0,'X90Y180')
		[slope1,intercept1]=dragalphascan.fitdragalphascan(tdata1,data1,'Y90X180')
		pyplot.show()
        #for gt in 'X90Y180' 'Y90X180'; do python dragalphascan.py -n 100 --alphastart 0 --alphastop 3 --alphastep 0.1 --draggate $gt --qubitid Q6; done
