import numpy
import sys
from matplotlib import pyplot
t,b0,b1=numpy.loadtxt(sys.argv[1])
fmax=1./(numpy.diff(t).mean())
df=fmax/(len(t)-1)
length=len(t)#//2
f=numpy.arange(length)*df
fftval=numpy.fft.fft(b0)[0:length]
pyplot.figure(figsize=(8,4))
pyplot.subplot(121)
pyplot.plot(t,b0)
pyplot.subplot(122)
pyplot.plot(f,abs(fftval))
pyplot.show()
print(f[0:5],f[-5:-1],df)
print(t[0:5],t[-5:-1],fmax)
