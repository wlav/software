import datetime
import argparse
import sys
from squbic import *
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import experiment
class c_targetdetune(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid_c=None
		self.qubitid_t=None
		pass
	def targetdetuneseqs(self,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,elementlength=80,elementstep=2e-6,readoutdrvamp=None,qubitdrvamp=None,readwidth=None,fqubit=None,fread=None,rdc=0,framsey=None,qubitid_c='Q5',qubitid_t='Q4',ctrl='I',qubitidread=['Q5','Q4','Q3']):
		self.qubitid_c=qubitid_c
		self.qubitid_t=qubitid_t
		print('marker done')
		mod90_1={}
		mod90_2={}
		fgate=self.qchip.getfreq(qubitid_t+'.freq')
		if framsey:
			fgate=fgate+framsey
			mod90_1.update(dict(fcarrier=fgate))
			mod90_2.update(dict(fcarrier=fgate))
		l90=self.qchip.gates[qubitid_t+'X90'].tlength()
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		run=0

		for irun in range(elementlength):
			therald=run
			self.seqs.add(therald,self.qchip.gates[qubitid_t+'read'])

			tini=run+delay1
			if ctrl=='X180':
				self.seqs.add(tini,              self.qchip.gates[qubitid_c+'X180'])
				t90_1=self.seqs.tend()
			else:
				t90_1=tini
			ti180=t90_1-tini

			tramsey=elementstep*(irun)
			t90_2=t90_1+l90+tramsey
			p90_1=self.qchip.gates[qubitid_t+'X90'].pcalc(dt=ti180,freq=fgate)[0]
			p90_2=self.qchip.gates[qubitid_t+'X90'].pcalc(dt=ti180+l90+tramsey,freq=fgate)[0]
			mod90_1.update(dict(pcarrier=p90_1))
			mod90_2.update(dict(pcarrier=p90_2))

			self.seqs.add(t90_1,         self.qchip.gates[qubitid_t+'X90'].modify(mod90_1))
			self.seqs.add(t90_2,         self.qchip.gates[qubitid_t+'X90'].modify(mod90_2))

			treaddrv=self.seqs.tend()
			self.seqs.add(treaddrv,self.qchip.gates[qubitid_t+'read'])
			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
	def targetdetunedata(self,nget):
		data=targetdetune.acqdata(nget)
		return data
	def processtargetdetune(self,dt,filename,loaddataset,plot=True):
		c=self.loadjsondata(filename)
		print('c.keys()',c.keys())
		data=c[list(c.keys())[0]]
		targetdetune_result=self.process3(data,qubitid=self.qubitid_t,lengthperrow=self.bufwidth_dict[self.qubitid_t],training=False,loaddataset=loaddataset)
		print('separation',targetdetune_result['separation'])
		t2star,foffset,fiterr=self.fitramsey(dt,targetdetune_result['population_norm'],plot=plot)
		return [data,targetdetune_result['separation'],targetdetune_result['iqafterherald'],targetdetune_result['population_norm'],t2star,foffset,fiterr]
if __name__=="__main__":
	for ctrl in ['I','X180']:
		parser,cmdlinestr=experiment.cmdoptions()
		#parser.set_defaults(elementstep=2e-6)
		parser.set_defaults(elementstep=4e-7)
		clargs=parser.parse_args()
		targetdetune=c_targetdetune(**clargs.__dict__)

		targetdetune.targetdetuneseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readwidth=clargs.readwidth,elementlength=clargs.elementlength,elementstep=clargs.elementstep,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,framsey=clargs.framsey,qubitid_c=clargs.qubitid_c,qubitid_t=clargs.qubitid_t,ctrl=ctrl,qubitidread=clargs.qubitidread)
		targetdetune.run(bypass=clargs.bypass or not clargs.processfile=='')
		if clargs.processfile=='':
			if clargs.sim:
				targetdetune.sim()
			data=targetdetune.targetdetunedata(clargs.nget)
			fprocess=targetdetune.savejsondata(filename=clargs.filename,extype='targetdetune'+ctrl,cmdlinestr=cmdlinestr,data=data)
			print('save data to ',fprocess)
		else:
			fprocess=clargs.processfile
		[rawdata,separation,iqafterherald,population_norm,t2star,foffset,fiterr]=targetdetune.processtargetdetune(dt=clargs.elementstep,filename=fprocess,loaddataset=clargs.dataset)
		print('rabi training dataset ',clargs.dataset)
		print('t2star',t2star)
		print('foffset',foffset)
		if clargs.plot:
			pyplot.grid()
			pyplot.show()
