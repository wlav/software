import matplotlib
matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import experiment

class c_rabixtalk(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid_t=None
		self.qubitid_c=None
		self.qubitid_x=None
	def rabixtalkseqs(self,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,elementlength=80,elementstep=4e-9,restart=True,readoutdrvamp=None,qubitdrvamp=None,readwidth=None,fqubit=None,fread=None,rdc=0,preadout=None,qubitid_t='Q5',qubitid_c='Q6',qubitidread=['Q7','Q6','Q5'],phase_offset_ctrl=0,amp_multiplier_ctrl=0,phase_offset_tgt=0,amp_multiplier_tgt=0,wrabi_ctrl=0,qubitid_x='Q7'):
		self.qubitid_t=qubitid_t
		self.qubitid_c=qubitid_c
		self.qubitid_x=qubitid_x
		if restart:
			self.initseqs()
		readoutdrvamp0,readoutdrvamp1,readoutdrvamp2=self.rdrvcalc(readoutdrvamp,dcoffset=rdc)
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		modrdrv={}
		modread={}
		modtgt={}
		modtgt_simul={}
		modctrl={}
		modctrl_simul={}
		if readoutdrvamp is not None:
			modrdrv.update(dict(amp=readoutdrvamp0))
		if qubitdrvamp is not None:
			modtgt.update(dict(amp=qubitdrvamp))
			modtgt_simul.update(dict(amp=qubitdrvamp))
		if fqubit is not None:
			modtgt.update(dict(fcarrier=fqubit))
			modtgt_simul.update(dict(fcarrier=fqubit))
		if readwidth is not None:
			modrdrv.update(dict(twidth=readwidth))
			modread.update(dict(twidth=readwidth))
		if fread is not None:
			modrdrv.update(dict(fcarrier=fread))
			modread.update(dict(fcarrier=fread))
		if preadout is not None:
			modread.update(dict(pcarrier=preadout))

		run=0
		for irun_tgt in range(elementlength):

			therald=run
			self.seqs.add(therald,self.qchip.gates[qubitid_c+'read'])
			self.seqs.add(therald,self.qchip.gates[qubitid_t+'read'].modify([modrdrv,modread]))
			trabi=run+delay1
			wrabi_tgt=elementstep*irun_tgt
			if wrabi_tgt!=0:
				modtgt.update(dict(twidth=wrabi_tgt))
				self.seqs.add(trabi,         	 self.qchip.gates[qubitid_t+'rabi'].modify(modtgt))

			if wrabi_ctrl!=0:
				modctrl.update(dict(twidth=wrabi_ctrl))
				self.seqs.add(trabi,         	 self.qchip.gates[qubitid_c+'rabi'].modify(modctrl))

			pcarrier_tgt_simul_tmp=self.qchip.gates[qubitid_t+'rabi'].pcalc(padd=phase_offset_tgt)[0]
			amp_tgt_simul_tmp=self.qchip.gates[qubitid_t+'rabi'].paralist[0]['amp']*amp_multiplier_tgt
			#modtgt_simul.update(dict(twidth=wrabi_tgt,dest=qubitid_c+'.qdrv',amp=amp_tgt_simul_tmp,pcarrier=pcarrier_tgt_simul_tmp))

			pcarrier_ctrl_simul_tmp=self.qchip.gates[qubitid_c+'rabi'].pcalc(padd=phase_offset_ctrl)[0]
			amp_ctrl_simul_tmp=self.qchip.gates[qubitid_c+'rabi'].paralist[0]['amp']*amp_multiplier_ctrl
			#modctrl_simul.update(dict(twidth=wrabi_ctrl,dest=qubitid_t+'.qdrv',amp=amp_ctrl_simul_tmp,pcarrier=pcarrier_ctrl_simul_tmp))

			if wrabi_tgt!=0 and wrabi_ctrl!=0:
				self.seqs.add(trabi,self.qchip.gates[qubitid_x+'xtalk'+qubitid_c+qubitid_t].modify([dict(twidth=wrabi_ctrl,amp=amp_ctrl_simul_tmp,pcarrier=pcarrier_ctrl_simul_tmp),dict(twidth=wrabi_tgt,amp=amp_tgt_simul_tmp,pcarrier=pcarrier_tgt_simul_tmp)]))
			elif wrabi_tgt==0 and wrabi_ctrl!=0:
				self.seqs.add(trabi,self.qchip.gates[qubitid_x+'xtalk'+qubitid_c+qubitid_t].modify([dict(twidth=wrabi_ctrl,amp=amp_ctrl_simul_tmp,pcarrier=pcarrier_ctrl_simul_tmp),dict(amp=0)]))
			elif wrabi_tgt!=0 and wrabi_ctrl==0:
				self.seqs.add(trabi,self.qchip.gates[qubitid_x+'xtalk'+qubitid_c+qubitid_t].modify([dict(amp=0),dict(twidth=wrabi_tgt,amp=amp_tgt_simul_tmp,pcarrier=pcarrier_tgt_simul_tmp)]))
			else:
				self.seqs.add(trabi,self.qchip.gates[qubitid_x+'xtalk'+qubitid_c+qubitid_t].modify([dict(amp=0),dict(amp=0)]))
			#self.seqs.add(trabi,         	 self.qchip.gates[qubitid_c+'rabi'].modify(modctrl_simul))
			#self.seqs.add(trabi,         	 self.qchip.gates[qubitid_t+'rabi'].modify(modtgt_simul))

			treaddrv=self.seqs.tend()
			self.seqs.add(treaddrv,self.qchip.gates[qubitid_c+'read'])
			self.seqs.add(treaddrv,self.qchip.gates[qubitid_t+'read'].modify([modrdrv,modread]))
			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
		accout_list=['accout_0','accout_1','accout_2']
		self.accout_dict=dict(zip(qubitidread,accout_list))
		print('fread,fqubit,readoutdrvamp,qubitdrvamp',fread,fqubit,readoutdrvamp,qubitdrvamp)
	def rabixtalkacq(self,nget):
		data=self.acqdata(nget)
		return data
	def processrabixtalk(self,dt,filename,dumpdataset,loaddataset,plot=False):
		c=self.loadjsondata(filename)
		print('self.bufwidth_dict',self.bufwidth_dict,'   self.accout_dict',self.accout_dict)
		print('accout in use: ',self.accout_dict[self.qubitid_c],self.accout_dict[self.qubitid_t])
		data_list=[c[self.accout_dict[self.qubitid_c]],c[self.accout_dict[self.qubitid_t]]]
		ctrl_result=self.process3(data_list[0],qubitid=self.qubitid_c,lengthperrow=self.bufwidth_dict[self.qubitid_c],training=loaddataset=='',dumpdataset=dumpdataset,loaddataset=loaddataset)
		tgt_result=self.process3(data_list[1],qubitid=self.qubitid_t,lengthperrow=self.bufwidth_dict[self.qubitid_t],training=loaddataset=='',dumpdataset=dumpdataset,loaddataset=loaddataset)
		#[amp_c,period_c,fiterr_c]=self.fitrabi(dx=dt,data=ctrl_result['population_norm'],plot=plot,figname=filename)
		#[amp_t,period_t,fiterr_t]=self.fitrabi(dx=dt,data=tgt_result['population_norm'],plot=plot,figname=filename)
		#return [data_list[0],ctrl_result['separation'],ctrl_result['iqafterherald'],ctrl_result['population_norm'],amp_c,period_c,fiterr_c,data_list[1],tgt_result['separation'],tgt_result['iqafterherald'],tgt_result['population_norm'],amp_t,period_t,fiterr_t]
		return [data_list[0],ctrl_result['separation'],ctrl_result['iqafterherald'],ctrl_result['population_norm'],data_list[1],tgt_result['separation'],tgt_result['iqafterherald'],tgt_result['population_norm']]

if __name__=="__main__":
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	elementlength_local=40#10#40
	elementlength_control=40#8#40
	elementstep_local=8e-9#4e-9#32e-9#8e-9
	#para_dict={'Q6':{'phase_offset':4.925-numpy.pi,'amp_multiplier':1.4},'Q5':{'phase_offset':0.85+numpy.pi,'amp_multiplier':0.63}}
	#para_dict={'Q6':{'phase_offset':4.925-numpy.pi-(1.0/8.0*numpy.pi+numpy.pi),'amp_multiplier':1.4/1.0},'Q5':{'phase_offset':0.85+numpy.pi-(6.0/8.0*numpy.pi+numpy.pi),'amp_multiplier':0.63/0.75}}
	#para_dict={'Q6':{'phase_offset':4.925-0.25,'amp_multiplier':1.4/1.0},'Q5':{'phase_offset':0.85-2.25,'amp_multiplier':0.63/0.70}}
	#para_dict={'Q6':{'phase_offset':0.85-0.25+numpy.pi,'amp_multiplier':0.63/1.0},'Q5':{'phase_offset':4.925-2.25+numpy.pi,'amp_multiplier':1.4/0.70}}
	#para_dict={'Q6':{'phase_offset':-(0.85-0.25)+numpy.pi,'amp_multiplier':1.0/0.63},'Q5':{'phase_offset':-(4.925-2.25)+numpy.pi,'amp_multiplier':0.70/1.4}}
	#para_dict={'Q6':{'phase_offset':-(0.85-0.25),'amp_multiplier':1.0/0.63},'Q5':{'phase_offset':-(4.925-2.25),'amp_multiplier':0.70/1.4}}
	#para_dict={'Q6':{'phase_offset':2.75,'amp_multiplier':1.625},'Q5':{'phase_offset':0.4,'amp_multiplier':0.5}}
	#para_dict={'Q6':{'phase_offset':2.75+numpy.pi,'amp_multiplier':1.625},'Q5':{'phase_offset':0.4+numpy.pi,'amp_multiplier':0.5}}
	#sweepQ5phase_map=numpy.arange(-0.2,0.21,0.4)
	#sweepQ5amp_map=numpy.arange(-0.05,0.06,0.1)
	sweepQ5phase_map=numpy.array([0])
	sweepQ5amp_map=numpy.array([0])
	for sweepQ5phase in sweepQ5phase_map:
		for sweepQ5amp in sweepQ5amp_map:
			#para_dict={'Q6':{'phase_offset':-(0.85-0.25)+numpy.pi,'amp_multiplier':1.0/0.63},'Q5':{'phase_offset':-(4.925-2.25)+numpy.pi+sweepQ5phase,'amp_multiplier':0.70/1.4+sweepQ5amp}}
			#para_dict={'Q6':{'phase_offset':4.925-0.25,'amp_multiplier':1.4/1.0},'Q5':{'phase_offset':0.85-2.25+sweepQ5phase,'amp_multiplier':0.63/0.70+sweepQ5amp}}
			#para_dict={'Q6':{'phase_offset':2.75+numpy.pi,'amp_multiplier':0},'Q5':{'phase_offset':0.4+numpy.pi+sweepQ5phase,'amp_multiplier':0}}
			para_dict={'Q6':{'phase_offset':2.75+numpy.pi,'amp_multiplier':1.625},'Q5':{'phase_offset':0.4+numpy.pi+sweepQ5phase,'amp_multiplier':0.5+sweepQ5amp}}
			ctrl_data=[]
			tgt_data=[]
			#for irun_ctrl in range(elementlength_local):
			for irun_ctrl in range(elementlength_control):
				wrabi_ctrl=elementstep_local*irun_ctrl
				parser,cmdlinestr=experiment.cmdoptions()
				print(cmdlinestr)
				parser.set_defaults(elementstep=elementstep_local)
				clargs=parser.parse_args()
				rabixtalk=c_rabixtalk(**clargs.__dict__)
				if clargs.sim:
					rabixtalk.setsim()
				rabixtalk.rabixtalkseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readwidth=clargs.readwidth,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,elementlength=elementlength_local,elementstep=elementstep_local,fqubit=clargs.fqubit,preadout=clargs.preadout,fread=clargs.fread,qubitid_t=clargs.qubitid_t,qubitid_c=clargs.qubitid_c,qubitidread=clargs.qubitidread,phase_offset_ctrl=para_dict[clargs.qubitid_c]['phase_offset'],amp_multiplier_ctrl=para_dict[clargs.qubitid_c]['amp_multiplier'],phase_offset_tgt=para_dict[clargs.qubitid_t]['phase_offset'],amp_multiplier_tgt=para_dict[clargs.qubitid_t]['amp_multiplier'],wrabi_ctrl=wrabi_ctrl,qubitid_x=clargs.qubitid_x)
				if clargs.processfile=='':
					rabixtalk.run(bypass=clargs.bypass)
					data=rabixtalk.rabixtalkacq(clargs.nget)
					fprocess=rabixtalk.savejsondata(filename=clargs.filename+'_elementlength'+str(elementlength_local)+'_elementstep'+str(elementstep_local),extype='rabixtalk3',cmdlinestr=cmdlinestr,data=data)
					print('save data to ',fprocess)
					if clargs.sim:
						rabixtalk.sim()
				else:
					fprocess=clargs.processfile
				#[rawdata_c,separation_c,iqafterherald_c,population_norm_c,amp_c,period_c,fiterr_c,rawdata_t,separation_t,iqafterherald_t,population_norm_t,amp_t,period_t,fiterr_t]=rabixtalk.processrabixtalk(dt=elementstep_local,filename=fprocess,dumpdataset='tmp',loaddataset=clargs.dataset,plot=False)
				#with open(clargs.filename+'_periodamp_rabixtalk3_'+timestamp+'.dat','a') as f:
				#	numpy.savetxt(f,numpy.array([period_c,period_t,amp_c,amp_t]).reshape((1,-1)))
				[rawdata_c,separation_c,iqafterherald_c,population_norm_c,rawdata_t,separation_t,iqafterherald_t,population_norm_t]=rabixtalk.processrabixtalk(dt=elementstep_local,filename=fprocess,dumpdataset='tmp',loaddataset=clargs.dataset,plot=clargs.plot)
				ctrl_data.append(population_norm_c)
				with open(clargs.filename+'_rabixtalk3_ctrl_'+timestamp+'.dat','a') as f:
					numpy.savetxt(f,population_norm_c.reshape((1,-1)))
				tgt_data.append(population_norm_t)
				with open(clargs.filename+'_rabixtalk3_tgt_'+timestamp+'.dat','a') as f:
					numpy.savetxt(f,population_norm_t.reshape((1,-1)))
				print(rabixtalk.hf.adcminmax())
		
			plot_dict={'control':ctrl_data,'target':tgt_data} # ctrl_data is list, tgt_data is list
			for item in plot_dict.keys():
				pyplot.figure()
				x_axis=elementstep_local*1e9*numpy.arange(len(plot_dict[item][0]))
				y_axis=elementstep_local*1e9*numpy.arange(len(plot_dict[item]))
				pyplot.pcolormesh(rabixtalk.repack1D_cent2edge(x_axis),rabixtalk.repack1D_cent2edge(y_axis),plot_dict[item],cmap='RdBu_r')
				pyplot.clim(0,1)
				pyplot.legend()
				pyplot.xlabel('Pulse length on target line (ns)')
				pyplot.ylabel('Pulse length on control line (ns)')
				pyplot.colorbar(label='P(|1>)')
				pyplot.title('Read on %s line'%item)
				pyplot.tight_layout()
				pyplot.savefig(clargs.filename+'_p'+str(sweepQ5phase)+'_a'+str(sweepQ5amp)+'_rabixtalk3_'+'qubitid_c'+clargs.qubitid_c+'_qubitid_t'+clargs.qubitid_t+'_'+item+'_elementlength'+str(elementlength_local)+'_elementstep'+str(elementstep_local)+'_'+timestamp+'.pdf')
				#pyplot.show()
# python rabi_xtalk3.py --plot -n 20 --qubitid_c Q6 --qubitid_t Q5 --qubitid_x Q7
