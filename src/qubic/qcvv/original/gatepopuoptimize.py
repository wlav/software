#import datetime
#import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
#from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
#import time
#import init
#import pprint
import experiment
#from rabi_w import c_rabi
from gatepopu import c_gatepopu
from scipy.optimize import minimize,fmin,fminbound

def gatepopuoptimize(x,gate,ngate,nget,delayread,delay1,delaybetweenelement,qubitid,qubitidread=['Q7','Q6','Q5'],qubitcfg='qubitcfg.json',ip='192.168.1.124',wiremapmodule='wiremap_X6Y8_20210629'):
	parser,cmdlinestr=experiment.cmdoptions()
	clargs=parser.parse_args()
	gatepopu=c_gatepopu(qubitcfg=qubitcfg,ip=ip,wiremapmodule=wiremapmodule)
	para={'delayread':delayread,'delay1':delay1,'delaybetweenelement':delaybetweenelement,'qubitdrvamp':x,'elementlength':1,'gate':gate,'ngate':ngate,'qubitid':qubitid,'qubitidread':qubitidread}
	gatepopu.gatepopuseqs(**para)
	gatepopu.run(bypass=clargs.bypass)
	data=gatepopu.gatepopuacq(nget)
	fprocess=gatepopu.savejsondata(filename='',extype='tmp',cmdlinestr='',data=data,timeinfo=False)
	[rawdata,separation,iqafterherald,population_norm]=gatepopu.processgatepopu(dt=clargs.elementstep,filename=fprocess,loaddataset=clargs.dataset,plot=clargs.plot)
	result=-population_norm.mean()
	print("########qubitdrvamp result########",gate,ngate,para['qubitdrvamp'],result)
	sys.stdout.flush()

	return  result

if __name__=="__main__":
	gate='X90'
	nget=2
	if '270' in gate:
		center  = 0.94257813
		halfspan0=0.05
		for i in range(9):
			halfspan=halfspan0/(2**i)
			ngate=4*(2**i)+1 if '180' in gate else 4*(2**i)+2
			center=fminbound(func=gatepopuoptimize,x1=center-halfspan,x2=center+halfspan,xtol=0.000001,disp=1,args=(gate,ngate,nget,600e-9,'Q1',['Q3','Q2','Q1'],'qubitcfg_mitll.json','192.168.1.123','wiremap_X6Y8_20210629'))
			print('range',center-halfspan,center,center+halfspan, 32768*halfspan)
	elif '180' in gate:
		center=0.94257813
		halfspan0=0.01
		for i in range(5):
			halfspan=halfspan0/(2**i)
			ngate=4*(2**(i*2))+1
			center=fminbound(func=gatepopuoptimize,x1=center-halfspan,x2=center+halfspan,xtol=0.000001,disp=1,args=(gate,ngate,nget,600e-9,'Q1',['Q3','Q2','Q1'],'qubitcfg_mitll.json','192.168.1.123','wiremap_X6Y8_20210629'))
			print('range',center-halfspan,center,center+halfspan, 32768*halfspan)
	elif '90' in gate:
		center=0.6
		halfspan0=0.05
		#for i in range(4):
		#	halfspan=halfspan0/(4**i)
		#	ngate=4*(2**(i*2))+2
		for i in range(7):
			halfspan=halfspan0/(2**i)
			ngate=4*(2**(i*1))+2
			#center=fminbound(func=gatepopuoptimize,x1=center-halfspan,x2=center+halfspan,xtol=1e-4,disp=True,args=(gate,ngate,nget,680e-9,12e-6,600e-6,'Q6',['Q7','Q6','Q5'],'qubitcfg_chip57.json','192.168.1.123'))
			center=fminbound(func=gatepopuoptimize,x1=center-halfspan,x2=center+halfspan,xtol=halfspan/10,disp=True,args=(gate,ngate,nget,680e-9,12e-6,600e-6,'Q6',['Q7','Q6','Q5'],'qubitcfg_chip57.json','192.168.1.123','wiremap_X6Y8_20210629'))
			print('range',center-halfspan,center,center+halfspan, 32768*halfspan)
