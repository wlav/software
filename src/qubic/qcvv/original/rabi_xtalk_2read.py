import matplotlib
matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import experiment

class c_rabixtalk(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid_t=None
		self.qubitid_c=None
	def rabixtalkseqs(self,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,elementlength=80,elementstep=4e-9,restart=True,readoutdrvamp=None,qubitdrvamp=None,readwidth=None,fqubit=None,fread=None,rdc=0,preadout=None,qubitid_t='Q7',qubitid_c='Q6',qubitidread=['Q7','Q6','Q5'],amp_multiplier=0,phase_offset=0,ctrl='I'):
		self.qubitid_t=qubitid_t
		self.qubitid_c=qubitid_c
		if restart:
			self.initseqs()
		readoutdrvamp0,readoutdrvamp1,readoutdrvamp2=self.rdrvcalc(readoutdrvamp,dcoffset=rdc)
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		modrdrv={}
		modread={}
		modrabi={}
		modrabi_simul={}
		if readoutdrvamp is not None:
			modrdrv.update(dict(amp=readoutdrvamp0))
		if qubitdrvamp is not None:
			modrabi.update(dict(amp=qubitdrvamp))
			modrabi_simul.update(dict(amp=qubitdrvamp))
		if fqubit is not None:
			modrabi.update(dict(fcarrier=fqubit))
			modrabi_simul.update(dict(fcarrier=fqubit))
		if readwidth is not None:
			modrdrv.update(dict(twidth=readwidth))
			modread.update(dict(twidth=readwidth))
		if fread is not None:
			modrdrv.update(dict(fcarrier=fread))
			modread.update(dict(fcarrier=fread))
		if preadout is not None:
			modread.update(dict(pcarrier=preadout))

		run=0
		for irun in range(elementlength):

			therald=run
			self.seqs.add(therald,self.qchip.gates[qubitid_c+'read'])
			self.seqs.add(therald,self.qchip.gates[qubitid_t+'read'].modify([modrdrv,modread]))
			tini=run+delay1
			if ctrl=='X180':
				self.seqs.add(tini,             self.qchip.gates[qubitid_c+'X180'])
				trabi=self.seqs.tend()
			else:
				trabi=tini
			ti180=trabi-tini
			wrabi=elementstep*irun
			if wrabi!=0:
				pcarrier_tmp=self.qchip.gates[qubitid_t+'rabi'].pcalc(dt=ti180)[0]
				modrabi.update(dict(twidth=wrabi,pcarrier=pcarrier_tmp))
				self.seqs.add(trabi,         	 self.qchip.gates[qubitid_t+'rabi'].modify(modrabi))

				pcarrier_simul_tmp=self.qchip.gates[qubitid_t+'rabi'].pcalc(dt=ti180,padd=phase_offset)[0]
				amp_simul_tmp=self.qchip.gates[qubitid_t+'rabi'].paralist[0]['amp']*amp_multiplier
				modrabi_simul.update(dict(twidth=wrabi,dest=qubitid_c+'.qdrv',amp=amp_simul_tmp,pcarrier=pcarrier_simul_tmp))
				self.seqs.add(trabi,         	 self.qchip.gates[qubitid_t+'rabi'].modify(modrabi_simul))

			treaddrv=self.seqs.tend()
			self.seqs.add(treaddrv,self.qchip.gates[qubitid_c+'read'])
			self.seqs.add(treaddrv,self.qchip.gates[qubitid_t+'read'].modify([modrdrv,modread]))
			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
		accout_list=['accout_0','accout_1','accout_2']
		self.accout_dict=dict(zip(qubitidread,accout_list))
		print('fread,fqubit,readoutdrvamp,qubitdrvamp',fread,fqubit,readoutdrvamp,qubitdrvamp)
	def rabixtalkacq(self,nget):
		data=self.acqdata(nget)
		return data
	def processrabixtalk(self,dt,filename,dumpdataset,loaddataset,plot=False):
		c=self.loadjsondata(filename)
		print('self.bufwidth_dict',self.bufwidth_dict,'   self.accout_dict',self.accout_dict)
		print('accout in use: ',self.accout_dict[self.qubitid_c],self.accout_dict[self.qubitid_t])
		data_list=[c[self.accout_dict[self.qubitid_c]],c[self.accout_dict[self.qubitid_t]]]
		ctrl_result=self.process3(data_list[0],qubitid=self.qubitid_c,lengthperrow=self.bufwidth_dict[self.qubitid_c],training=False,loaddataset=loaddataset)
		tgt_result=self.process3(data_list[1],qubitid=self.qubitid_t,lengthperrow=self.bufwidth_dict[self.qubitid_t],training=False,loaddataset=loaddataset)
		#[amp_ctrl,period_ctrl,fiterr_ctrl]=self.fitrabi(dx=dt,data=ctrl_result['population_norm'],plot=plot,figname=filename)
		[amp_ctrl,period_ctrl,tdecay_ctrl,fiterr_ctrl]=self.fitrabidecay(dt=dt,data=ctrl_result['population_norm'],plot=plot,figname=filename)
		#[amp_tgt,period_tgt,fiterr_tgt]=self.fitrabi(dx=dt,data=tgt_result['population_norm'],plot=plot,figname=filename)
		[amp_tgt,period_tgt,tdecay_tgt,fiterr_tgt]=self.fitrabidecay(dt=dt,data=tgt_result['population_norm'],plot=plot,figname=filename)
		return [data_list[0],ctrl_result['separation'],ctrl_result['iqafterherald'],ctrl_result['population_norm'],amp_ctrl,period_ctrl,fiterr_ctrl,data_list[1],tgt_result['separation'],tgt_result['iqafterherald'],tgt_result['population_norm'],amp_tgt,period_tgt,fiterr_tgt]

if __name__=="__main__":
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	rabi_rate=[]
	amp_multiplier_map=numpy.arange(0.0,2.1,0.5)
	phase_offset_map=numpy.arange(0.0,numpy.pi*2,numpy.pi/3)
	ctrl='X180'
	#ctrl='I'
	for amp_multiplier in amp_multiplier_map:
		rabi_rate_tmp=[]
		for phase_offset in phase_offset_map:
			print('amp_multiplier',amp_multiplier)
			print('phase_offset',phase_offset)
			parser,cmdlinestr=experiment.cmdoptions()
			print(cmdlinestr)
			parser.set_defaults(elementstep=4e-9)
			clargs=parser.parse_args()
			rabixtalk=c_rabixtalk(**clargs.__dict__)
			if clargs.sim:
				rabixtalk.setsim()
			rabixtalk.rabixtalkseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readwidth=clargs.readwidth,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,elementlength=clargs.elementlength,elementstep=clargs.elementstep,fqubit=clargs.fqubit,preadout=clargs.preadout,fread=clargs.fread,qubitid_t=clargs.qubitid_t,qubitid_c=clargs.qubitid_c,qubitidread=clargs.qubitidread,amp_multiplier=amp_multiplier,phase_offset=phase_offset,ctrl=ctrl)
			if clargs.processfile=='':
				rabixtalk.run(bypass=clargs.bypass)
				data=rabixtalk.rabixtalkacq(clargs.nget)
				fprocess=rabixtalk.savejsondata(filename=clargs.filename+'_'+ctrl+'_scanamp'+str(amp_multiplier)+'_scanphase'+str(phase_offset),extype='rabixtalk',cmdlinestr=cmdlinestr,data=data)
				print('save data to ',fprocess)
				if clargs.sim:
					rabixtalk.sim()
			else:
				fprocess=clargs.processfile
			[rawdata_c,separation_c,iqafterherald_c,population_norm_c,amp_c,period_c,fiterr_c,rawdata_t,separation_t,iqafterherald_t,population_norm_t,amp_t,period_t,fiterr_t]=rabixtalk.processrabixtalk(dt=clargs.elementstep,filename=fprocess,dumpdataset=fprocess[:-4],loaddataset=clargs.dataset,plot=clargs.plot)
			rabi_rate_tmp.append(1.0/period_t)
			print('period_t,period_c',period_t,period_c)
			print('separation_t,separation_c',separation_t,separation_c)
			print('amp_t,amp_c',amp_t,amp_c)
			#rabixtalk.plotrawdata(d1=rawdata,figname='blobs'+fprocess)
			rabixtalk.plotpopulation_norm(population_norm=population_norm_t,figname='population_t_'+fprocess)
			rabixtalk.plotpopulation_norm(population_norm=population_norm_c,figname='population_c_'+fprocess)
			print(rabixtalk.hf.adcminmax())
			if clargs.plot:
				pyplot.grid()
				#pyplot.show()
		rabi_rate.append(rabi_rate_tmp)
		with open(clargs.filename+'rabi_xtalk_'+timestamp+'.dat','a') as f:
			numpy.savetxt(f,numpy.array(rabi_rate_tmp).reshape((1,-1)))
	pyplot.figure()
	pyplot.pcolormesh(rabixtalk.repack1D_cent2edge(phase_offset_map),rabixtalk.repack1D_cent2edge(amp_multiplier_map),rabi_rate,cmap='RdBu_r')
	pyplot.legend()
	pyplot.xlabel('Phase Offset (rad)')
	pyplot.ylabel('Amplitude Multiplier')
	pyplot.colorbar(label='Rabi Rate (Hz)')
	pyplot.savefig(clargs.filename+'_rabixtalk_'+'qubitid_c'+clargs.qubitid_c+'_qubitid_t'+clargs.qubitid_t+'_'+timestamp+'.pdf')
	#python rabi_xtalk_2read.py --plot -n 20 --qubitid_c Q6 --qubitid_t Q5 -es 8e-9 -el 40
