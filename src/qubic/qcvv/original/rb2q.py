import matplotlib
matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
from matplotlib import pyplot,patches
import datetime
import argparse
import sys
import numpy
import time
import init
import pprint
import experiment
import copy
from itertools import product

class c_rb2q(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid_list=None
		pass
	def rb2qseqs(self,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,restart=True,qubitid_list=['Q6','Q5'],qubitidread=['Q7','Q6','Q5'],rbgates=[[['Q6X90','Q5X90']],[['I','I'],['Q6Z90','Q5Z-90']]],cnotZcorr_c=0,cnotZcorr_t=0):
		self.qubitid_list=qubitid_list
		if restart:
			self.initseqs()
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		run=0
		for gates in rbgates:
			therald=run
			theraldread=run+delayread
			for qubitid in qubitid_list:
				self.seqs.add(therald,self.qchip.gates[qubitid+'readoutdrv'])
				self.seqs.add(theraldread,self.qchip.gates[qubitid+'readout'])
			run=self.seqs.tend()+delay1
			tini=run
			padd_dict=dict(zip(qubitid_list,[0]*len(qubitid_list)))
			for gate2q in gates:
				if all(['meas' in x for x in gate2q]):
					treaddrv=self.seqs.tend()
					treadout=treaddrv+delayread
					for qubitid in qubitid_list:
						self.seqs.add(treaddrv,self.qchip.gates[qubitid+'readoutdrv'])
						self.seqs.add(treaddrv+delayread,self.qchip.gates[qubitid+'readout'])
					run=self.seqs.tend()+delaybetweenelement
				elif all(['CNOT' in x for x in gate2q]):
					modcr={}
					modxgate={}
					qubitid_c=gate2q[0][:2]
					qubitid_t=gate2q[0][2:4]
					for qubitid,corr in zip([qubitid_c,qubitid_t],[cnotZcorr_c,cnotZcorr_t]):
						padd_dict[qubitid]=padd_dict[qubitid]-corr/180.*numpy.pi
					modcr.update(dict(pcarrier=self.qchip.gates['CR('+qubitid_c+qubitid_t+')'].pcalc(dt=run-tini,padd=padd_dict[qubitid_c])[0]))
					self.seqs.add(run,self.qchip.gates['CR('+qubitid_c+qubitid_t+')'].modify(modcr))
					run=self.seqs.tend()
					modxgate.update(dict(pcarrier=self.qchip.gates['CNOT('+qubitid_c+qubitid_t+').'+qubitid_t+'X'].pcalc(dt=run-tini,padd=padd_dict[qubitid_t])[0]))
					self.seqs.add(self.seqs.tend(),self.qchip.gates['CNOT('+qubitid_c+qubitid_t+').'+qubitid_t+'X'].modify(modxgate))
					run=self.seqs.tend()
				else:
					for i,gate in enumerate(gate2q):
						gatemodi={}
						if gate[2:]=='pass':
							pass
						else:
							if 'Z' in gate:
								padd_dict[gate[:2]]=padd_dict[gate[:2]]-float(gate[3:])/180.*numpy.pi
							else:
								pnew=self.qchip.gates[gate].pcalc(dt=run-tini,padd=padd_dict[gate[:2]])[0]
								gatemodi.update(dict(pcarrier=pnew))
								self.seqs.add(run,self.qchip.gates[gate].modify(gatemodi))
					run=self.seqs.tend()
		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
	def processrb2q(self,filename,loaddataset_list=None):
		c=self.loadjsondata(filename)
		print('c.keys(),self.bufwidth_dict',c.keys(),self.bufwidth_dict)
		data_list=[c[list(c.keys())[0]],c[c.keys()[1]]]  # data_list, qubitid_list correspond to loelementsdest in hfbridge
		meas_binned=self.process2q(data_list=data_list,qubitid_list=self.qubitid_list,lengthperrow_list=[self.bufwidth_dict[qid] for qid in self.qubitid_list],loaddataset_list=loaddataset_list)
		return meas_binned


if __name__=="__main__":
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	circuits_file='circuits_20200515_222257_247882.txt'
	header=open(circuits_file,'r').read().splitlines()
	qubitid_list=header[0].split(' ')
	circuit_length=[int(x) for x in header[1].split(' ')]
	n_circuits=int(header[2])
	print('qubitid_list:',qubitid_list,'   circuit_length:',circuit_length,'   n_circuits:',n_circuits)
	circuits=numpy.loadtxt(circuits_file,dtype=str,skiprows=3).tolist()
	idx_list=[idx+1 for idx,val in enumerate(circuits) if val==[qubitid_list[0]+'meas',qubitid_list[1]+'meas']]
	rbgates_list=[circuits[i: j] for i, j in zip([0]+idx_list, idx_list+([len(circuits)] if idx_list[-1]!=len(circuits) else []))]
	rbgates_full=[]
	for i_circ in range(n_circuits):
		rbgates_full.append(rbgates_list[i_circ*len(circuit_length):(i_circ+1)*len(circuit_length)])
	cnotZcorr_c=1.18*180.0-3.0
	cnotZcorr_t=0.0
	#cnotXcorr_t= 0.4*180.0+3.0 to txgate   Value should be set after cnot optimization in qubitcfg.json (CNOT(Q7Q6).Q6X twidth). Should use cnot gate in the future.
	#tcr=120e-9   Value should be set after cnot optimization in qubitcfg.json (CR(Q7Q6) twidth). Should use cnot gate in the future.
	for i_circ in range(n_circuits):
		print('measurement: %3d of %3d'%(i_circ+1,n_circuits))
		parser,cmdlinestr=experiment.cmdoptions()
		clargs=parser.parse_args()
		rb2q=c_rb2q(**clargs.__dict__)
		if clargs.sim:
			rb2q.setsim()
		rb2q.rb2qseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,qubitid_list=qubitid_list,qubitidread=clargs.qubitidread,rbgates=rbgates_full[i_circ],cnotZcorr_c=cnotZcorr_c,cnotZcorr_t=cnotZcorr_t)
		rb2q.run()
		data=rb2q.acqdata(clargs.nget)
		fprocess=rb2q.savejsondata(filename=clargs.filename,extype='rb2q',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
		if clargs.sim:
			rb2q.sim()
		meas_binned=rb2q.processrb2q(filename=fprocess,loaddataset_list=None)
		#population_norm_tmp=meas_binned[:len(meas_binned)/4].reshape((1,-1))
		population_norm_tmp=meas_binned[0].reshape((1,-1))
		population_norm_array=population_norm_tmp if i_circ==0 else numpy.append(population_norm_array,population_norm_tmp,axis=0)
		with open('rb2q_population_norm_'+timestamp+'.dat','a') as f:
			numpy.savetxt(f,population_norm_tmp)
		print(rb2q.hf.adcminmax())
	#population_norm_array=population_norm_array[2:,:]
	print('population_norm_array',population_norm_array)
	recovery_population_array=1-population_norm_array
	recovery_population_mean=numpy.mean(recovery_population_array,axis=0)
	recovery_population_std=numpy.std(recovery_population_array,axis=0)
	el_clif_list_pair=numpy.array(circuit_length)
	numpy.savetxt('rb2q_recovery_population_array'+fprocess,recovery_population_array)
	rb2q.plotrb(recovery_population_array,recovery_population_mean,recovery_population_std,el_clif_list_pair,figname=fprocess)
	[rb_p,rb_a]=rb2q.fitrb(el_clif_list_pair,recovery_population_mean,yerr=recovery_population_std,figname=fprocess)
	print('rb_p,rb_a',rb_p,rb_a)
	if clargs.plot:
		pyplot.grid()
		pyplot.show()
