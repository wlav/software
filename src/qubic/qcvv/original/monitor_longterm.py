import matplotlib
matplotlib.use('Agg')
import datetime
import argparse
import sys
from squbic import *
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import experiment
from rabi_w import c_rabi
from t1meas import c_t1meas
from ramsey import c_ramsey
from spinecho import c_spinecho
from vna import c_vna

if __name__=="__main__":
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	exper=experiment.c_experiment()
	parser,cmdlinestr=experiment.cmdoptions()
	clargs=parser.parse_args()
	elementstep_rabi=4e-9
	elementstep_t1meas=2e-6
	elementstep_ramsey=1e-6
	elementstep_spinecho=1e-6
	delaybetweenelement_vna=2e-6
	elementlength_vna=1024
	vnapara_dict={'Q6':{'readoutdrvamp':exper.qchip.gates['Q6readoutdrv'].paralist[0]['amp'],'fstart':exper.qchip.qubits['Q6'].readfreq-3e6,'fstop':exper.qchip.qubits['Q6'].readfreq+3e6},
			'Q5':{'readoutdrvamp':exper.qchip.gates['Q5readoutdrv'].paralist[0]['amp'],'fstart':exper.qchip.qubits['Q5'].readfreq-3e6,'fstop':exper.qchip.qubits['Q5'].readfreq+3e6}}
	while True:
		try:
			for index,qubitid in enumerate(['Q5','Q6']):
				result=[datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')]

				rabi=c_rabi()
				rabi.rabiseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readwidth=clargs.readwidth,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,elementlength=clargs.elementlength,elementstep=elementstep_rabi,fqubit=clargs.fqubit,preadout=clargs.preadout,fread=clargs.fread,qubitid=qubitid,qubitidread=clargs.qubitidread)
				rabi.run(bypass=clargs.bypass)
				data=rabi.rabiacq(clargs.nget)
				fprocess=rabi.savejsondata(filename=clargs.filename,extype=qubitid+'_rabi',cmdlinestr=cmdlinestr,data=data,timeinfo=False)
				[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=rabi.processrabi(dt=elementstep_rabi,filename=fprocess,dumpdataset=fprocess[:-4],loaddataset=clargs.dataset,plot=False)
				result.extend([qubitid,str(period),str(separation),str(amp)])
				print('qubitid',qubitid,'  period',period,'  separation',separation,'  amp',amp)

				t1meas=c_t1meas()
				t1meas.t1measseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,elementstep=elementstep_t1meas,qubitid=qubitid,qubitidread=clargs.qubitidread)
				t1meas.run(bypass=clargs.bypass)
				data=t1meas.acqdata(clargs.nget)
				fprocess=t1meas.savejsondata(filename=clargs.filename,extype=qubitid+'t1meas',cmdlinestr=cmdlinestr,data=data,timeinfo=False)
				[rawdata_t1meas,separation_t1meas,iqafterherald_t1meas,population_norm_t1meas,t1fit,fiterr_t1meas]=t1meas.processt1meas(dt=elementstep_t1meas,filename=fprocess,loaddataset=clargs.dataset)
				result.append(str(t1fit))
				print('t1fit',t1fit)

				ramsey=c_ramsey()
				ramsey.ramseyseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readwidth=clargs.readwidth,elementlength=clargs.elementlength,elementstep=elementstep_ramsey,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,framsey=clargs.framsey,qubitid=qubitid,qubitidread=clargs.qubitidread)
				ramsey.run(bypass=clargs.bypass)
				data=ramsey.acqdata(clargs.nget)
				fprocess=ramsey.savejsondata(filename=clargs.filename,extype=qubitid+'ramsey',cmdlinestr=cmdlinestr,data=data,timeinfo=False)
				[rawdata_ramsey,separation_ramsey,iqafterherald_ramsey,population_norm_ramsey,t2star,foffset,fiterr_ramsey]=ramsey.processramsey(dt=elementstep_ramsey,filename=fprocess,loaddataset=clargs.dataset)
				result.append(str(t2star))
				print('t2star',t2star)

				spinecho=c_spinecho()
				spinecho.spinechoseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,elementstep=elementstep_spinecho,framsey=clargs.framsey,qubitid=qubitid,qubitidread=clargs.qubitidread)
				spinecho.run(bypass=clargs.bypass)
				data=spinecho.acqdata(clargs.nget)
				fprocess=spinecho.savejsondata(filename=clargs.filename,extype=qubitid+'spinecho',cmdlinestr=cmdlinestr,data=data,timeinfo=False)
				[rawdata_spinecho,separation_spinecho,iqafterherald_spinecho,population_norm_spinecho,t2e,fiterr_spinecho]=spinecho.processspinecho(dt=elementstep_spinecho,filename=fprocess,loaddataset=clargs.dataset)
				result.append(str(t2e))
				print('t2e',t2e)

				vna=c_vna()
				vna.vnaseqs(delayread=clargs.delayread,delaybetweenelement=delaybetweenelement_vna,elementlength=elementlength_vna,readoutdrvamp=vnapara_dict[qubitid]['readoutdrvamp'],readwidth=clargs.readwidth,fstart=vnapara_dict[qubitid]['fstart'],fstop=vnapara_dict[qubitid]['fstop'],rdc=0,qubitidread=clargs.qubitidread)
				vna.run(bypass=clargs.bypass)
				data=vna.vnadata(clargs.nget)
				fprocess=vna.savejsondata(filename=clargs.filename,extype='vna',cmdlinestr=cmdlinestr+'_readoutdrvamp'+str(vnapara_dict[qubitid]['readoutdrvamp'])+'_fstart'+str(vnapara_dict[qubitid]['fstart'])+'_fstop'+str(vnapara_dict[qubitid]['fstop']),data=data)
				c=vna.processvna(fprocess)

				with open('monitor_'+timestamp+'.dat','a') as f:
					f.write(' '.join(result)+'\n')
					f.close()
		except KeyboardInterrupt:
			break

# python monitor_longterm.py -n 50
