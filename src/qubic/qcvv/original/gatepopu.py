import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import experiment
class c_gatepopu(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid=None
		pass
	def gatepopuseqs(self,gate,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,elementlength=80,elementstep=4e-9,readoutdrvamp=None,qubitdrvamp=None,readwidth=None,fqubit=None,fread=None,rdc=0,preadout=None,ngate=10,qubitid='Q7',qubitidread=['Q5','Q4','Q3']):
		self.qubitid=qubitid
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')

		run=0;
		gate=qubitid+gate if gate!='I' else gate
		for irun in range(elementlength):
			therald=run
			self.seqs.add(therald,self.qchip.gates[qubitid+'read'])
			run=self.seqs.tend()+delay1
			tini=run
			for igate in range(ngate):
				modqNgatepopu={}
				if gate=='I':
					run=run
				else:
					pnew=self.qchip.gates[gate].pcalc(dt=run-tini)[0]
					#if run-tini>1e-14:
					#	modqNgatepopu.update(dict(pcarrier=pnew))
					modqNgatepopu.update(dict(pcarrier=float(pnew)))
					if qubitdrvamp:
						#modqNgatepopu.update(dict(amp=qubitdrvamp))
						modqNgatepopu.update(dict(amp=float(qubitdrvamp)))
					self.seqs.add(run,self.qchip.gates[gate].modify(modqNgatepopu))
					#print 'tgatepopu',run,igate,modqNgatepopu
					run=self.seqs.tend()
			treaddrv=run
			self.seqs.add(treaddrv,self.qchip.gates[qubitid+'read'])
			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		#self.bufwidth=[self.seqs.countdest('Q5.read'),self.seqs.countdest('Q4.read'),self.seqs.countdest('Q3.read')]
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
	def gatepopuacq(self,nget):
		data=self.acqdata(nget)
		return data
	def processgatepopu(self,dt,filename,loaddataset,plot=False):
		c=self.loadjsondata(filename)
		print('c.keys()',c.keys())
		data=c[list(c.keys())[0]]
		return self.processgatepopudata(dt=dt,data=data,loaddataset=loaddataset,plot=plot)
	def processgatepopudata(self,dt,data,loaddataset,plot=False):
		#self.gatepopu_result=self.process3(data,lengthperrow=max(self.bufwidth),training=False,loadname=loadname)
		self.gatepopu_result=self.process3(data,qubitid=self.qubitid,lengthperrow=self.bufwidth_dict[self.qubitid],training=False,loaddataset=loaddataset)
		return [data,self.gatepopu_result['separation'],self.gatepopu_result['iqafterherald'],self.gatepopu_result['population_norm']]
	def plotgatepopucol(self,bufwidth):
		pass


if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	print(cmdlinestr)
	clargs=parser.parse_args()
	gatepopu=c_gatepopu(**clargs.__dict__)
	gatepopu.gatepopuseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readwidth=clargs.readwidth,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,elementlength=clargs.elementlength,fqubit=clargs.fqubit,preadout=clargs.preadout,fread=clargs.fread,gate=clargs.gate,ngate=1001,qubitid=clargs.qubitid,qubitidread=clargs.qubitidread)
	if clargs.processfile=='':
		if clargs.sim:
			gatepopu.hf.sim1()
		gatepopu.run(bypass=clargs.bypass)
		data=gatepopu.gatepopuacq(clargs.nget)
		fprocess=gatepopu.savejsondata(filename=clargs.filename,extype='gatepopu',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
	else:
		fprocess=clargs.processfile
	[rawdata,separation,iqafterherald,population_norm]=gatepopu.processgatepopu(dt=clargs.elementstep,filename=fprocess,loaddataset=clargs.dataset,plot=clargs.plot)
	gatepopu.plotrawdata(d1=rawdata,figname='blobs'+fprocess)
	gatepopu.plotafterheraldingtest(iqafterherald)
	gatepopu.plotpopulation_norm(population_norm=population_norm,figname='population'+fprocess)
	if clargs.plot:
		pyplot.grid()
		pyplot.show()
	print(gatepopu.hf.adcminmax())
		#gatepopu.plotgatepopucol(bufwidth=80)   # uncomment this line and "return" line in experience.process3 when dealing with the animation
