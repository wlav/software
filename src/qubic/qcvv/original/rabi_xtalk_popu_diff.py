import matplotlib
matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
from experiment import c_experiment
import glob
import os
from rabi_xtalk import c_rabixtalk

if __name__=="__main__":
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	#amp_multiplier_map=numpy.arange(0.0,2.0,0.125) #***post-processing***#
	#phase_offset_map=numpy.arange(0.0,numpy.pi*2,numpy.pi/8) #***post-processing***#
	amp_multiplier_map=numpy.arange(0.625,0.8,0.125)
	#phase_offset_map=numpy.arange(0.0,numpy.pi*2,numpy.pi/16)
	phase_offset_map=numpy.arange(0.0,2.0,0.025)

	#file_name_I=glob.glob("./tmptmptmp_replot/*.dat") #***post-processing***#
	#file_name_I.sort(key=os.path.getmtime) #***post-processing***#
	#file_name_X180=glob.glob("./tmptmptmp_replot_X180/*.dat") #***post-processing***#
	#file_name_X180.sort(key=os.path.getmtime) #***post-processing***#
	#file_name=[file_name_I,file_name_X180] #***post-processing***#
	for index1,amp_multiplier in enumerate(amp_multiplier_map):
		popu_data=[]
		for index_gate,ctrl in enumerate(['I','X180']):
			popu_data_tmp=[]
			for index2,phase_offset in enumerate(phase_offset_map):
				rabixtalk=c_rabixtalk()
				parser,cmdlinestr=rabixtalk.cmdoptions()
				print cmdlinestr
				parser.set_defaults(elementstep=4e-9)
				clargs=parser.parse_args()
				if clargs.sim:
					rabixtalk.setsim()
				rabixtalk.rabixtalkseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readwidth=clargs.readwidth,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,elementlength=clargs.elementlength,elementstep=clargs.elementstep,fqubit=clargs.fqubit,preadout=clargs.preadout,fread=clargs.fread,qubitid_t=clargs.qubitid_t,qubitid_c=clargs.qubitid_c,qubitidread=clargs.qubitidread,amp_multiplier=amp_multiplier,phase_offset=phase_offset,ctrl=ctrl)
				if clargs.processfile=='':
				#if False: #***post-processing***#
					rabixtalk.run(bypass=clargs.bypass)
					data=rabixtalk.rabixtalkacq(clargs.nget)
					fprocess=rabixtalk.savejsondata(filename=clargs.filename+'_'+ctrl+'_scanamp'+str(amp_multiplier)+'_scanphase'+str(phase_offset),extype='rabixtalk',cmdlinestr=cmdlinestr,data=data)
					print 'save data to ',fprocess
					if clargs.sim:
						rabixtalk.sim()
				else:
					fprocess=clargs.processfile
					#fprocess=file_name[index_gate][index1*len(phase_offset_map)+index2] #***post-processing***#
				#[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr,foffset]=rabixtalk.processrabixtalk(dt=clargs.elementstep,filename=fprocess,dumpdataset='tmp',loaddataset=clargs.dataset,plot=clargs.plot) #***post-processing***#
				[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr,foffset]=rabixtalk.processrabixtalk(dt=clargs.elementstep,filename=fprocess,dumpdataset=fprocess[:-4],loaddataset=clargs.dataset,plot=clargs.plot)
				popu_data_tmp.append(population_norm)
				#print 'period',period
				#print 'separation',separation
				#print 'amp',amp
				#rabixtalk.plotrawdata(d1=rawdata,figname='blobs'+fprocess)
				#rabixtalk.plotpopulation_norm(population_norm=population_norm,figname='population'+fprocess)
				#print rabixtalk.hf.adcminmax()
				if clargs.plot:
					pyplot.grid()
					#pyplot.show()
			popu_data.append(popu_data_tmp)
		popu_data.append((numpy.array(popu_data)[0]-numpy.array(popu_data)[1]).tolist())
		for index_plot,item in enumerate(popu_data):
			pyplot.figure()
			x_axis=clargs.elementstep*numpy.arange(clargs.elementlength)
			y_axis=phase_offset_map
			pyplot.pcolormesh(rabixtalk.repack1D_cent2edge(x_axis),rabixtalk.repack1D_cent2edge(y_axis),item,cmap='RdBu_r')
			if index_plot==2:
				fig_title='Difference'
				#pyplot.clim(-1,1)
			else:
				fig_title='Control|%s>'%str(index_plot)
				pyplot.clim(0,1)
			pyplot.legend()
			pyplot.xlabel('Pulse Length (s)')
			pyplot.ylabel('Phase Offset (rad)')
			pyplot.colorbar(label='P(|1>)')
			pyplot.title(fig_title)
			pyplot.tight_layout()
			pyplot.savefig(clargs.filename+'_rabixtalk_popudiff_'+str(amp_multiplier)+'_'+fig_title+'_'+timestamp+'.pdf')

	#python rabi_xtalk_popu_diff.py --plot -n 20 --qubitid_c Q6 --qubitid_t Q5 -es 8e-9 -el 40 -b
	#python rabi_xtalk_popu_diff.py --plot -n 20 --qubitid_c Q6 --qubitid_t Q5 -es 8e-9 -el 40
