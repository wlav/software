#import matplotlib
#matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import copy
from experiment import c_experiment
from rabi_w import c_rabi
import qutip
import math

class c_crIYamp(c_rabi):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit'):
		c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg)
		self.qubitid_c=None
		self.qubitid_t=None
		pass
	def crIYampseqs(self,delayread=712e-9,delay1=12e-6,delaybetweenelement=600e-6,elementlength=80,elementstep=4e-9,qubitid_c='Q6',qubitid_t='Q5',ctrl='I',meas='I',pcr=0,acr=0.45,tygate=32e-9,aygate=0,qubitidread=['Q7','Q6','Q5'],fixed_ramp=False):
		self.qubitid_c=qubitid_c
		self.qubitid_t=qubitid_t
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		modcr={}
		modygate={}
		self.tcr_list=[]
		run=0
		for irun in range(elementlength):
			# Readout
			therald=run
			self.seqs.add(therald,self.qchip.gates[qubitid_t+'read'])

			run=self.seqs.tend()+delay1
			tini=run

			# Control state preparation (control qubit: I or X180)
			if ctrl=='X180':
				self.seqs.add(run,         	 self.qchip.gates[qubitid_c+'X180'])
				run=self.seqs.tend()

			# CR gate (drive the control qubit at the frequency of the target frequency)
			if fixed_ramp:
				tcr=elementstep*(irun+math.ceil(64e-9/elementstep))
				modcr.update(dict(amp=acr,twidth=tcr,pcarrier=self.qchip.gates['CR('+qubitid_c+qubitid_t+')'].pcalc(dt=run-tini,padd=pcr)[0]))
			else:
				tcr=elementstep*irun
				modcr.update(dict(amp=acr,twidth=tcr,pcarrier=self.qchip.gates['CR('+qubitid_c+qubitid_t+')'].pcalc(dt=run-tini,padd=pcr)[0],env=[{'env_func': 'cos_edge_square', 'paradict': {'ramp_fraction': 0.25,'ramp_length':None}}]))
			self.tcr_list.append(tcr)
			if tcr>1e-14:
				self.seqs.add(run,self.qchip.gates['CR('+qubitid_c+qubitid_t+')'].modify(modcr))
				run=self.seqs.tend()

			# CNOT Y gate (target qubit)
			if tygate>1e-14:
				modygate.update(dict(amp=abs(aygate),twidth=tygate,pcarrier=self.qchip.gates['CNOT('+qubitid_c+qubitid_t+').'+qubitid_t+'Y'].pcalc(dt=run-tini,padd=0 if aygate>=0 else numpy.pi)[0]))
				self.seqs.add(run,self.qchip.gates['CNOT('+qubitid_c+qubitid_t+').'+qubitid_t+'Y'].modify(modygate))
				run=self.seqs.tend()

			# Projection (target qubit: Y-90, X90 or I)
			if meas=='I':
				pmeas=0
			else:
				pmeas=self.qchip.gates[qubitid_t+meas].pcalc(dt=run-tini)[0]
				self.seqs.add(run,self.qchip.gates[qubitid_t+meas].modify(dict(pcarrier=pmeas)))

			# Readout
			treaddrv=self.seqs.tend()
			self.seqs.add(treaddrv,self.qchip.gates[qubitid_t+'read'])

			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
	def processcrIYamp(self,dt,filename,loaddataset):
		c=self.loadjsondata(filename)
		print('c.keys()',c.keys())
		data=c[list(c.keys())[0]]
		crIYamp_result=self.process3(data,qubitid=self.qubitid_t,lengthperrow=self.bufwidth_dict[self.qubitid_t],training=False,loaddataset=loaddataset)
		return [data,crIYamp_result['separation'],crIYamp_result['iqafterherald'],crIYamp_result['population_norm']]


if __name__=="__main__":
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	crIYamp=c_crIYamp()
	parser,cmdlinestr=crIYamp.cmdoptions()
	clargs=parser.parse_args()
	#parser.add_argument('--pcr',help='starting phase for CR gate',dest='pcr',type=float,default=3.925)
	#parser.add_argument('--tygate',help='Y rotation for CNOT gate',dest='tygate',type=float,default=0e-9)
	pcr=3.925
	acr=0.45
	tygate=32e-9
	aygate=-0.2#-0.1#0#0.1
	result_filename=clargs.filename+'_crIYamp'+'_pcr'+str(pcr)+'_tygate'+str(tygate)+'_'+timestamp
	if clargs.processfile=='':
		for ctrl in ['I','X180']:
			for meas in ['Y-90','X90','I']:
				crIYamp=c_crIYamp()
				parser,cmdlinestr=crIYamp.cmdoptions()
				clargs=parser.parse_args()
				#crIYamp.initseqs()
				if clargs.sim:
					crIYamp.setsim()
				crIYamp.crIYampseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,elementstep=clargs.elementstep,qubitid_c=clargs.qubitid_c,qubitid_t=clargs.qubitid_t,ctrl=ctrl,meas=meas,pcr=pcr,acr=acr,tygate=tygate,aygate=aygate,qubitidread=clargs.qubitidread,fixed_ramp=clargs.fixed_ramp)
				crIYamp.run()
				data=crIYamp.rabiacq(clargs.nget)
				fprocess=crIYamp.savejsondata(filename=clargs.filename,extype='crIYamp',cmdlinestr=cmdlinestr,data=data)
				print('save data to ',fprocess)
				if clargs.sim:
					crIYamp.sim()
				[rawdata,separation,iqafterherald,population_norm]=crIYamp.processcrIYamp(dt=clargs.elementstep,filename=fprocess,loaddataset=clargs.dataset)
				if clargs.readcorr:
					population_norm=crIYamp.readoutcorrection(qubitid_list=[clargs.qubitid_t],meas_binned=numpy.vstack((1-population_norm,population_norm)),corr_matrix=numpy.load(clargs.corrmx))[1]
					print('corrected population_norm',population_norm)
				if clargs.plot:
					pyplot.grid()
					pyplot.show()
				pnt_traj=1-2*population_norm
				with open(result_filename+'.dat','a') as f:
					numpy.savetxt(f,[pnt_traj])
		with open(result_filename+'.dat','a') as f:
			numpy.savetxt(f,[numpy.array(crIYamp.tcr_list)])
		crdata=numpy.loadtxt(result_filename+'.dat')
	else:
		crdata=numpy.loadtxt(clargs.processfile)
	print('crdata[0,:]',crdata[0,:],crdata[0,:].shape)
	print('crdata[3,:]',crdata[3,:],crdata[3,:].shape)
	cost0=sum((crdata[0,:]-crdata[0,:].mean())**2)
	cost1=sum((crdata[3,:]-crdata[3,:].mean())**2)
	print('cost0',cost0,'cost1',cost1)
	for index,target_projection in enumerate(['Target <X>','Target <Y>','Target <Z>']):
		pyplot.figure()
		pyplot.plot(crdata[6,:],crdata[index,:],label='Control |0>',color='b')
		pyplot.plot(crdata[6,:],crdata[index+3,:],label='Control |1>',color='r')
		pyplot.legend()
		pyplot.xlabel('Pulse Length (ns)')
		pyplot.ylabel(target_projection)
		pyplot.ylim(-1.1,1.1)
		pyplot.savefig(result_filename+'_'+str(index)+'.png')
	distance=numpy.sqrt((crdata[0,:]-crdata[3,:])**2+(crdata[1,:]-crdata[4,:])**2+(crdata[2,:]-crdata[5,:])**2)/2.0
	pyplot.figure()
	pyplot.plot(crdata[6,:],distance)
	pyplot.xlabel('Pulse Length (ns)')
	pyplot.ylabel(r'$|\vec{R}|$')
	pyplot.ylim(-0.05,1.05)
	pyplot.savefig(result_filename+'_R'+'.png')
	print('distance',distance)
	bloch=qutip.Bloch()
	bloch.add_points(crdata[0:3,:])
	bloch.add_points(crdata[3:6,:])
	bloch.show()
	pyplot.show()
	bloch.save(result_filename+'_T'+'.png')

# python crIYamp.py -n 50 --qubitid_c Q6 --qubitid_t Q5 --readcorr
# python crIYamp.py -n 50 --qubitid_c Q6 --qubitid_t Q5 --readcorr --fixed_ramp
