import datetime
from matplotlib import pyplot
import numpy
import experiment
import json
import re
from simp2q import c_simp2q
import trueq
import qubic_trueq
import importlib


def cnottestbyrb(qubitid_list=['Q6','Q5'],n_random_cycles=[2,4,8,16,32],n_circuits=20,delaybetweenelement=600e-6,nget=2):
	n_random_cycles=n_random_cycles
	n_circuits=n_circuits
	parser,cmdlinestr=experiment.cmdoptions()
	clargs=parser.parse_args()
	simp2q=c_simp2q(**clargs.__dict__)

	config=trueq.Config.from_yaml('trueq_gate_'+clargs.qubitcfg[9:-5]+'.yaml')
	print('trueq_gatecfg',config)

	wiremap=importlib.import_module(clargs.wiremapmodule)
	gatesall=wiremap.gatesall
	elementlistall=wiremap.elementlistall
	destlistall=wiremap.destlistall
	patchlistall=wiremap.patchlistall

	delayafterheralding=12e-6
	delaybetweenelement=delaybetweenelement
	firsttime=True
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	circuits=qubic_trueq.trueq_srb(timestamp=timestamp,qubitid_list=qubitid_list,n_random_cycles=n_random_cycles,n_circuits=n_circuits,independent=False,readoutcorrection=True,twirl=None)#"SU")
	transpiler = trueq.Compiler.from_config(config)
	transpiled_circuit = transpiler.compile(circuits)
	trueq_result=[]
	index=0
	lastcmdarraylength=0
	for circuit in transpiled_circuit:
		index=index+1
		[qubitid,cmdarray]=qubic_trueq.trueq_circ_to_qubic2(circuit,heralding=delayafterheralding>0)
		gates=[simp2q.qchip.gates[qubitid_list[0]+qubitid_list[1]+'CNOT']]+[simp2q.qchip.gates[g] for g in gatesall if any([qid in g for qid in qubitid+['M0']]) and any([x in g for x in ['X90','read','mark']])]
		elementlist={k:elementlistall[k] for k in elementlistall.keys() if any([qid in k for qid in qubitid+['M0']])}
		destlist={k:destlistall[k] for k in destlistall.keys() if any([qid in k for qid in qubitid+['M0']])}
		patchlist={k:patchlistall[k] for k in patchlistall.keys() if any([qid in k for qid in qubitid+['M0']])}
		cmdarraylength=simp2q.simp2qseqsgen(delayafterheralding=delayafterheralding,delaybetweenelement=delaybetweenelement,cmdarray=cmdarray,gates=gates if firsttime else None,elementlist=elementlist,destlist=destlist,patchlist=patchlist,qubitid=qubitid)
		simp2q.run(memwrite=firsttime,memclear=firsttime,cmdclear=cmdarraylength<lastcmdarraylength,preread=firsttime,cmdclearlength=lastcmdarraylength*4)
		lastcmdarraylength=cmdarraylength
		data=simp2q.simp2qacq(nget)
		fprocess=simp2q.savejsondata(filename=clargs.filename,extype='simp2q',cmdlinestr=cmdlinestr,data=data)
		trueq_return,meas_binned=simp2q.processsimp2q(filename=fprocess,loaddataset=clargs.dataset)
		trueq_result.extend(trueq_return)
		firsttime=False
	print(trueq_result)
	timestampresult=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	resultfilename='circuits_'+timestamp+'_'+timestampresult+'.dat'
	print('resultfilename',resultfilename)
	with open(resultfilename,'w') as f_trueqresult:
		json.dump(trueq_result,f_trueqresult)
	result_circuits=qubic_trueq.rb1q_process(resultfilename,plot=True)#False)
	return result_circuits.fit()[0]
	#return [item for item in result_circuits.fit().subset()]

if __name__=="__main__":
	result_list=cnottestbyrb(qubitid_list=['Q6','Q5'],n_random_cycles=[2,8,16],n_circuits=10,delaybetweenelement=600e-6,nget=2)
