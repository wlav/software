import trueq
from matplotlib import pyplot
import numpy
import datetime
import sys
import time
import json
import re

def trueq_srb(timestamp,qubitid_list,n_random_cycles,n_circuits,readoutcorrection=True,independent=False,twirl=None):
	if independent:
		labels=[int(x[1:]) for x in qubitid_list]
	else:
		labels=[[int(x[1:]) for x in qubitid_list]]

	circuits = trueq.make_srb(labels, n_random_cycles, n_circuits,twirl=twirl)

	if (readoutcorrection):
		circuits.append(trueq.make_rcal(circuits.labels))
	circuits.save('circuits_'+timestamp+'.bin')
	return circuits

def trueq_xrb(timestamp,qubitid_list,n_random_cycles,n_circuits,readoutcorrection=True,independent=False,twirl=None):
	if independent:
		labels=[int(x[1:]) for x in qubitid_list]
	else:
		labels=[[int(x[1:]) for x in qubitid_list]]

	circuits = trueq.make_srb(labels, n_random_cycles, n_circuits, twirl=twirl)
	circuits += trueq.make_xrb(labels, n_random_cycles, n_circuits, twirl=twirl)

	if (readoutcorrection):
		circuits.append(trueq.make_rcal(circuits.labels))
	circuits.save('circuits_'+timestamp+'.bin')
	return circuits

def trueq_irb(timestamp,qubitid_list,n_random_cycles,n_circuits,interleaved_gate=trueq.Gate.cnot,readoutcorrection=True,twirl=None):
	if len(qubitid_list)==1:
		labels = [int(qubitid_list[0][1:])]
		cycle = {labels[0]: interleaved_gate}
	elif len(qubitid_list)==2:
		labels = [[int(x[1:]) for x in qubitid_list]]
		cycle = {tuple(labels[0]): interleaved_gate}
	else:
		print('how many qubit do you have?')
	circuits = trueq.make_irb(cycle, n_random_cycles, n_circuits, twirl=twirl)
	circuits += trueq.make_srb(labels, n_random_cycles, n_circuits, twirl=twirl)
	if (readoutcorrection):
		circuits.append(trueq.make_rcal(circuits.labels))
	circuits.save('circuits_'+timestamp+'.bin')
	return circuits

def trueq_cb(timestamp,qubitid_list,n_random_cycles=[4,12,64],n_circuits=30,gate=trueq.Gate.cnot,n_decays=20,readoutcorrection=True):
	if len(qubitid_list)==1:
		labels = [int(qubitid_list[0][1:])]
		cycle = {(labels[0],): gate}
	elif len(qubitid_list)==2:
		labels = [[int(x[1:]) for x in qubitid_list]]
		cycle = {tuple(labels[0]): gate}
	else:
		print('how many qubit do you have?')
	circuits = trueq.make_cb(cycle, n_random_cycles, n_circuits, n_decays)
	if (readoutcorrection):
		circuits.append(trueq.make_rcal(circuits.labels))
	circuits.save('circuits_'+timestamp+'.bin')
	return circuits

def rb1q_process(resultfilename,trueqsim=True,plot=True):
	pattern='(\S+_\S+_\S+_\S+)(_\S+_\S+_\S+).dat'
	m=re.match(pattern,resultfilename)
	if m:
		g=m.groups()
		resultfilename='%s%s.dat'%(g[0],g[1])
		circuits=trueq.load('%s.bin'%g[0])
		if trueqsim:
			s=trueq.Simulator()
			s.run(circuits)
			simulation_results=circuits.results
			print('simulation_results',simulation_results)
		with open(resultfilename) as resultfile:
			circuits.results=json.load(resultfile)
			measurement_results=circuits.results
		for i in range(len(measurement_results)):
			print('Sim',{k: v for k, v in sorted(simulation_results[i].items(), key=lambda item: item[1], reverse=True)},'Meas',{k: v for k, v in sorted(measurement_results[i].items(), key=lambda item: item[1], reverse=True)})
		if plot:
			print(circuits.fit().estimates)
			circuits.plot.raw()
			#pyplot.savefig(resultfilename+'.png')
			pyplot.savefig(resultfilename+'.pdf')
			pyplot.show()
	else:
		print('wrong result file name')
	return circuits

def trueq_circuits_to_qubic2(circuits,heralding=False,phmeas=False):
	cmdarray=[]
	circuitindex=0
	for circuit in circuits:
		(qubitids,cmdarray_1)=trueq_circ_to_qubic2(circuit,heralding=heralding,phmeas=phmeas)
		circuitindex+=1
		cmdarray.extend(cmdarray_1)
	#print('trueq_circuits_to_qubic2',len(cmdarray))
	return (qubitids,cmdarray)

def trueq_circ_to_qubic2(circuit,heralding=False,cnotphdeg=0,phmeas=False):
	labels=circuit.labels
	qubitids=['Q%d'%i for i in labels]
	cmdarray=[]
	cmdarray.append((False,[('M0mark',None,None)]))
#	for circuit in transpiled_circuit:
	freq={l:None for l in labels}
	phsdeg={l:0 for l in labels}
	newcircuit=True

	for cycle in circuit:
		#print(cycle.meas,cycle.gates)
		cmds=[]
		if heralding and newcircuit:
			for qid in qubitids:
				cmds.append((qid+'read',None,None))
			cmdarray.append((newcircuit,cmds))
			newcircuit=False
		cmds=[]
		for k,v in cycle.meas.items():
			qubitid=''.join(['Q%d'%i for i in k])
			cmds.append((qubitid+'read',None,0.0 if phmeas else None))
			gate='meas'
			phi=0
		for k,v in cycle.gates.items():
			qubitid=''.join(['Q%d'%i for i in k])
			gate=v.name.upper()
			if gate=='Z':
				phi=round(v.parameters['phi'],8)
				phsdeg[k[0]]+=phi
#				print('Z phsdeg[k[0]]',phsdeg[k[0]])
			elif gate in ['X','Y']:
				phi=round(v.parameters['phi'],8)
				if phi!=90:
					print('X gate phase: %s'%(str(phi)))
					exit(1)
				cmds.append(('%s%s%d'%(qubitid,gate,phi),freq[k[0]],phsdeg[k[0]]))
#				print('X phsdeg[k[0]]',phsdeg[k[0]])
			elif gate=='CNOT':
				cmds.append(('%s%s'%(qubitid,gate),(freq[k[0]],freq[k[1]]),(phsdeg[k[0]],phsdeg[k[1]]))) # ?? CNOT phini
				phsdeg[k[0]]+=cnotphdeg #(5.0+0.4-numpy.pi*0/2.0)/numpy.pi*180    # -pi/2 -pi -pi*3/2 -0.1
		#print(cycle,gate,phi)
		if len(cmds)>0:
			cmdarray.append((newcircuit,cmds))
		newcircuit=False
	cmdarray.append((False,[('M0mark',None,None)]))
#	print(cmdarray)
	return (qubitids,cmdarray)

if __name__=="__main__":
	starttime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')

	config=trueq.Config.from_yaml('trueq_gate_'+clargs.qubitcfg[9:-5]+'.yaml')
	print('trueq_gatecfg',config)

	#	if sys.argv[1]=='1q':
	qubitid_list = ['Q5']
	#else:
	#	qubitid_list = ['Q6', 'Q5']
	#n_random_cycles = [2, 64 ,256]
	#n_random_cycles = [2, 4]
	#n_random_cycles = [2, 4, 8]
	#n_random_cycles = [2, 64,256]
	#n_random_cycles = [4, 32, 128]
	#n_random_cycles = [4, 64, 256]
	#n_random_cycles = [4]
	#n_random_cycles = [1]
	n_random_cycles = [4,32,256 ]
	#n_random_cycles = [4,  64,  512]
	#n_circuits = 4
	n_circuits = 3 
	#n_circuits = 1 

	from simp import c_simp
	simp=c_simp()
	parser,cmdlinestr=simp.cmdoptions()
	clargs=parser.parse_args()

	gates=[simp.qchip.gates['M0mark']
,simp.qchip.gates['Q6X90']
,simp.qchip.gates['Q6read']
,simp.qchip.gates['Q5X90']
,simp.qchip.gates['Q5read']
]

	elementlist={'Q6.qdrv':2,'Q5.qdrv':3,'Q7.qdrv':1,'Q6.rdrv':5,'Q5.rdrv':6,'Q7.rdrv':4,'Q6.read':9,'Q5.read':10,'M0.mark':12}
	destlist=   {'Q6.qdrv':2,'Q5.qdrv':3,'Q7.qdrv':1,'Q6.rdrv':0,'Q5.rdrv':0,'Q7.rdrv':0,'Q6.read':5,'Q5.read':6,'M0.mark':13}
	patchlist={'Q6.rdrv':0,'Q6.read':0,'Q6.qdrv':0,'Q5.rdrv':8e-9,'Q5.read':8e-9,'Q5.qdrv':8e-9,'M0.mark':0}
	delayafterheralding=12e-6
	delaybetweenelement=600e-6
	#circuits_file=clargs.qubic_file
	firsttime=True

	testarray=[
			(0, [('M0mark', None, None)]),
			[1,[('Q6read',None,None)]]
			,[0,[('Q6X90',None,1.57)]]
			,(False, [('Q6X90', None, 90)])
			#			,[0,[('Q6X90',None,0)]]
			,[0,[('Q6read',None,None)]]
			,(0, [('M0mark', None, None)])
		]
	testarray=[
	(False, [('M0mark', None, None)])
	,(True, [('Q6read', None, None, 0)])
	,(False, [('Q6X90', None, -1.5707963267948966)])
	,(False, [('Q6X90', None, -1.5707963267948966)])
	,(False, [('Q6X90', None, -3.141592653589793)])
	,(False, [('Q6X90', None, 0.0)])
	,(False, [('Q6read', None, None, 0)])
	,(False, [('M0mark', None, None)])
	]
	if clargs.sim:
		simp.setsim()
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	datetimeformat='%Y%m%d_%H%M%S_%f'
	print('timestamp to start',(datetime.datetime.strptime(timestamp,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).seconds)
	circuits=trueq_srb(timestamp=timestamp,qubitid_list=clargs.qubitid_list,n_random_cycles=n_random_cycles,n_circuits=n_circuits)




	cirgentime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	transpiler = trueq.Compiler.from_config(config)
	transpiled_circuit = transpiler.compile(circuits)
	print('cirgentime to start',(datetime.datetime.strptime(cirgentime,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).seconds)
	trueq_result=[]
	index=0
	lastcmdarraylength=0
	for circuit in transpiled_circuit:
		print('index',index)
		index=index+1
#		filename='circuits_20200601_151531_811498.bin'
#		pattern='(\S+)_(\S+_\S+_\S+).bin'
#		m=re.match(pattern,filename)
#		timestamp=m.group(2)
#		print(timestamp)
#		circuits=trueq.load(filename)
		[qubitid,cmdarray]=trueq_circ_to_qubic2(circuit,heralding=delayafterheralding>0)
		trueq2qubic=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		print('trueq2qubic to start',(datetime.datetime.strptime(trueq2qubic,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).seconds)
		#print('cmdarray len',len(cmdarray))
		cmdarraylength=simp.simpseqsgen(delayafterheralding=delayafterheralding,delaybetweenelement=delaybetweenelement,cmdarray=cmdarray,gates=gates if firsttime else None,elementlist=elementlist,destlist=destlist,patchlist=patchlist,qubitid=qubitid)
		simpseqsgen=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		print('simpseqsgen to start',(datetime.datetime.strptime(simpseqsgen,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).seconds)
		#simp.simpseqsgen(delayafterheralding=delayafterheralding,delaybetweenelement=delaybetweenelement,cmdarray=testarray,gates=gates if firsttime else None,elementlist=elementlist,destlist=destlist,patchlist=patchlist,qubitid=qubitid)
		simp.run(memwrite=firsttime,memclear=firsttime,cmdclear=cmdarraylength<lastcmdarraylength,preread=firsttime,cmdclearlength=lastcmdarraylength*4)
		lastcmdarraylength=cmdarraylength
		print('cmdarray this',cmdarraylength,'last',lastcmdarraylength)
		simprun=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		print('simprun to start',(datetime.datetime.strptime(simprun,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).seconds)
		data=simp.simpacq(clargs.nget)
		simpacq=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		print('simpacq to start',(datetime.datetime.strptime(simpacq,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).seconds)
		fprocess=simp.savejsondata(filename=clargs.filename,extype='simp',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
		if clargs.sim:
			simp.sim()
#		[rawdata,separation,iqafterherald,population_norm]=simp.processsimp(filename=fprocess,loaddataset=clargs.dataset)
		[rawdata,separation,population_norm,count_0,count_1]=simp.processsimp(filename=fprocess,loaddataset=clargs.dataset)
#		simp.plotrawdata(d1=rawdata)
#		pyplot.show() 
		result=[{'0':int(count_0[i]),'1':int(count_1[i])} for i in range(len(count_0))]
		trueq_result.extend(result)
		firsttime=False

	print(trueq_result)
	s=trueq.Simulator()
	s.run(circuits)
	print(circuits.expectation_values())
	print(circuits.results)
	timestampresult=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	resultfilename='circuits_'+timestamp+'_'+timestampresult+'.dat'
	with open(resultfilename,'w') as f_trueqresult:
		json.dump(trueq_result,f_trueqresult)
##		print('resultfile %s'%resultfilename)
	beforerb1q_process=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	print('beforerb1q_process to start',(datetime.datetime.strptime(beforerb1q_process,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).seconds)
	rb1q_process(resultfilename)
