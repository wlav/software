import datetime
import argparse
import sys
from squbic import *
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
from scipy import signal
import experiment
class c_dps(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		pass
	def dpsseqs(self,delayread=532e-9,delaybetweenelement=600e-6,elementlength=1024,readoutdrvamp=None,readwidth=None,qubitid0='Q5',qubitid1='Q4',qubitid2='Q3'):
		dp=2*numpy.pi/(elementlength-1)
		self.phase=numpy.arange(0,2*numpy.pi+dp,dp)[:elementlength]
		modrdrv0={}
		modread0={}
		modrdrv1={}
		modread1={}
		modrdrv2={}
		modread2={}
		self.qubitid0=qubitid0
		self.qubitid1=qubitid1
		self.qubitid2=qubitid2
		if readoutdrvamp is not None:
			readoutdrvamp0,readoutdrvamp1,readoutdrvamp2=self.rdrvcalc(readoutdrvamp,dcoffset=0)
		else:
			readoutdrvamp0=0.333
			readoutdrvamp1=0.333
			readoutdrvamp2=0.333
		print('readoutdrvamp0,readoutdrvamp1,readoutdrvamp2',readoutdrvamp0,readoutdrvamp1,readoutdrvamp2)
		modrdrv0.update(dict(amp=readoutdrvamp0))
		modrdrv1.update(dict(amp=readoutdrvamp1))
		modrdrv2.update(dict(amp=readoutdrvamp2))
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		trun=0
		for prun in self.phase:
			modrdrv0.update(dict(pcarrier=prun))
			modrdrv1.update(dict(pcarrier=prun))
			modrdrv2.update(dict(pcarrier=prun))
			self.readall(trdrv=trun,qubitsamps={self.qubitid0:(modrdrv0,modread0),self.qubitid1:(modrdrv1,modread1),self.qubitid2:(modrdrv2,modread2)})
			trun=self.seqs.tend()+delaybetweenelement
		print('period',trun)
		self.seqs.setperiod(period=trun)
	def dpsdata(self,nget):
		data=self.acqdata(nget)
		return data
	def processdps(self,filename):
		c0=self.loadjsondata(filename)['accout_0']
		c1=self.loadjsondata(filename)['accout_1']
		c2=self.loadjsondata(filename)['accout_2']
		cavr0=c0.reshape((-1,len(self.phase))).mean(0)
		cavr1=c1.reshape((-1,len(self.phase))).mean(0)
		cavr2=c2.reshape((-1,len(self.phase))).mean(0)
		return [cavr0,cavr1,cavr2]
	def dpsplot(self,c0,c1,c2):
		pyplot.figure(1)
		pyplot.subplot(211)
		#pyplot.semilogy(self.phase,abs(c))
		pyplot.plot(self.phase,(abs(c0)-numpy.mean(abs(c0)))/numpy.mean(abs(c0)),label=self.qubitid0)
		pyplot.plot(self.phase,(abs(c1)-numpy.mean(abs(c1)))/numpy.mean(abs(c1)),label=self.qubitid1)
		pyplot.plot(self.phase,(abs(c2)-numpy.mean(abs(c2)))/numpy.mean(abs(c2)),label=self.qubitid2)
		print(self.qubitid0+' Vrms,Vmean,Vrms/Vmean',numpy.std(abs(c0)),numpy.mean(abs(c0)),1.0*(numpy.std(abs(c0)))/numpy.mean(abs(c0)))
		print(self.qubitid1+' Vrms,Vmean,Vrms/Vmean',numpy.std(abs(c1)),numpy.mean(abs(c1)),1.0*(numpy.std(abs(c1)))/numpy.mean(abs(c1)))
		print(self.qubitid2+' Vrms,Vmean,Vrms/Vmean',numpy.std(abs(c2)),numpy.mean(abs(c2)),1.0*(numpy.std(abs(c2)))/numpy.mean(abs(c2)))
		#pyplot.plot(self.phase,abs(c0),label=self.qubitid0)
		#pyplot.plot(self.phase,abs(c1),label=self.qubitid1)
		#pyplot.plot(self.phase,abs(c2),label=self.qubitid2)
		print(self.qubitid0+' Vpp,Vmean,Vpp/Vmean',max(abs(c0))-min(abs(c0)),numpy.mean(abs(c0)),1.0*(max(abs(c0))-min(abs(c0)))/numpy.mean(abs(c0)))
		print(self.qubitid1+' Vpp,Vmean,Vpp/Vmean',max(abs(c1))-min(abs(c1)),numpy.mean(abs(c1)),1.0*(max(abs(c1))-min(abs(c1)))/numpy.mean(abs(c1)))
		print(self.qubitid2+' Vpp,Vmean,Vpp/Vmean',max(abs(c2))-min(abs(c2)),numpy.mean(abs(c2)),1.0*(max(abs(c2))-min(abs(c2)))/numpy.mean(abs(c2)))
		pyplot.ylabel('Amplitude',fontsize=13)
		pyplot.tight_layout()
		pyplot.subplot(212)
		pyplot.plot(self.phase,signal.detrend(numpy.unwrap(numpy.angle(c0))),label=self.qubitid0)
		pyplot.plot(self.phase,signal.detrend(numpy.unwrap(numpy.angle(c1))),label=self.qubitid1)
		pyplot.plot(self.phase,signal.detrend(numpy.unwrap(numpy.angle(c2))),label=self.qubitid2)
		pyplot.ylabel('Phase (rad)',fontsize=13)
		pyplot.xlabel('Phase Shift (rad)',fontsize=13)
		print(self.qubitid0+' phase (rad)',max(signal.detrend(numpy.unwrap(numpy.angle(c0))))-min(signal.detrend(numpy.unwrap(numpy.angle(c0)))))
		print(self.qubitid1+' phase (rad)',max(signal.detrend(numpy.unwrap(numpy.angle(c1))))-min(signal.detrend(numpy.unwrap(numpy.angle(c1)))))
		print(self.qubitid2+' phase (rad)',max(signal.detrend(numpy.unwrap(numpy.angle(c2))))-min(signal.detrend(numpy.unwrap(numpy.angle(c2)))))
		if False:
			amp_result0=1.0*(max(abs(c0))-min(abs(c0)))/numpy.mean(abs(c0))
			amp_result1=1.0*(max(abs(c1))-min(abs(c1)))/numpy.mean(abs(c1))
			amp_result2=1.0*(max(abs(c2))-min(abs(c2)))/numpy.mean(abs(c2))
			phase_result0=max(signal.detrend(numpy.unwrap(numpy.angle(c0))))-min(signal.detrend(numpy.unwrap(numpy.angle(c0))))
			phase_result1=max(signal.detrend(numpy.unwrap(numpy.angle(c1))))-min(signal.detrend(numpy.unwrap(numpy.angle(c1))))
			phase_result2=max(signal.detrend(numpy.unwrap(numpy.angle(c2))))-min(signal.detrend(numpy.unwrap(numpy.angle(c2))))
			print('summary %.3e %.3e %.3e %.3e %.3e %.3e'%(amp_result0,amp_result1,amp_result2,phase_result0,phase_result1,phase_result2))
		#pyplot.plot(self.phase,numpy.unwrap(numpy.angle(c)))
		pyplot.legend()
		pyplot.tight_layout()
		pyplot.figure(2)
		ax1=pyplot.subplot(111)
		ax1.set_aspect('equal')
		lim0=max(1.1*max(max(abs(c0.real)),max(abs(c0.imag))),0.1)
		lim1=max(1.1*max(max(abs(c1.real)),max(abs(c1.imag))),0.1)
		lim2=max(1.1*max(max(abs(c2.real)),max(abs(c2.imag))),0.1)
		lim=max(lim0,lim1,lim2)
		ax1.set_xlim([-lim,lim])
		ax1.set_ylim([-lim,lim])
		ax1.set_xlabel('I (a.u.)',fontsize=13)
		ax1.set_ylabel('Q (a.u.)',fontsize=13)
		pyplot.plot(c0.real,c0.imag,'.',label=self.qubitid0)
		pyplot.plot(c1.real,c1.imag,'.',label=self.qubitid1)
		pyplot.plot(c2.real,c2.imag,'.',label=self.qubitid2)
		pyplot.legend()
if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(delaybetweenelement=2e-6)
	parser.set_defaults(elementlength=1024)
	clargs=parser.parse_args()
	dps=c_dps(**clargs.__dict__)
	qubitid0="Q5"
	qubitid1="Q4"
	qubitid2="Q3"
	dps.dpsseqs(delayread=clargs.delayread,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,readoutdrvamp=clargs.readoutdrvamp,readwidth=clargs.readwidth,qubitid0=qubitid0,qubitid1=qubitid1,qubitid2=qubitid2)
	if clargs.processfile=='':
		if clargs.sim:
			dps.sim()
		dps.run(bypass=clargs.bypass)
		data=dps.dpsdata(clargs.nget)
		fprocess=dps.savejsondata(filename=clargs.filename,extype='dps',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
	else:
		fprocess=clargs.processfile
	[c0,c1,c2]=dps.processdps(fprocess)
	print('adc min max',dps.hf.adcminmax())
	if clargs.plot:
		dps.dpsplot(c0,c1,c2)
		pyplot.grid()
		pyplot.show()
	# python dps.py --plot --delayread 532e-9 -n 20
