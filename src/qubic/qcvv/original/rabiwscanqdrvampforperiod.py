import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
from experiment import c_experiment
from rabi_w import c_rabi
from scipy.optimize import minimize,fmin,fminbound
def rabiwscanqdrvampforperiod(x0,fread,fqubit,readoutdrvamp,delayread,nget,period_target,delay1,delaybetweenelement,elementlength,elementstep,qubitid,qubitidread=['Q7','Q6','Q5'],qubitcfg='qubitcfg.json',ip='192.168.1.124'):
	dt=elementstep

	print('fread,fqubit,readoutdrvamp',fread,fqubit,readoutdrvamp)
	print('delayread,nget',delayread,nget)
	print('period_target',period_target)
	fread=fread
	fqubit=fqubit
	readoutdrvamp=readoutdrvamp
	qubitdrvamp=x0
	#readoutdrvamp=0.359
	#fread=159.014731445e6
	#fqubit=-85e6
	
	rabi=c_rabi(qubitcfg=qubitcfg,ip=ip)
	rabi.rabiseqs(delayread=delayread,delay1=delay1,delaybetweenelement=delaybetweenelement,elementlength=elementlength,elementstep=elementstep,rdc=0,fqubit=fqubit,fread=fread,qubitdrvamp=qubitdrvamp,readoutdrvamp=readoutdrvamp,qubitid=qubitid,qubitidread=qubitidread)
	rabi.run()
	data=rabi.rabiacq(nget)
	cmdlinestr='readoutdrvamp'+str(readoutdrvamp)+'_fread'+str(fread)+'_qubitdrvamp'+str(qubitdrvamp)+'_fqubit'+str(fqubit)+'_qubitid'+qubitid
	fprocess=rabi.savejsondata(filename='',extype='rabi',cmdlinestr=cmdlinestr,data=data)
	print('save data to ',fprocess)
	[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=rabi.processrabi(dt=dt,filename=fprocess,dumpdataset=fprocess[:-4],loaddataset='',plot=False,isfitdecay=False)
	#result=abs(0.5-amp)
	#result=-separation
	result=abs(period*1e9-period_target)
	print("########readoutdrvamp,fread,qubitdrvamp,fqubit########",readoutdrvamp,fread,qubitdrvamp,fqubit)
	print("################result,amp,separation#################",result,amp,separation)
	#rabi.plotrawdata(d1=rawdata,figname='blobs'+fprocess)
	#rabi.plotpopulation_norm(population_norm=population_norm,figname='population'+fprocess)
	sys.stdout.flush()
	return result
if __name__=="__main__":
	#fmin(rabiwscanqdrvampforperiod,-85e6,ftol=0.1)
	#fine=fminbound(func=rabiwscanqdrvampforperiod,x1=0.5,x2=0.55,xtol=0.001,disp=1)
	#coarse=fmin(rabiwscanqdrvampforperiod,0.5,ftol=0.1)
	#fine=fminbound(func=rabiwscanqdrvampforperiod,x1=coarse*0.9,x2=min(coarse*1.1,1.0),xtol=0.001,disp=1)
	qubitdrvamp_coarse=fmin(rabiwscanqdrvampforperiod,0.5,ftol=0.1,args=(159e6,-85e6,0.359,944e-9,50,64,'Q5'))[0]
	#qubitdrvamp_coarse=0.95
	qubitdrvamp=fminbound(func=rabiwscanqdrvampforperiod,x1=qubitdrvamp_coarse*0.9,x2=min(qubitdrvamp_coarse*1.1,1.0),xtol=0.001,disp=1,args=(159e6,-85e6,0.359,944e-9,50,64,'Q5'))
	print('qubitdrvamp',qubitdrvamp)
