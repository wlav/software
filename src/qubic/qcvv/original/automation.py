import matplotlib
matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
import numpy
import json
import re
import datetime
from squbic import c_qchip
#from alignment import c_alignment
#from vna import c_vna
from rabi_w import c_rabi
#from rabiwscanfreadforseparation import rabiwscanfreadforseparation
#from rabiwscanfqubitforseparation import rabiwscanfqubitforseparation
#from rabiwscanrdrvampforseparation import rabiwscanrdrvampforseparation
#from rabiwscanqdrvampforperiod import rabiwscanqdrvampforperiod
from rabiwscanfread import rabiwscanfread,freadoptimize
from rabiwscanfqubit import rabiwscanfqubit,fqubitoptimize
from rabiwscanrdrvamp import rabiwscanrdrvamp
from rabiwscanqdrvamp import rabiwscanqdrvamp
from gatepopuoptimize import gatepopuoptimize
from scipy.optimize import minimize,fmin,fminbound
#from rabiwscanfreadforamp import rabiwscanfreadforamp
#from rabiwscanrdrvampforamp import rabiwscanrdrvampforamp
#from rabiwscanfqubitforamp import rabiwscanfqubitforamp
from ramsey import c_ramsey
import experiment
from rb import c_rb
from switch import switch
import time


print(datetime.datetime.now())
datetime_tmp=datetime.datetime.now()

parser,cmdlinestr=experiment.cmdoptions()
clargs=parser.parse_args()

#qubitid_in_use=['Q0','Q1','Q2','Q3','Q4','Q5','Q6','Q7']
qubitid_in_use=['Q6','Q5']
switch_map={'Q0':6,'Q1':0,'Q2':2,'Q3':3,'Q4':5,'Q5':7,'Q6':1,'Q7':4}

# ***** Two-qubit gate requirements: readoutdrvamp < 0.5, qubitdrvamp < 0.5 *****
para_init_full={
		'Q0':{'fread_arg':-161.2e6,
			'fread_halfspan':3e6,
			'fqubit_arg':-60e6,
			'fqubit_halfspan':10e6,
			'readoutdrvamp_arg':0.30,
			'readoutdrvamp_halfspan':0.20,
			'qubitdrvamp_arg':0.20,
			'qubitdrvamp_halfspan':0.10,
			'alpha0':1.61,
			'readwidth':2e-6,
			'period_target':128,
			'elementlength_rabiwscan':80,
			'elementstep_rabiwscan':4e-9,
			'elementlength_ramsey':80,
			'elementstep_ramsey':2e-7},
		'Q1':{'fread_arg':-94.6e6,
			'fread_halfspan':3e6,
			'fqubit_arg':-270e6,
			'fqubit_halfspan':10e6,
			'readoutdrvamp_arg':0.35,
			'readoutdrvamp_halfspan':0.15,
			'qubitdrvamp_arg':0.40,
			'qubitdrvamp_halfspan':0.10,
			'alpha0':-0.95,
			'readwidth':2e-6,
			'period_target':128,
			'elementlength_rabiwscan':80,
			'elementstep_rabiwscan':4e-9,
			'elementlength_ramsey':80,
			'elementstep_ramsey':2e-7},
		'Q2':{'fread_arg':-37.1e6,
			'fread_halfspan':3e6,
			'fqubit_arg':-200e6,
			'fqubit_halfspan':10e6,
			'readoutdrvamp_arg':0.30,
			'readoutdrvamp_halfspan':0.20,
			'qubitdrvamp_arg':0.35,
			'qubitdrvamp_halfspan':0.15,
			'alpha0':0.97,
			'readwidth':2e-6,
			'period_target':128,
			'elementlength_rabiwscan':80,
			'elementstep_rabiwscan':4e-9,
			'elementlength_ramsey':80,
			'elementstep_ramsey':2e-7},
		'Q3':{'fread_arg':16.0e6,
			'fread_halfspan':3e6,
			'fqubit_arg':-30e6,
			'fqubit_halfspan':10e6,
			'readoutdrvamp_arg':0.35,
			'readoutdrvamp_halfspan':0.15,
			'qubitdrvamp_arg':0.25,
			'qubitdrvamp_halfspan':0.10,
			'alpha0':2.54,
			'readwidth':2e-6,
			'period_target':128,
			'elementlength_rabiwscan':80,
			'elementstep_rabiwscan':4e-9,
			'elementlength_ramsey':80,
			'elementstep_ramsey':2e-7},
		'Q4':{'fread_arg':70.1e6,
			'fread_halfspan':3e6,
			'fqubit_arg':50e6,
			'fqubit_halfspan':10e6,
			'readoutdrvamp_arg':0.30,
			'readoutdrvamp_halfspan':0.20,
			'qubitdrvamp_arg':0.25,
			'qubitdrvamp_halfspan':0.10,
			'alpha0':-1.48,
			'readwidth':2e-6,
			'period_target':128,
			'elementlength_rabiwscan':80,
			'elementstep_rabiwscan':4e-9,
			'elementlength_ramsey':80,
			'elementstep_ramsey':2e-7},
		'Q5':{'fread_arg':98.1e6,#128.0e6,
			'fread_halfspan':3e6,
			'fqubit_arg':-7e6,#230e6,
			'fqubit_halfspan':10e6,
			'readoutdrvamp_arg':0.25,#0.40,
			'readoutdrvamp_halfspan':0.05,#0.10,
			'qubitdrvamp_arg':0.40,
			'qubitdrvamp_halfspan':0.10,
			'alpha0':1.4,#-0.83,
			'readwidth':2e-6,
			'period_target':128,
			'elementlength_rabiwscan':80,
			'elementstep_rabiwscan':4e-9,
			'elementlength_ramsey':80,
			'elementstep_ramsey':2e-7},
		'Q6':{'fread_arg':154.5e6,#189.2e6,
			'fread_halfspan':3e6,
			'fqubit_arg':175e6,#185e6,
			'fqubit_halfspan':10e6,
			'readoutdrvamp_arg':0.33,#0.40,
			'readoutdrvamp_halfspan':0.15,#0.10,
			'qubitdrvamp_arg':0.40,#0.38,
			'qubitdrvamp_halfspan':0.10,
			'alpha0':0.1,#-0.25,
			'readwidth':2e-6,
			'period_target':128,
			'elementlength_rabiwscan':80,
			'elementstep_rabiwscan':4e-9,
			'elementlength_ramsey':80,
			'elementstep_ramsey':2e-7},
		'Q7':{'fread_arg':252.3e6,
			'fread_halfspan':3e6,
			'fqubit_arg':145e6,
			'fqubit_halfspan':10e6,
			'readoutdrvamp_arg':0.40,
			'readoutdrvamp_halfspan':0.10,
			'qubitdrvamp_arg':0.32,
			'qubitdrvamp_halfspan':0.10,
			'alpha0':0.06,
			'readwidth':2e-6,
			'period_target':128,
			'elementlength_rabiwscan':80,
			'elementstep_rabiwscan':4e-9,
			'elementlength_ramsey':80,
			'elementstep_ramsey':2e-7}
		}

para_init={qid: para_init_full[qid] for qid in qubitid_in_use}

# Reduce qubitdrvamp to make the Chevron fringes sharp so that rabiwscanfqubitforamp can find the accurate fqubit
cfgname=clargs.qubitcfg[:-5]
ip=clargs.ip
wiremapmodule=clargs.wiremapmodule
delayread=clargs.delayread
print('cfgname',cfgname)
print('ip',ip)
print('wiremapmodule',wiremapmodule)
print('delayread',delayread)
#qubitidread=clargs.qubitidread
#print(qubitidread)

flag_drag=True
flag_rb=False

delay1=12e-6
delaybetweenelement=600e-6

nget_rabiwscan=20
nget_ramsey=50
nget_gatepopuoptimize=2
nget_rabi=50

#elementstep_ramsey=2e-6 # 4e-7
gatelist=['X90']#,'Y90','X180','Y180','X270','Y270']#['X90','Y90','X180']

for qubitid in para_init:
	# analog switch if needed
	#switch(port=switch_map[qubitid])
	#time.sleep(5)

	with open(cfgname+'.json') as jfile:
		chipcfg=json.load(jfile)
	qchip=c_qchip(chipcfg)

	# qubit id
	print('qubitid',qubitid)

	# qubitidread
	qubitidread=[qubitid]
	print('qubitidread',qubitidread)

	# Initiate the read pulses
	qchip.updatecfg({('Gates',qubitid+'readout',0,'twidth'):para_init[qubitid]['readwidth'],('Gates',qubitid+'readoutdrv',0,'twidth'):para_init[qubitid]['readwidth'],('Gates',qubitid+'read',0,'twidth'):para_init[qubitid]['readwidth'],('Gates',qubitid+'read',1,'twidth'):para_init[qubitid]['readwidth'],('Gates',qubitid+'read',1,'t0'):delayread},wfilename=cfgname+'.json')

	if flag_drag:
		# Initiate the alpha for DRAG envelope
		alpha0=para_init[qubitid]['alpha0']
		qchip.updatecfg({('Gates',qubitid+'rabi',0,'env',0,'paradict','alpha'):alpha0,('Gates',qubitid+'rabiaxn',0,'env',0,'paradict','alpha'):alpha0,('Gates',qubitid+'X90',0,'env',0,'paradict','alpha'):alpha0,('Gates',qubitid+'Y90',0,'env',0,'paradict','alpha'):alpha0,('Gates',qubitid+'X180',0,'env',0,'paradict','alpha'):alpha0,('Gates',qubitid+'Y180',0,'env',0,'paradict','alpha'):alpha0,('Gates',qubitid+'X270',0,'env',0,'paradict','alpha'):alpha0,('Gates',qubitid+'Y270',0,'env',0,'paradict','alpha'):alpha0},wfilename=cfgname+'.json')


	qchip.updatecfg({('Gates',qubitid+'rabi',0,'twidth'):1e-6},wfilename=cfgname+'.json') # Q7rabi twidth: update in 'qubitcfg.json' # 64e-9, 132e-9, ...

	print(datetime.datetime.now())
	print(datetime.datetime.now()-datetime_tmp)
	datetime_tmp=datetime.datetime.now()


	fread_arg=para_init[qubitid]['fread_arg']
	fread_halfspan=para_init[qubitid]['fread_halfspan']
	fread_x1=fread_arg-fread_halfspan
	fread_x2=fread_arg+fread_halfspan

	fqubit_arg=para_init[qubitid]['fqubit_arg']
	fqubit_halfspan=para_init[qubitid]['fqubit_halfspan']
	fqubit_x1=fqubit_arg-fqubit_halfspan
	fqubit_x2=fqubit_arg+fqubit_halfspan

	readoutdrvamp_arg=para_init[qubitid]['readoutdrvamp_arg']
	readoutdrvamp_halfspan=para_init[qubitid]['readoutdrvamp_halfspan']
	readoutdrvamp_x1=readoutdrvamp_arg-readoutdrvamp_halfspan
	readoutdrvamp_x2=readoutdrvamp_arg+readoutdrvamp_halfspan

	qubitdrvamp_arg=para_init[qubitid]['qubitdrvamp_arg']
	qubitdrvamp_halfspan=para_init[qubitid]['qubitdrvamp_halfspan']
	qubitdrvamp_x1=qubitdrvamp_arg-qubitdrvamp_halfspan
	qubitdrvamp_x2=qubitdrvamp_arg+qubitdrvamp_halfspan

	period_target=para_init[qubitid]['period_target']

	elementlength_rabiwscan=para_init[qubitid]['elementlength_rabiwscan']
	elementstep_rabiwscan=para_init[qubitid]['elementstep_rabiwscan']
	elementlength_ramsey=para_init[qubitid]['elementlength_ramsey']
	elementstep_ramsey=para_init[qubitid]['elementstep_ramsey']


	qchip.updatecfg({('Qubits',qubitid,'readfreq'):fread_arg,('Qubits',qubitid,'freq'):fqubit_arg,('Gates',qubitid+'readoutdrv',0,'amp'):readoutdrvamp_arg,('Gates',qubitid+'read',0,'amp'):readoutdrvamp_arg,('Gates',qubitid+'rabi',0,'amp'):qubitdrvamp_arg},wfilename=cfgname+'.json')
	qchip.updatecfg({('Gates',qubitid+'X90',0,'twidth'):round(1e-9*period_target/4,15),('Gates',qubitid+'Y90',0,'twidth'):round(1e-9*period_target/4,15),('Gates',qubitid+'X180',0,'twidth'):round(1e-9*period_target/2,15),('Gates',qubitid+'Y180',0,'twidth'):round(1e-9*period_target/2,15),('Gates',qubitid+'X270',0,'twidth'):round(1e-9*period_target/4*3,15),('Gates',qubitid+'Y270',0,'twidth'):round(1e-9*period_target/4*3,15)},wfilename=cfgname+'.json')


	print('*******************rabiwscan step 1***************************')
	fread=rabiwscanfread(fread_arg,fread_x1,fread_x2,fqubit_arg,readoutdrvamp_arg,qubitdrvamp_arg,delayread,nget_rabiwscan,delay1,delaybetweenelement,elementlength_rabiwscan,elementstep_rabiwscan,qubitid,qubitidread,cfgname+'.json',ip,wiremapmodule,50e3)
	qchip.updatecfg({('Qubits',qubitid,'readfreq'):fread},wfilename=cfgname+'.json')

	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	qchip.updatecfg({('Qubits',qubitid,'readfreq'):fread},wfilename=cfgname+'_rabiwscanfreadforamp_'+timestamp+'.json')

	print('rabiwscanfreadforamp',datetime.datetime.now())
	print(datetime.datetime.now()-datetime_tmp)
	datetime_tmp=datetime.datetime.now()


	print('*******************rabiwscan step 2***************************')
	fqubit=rabiwscanfqubit(fqubit_arg,fqubit_x1,fqubit_x2,fread,readoutdrvamp_arg,qubitdrvamp_arg,delayread,nget_rabiwscan,delay1,delaybetweenelement,elementlength_rabiwscan,elementstep_rabiwscan,qubitid,qubitidread,cfgname+'.json',ip,wiremapmodule,50e3)
	qchip.updatecfg({('Qubits',qubitid,'freq'):fqubit},wfilename=cfgname+'.json')

	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	qchip.updatecfg({('Qubits',qubitid,'freq'):fqubit},wfilename=cfgname+'_rabiwscanfqubitforamp_'+timestamp+'.json')
	print('rabiwscanfqubitforamp',datetime.datetime.now())
	print(datetime.datetime.now()-datetime_tmp)
	datetime_tmp=datetime.datetime.now()


	print('*******************rabiwscan step 3***************************')
	readoutdrvamp=fminbound(func=rabiwscanrdrvamp,x1=readoutdrvamp_x1,x2=readoutdrvamp_x2,xtol=0.005,disp=3,args=(fread,fqubit,qubitdrvamp_arg,delayread,nget_rabiwscan,delay1,delaybetweenelement,elementlength_rabiwscan,elementstep_rabiwscan,qubitid,qubitidread,cfgname+'.json',ip,wiremapmodule))
	qchip.updatecfg({('Gates',qubitid+'readoutdrv',0,'amp'):readoutdrvamp,('Gates',qubitid+'read',0,'amp'):readoutdrvamp},wfilename=cfgname+'.json')

	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	qchip.updatecfg({('Gates',qubitid+'readoutdrv',0,'amp'):readoutdrvamp,('Gates',qubitid+'read',0,'amp'):readoutdrvamp},wfilename=cfgname+'_rabiwscanrdrvampforamp_'+timestamp+'.json')
	print('rabiwscanrdrvampforamp',datetime.datetime.now())
	print(datetime.datetime.now()-datetime_tmp)
	datetime_tmp=datetime.datetime.now()


	print('*******************rabiwscan step 4***************************')
	qubitdrvamp=fminbound(func=rabiwscanqdrvamp,x1=qubitdrvamp_x1,x2=qubitdrvamp_x2,xtol=0.005,disp=3,args=(fread,fqubit,readoutdrvamp,delayread,nget_rabiwscan,period_target,delay1,delaybetweenelement,elementlength_rabiwscan,elementstep_rabiwscan,qubitid,qubitidread,cfgname+'.json',ip,wiremapmodule))
	qchip.updatecfg({('Gates',qubitid+'rabi',0,'amp'):qubitdrvamp,('Gates',qubitid+'rabiaxn',0,'amp'):qubitdrvamp,('Gates',qubitid+'X90',0,'amp'):qubitdrvamp,('Gates',qubitid+'Y90',0,'amp'):qubitdrvamp,('Gates',qubitid+'X180',0,'amp'):qubitdrvamp,('Gates',qubitid+'Y180',0,'amp'):qubitdrvamp,('Gates',qubitid+'X270',0,'amp'):qubitdrvamp,('Gates',qubitid+'Y270',0,'amp'):qubitdrvamp},wfilename=cfgname+'.json')

	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	qchip.updatecfg({('Gates',qubitid+'rabi',0,'amp'):qubitdrvamp,('Gates',qubitid+'rabiaxn',0,'amp'):qubitdrvamp,('Gates',qubitid+'X90',0,'amp'):qubitdrvamp,('Gates',qubitid+'Y90',0,'amp'):qubitdrvamp,('Gates',qubitid+'X180',0,'amp'):qubitdrvamp,('Gates',qubitid+'Y180',0,'amp'):qubitdrvamp,('Gates',qubitid+'X270',0,'amp'):qubitdrvamp,('Gates',qubitid+'Y270',0,'amp'):qubitdrvamp},wfilename=cfgname+'_rabiwscanqdrvampforperiod_'+timestamp+'.json')
	print('rabiwscanqdrvampforperiod',datetime.datetime.now())
	print(datetime.datetime.now()-datetime_tmp)
	datetime_tmp=datetime.datetime.now()


	#######ramsey update fqubit##########
	ramsey=c_ramsey(qubitcfg=cfgname+'.json',ip=ip,wiremapmodule=wiremapmodule)
	ramsey.ramseyseqs(delayread=delayread,delay1=delay1,delaybetweenelement=delaybetweenelement,elementlength=elementlength_ramsey,elementstep=elementstep_ramsey,qubitid=qubitid,qubitidread=qubitidread)
	ramsey.run()
	data=ramsey.acqdata(nget_ramsey)
	cmdlinestr='fqubit'+str(fqubit)+'_qubitid'+qubitid
	fprocess=ramsey.savejsondata(filename='',extype='ramsey',cmdlinestr=cmdlinestr,data=data)
	print('save data to ',fprocess)
	[rawdata,separation,iqafterherald,population_norm,t2star,foffset,fiterr]=ramsey.processramsey(dt=elementstep_ramsey,filename=fprocess,loaddataset=clargs.dataset,plot=False)
	print('rabi training dataset ',clargs.dataset)
	print('t2star',t2star)
	print('foffset',foffset)
	framsey=[-foffset,foffset]
	foffset_list=[]
	for i in range(2):
		ramsey.initseqs()
		ramsey.ramseyseqs(delayread=delayread,delay1=delay1,delaybetweenelement=delaybetweenelement,elementlength=elementlength_ramsey,elementstep=elementstep_ramsey,framsey=framsey[i],qubitid=qubitid,qubitidread=qubitidread)
		ramsey.run()
		data=ramsey.acqdata(nget_ramsey)
		cmdlinestr='fqubit'+str(fqubit)+'_framsey'+str(framsey[i])+'_qubitid'+qubitid
		fprocess=ramsey.savejsondata(filename='',extype='ramsey',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
		[rawdata,separation,iqafterherald,population_norm,t2star,foffset,fiterr]=ramsey.processramsey(dt=elementstep_ramsey,filename=fprocess,loaddataset=clargs.dataset,plot=False)
		foffset_list.append(foffset)
		print('rabi training dataset ',clargs.dataset)
		print('t2star',t2star)
		print('foffset',foffset)

	fqubit=fqubit+framsey[numpy.argmin(foffset_list)]
	print('fqubit after ramsey',fqubit,foffset_list,numpy.argmin(foffset_list),framsey[numpy.argmin(foffset_list)])
	qchip.updatecfg({('Qubits',qubitid,'freq'):fqubit},wfilename=cfgname+'.json')

	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	qchip.updatecfg({('Qubits',qubitid,'freq'):fqubit},wfilename=cfgname+'_ramsey_'+timestamp+'.json')
	print('ramsey',datetime.datetime.now())
	print(datetime.datetime.now()-datetime_tmp)
	datetime_tmp=datetime.datetime.now()


	#####  dragalphascan #####
	print('*****currently skip**************DRAG alpha scan**************current skip*****')
	#if flag_drag:
	#	alpha=1.0
	#	qchip.updatecfg({('Gates',qubitid+'rabi',0,'env',0,'paradict','alpha'):alpha,('Gates',qubitid+'rabiaxn',0,'env',0,'paradict','alpha'):alpha,('Gates',qubitid+'X90',0,'env',0,'paradict','alpha'):alpha,('Gates',qubitid+'Y90',0,'env',0,'paradict','alpha'):alpha,('Gates',qubitid+'X180',0,'env',0,'paradict','alpha'):alpha,('Gates',qubitid+'Y180',0,'env',0,'paradict','alpha'):alpha,('Gates',qubitid+'X270',0,'env',0,'paradict','alpha'):alpha,('Gates',qubitid+'Y270',0,'env',0,'paradict','alpha'):alpha},wfilename=cfgname+'.json')

	print('*******************gate popu optimize*******************')
	optipara={'90':{'halfspan':0.05,'repeat':7,'ngatemult':1,'ngateadd':2},
	'180':{'halfspan':0.05,'repeat':9,'ngatemult':1,'ngateadd':1},
	'270':{'halfspan':0.05,'repeat':9,'ngatemult':1,'ngateadd':2}
	}
	for gate in gatelist:
		m=re.match('(?P<direction>\S)(?P<ang>\d*)',gate)
		print('m.group',m.groups(),'gate',gate)
		(direction,ang)=m.groups()
		center=qubitdrvamp
		halfspan0=optipara[ang]['halfspan']
		for i in range(optipara[ang]['repeat']):
			halfspan=halfspan0/(2**i)
			ngate=4*(2**(i*optipara[ang]['ngatemult']))+optipara[ang]['ngateadd']
			center=fminbound(func=gatepopuoptimize,x1=center-halfspan,x2=center+halfspan,xtol=halfspan/10,disp=True,args=(gate,ngate,nget_gatepopuoptimize,delayread,delay1,delaybetweenelement,qubitid,qubitidread,cfgname+'.json',ip,wiremapmodule))
			qchip.updatecfg({('Gates',qubitid+gate,0,'amp'):center},wfilename=cfgname+'.json')
			print('range',center-halfspan,center,center+halfspan, 32768*halfspan)
		timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		qchip.updatecfg({('Gates',qubitid+gate,0,'amp'):center},wfilename=cfgname+'_optimize'+gate+'_'+timestamp+'.json')
		print('optimize %s'%gate,datetime.datetime.now())
		print(datetime.datetime.now()-datetime_tmp)
		datetime_tmp=datetime.datetime.now()

	# Y-90
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	qchip.updatecfg({('Gates',qubitid+'Y-90',0,'amp'):chipcfg['Gates'][qubitid+'Y90'][0]['amp'],('Gates',qubitid+'Y-90',0,'twidth'):chipcfg['Gates'][qubitid+'Y90'][0]['twidth']},wfilename=cfgname+'.json')
	qchip.updatecfg({('Gates',qubitid+'Y-90',0,'amp'):chipcfg['Gates'][qubitid+'Y90'][0]['amp'],('Gates',qubitid+'Y-90',0,'twidth'):chipcfg['Gates'][qubitid+'Y90'][0]['twidth']},wfilename=cfgname+'_optimizeY-90_'+timestamp+'.json')
	if flag_drag:
		qchip.updatecfg({('Gates',qubitid+'Y-90',0,'env',0,'paradict','alpha'):chipcfg['Gates'][qubitid+'Y90'][0]['env'][0]['paradict']['alpha']},wfilename=cfgname+'.json')
		qchip.updatecfg({('Gates',qubitid+'Y-90',0,'env',0,'paradict','alpha'):chipcfg['Gates'][qubitid+'Y90'][0]['env'][0]['paradict']['alpha']},wfilename=cfgname+'_optimizeY-90_'+timestamp+'.json')

	#if flag_rb:
	#	Rabi
	#	Randomized Benchmarking

if 0:
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	with open(cfgname+'.json') as jfile:
		chipcfg=json.load(jfile)
	qubitid_c='Q6'
	qubitid_t='Q5'
	CR_amp=0.45#0.25

	# CR envelope: DRAG
	#qchip.updatecfg({('Gates','CR('+qubitid_c+qubitid_t+')',0,'amp'):chipcfg['Gates'][qubitid_t+'rabi'][0]['amp'],('Gates','CR('+qubitid_c+qubitid_t+')',0,'env',0,'paradict','alpha'):chipcfg['Gates'][qubitid_t+'rabi'][0]['env'][0]['paradict']['alpha']},wfilename=cfgname+'.json')
	#qchip.updatecfg({('Gates','CR('+qubitid_c+qubitid_t+')',0,'amp'):chipcfg['Gates'][qubitid_t+'rabi'][0]['amp'],('Gates','CR('+qubitid_c+qubitid_t+')',0,'env',0,'paradict','alpha'):chipcfg['Gates'][qubitid_t+'rabi'][0]['env'][0]['paradict']['alpha']},wfilename=cfgname+'_optimizeCR_'+timestamp+'.json')

	# CR envelope: DRAG
	#qchip.updatecfg({('Gates','CR('+qubitid_c+qubitid_t+')',0,'amp'):CR_amp,('Gates','CR('+qubitid_c+qubitid_t+')',0,'env',0,'paradict','alpha'):chipcfg['Gates'][qubitid_t+'rabi'][0]['env'][0]['paradict']['alpha']},wfilename=cfgname+'.json')
	#qchip.updatecfg({('Gates','CR('+qubitid_c+qubitid_t+')',0,'amp'):CR_amp,('Gates','CR('+qubitid_c+qubitid_t+')',0,'env',0,'paradict','alpha'):chipcfg['Gates'][qubitid_t+'rabi'][0]['env'][0]['paradict']['alpha']},wfilename=cfgname+'_optimizeCR_'+timestamp+'.json')

	# CR envelope: cos_edge_square
	qchip.updatecfg({('Gates','CR('+qubitid_c+qubitid_t+')',0,'amp'):CR_amp},wfilename=cfgname+'.json')
	qchip.updatecfg({('Gates','CR('+qubitid_c+qubitid_t+')',0,'amp'):CR_amp},wfilename=cfgname+'_optimizeCR_'+timestamp+'.json')

	# CNOT.X
	qchip.updatecfg({('Gates','CNOT('+qubitid_c+qubitid_t+').'+qubitid_t+'X',0,'amp'):chipcfg['Gates'][qubitid_t+'rabi'][0]['amp'],('Gates','CNOT('+qubitid_c+qubitid_t+').'+qubitid_t+'X',0,'env',0,'paradict','alpha'):chipcfg['Gates'][qubitid_t+'rabi'][0]['env'][0]['paradict']['alpha']},wfilename=cfgname+'.json')
	qchip.updatecfg({('Gates','CNOT('+qubitid_c+qubitid_t+').'+qubitid_t+'X',0,'amp'):chipcfg['Gates'][qubitid_t+'rabi'][0]['amp'],('Gates','CNOT('+qubitid_c+qubitid_t+').'+qubitid_t+'X',0,'env',0,'paradict','alpha'):chipcfg['Gates'][qubitid_t+'rabi'][0]['env'][0]['paradict']['alpha']},wfilename=cfgname+'_optimizeCNOTX_'+timestamp+'.json')

	# CNOT
	qchip.updatecfg({('Gates',qubitid_c+qubitid_t+'CNOT',0,'amp'):0,('Gates',qubitid_c+qubitid_t+'CNOT',0,'twidth'):4e-9,('Gates',qubitid_c+qubitid_t+'CNOT',0,'env',0,'paradict','alpha'):chipcfg['Gates'][qubitid_c+'rabi'][0]['env'][0]['paradict']['alpha'],('Gates',qubitid_c+qubitid_t+'CNOT',2,'env',0,'paradict','alpha'):chipcfg['Gates'][qubitid_t+'rabi'][0]['env'][0]['paradict']['alpha']},wfilename=cfgname+'.json')
	qchip.updatecfg({('Gates',qubitid_c+qubitid_t+'CNOT',0,'amp'):0,('Gates',qubitid_c+qubitid_t+'CNOT',0,'twidth'):4e-9,('Gates',qubitid_c+qubitid_t+'CNOT',0,'env',0,'paradict','alpha'):chipcfg['Gates'][qubitid_c+'rabi'][0]['env'][0]['paradict']['alpha'],('Gates',qubitid_c+qubitid_t+'CNOT',2,'env',0,'paradict','alpha'):chipcfg['Gates'][qubitid_t+'rabi'][0]['env'][0]['paradict']['alpha']},wfilename=cfgname+'_optimizeCNOT_'+timestamp+'.json')

	print(datetime.datetime.now())
