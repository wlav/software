import matplotlib
matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import experiment
import glob
import os

class c_rabixtalk(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid_t=None
		self.qubitid_c=None
	def rabixtalkseqs(self,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,elementlength=80,elementstep=4e-9,restart=True,readoutdrvamp=None,qubitdrvamp=None,readwidth=None,fqubit=None,fread=None,rdc=0,preadout=None,qubitid_t='Q7',qubitid_c='Q6',qubitidread=['Q7','Q6','Q5'],amp_multiplier=0,phase_offset=0,ctrl='I'):
		self.qubitid_t=qubitid_t
		self.qubitid_c=qubitid_c
		if restart:
			self.initseqs()
		readoutdrvamp0,readoutdrvamp1,readoutdrvamp2=self.rdrvcalc(readoutdrvamp,dcoffset=rdc)
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		modrdrv={}
		modread={}
		modrabi={}
		modrabi_simul={}
		if readoutdrvamp is not None:
			modrdrv.update(dict(amp=readoutdrvamp0))
		if qubitdrvamp is not None:
			modrabi.update(dict(amp=qubitdrvamp))
			modrabi_simul.update(dict(amp=qubitdrvamp))
		if fqubit is not None:
			modrabi.update(dict(fcarrier=fqubit))
			modrabi_simul.update(dict(fcarrier=fqubit))
		if readwidth is not None:
			modrdrv.update(dict(twidth=readwidth))
			modread.update(dict(twidth=readwidth))
		if fread is not None:
			modrdrv.update(dict(fcarrier=fread))
			modread.update(dict(fcarrier=fread))
		if preadout is not None:
			modread.update(dict(pcarrier=preadout))

		run=0
		for irun in range(elementlength):

			therald=run
			theraldread=run+delayread
			self.seqs.add(therald,self.qchip.gates[qubitid_t+'read'].modify([modrdrv,modread]))
			tini=run+delay1
			if ctrl=='X180':
				self.seqs.add(tini,             self.qchip.gates[qubitid_c+'X180'])
				trabi=self.seqs.tend()
			else:
				trabi=tini
			ti180=trabi-tini
			wrabi=elementstep*irun
			if wrabi!=0:
				pcarrier_tmp=self.qchip.gates[qubitid_t+'rabi'].pcalc(dt=ti180)[0]
				modrabi.update(dict(twidth=wrabi,pcarrier=pcarrier_tmp))
				self.seqs.add(trabi,         	 self.qchip.gates[qubitid_t+'rabi'].modify(modrabi))

				pcarrier_simul_tmp=self.qchip.gates[qubitid_t+'rabi'].pcalc(dt=ti180,padd=phase_offset)[0]
				amp_simul_tmp=self.qchip.gates[qubitid_t+'rabi'].paralist[0]['amp']*amp_multiplier
				modrabi_simul.update(dict(twidth=wrabi,dest=qubitid_c+'.qdrv',amp=amp_simul_tmp,pcarrier=pcarrier_simul_tmp))
				self.seqs.add(trabi,         	 self.qchip.gates[qubitid_t+'rabi'].modify(modrabi_simul))

			treaddrv=self.seqs.tend()
			self.seqs.add(treaddrv,self.qchip.gates[qubitid_t+'read'].modify([modrdrv,modread]))
			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
		print('fread,fqubit,readoutdrvamp,qubitdrvamp',fread,fqubit,readoutdrvamp,qubitdrvamp)
	def rabixtalkacq(self,nget):
		data=self.acqdata(nget)
		return data
	def processrabixtalk(self,dt,filename,dumpdataset,loaddataset,plot=False):
		c=self.loadjsondata(filename)
		return self.processrabixtalkdata(dt=dt,data=c[list(c.keys())[0]],dumpdataset=dumpdataset,loaddataset=loaddataset,plot=plot,filename=filename)
	def processrabixtalkdata(self,dt,data,dumpdataset,loaddataset,plot=False,filename=''):
		self.rabixtalk_result=self.process3(data,qubitid=self.qubitid_t,lengthperrow=self.bufwidth_dict[self.qubitid_t],training=loaddataset=='',dumpdataset=dumpdataset,loaddataset=loaddataset)
		#[amp,period,fiterr]=self.fitrabi(dx=dt,data=self.rabixtalk_result['population_norm'],plot=plot,figname=filename)
		[amp,period,tdecay,fiterr]=self.fitrabidecay(dt=dt,data=self.rabixtalk_result['population_norm'],plot=plot,figname=filename)
		return [data,self.rabixtalk_result['separation'],self.rabixtalk_result['iqafterherald'],self.rabixtalk_result['population_norm'],amp,period,fiterr]

if __name__=="__main__":
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	rabi_rate=[]
	#amp_multiplier_map=numpy.arange(0.0,1.25,0.10)
	#amp_multiplier_map=numpy.arange(0.55,0.77,0.02)
	#amp_multiplier_map=numpy.arange(1.10,2.00,0.05)
	#amp_multiplier_map=numpy.arange(0.63-0.15,0.63+0.15+0.01,0.03)
	#amp_multiplier_map=numpy.arange(0.55,0.75+0.001,0.01)
	amp_multiplier_map=numpy.arange(0.0,2.0,0.125)
	#amp_multiplier_map=numpy.arange(0.5,0.9,0.025)#Q7Q6
	amp_multiplier_map=numpy.arange(0.85,1.25,0.025)#Q7Q5
	#phase_offset_map=numpy.arange(0.0,numpy.pi*2,numpy.pi/10)
	#phase_offset_map=numpy.arange(0.60,1.15,0.05)
	#phase_offset_map=numpy.arange(4.50,5.30,0.05)
	#phase_offset_map=numpy.arange(0.85+numpy.pi-0.5,0.85+numpy.pi+0.5+0.01,0.1)
	#phase_offset_map=numpy.arange(0.85+numpy.pi-0.1,0.85+numpy.pi+0.1+0.001,0.02)
	phase_offset_map=numpy.arange(0.0,numpy.pi*2,numpy.pi/8)
	#phase_offset_map=numpy.arange(1.8,2.6,0.05)#Q7Q6
	phase_offset_map=numpy.arange(0.1,0.9,0.05)#Q7Q5
	ctrl='I'#'X180'

	#file_name=glob.glob("./tmp_replot/*.dat")
	#file_name=glob.glob("./tmp_replot_X180/*.dat")
	#file_name=glob.glob("./tmptmp_replot/*.dat")
	#file_name=glob.glob("./tmptmp_replot_X180/*.dat")
	#file_name=glob.glob("./tmptmptmp_replot/*.dat")
	#file_name=glob.glob("./tmptmptmp_replot_X180/*.dat")
	#file_name.sort(key=os.path.getmtime)
	#for index1,amp_multiplier in enumerate(amp_multiplier_map):
	for amp_multiplier in amp_multiplier_map:
		rabi_rate_tmp=[]
		#for index2,phase_offset in enumerate(phase_offset_map):
		for phase_offset in phase_offset_map:
			parser,cmdlinestr=experiment.cmdoptions()
			print(cmdlinestr)
			parser.set_defaults(elementstep=4e-9)
			clargs=parser.parse_args()
			rabixtalk=c_rabixtalk(**clargs.__dict__)
			if clargs.sim:
				rabixtalk.setsim()
			rabixtalk.rabixtalkseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readwidth=clargs.readwidth,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,elementlength=clargs.elementlength,elementstep=clargs.elementstep,fqubit=clargs.fqubit,preadout=clargs.preadout,fread=clargs.fread,qubitid_t=clargs.qubitid_t,qubitid_c=clargs.qubitid_c,qubitidread=clargs.qubitidread,amp_multiplier=amp_multiplier,phase_offset=phase_offset,ctrl=ctrl)
			if clargs.processfile=='':
			#if False:
				rabixtalk.run(bypass=clargs.bypass)
				data=rabixtalk.rabixtalkacq(clargs.nget)
				fprocess=rabixtalk.savejsondata(filename=clargs.filename+'_'+ctrl+'_scanamp'+str(amp_multiplier)+'_scanphase'+str(phase_offset),extype='rabixtalk',cmdlinestr=cmdlinestr,data=data)
				print('save data to ',fprocess)
				if clargs.sim:
					rabixtalk.sim()
			else:
				fprocess=clargs.processfile
				#fprocess=file_name[index1*len(phase_offset_map)+index2]
			[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=rabixtalk.processrabixtalk(dt=clargs.elementstep,filename=fprocess,dumpdataset=fprocess[:-4],loaddataset=clargs.dataset,plot=clargs.plot)
			rabi_rate_tmp.append(1/period)
			#rabi_rate_tmp.append(foffset)
			print('period',period)
			print('separation',separation)
			print('amp',amp)
			#rabixtalk.plotrawdata(d1=rawdata,figname='blobs'+fprocess)
			rabixtalk.plotpopulation_norm(population_norm=population_norm,figname='population'+fprocess)
			print(rabixtalk.hf.adcminmax())
			if clargs.plot:
				pyplot.grid()
				#pyplot.show()
		rabi_rate.append(rabi_rate_tmp)
		with open(clargs.filename+'_rabixtalk_'+ctrl+'_qubitid_c'+clargs.qubitid_c+'_qubitid_t'+clargs.qubitid_t+'_'+timestamp+'.dat','a') as f:
			numpy.savetxt(f,numpy.array(rabi_rate_tmp).reshape((1,-1)))
	pyplot.figure()
	pyplot.pcolormesh(rabixtalk.repack1D_cent2edge(phase_offset_map),rabixtalk.repack1D_cent2edge(amp_multiplier_map),rabi_rate,cmap='RdBu_r')
	pyplot.legend()
	pyplot.xlabel('Phase Offset (rad)')
	pyplot.ylabel('Amplitude Multiplier')
	pyplot.colorbar(label='Rabi Rate (Hz)')
	pyplot.savefig(clargs.filename+'_rabixtalk_'+ctrl+'_qubitid_c'+clargs.qubitid_c+'_qubitid_t'+clargs.qubitid_t+'_'+timestamp+'.pdf')
	#python rabi_xtalk.py --plot -n 20 --qubitid_c Q6 --qubitid_t Q5 -es 8e-9 -el 40
