import datetime
import argparse
import sys
from squbic import *
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
from scipy import signal
import experiment
import vxi11
class c_twpalive(experiment.c_experiment):
 	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		pass
	def twpaliveseqs(self,delayread=668e-9,delaybetweenelement=600e-6,elementlength=1024,readoutdrvamp=None,readwidth=None,fstart=-500e6,fstop=500e6,rdc=0):
		df=1.0*(fstop-fstart)/(elementlength-1)
		self.freq=numpy.arange(fstart,fstop+df,df)[:elementlength] # avoid rounding error in python, make sure that self.freq length is elementlength
		print len(self.freq)
		modq7rdrv={}
		modq7read={}
		if readoutdrvamp:
			modq7rdrv.update(dict(amp=min(1-rdc,readoutdrvamp)))
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print 'marker done'
		trun=0
		for frun in self.freq:
			modq7rdrv.update(dict(fcarrier=frun))
			modq7read.update(dict(fcarrier=frun))
			self.readall(trdrv=trun,qubitsamps={"Q7":(modq7rdrv,modq7read)})
			#trun=trun+delaybetweenelement
			trun=self.seqs.tend()+delaybetweenelement
		print 'period',trun
		self.seqs.setperiod(period=trun)
	def twpalivedata(self,nget):
		data=self.acqdata(nget)
		return data
	def processtwpalive(self,filename):
		c=self.loadjsondata(filename)['accout_0']
		cavr=c.reshape((-1,len(self.freq))).mean(0)
		#add process here
		return cavr
	def twpaliveplot(self,freq,c):
		pyplot.subplot(211)
		pyplot.semilogy(freq,abs(c))
		pyplot.tight_layout()
		pyplot.subplot(212)
		pyplot.plot(freq,signal.detrend(numpy.unwrap(numpy.angle(c))))
		pyplot.tight_layout()
if __name__=="__main__":
	instr=vxi11.Instrument("192.168.1.31")
	print instr.ask("*IDN?\n")
	instr.write("OUTP %s \r\n"%('ON'))
	print 'BNC845 status',instr.ask("OUTP?\n")
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(delaybetweenelement=2e-6)
	twpalive_elementlength=1024
	parser.set_defaults(elementlength=twpalive_elementlength)
	clargs=parser.parse_args()
	twpalive=c_twpalive(**clargs.__dict__)
	twpalive.twpaliveseqs(delayread=clargs.delayread,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,readoutdrvamp=clargs.readoutdrvamp,readwidth=None,fstart=clargs.fstart,fstop=clargs.fstop,rdc=0)
	twpalive.run(bypass=clargs.bypass)
	twpalive_fstart=6.4e9
	twpalive_fstop=8.9e9
	twpalive_fstep=50e6
	twpalive_pstart=10.0
	twpalive_pstop=25.0
	twpalive_pstep=0.25
	twpalive_timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	twpalive_filename='%s.dat'%('_'.join(['twpalive','pumpf',str(twpalive_fstart),str(twpalive_fstop),str(twpalive_fstep),'pumpp',str(twpalive_pstart),str(twpalive_pstop),str(twpalive_pstep),twpalive_timestamp]))
	for twpalive_freq in numpy.arange(twpalive_fstart,twpalive_fstop,twpalive_fstep):
		instr.write("FREQ %s %s\r\n"%(twpalive_freq,'Hz'))
		for twpalive_power in numpy.arange(twpalive_pstart,twpalive_pstop,twpalive_pstep):
			instr.write("POW %s %s\r\n"%(twpalive_power,'dBm'))
			print 'Pump Freq: ',instr.ask("FREQ?\n")
			print 'Pump Power: ',instr.ask("POW?\n")
			time.sleep(1)
			if clargs.processfile=='':
				if clargs.sim:
					twpalive.sim()
			#	twpalive.run(bypass=clargs.bypass)
				data=twpalive.twpalivedata(clargs.nget)
				fprocess=twpalive.savejsondata(filename='pumpf'+str(twpalive_freq)+'_pumpp'+str(twpalive_power)+clargs.filename,extype='twpalive',cmdlinestr=cmdlinestr,data=data)
				print 'save data to ',fprocess
			else:
				fprocess=clargs.processfile
			#[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=twpalive.processtwpalive(dt=clargs.elementstep,filename=fprocess,swapdata=clargs.swapdata,swappre=clargs.swappre)
			c=twpalive.processtwpalive(fprocess)
		#	twpalive.plotrawdata(rawdata)
		#	twpalive.plotafterheraldingtest(iqafterherald)
		#	twpalive.plotpopulation_norm(population_norm)
			if clargs.plot:
				twpalive.twpaliveplot(twpalive.freq,c)
				pyplot.grid()
				pyplot.show()
			with open(twpalive_filename,'a') as f:
				numpy.savetxt(f,(twpalive.freq,c))
			time.sleep(1)
	instr.write("OUTP %s \r\n"%('OFF'))
	print 'BNC845 status',instr.ask("OUTP?\n")
	twpalive_off_filename='%s.dat'%('_'.join(['twpalive','pumpoff',twpalive_timestamp]))
	if clargs.processfile=='':
		if clargs.sim:
			twpalive.sim()
		twpalive.run(bypass=clargs.bypass)
		data=twpalive.twpalivedata(clargs.nget)
		fprocess=twpalive.savejsondata(filename='pumpoff'+clargs.filename,extype='twpalive',cmdlinestr=cmdlinestr,data=data)
		print 'save data to ',fprocess
	else:
		fprocess=clargs.processfile
	c=twpalive.processtwpalive(fprocess)
#   twpalive.plotrawdata(rawdata)
#   twpalive.plotafterheraldingtest(iqafterherald)
#   twpalive.plotpopulation_norm(population_norm)
	if clargs.plot:
		twpalive.twpaliveplot(twpalive.freq,c)
		pyplot.grid()
		pyplot.show()
	with open(twpalive_off_filename,'a') as f:
		numpy.savetxt(f,(twpalive.freq,c))
