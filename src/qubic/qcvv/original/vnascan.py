import matplotlib
matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
import datetime
import argparse
import sys
from squbic import *
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
from scipy import signal
import experiment
from vna import c_vna
import json
import devcom
from stepvat import stepvat 

if __name__=="__main__":
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(delaybetweenelement=2e-6)
	parser.set_defaults(elementlength=1024)
	clargs=parser.parse_args()
	vna=c_vna(**clargs.__dict__)
	flag_fulldac=False
	namestr='rdrv' if flag_fulldac else 'atten'
	readoutdrvamp_map=numpy.arange(0.01,1.0,0.02).round(15) if flag_fulldac else numpy.arange(-31.5,0.5,0.5).round(15)
	bandwidth=6e6
	with open(clargs.qubitcfg) as jfile:
		chipcfg=json.load(jfile)
	#cf=[chipcfg['Qubits'][qid]['readfreq'] for qid in chipcfg['Qubits'] if 'readfreq' in chipcfg['Qubits'][qid]]
	cf=[chipcfg['Qubits'][qid]['readfreq'] for qid in ['Q1']]#chipcfg['Qubits'] if 'readfreq' in chipcfg['Qubits'][qid]]
	#cf=[-197.1e6,-138.1e6,-77.1e6,-1e6,1e6,97.75e6,154.4e6,211e6]
	#cf=[-161.1e6,-93.7e6,-37.1e6,16.5e6,69.2e6,128.5e6,189.8e6,252.8e6]
	#cf=[-32.68e6,-69.41e6,-90.43e6,-120.5e6,-138.89e6,-172.96e6]
	#cf=[cf[3]]
	print('cf',cf)
	flist=[]
	for f in cf:
		flist.extend(list(numpy.linspace(f-bandwidth/2,f+bandwidth/2,int(1024/len(cf)))))
	print('flist length',len(flist))
	
	vna.vnaseqs(delayread=clargs.delayread,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,readoutdrvamp=1.0,readwidth=clargs.readwidth,rdc=0,freqlist=flist,qubitidread=clargs.qubitidread)
	vna.run()
	punchout_amp=[]
	punchout_ph=[]
	for ireadoutdrvamp in range(len(readoutdrvamp_map)):
		if flag_fulldac:
			memscale=readoutdrvamp_map[ireadoutdrvamp]
			vna.hf.elementmemwrite(memscale=memscale)
			vna.hf.write((('start',0),('start',1),('start',0)))
			for i in range(2):
				vna.hf.accbuf()
				vna.hf.buf_monout(delay=0.0)
		else:
			stepvat(device='attn1',atten=-readoutdrvamp_map[ireadoutdrvamp])
		cmdlinestr=namestr+str(readoutdrvamp_map[ireadoutdrvamp])+'_fstart'+str(flist[0])+'_fstop'+str(flist[-1])
		data=vna.vnadata(clargs.nget)
		fprocess=vna.savejsondata(filename=clargs.filename,extype='vnascan',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
		c=vna.processvna(fprocess)
		punchout_amp.append(abs(c))
		punchout_ph.append(signal.detrend(numpy.unwrap(numpy.angle(c))))
#		if clargs.plot:
#			pyplot.figure('raw')
#			pyplot.subplot(211)
#			pyplot.semilogy(vna.freq,abs(c),linewidth=0.5)#,label='rdrv '+str(readoutdrvamp_map[ireadoutdrvamp]))
#			pyplot.tight_layout()
#			pyplot.subplot(212)
#			pyplot.plot(vna.freq,signal.detrend(numpy.unwrap(numpy.angle(c))))#,linewidth=0.5,label='rdrv '+str(readoutdrvamp_map[ireadoutdrvamp]))
#			pyplot.tight_layout()
#	pyplot.savefig('processed_'+namestr+'start'+str(readoutdrvamp_map[0])+'_'+namestr+'stop'+str(readoutdrvamp_map[-1])+'_fstart'+str(flist[0])+'_fstop'+str(flist[-1])+'_'+timestamp+'.pdf')
	numpy.savetxt('processed_punchout_amp_'+namestr+'start'+str(readoutdrvamp_map[0])+'_'+namestr+'stop'+str(readoutdrvamp_map[-1])+'_fstart'+str(flist[0])+'_fstop'+str(flist[-1])+'_'+timestamp+'.dat',punchout_amp)
	numpy.savetxt('processed_punchout_ph_'+namestr+'start'+str(readoutdrvamp_map[0])+'_'+namestr+'stop'+str(readoutdrvamp_map[-1])+'_fstart'+str(flist[0])+'_fstop'+str(flist[-1])+'_'+timestamp+'.dat',punchout_ph)

	for i in range(len(cf)):
		plot2D_para={'amp':{'pcolormesh_z':numpy.array(punchout_amp)[:,i*int(1024/len(cf)):(i+1)*int(1024/len(cf))],'colorbar':'Magnitude Response (a.u.)'},
						'ph':{'pcolormesh_z':numpy.array(punchout_ph)[:,i*int(1024/len(cf)):(i+1)*int(1024/len(cf))],'colorbar':'Phase Response (rad)'}}
		for item in ['amp','ph']:
			pyplot.figure()
			pyplot.pcolormesh(vna.repack1D_cent2edge(numpy.array(flist[i*int(1024/len(cf)):(i+1)*int(1024/len(cf))])),vna.repack1D_cent2edge(readoutdrvamp_map),plot2D_para[item]['pcolormesh_z'],cmap='RdBu_r')
			pyplot.xlabel('Mod. Frequency (Hz)')
			pyplot.ylabel('Readout Drive Amp (Normalized)' if flag_fulldac else 'Step Attenuator Attenuation (dB)')
			pyplot.colorbar(label=plot2D_para[item]['colorbar'])
			pyplot.tight_layout()
			pyplot.savefig('processed_punchout_'+item+'_fstart'+str(flist[i*int(1024/len(cf))])+'_fstop'+str(flist[(i+1)*int(1024/len(cf))-1])+'_'+timestamp+'.pdf')
	#pyplot.show()

# python vnascan.py --plot -n 20
