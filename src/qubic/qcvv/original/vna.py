import datetime
import argparse
import sys
#from squbic import *
from matplotlib import pyplot,patches
#from qubic_t1 import cmdadd, cmdgen
import numpy
#from ether import c_ether
#from mem_gateway import c_mem_gateway
import time
from ..qubic import init
import pprint
from scipy import signal
from ..qubic import experiment
class c_vna(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		pass
	def vnaseqs(self,delayread=668e-9,delaybetweenelement=600e-6,elementlength=1024,readoutdrvamp=None,readwidth=None,fstart=-500e6,fstop=500e6,rdc=0,freqlist=None,qubitidread=['Q5','Q4','Q1']):
		if freqlist:
			self.freq=freqlist
			elementlength=len(self.freq)
		else:
			#df=1.0*(fstop-fstart)/(elementlength-1)
			#self.freq=numpy.arange(fstart,fstop+df,df)[:elementlength] # avoid rounding error in python, make sure that self.freq length is elementlength
			self.freq=numpy.linspace(fstart,fstop,elementlength)
		modrdrv={}
		modread={}
		if readoutdrvamp:
			modrdrv.update(dict(amp=min(1-rdc,readoutdrvamp)))
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		trun=0
		for frun in self.freq:
			modrdrv.update(dict(fcarrier=float(frun)))
			modread.update(dict(fcarrier=float(frun)))
			self.readall(trdrv=trun,qubitsamps={qubitidread[2]:(modrdrv,modread)})
			trun=self.seqs.tend()+delaybetweenelement
		print('period',trun)
		self.seqs.setperiod(period=trun)
	def vnadata(self,nget):
		data=self.acqdata(nget)
		return data
	def processvna(self,filename):
		c_tmp=self.loadjsondata(filename)
		c=c_tmp[list(c_tmp.keys())[0]]
		cavr=c.reshape((-1,len(self.freq))).mean(0)
		return cavr
	def vnaplot(self,c,figname='vna_amp_phase'):
		pyplot.subplot(211)
		pyplot.semilogy(self.freq,abs(c))
		#pyplot.plot(self.freq,abs(c))
		#points_index=[i for i in range(len(self.freq)) if abs(i-len(self.freq)/2)>10]
		#pyplot.plot(self.freq[points_index],abs(c[points_index]))
		pyplot.xlabel('Frequency (Hz)')
		pyplot.ylabel('Amplitude (a.u.)')
		pyplot.grid()
		pyplot.tight_layout()
		pyplot.subplot(212)
		pyplot.plot(self.freq,signal.detrend(numpy.unwrap(numpy.angle(c))))
		pyplot.xlabel('Frequency (Hz)')
		pyplot.ylabel('Phase (rad)')
		pyplot.grid()
		pyplot.tight_layout()
		#pyplot.figure(2)
		#pyplot.plot(self.freq,abs(c))
		#pyplot.figure(3)
		#pyplot.plot(self.freq[1:],numpy.diff(abs(c)))
		#pyplot.figure(4)
		#pyplot.plot(self.freq[1:],numpy.diff(signal.detrend(numpy.unwrap(numpy.angle(c)))))
		pyplot.savefig(figname+'.pdf')
def main():
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(delaybetweenelement=2e-6)
	parser.set_defaults(elementlength=1024)
	clargs=parser.parse_args()
	vna=c_vna(**clargs.__dict__)
	if clargs.sim:
		vna.setsim()
	vna.vnaseqs(delayread=clargs.delayread,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,readoutdrvamp=clargs.readoutdrvamp,readwidth=clargs.readwidth,fstart=clargs.fstart,fstop=clargs.fstop,rdc=0,qubitidread=clargs.qubitidread)
	if clargs.processfile=='':
		vna.run(bypass=clargs.bypass)
		data=vna.vnadata(clargs.nget)
		fprocess=vna.savejsondata(filename=clargs.filename,extype='vna',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
		if clargs.sim:
			vna.sim()
	else:
		fprocess=clargs.processfile
	c=vna.processvna(fprocess)
	print('adc minmax',vna.hf.adcminmax())
#	with open('ampforplot'+fprocess,'a') as f1:
#		numpy.savetxt(f1,(vna.freq,abs(c)))
#	with open('phaseforplot'+fprocess,'a') as f2:
#		numpy.savetxt(f2,(vna.freq,signal.detrend(numpy.unwrap(numpy.angle(c)))))
#	with open('complexforplot'+fprocess,'a') as f3:
#		numpy.savetxt(f3,(vna.freq,c))
	vna.vnaplot(c,figname=fprocess)
	if clargs.plot:
		pyplot.show()
if __name__=="__main__":
	main()
