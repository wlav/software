#import matplotlib
#matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing

import datetime
from matplotlib import pyplot
import numpy
from ..qubic import experiment
import json
import re
import importlib

class c_simp(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,simpseqs=True,**kwargs)
		pass
	def simpseqsgen(self,delayafterheralding=12e-6,delaybetweenelement=600e-6,cmdarray=None,gates=None,elementlist={},destlist={},patchlist={},qubitid=None):
		self.qubitid=qubitid
		self.hf.setmap(elementlist=elementlist,destlist=destlist,patchlist=patchlist)
		if gates is not None:
			self.seqs.setgates(gates)
			self.hf.simpmemcmd(self.seqs.gatelist)
		self.seqs.updatecmd(cmdarray)
		(cmdlen,period)=self.hf.simpcmdgen(self.seqs.cmdarray,delaybetweenelement=delaybetweenelement,delayafterheralding=delayafterheralding)
		print('experiment period',period)
		self.seqs.setperiod(period)
		self.bufwidth_dict=self.hf.bufwidth_dict
		return cmdlen

	def simpacq(self,nget):
		data=self.acqdata(nget)
		return data
	def processsimp(self,filename,loaddataset=None,loaddataset_list=None):
		c=self.loadjsondata(filename)
		#print('c.keys()',c.keys())
		print('proccessimp bufwidth_dict',self.bufwidth_dict)
		if len(self.qubitid)==1:
			data=c[list(c.keys())[0]]
			print('self.qubitid',self.qubitid,type(self.qubitid))
			simp_result=self.process_trueq(data,qubitid=self.qubitid[0],lengthperrow=self.bufwidth_dict[self.qubitid[0]],loaddataset=loaddataset)
			print(simp_result,type(simp_result))
			trueq_result=[{'0':int(simp_result['0'][i]),'1':int(simp_result['1'][i])} for i  in range(len(simp_result['0']))]
		elif len(self.qubitid)==2:
			print('processsimp qubitid',self.qubitid)
			data=[c[self.hf.accout_dict[self.qubitid[0]]],c[self.hf.accout_dict[self.qubitid[1]]]]
			process2q_result=self.process2q(data_list=data,qubitid_list=self.qubitid,lengthperrow_list=[self.bufwidth_dict[qid] for qid in self.qubitid],loaddataset_list=loaddataset_list)
			[meas_binned,meas_sum]=[process2q_result['meas_binned'],process2q_result['meas_sum']]
#			print('self.qubitid',self.qubitid,type(self.qubitid))
#			[meas_binned,meas_sum]=self.processrb2q(filename=fprocess,loaddataset_list=None)
			print('self.qubitid',self.qubitid)
			outcomes=[str(bin(i))[2:].zfill(len(self.qubitid)) for i in range(2**len(self.qubitid))]
			print('processsimp',meas_sum,meas_binned)
			print('processsimp',meas_sum.shape)
			trueq_result=[{o:int(count) for o,count in zip(outcomes,meas_sum[:,col])} for col in range(meas_sum.shape[1])]
		else:
			print('how many qubit do you have?')
		return trueq_result


def main():
	import trueq
	from ..qubictrueq import qubic_trueq
	datetimeformat='%Y%m%d_%H%M%S_%f'
	starttime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')

	n_random_cycles = [4,16,32,64,128,256,512]
	#n_random_cycles = [4,50,100,200,400] #IRB
	n_circuits = 30

#	from simp import c_simp
	parser,cmdlinestr=experiment.cmdoptions()
	parser.add_argument('--filestarttime',help='file start time',dest='filestarttime',type=str,default='')
	parser.add_argument('--rbprotocol',help='RB protocol',dest='rbprotocol',type=str,default='SRB')
	parser.add_argument('--trueqcfg',help='trueq config yaml',dest='trueqcfg',type=str)
	clargs=parser.parse_args()
	simp=c_simp(**clargs.__dict__)

	#config=trueq.Config.from_yaml('trueq_gate_'+clargs.qubitcfg[9:-5]+'.yaml')
	config=trueq.Config.from_yaml(clargs.trueqcfg)#'trueq_gate_'+clargs.qubitcfg[9:-5]+'.yaml')
	print('trueq_gatecfg',config)
	import imp
	wiremap=imp.load_source(name='wiremap',pathname=clargs.wiremapmodule)#importlib.import_module(wiremapmodule)
	#wiremap=importlib.import_module(clargs.wiremapmodule)
	gatesall=wiremap.gatesall
	elementlistall=wiremap.elementlistall
	destlistall=wiremap.destlistall
	patchlistall=wiremap.patchlistall

	if clargs.processfile=='':
		delayafterheralding=12e-6
		delaybetweenelement=600e-6
		#circuits_file=clargs.qubic_file
		firsttime=True

		if clargs.sim:
			simp.setsim()
		timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		print('timestamp to start',(datetime.datetime.strptime(timestamp,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).seconds)
		if clargs.rbprotocol=='SRB':
			circuits=qubic_trueq.trueq_srb(timestamp=timestamp,qubitid_list=clargs.qubitid_list,n_random_cycles=n_random_cycles,n_circuits=n_circuits,independent=True,readoutcorrection=True,twirl=None)
		if clargs.rbprotocol=='IRB':
			circuits=qubic_trueq.trueq_irb(timestamp=timestamp,qubitid_list=clargs.qubitid_list,n_random_cycles=n_random_cycles,n_circuits=n_circuits,interleaved_gate=trueq.Gate.x,readoutcorrection=True,twirl=None)
		if clargs.rbprotocol=='XRB':
			circuits=qubic_trueq.trueq_xrb(timestamp=timestamp,qubitid_list=clargs.qubitid_list,n_random_cycles=n_random_cycles,n_circuits=n_circuits,independent=True,readoutcorrection=True,twirl=None)
	
		#loadfilename='circuits_20200914_113728_899482.bin'
		#circuits=trueq.load(loadfilename)
		#pattern='(\S+)_(\S+_\S+_\S+).bin'
		#m=re.match(pattern,loadfilename)
		#timestamp=m.group(2)
		#print(timestamp)

		cirgentime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		transpiler = trueq.Compiler.from_config(config)
		transpiled_circuit = transpiler.compile(circuits)
		print('cirgentime to start',(datetime.datetime.strptime(cirgentime,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).seconds)
		trueq_result=[]
		index=0
		lastcmdarraylength=0
		circuitperstep=10
		#for circuit in transpiled_circuit[index*circuitperstep:min(circuitperstep*(index+1),len(circuits))]:
		for circuitindex in range(int(numpy.ceil(transpiled_circuit.n_circuits/circuitperstep))):
		#for circuitindex in [59,60]:
			#circuit=transpiled_circuit[circuitindex*circuitperstep:(circuitindex+1)*circuitperstep]
			circuit=transpiled_circuit[circuitindex*circuitperstep:min(len(transpiled_circuit),(circuitindex+1)*circuitperstep)]

			print('index',index)
			index=index+1
			[qubitid,cmdarray]=qubic_trueq.trueq_circuits_to_qubic2(circuit,heralding=delayafterheralding>0)
			print('circuit qubitid',qubitid)
			#print('cmdarray len',len(cmdarray))
			gates=[simp.qchip.gates[g] for g in gatesall if any([qid in g for qid in qubitid+['M0']]) and any([x in g for x in ['X90','read','mark']])]
			elementlist={k:elementlistall[k] for k in elementlistall.keys() if any([qid in k for qid in qubitid+['M0']])}
			destlist={k:destlistall[k] for k in destlistall.keys() if any([qid in k for qid in qubitid+['M0']])}
			patchlist={k:patchlistall[k] for k in patchlistall.keys() if any([qid in k for qid in qubitid+['M0']])}
			cmdarraylength=simp.simpseqsgen(delayafterheralding=delayafterheralding,delaybetweenelement=delaybetweenelement,cmdarray=cmdarray,gates=gates if firsttime else None,elementlist=elementlist,destlist=destlist,patchlist=patchlist,qubitid=qubitid)
			#simp.simpseqsgen(delayafterheralding=delayafterheralding,delaybetweenelement=delaybetweenelement,cmdarray=testarray,gates=gates if firsttime else None,elementlist=elementlist,destlist=destlist,patchlist=patchlist,qubitid=qubitid)
			simp.run(memwrite=firsttime,memclear=firsttime,cmdclear=(lastcmdarraylength==0 or cmdarraylength<lastcmdarraylength),preread=firsttime,cmdclearlength=lastcmdarraylength*4)
			#simp.run(memwrite=firsttime,memclear=firsttime,cmdclear=cmdarraylength<lastcmdarraylength,preread=firsttime,cmdclearlength=lastcmdarraylength*4)
			#simp.run(memwrite=firsttime,memclear=firsttime,cmdclear=True,preread=firsttime,cmdclearlength=lastcmdarraylength*4)
			print('cmdarray this',cmdarraylength,'last',lastcmdarraylength)
			lastcmdarraylength=cmdarraylength
			data=simp.simpacq(clargs.nget)
			fprocess=simp.savejsondata(filename=clargs.filename,extype='simp',cmdlinestr=cmdlinestr,data=data)
			print('save data to ',fprocess)
			if clargs.sim:
				simp.sim()
			#[rawdata,separation,population_norm,]
			trueq_return=simp.processsimp(filename=fprocess,loaddataset=clargs.dataset)
			print(trueq_return)
			trueq_result.extend(trueq_return)
			firsttime=False

		print(trueq_result)
	#	s=trueq.Simulator()
	#	s.run(circuits)
	#	print(circuits.expectation_values())
	#	print(circuits.results)
		timestampresult=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		resultfilename='circuits_'+timestamp+'_'+timestampresult+'.dat'
		with open(resultfilename,'w') as f_trueqresult:
			json.dump(trueq_result,f_trueqresult)
	##		print('resultfile %s'%resultfilename)
	else:
		resultfilename=clargs.processfile
	beforerb1q_process=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	print('beforerb1q_process to start',(datetime.datetime.strptime(beforerb1q_process,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).seconds)
	result_circuits=qubic_trueq.rb1q_process(resultfilename,plot=clargs.plot)
	###XRB###
	if clargs.rbprotocol=='XRB':
		print('###SRB###result_circuits.fit()[0]',result_circuits.fit()[0])
		print('###SRB###e_F',result_circuits.fit()[0].e_F.val,result_circuits.fit()[0].e_F.std)
		print('###SRB###p',result_circuits.fit()[0].p.val,result_circuits.fit()[0].p.std)
		print('###SRB###A',result_circuits.fit()[0].A.val,result_circuits.fit()[0].A.std)
		print('###XRB###result_circuits.fit()[1]',result_circuits.fit()[1])
		print('###XRB###e_U',result_circuits.fit()[1].e_U.val,result_circuits.fit()[1].e_U.std)
		print('###XRB###e_S',result_circuits.fit()[1].e_S.val,result_circuits.fit()[1].e_S.std)
		print('###XRB###u',result_circuits.fit()[1].u.val,result_circuits.fit()[1].u.std)
		print('###XRB###A',result_circuits.fit()[1].A.val,result_circuits.fit()[1].A.std)
		#circuits.plot.compare_rb()
	###IRB###
	if clargs.rbprotocol=='IRB':
		print('###IRB###result_circuits.fit()[0]',result_circuits.fit()[0])
		print('###IRB###e_F',result_circuits.fit()[0].e_F.val,result_circuits.fit()[0].e_F.std)
		print('###IRB###e_IU',result_circuits.fit()[0].e_IU.val,result_circuits.fit()[0].e_IU.std)
		print('###IRB###e_IL',result_circuits.fit()[0].e_IL.val,result_circuits.fit()[0].e_IL.std)
		print('###IRB###p',result_circuits.fit()[0].p.val,result_circuits.fit()[0].p.std)
		print('###IRB###A',result_circuits.fit()[0].A.val,result_circuits.fit()[0].A.std)
		print('###SRB###result_circuits.fit()[1]',result_circuits.fit()[1])
		print('###SRB###e_F',result_circuits.fit()[1].e_F.val,result_circuits.fit()[1].e_F.std)
		print('###SRB###p',result_circuits.fit()[1].p.val,result_circuits.fit()[1].p.std)
		print('###SRB###A',result_circuits.fit()[1].A.val,result_circuits.fit()[1].A.std)
		print('###Gate Fidelity###',simp.gate_fidelity(result_circuits.fit()[0].p.val,result_circuits.fit()[0].p.std,result_circuits.fit()[1].p.val,result_circuits.fit()[1].p.std))
		#circuits.plot.irb_summary()
	###SRB###
	if clargs.rbprotocol=='SRB':
		result_rb=result_circuits.fit()[0]
		fidelity=1-result_rb.e_F.val
		fidelity_std=result_rb.e_F.std
		SPAM=result_rb.A.val
		print('fidelity,fidelity_std,SPAM',fidelity,fidelity_std,SPAM)
		if clargs.filestarttime:
			qid=int(re.findall('\d+',clargs.qubitid_list[0])[0])
			scanfilename='statics_rb_%s.dat'%(clargs.filestarttime)
			with open(scanfilename,'a') as f:
				f.write('\n')
				numpy.savetxt(f,[qid,fidelity,fidelity_std,SPAM],newline=' ')
if __name__=="__main__":
	main()
