import datetime
import argparse
import sys
from squbic import *
from matplotlib import pyplot,patches
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import experiment
class c_ramsey(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid=None
		pass
	def ramseyseqs(self,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,elementlength=80,elementstep=2e-6,readoutdrvamp=None,qubitdrvamp=None,readwidth=None,fqubit=None,fread=None,rdc=0,framsey=None,qubitid='Q7',qubitidread=['Q5','Q4','Q3']):
		self.qubitid=qubitid
		readoutdrvamp0,readoutdrvamp1,readoutdrvamp2=self.rdrvcalc(readoutdrvamp,dcoffset=rdc)
		print('marker done')
		modrdrv={}
		modread={}
		if readoutdrvamp:
			modrdrv.update(dict(amp=readoutdrvamp0))
		if readwidth:
			modrdrv.update(dict(twidth=readwidth))
			modread.update(dict(twidth=readwidth))
		mod90_1={}
		mod90_2={}
		fgate=self.qchip.getfreq(qubitid+'.freq')
		if framsey:
			fgate=fgate+framsey
			mod90_1.update(dict(fcarrier=fgate))
			mod90_2.update(dict(fcarrier=fgate))
		if qubitdrvamp:
			mod90_1.update(dict(amp=qubitdrvamp))
			mod90_2.update(dict(amp=qubitdrvamp))
		l90=self.qchip.gates[qubitid+'X90'].tlength()
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		run=0

		for irun in range(elementlength):
			therald=run
			self.seqs.add(therald,self.qchip.gates[qubitid+'read'].modify([modrdrv,modread]))

			tramsey=elementstep*(irun)
			t90_1=self.seqs.tend()+delay1
			t90_2=t90_1+l90+tramsey
			p90_1=0
			#p90_2=2*numpy.pi*fgate*(tramsey+l90)
			p90_2=self.qchip.gates[qubitid+'X90'].pcalc(dt=tramsey+l90,freq=fgate)[0]
			mod90_1.update(dict(pcarrier=float(p90_1)))
			mod90_2.update(dict(pcarrier=float(p90_2)))

			self.seqs.add(t90_1,         self.qchip.gates[qubitid+'X90'].modify(mod90_1))
			self.seqs.add(t90_2,         self.qchip.gates[qubitid+'X90'].modify(mod90_2))

			treaddrv=self.seqs.tend()
			self.seqs.add(treaddrv,self.qchip.gates[qubitid+'read'].modify([modrdrv,modread]))
			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		#self.bufwidth=[self.seqs.countdest('Q5.read'),self.seqs.countdest('Q4.read'),self.seqs.countdest('Q3.read')]
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
	def ramseydata(self,nget):
		data=ramsey.acqdata(nget)
		return data
	def processramsey(self,dt,filename,loaddataset,plot=True):
		c=self.loadjsondata(filename)
		print('c.keys()',c.keys())
		data=c[list(c.keys())[0]]
		#ramsey_result=self.process3(c['accout_0'],lengthperrow=self.hf.getbufwidthdict()['accout_0'],training=False,loadname=loadname)
		#ramsey_result=self.process3(data,lengthperrow=max(self.bufwidth),training=False,loadname=loadname)
		ramsey_result=self.process3(data,qubitid=self.qubitid,lengthperrow=self.bufwidth_dict[self.qubitid],training=False,loaddataset=loaddataset)
		print('separation',ramsey_result['separation'])
		t2star,foffset,fiterr=self.fitramsey(dt,ramsey_result['population_norm'],plot=plot,figname=filename)
		return [data,ramsey_result['separation'],ramsey_result['iqafterherald'],ramsey_result['population_norm'],t2star,foffset,fiterr]
if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	#parser.set_defaults(elementstep=2e-6)
	#parser.set_defaults(elementstep=4e-7)
	parser.set_defaults(elementstep=1e-6)
	clargs=parser.parse_args()
	ramsey=c_ramsey(**clargs.__dict__)
	ramsey.ramseyseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readwidth=clargs.readwidth,elementlength=clargs.elementlength,elementstep=clargs.elementstep,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,framsey=clargs.framsey,qubitid=clargs.qubitid,qubitidread=clargs.qubitidread)
	ramsey.run(bypass=clargs.bypass or not clargs.processfile=='')
	if clargs.processfile=='':
		if clargs.sim:
			ramsey.sim()
		data=ramsey.ramseydata(clargs.nget)
		fprocess=ramsey.savejsondata(filename=clargs.filename,extype='ramsey',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
	else:
		fprocess=clargs.processfile
	[rawdata,separation,iqafterherald,population_norm,t2star,foffset,fiterr]=ramsey.processramsey(dt=clargs.elementstep,filename=fprocess,loaddataset=clargs.dataset)
	print('rabi training dataset ',clargs.dataset)
	print('t2star',t2star)
	print('foffset',foffset)
	#ramsey.plotrawdata(rawdata)
	#ramsey.plotafterheraldingtest(iqafterherald)
	#ramsey.plotpopulation_norm(population_norm)
	if clargs.plot:
		pyplot.grid()
		pyplot.show()
