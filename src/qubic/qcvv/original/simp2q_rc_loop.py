import datetime
from matplotlib import pyplot,patches
import numpy
import experiment
import json
import re
import trueq
import qubic_trueq
from simp2q import c_simp2q
import random
from itertools import permutations
import os
from trueq_config import config
import functools

easy_1q_gates=[trueq.Gate.cliff0, trueq.Gate.cliff1, trueq.Gate.cliff2, trueq.Gate.cliff3, 
		trueq.Gate.cliff4, trueq.Gate.cliff5, trueq.Gate.cliff6, trueq.Gate.cliff7, 
		trueq.Gate.cliff8, trueq.Gate.cliff9, trueq.Gate.cliff10, trueq.Gate.cliff11, 
		trueq.Gate.cliff12, trueq.Gate.cliff13, trueq.Gate.cliff14, trueq.Gate.cliff15, 
		trueq.Gate.cliff16, trueq.Gate.cliff17, trueq.Gate.cliff18, trueq.Gate.cliff19, 
		trueq.Gate.cliff20, trueq.Gate.cliff21, trueq.Gate.cliff22, trueq.Gate.cliff23]
hard_1q_gates=[trueq.Gate.from_generators("X", 45), trueq.Gate.from_generators("Y", 45), trueq.Gate.t]
hard_2q_gates=[trueq.Gate.cx, trueq.Gate.cy, trueq.Gate.cz]

def generate_random_circuit(qubits=[6,5],num_K_cycles=10,include_2q_gates=True,allow_parallel_hard_gates=True,control_target_list=None,random_SU2_gates=False):
	bare_circuit=None
	bare_circuit_collection=(trueq.CircuitCollection(circuits=None if bare_circuit is None else [bare_circuit]))
	circuit_cycles=[]
	for _ in range(num_K_cycles):
		if len(qubits)==1:
			if random_SU2_gates is True:
				easy_cycle={(qubits[0],): trueq.Gate.random(2)}
				hard_cycle={(qubits[0],): trueq.Gate.random(2)}
			else:
				easy_cycle={(qubits[0],): random.choice(easy_1q_gates)}
				hard_cycle={(qubits[0],): random.choice(hard_1q_gates)}
		elif len(qubits)>1:
			if include_2q_gates is True:
				if random_SU2_gates is True:
					easy_cycle={(q,): trueq.Gate.random(2) for q in qubits}
				else:
					easy_cycle={(q,): random.choice(easy_1q_gates + hard_1q_gates) for q in qubits}
				hard_cycle={}
				if control_target_list is not None:
					control_target=random.choice(control_target_list)
				else:
					control_target=random.choice(list(permutations(qubits,2)))
				hard_cycle[control_target]=random.choice(hard_2q_gates)
			elif include_2q_gates is False:
				if random_SU2_gates is True:
					easy_cycle={(q,): trueq.Gate.random(2) for q in qubits}
					hard_cycle={(q,): trueq.Gate.random(2) for q in qubits}
				else:
					easy_cycle={(q,): random.choice(easy_1q_gates) for q in qubits}
					hard_cycle={(q,): random.choice(hard_1q_gates) for q in qubits}
		circuit_cycles.extend([trueq.Cycle(easy_cycle),trueq.Cycle(hard_cycle, immutable=True)])
	if random_SU2_gates is True:
		circuit_cycles.append(trueq.Cycle({(q,): trueq.Gate.random(2) for q in qubits}))
	else:
		circuit_cycles.append(trueq.Cycle({(q,): random.choice(easy_1q_gates) for q in qubits}))
	bare_circuit=trueq.Circuit(circuit_cycles)
	bare_circuit.measure_all()
	bare_circuit_collection.append(bare_circuit)
	return bare_circuit,bare_circuit_collection

def plot_violin(violin,label):
	color=violin["bodies"][0].get_facecolor().flatten()
	labels.append((patches.Patch(color=color),label))


if __name__=="__main__":
	starttime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	datetimeformat='%Y%m%d_%H%M%S_%f'
	num_bare_circuits=1#20#1#100#50#30
	num_rc_circuits_list=range(5,100+5,5)#[2,5,10,20,40,100]#range(5,100+5,5)
	num_K_cycles_list=range(5,80+5,5)#[5,10,30,80]#range(5,80+5,5)
	include_2q_gates=True
	allow_parallel_hard_gates=False
	random_SU2_gates=False

	for num_rc_circuits in num_rc_circuits_list:
		TVD_all=[]
		cmdarraylength_all=[]
		elapsedtime_all=[]
		transpiletime_all=[]
		transfertime_all=[]
		seqgentime_all=[]
		runtime_all=[]
		acqtime_all=[]
		processtime_all=[]
		for num_K_cycles in num_K_cycles_list:
			TVD_row=[]
			cmdarraylength_row=[]
			elapsedtime_row=[]
			transpiletime_row=[]
			transfertime_row=[]
			seqgentime_row=[]
			runtime_row=[]
			acqtime_row=[]
			processtime_row=[]
			for ibare in range(num_bare_circuits):
				part1starttime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
				simp2q=c_simp2q()
				parser,cmdlinestr=simp2q.cmdoptions()
				clargs=parser.parse_args()

				gates=[simp2q.qchip.gates['M0mark'],simp2q.qchip.gates['Q6X90'],simp2q.qchip.gates['Q6read'],simp2q.qchip.gates['Q5X90'],simp2q.qchip.gates['Q5read'],simp2q.qchip.gates['Q6Q5CNOT']]
				elementlist={'Q6.qdrv':2,'Q5.qdrv':3,'Q7.qdrv':1,'Q6.rdrv':5,'Q5.rdrv':6,'Q7.rdrv':4,'Q6.read':9,'Q5.read':10,'M0.mark':12}
				destlist=   {'Q6.qdrv':2,'Q5.qdrv':3,'Q7.qdrv':1,'Q6.rdrv':0,'Q5.rdrv':0,'Q7.rdrv':0,'Q6.read':5,'Q5.read':6,'M0.mark':13}
				patchlist={'Q6.rdrv':0,'Q6.read':0,'Q6.qdrv':0,'Q5.rdrv':8e-9,'Q5.read':8e-9,'Q5.qdrv':8e-9,'M0.mark':0}
				delayafterheralding=12e-6
				delaybetweenelement=600e-6
				firsttime=True
				include_readout_calibration=False # Not use TrueQ readout correction
				control_target_list=None
				num_K_cycles=num_K_cycles

				qubits=[int(re.findall('\d+',q)[0]) for q in clargs.qubitid_list]
				bare_circuit,bare_circuit_collection=generate_random_circuit(qubits=qubits,num_K_cycles=num_K_cycles,include_2q_gates=include_2q_gates,allow_parallel_hard_gates=allow_parallel_hard_gates,control_target_list=control_target_list,random_SU2_gates=random_SU2_gates)
				#print('bare_circuit',bare_circuit)

				bare_circuits_collection=trueq.CircuitCollection()

				for _ in range(num_rc_circuits):
					bare_circuits_collection.append(bare_circuit)

				rc_circuits=trueq.randomly_compile(bare_circuit,n_compilations=num_rc_circuits)
				#print('rc_circuits',rc_circuits)

				rc_circuits_collection=trueq.CircuitCollection()
				rc_circuits_collection.append(rc_circuits)

				part1stoptime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
				part1time=(datetime.datetime.strptime(part1stoptime,datetimeformat)-datetime.datetime.strptime(part1starttime,datetimeformat)).total_seconds()
				print('part1time',part1time)
#####################################	
				for circuits_collection in [bare_circuits_collection,rc_circuits_collection]:
					timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
					if include_readout_calibration:
						circuits_collection.append(trueq.make_rcal(circuits_collection.labels))
					circuits=circuits_collection
					circuits.save('circuits_'+timestamp+'.bin')
					bare_circuit_collection.save('barecircuit_'+timestamp+'.bin')

					transpiler=trueq.compilation.get_transpiler(config)
					transpiled_circuit=transpiler.compile(circuits)
					part2_transpile_stoptime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
#####################################
					lastcmdarraylength=0
					[qubitid,cmdarray]=qubic_trueq.trueq_circuits_to_qubic2(transpiled_circuit,heralding=delayafterheralding>0)
					#for l in cmdarray:
					#	print(l)
					part2_transfer_stoptime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
#####################################
					cmdarraylength=simp2q.simp2qseqsgen(delayafterheralding=delayafterheralding,delaybetweenelement=delaybetweenelement,cmdarray=cmdarray,gates=gates if firsttime else None,elementlist=elementlist,destlist=destlist,patchlist=patchlist,qubitid=qubitid)
					part2_seqgen_stoptime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
#####################################
					simp2q.run(memwrite=firsttime,memclear=firsttime,cmdclear=cmdarraylength<lastcmdarraylength,preread=False,cmdclearlength=lastcmdarraylength*4)
					part2_run_stoptime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
#####################################
					lastcmdarraylength=cmdarraylength
					#print('cmdarray this',cmdarraylength,'last',lastcmdarraylength)
					data=simp2q.simp2qacq(clargs.nget)
					fprocess=simp2q.savejsondata(filename=clargs.filename,extype='simp2q',cmdlinestr=cmdlinestr,data=data)
					print('save data to ',fprocess)
					part2_acq_stoptime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
#####################################
					trueq_return,meas_binned=simp2q.processsimp2q(filename=fprocess,readcorr=True,corr_matrix=numpy.load(clargs.corrmx)) # Use QubiC readout correction
					if len(qubitid)==1:
						qubic_result=[{'0':meas_binned[0][i],'1':meas_binned[1][i]} for i in range(meas_binned.shape[1])]
					elif len(qubitid)==2:
						qubic_result=[{'00':meas_binned[0][i],'01':meas_binned[1][i],'10':meas_binned[2][i],'11':meas_binned[3][i]} for i in range(meas_binned.shape[1])]
					else:
						print('how many qubits do you have?')
					#print('qubic_result',qubic_result)
					timestampresult=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
					resultfilename='circuits_'+timestamp+'_'+timestampresult+'.dat'
					with open(resultfilename,'w') as f_qubicresult:
						json.dump(qubic_result,f_qubicresult)
					part2_process_stoptime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
#####################################
					bare_circuit=trueq.load('barecircuit%s.bin'%re.match('(\S+)(_\S+_\S+_\S+)(_\S+_\S+_\S+).dat',resultfilename).groups()[1])[0]
					ideal_results=trueq.Simulator().state(bare_circuit).probabilities().to_results()
					print('ideal_results',ideal_results)

					with open(resultfilename) as file_read:
						result_raw=json.loads(file_read.read())
					circuits_results={key: sum(item.get(key,0) for item in result_raw)/len(result_raw) for key in functools.reduce(lambda a,b:set(a)|set(b), result_raw)}
					print('avg_result_raw',circuits_results)
					TVD=ideal_results.tvd(trueq.Results(circuits_results))
					TVD_row.append(TVD[0])
					print('TVD',TVD)
					cmdarraylength_row.append(cmdarraylength)
					print('cmdarraylength',cmdarraylength)

					part2_transpile_time=(datetime.datetime.strptime(part2_transpile_stoptime,datetimeformat)-datetime.datetime.strptime(timestamp,datetimeformat)).total_seconds()
					print('part2_transpile_time',part2_transpile_time)
					part2_transfer_time=(datetime.datetime.strptime(part2_transfer_stoptime,datetimeformat)-datetime.datetime.strptime(part2_transpile_stoptime,datetimeformat)).total_seconds()
					print('part2_transfer_time',part2_transfer_time)
					part2_seqgen_time=(datetime.datetime.strptime(part2_seqgen_stoptime,datetimeformat)-datetime.datetime.strptime(part2_transfer_stoptime,datetimeformat)).total_seconds()
					print('part2_seqgen_time',part2_seqgen_time)
					part2_run_time=(datetime.datetime.strptime(part2_run_stoptime,datetimeformat)-datetime.datetime.strptime(part2_seqgen_stoptime,datetimeformat)).total_seconds()
					print('part2_run_time',part2_run_time)
					part2_acq_time=(datetime.datetime.strptime(part2_acq_stoptime,datetimeformat)-datetime.datetime.strptime(part2_run_stoptime,datetimeformat)).total_seconds()
					print('part2_acq_time',part2_acq_time)
					part2_process_time=(datetime.datetime.strptime(part2_process_stoptime,datetimeformat)-datetime.datetime.strptime(part2_acq_stoptime,datetimeformat)).total_seconds()
					print('part2_process_time',part2_process_time)

					part2stoptime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
					part2time=(datetime.datetime.strptime(part2stoptime,datetimeformat)-datetime.datetime.strptime(timestamp,datetimeformat)).total_seconds()
					print('part2time',part2time)
					elapsedtime=part1time+part2time
					elapsedtime_row.append(elapsedtime)
					print('elapsedtime',elapsedtime)
					transpiletime_row.append(part2_transpile_time)
					transfertime_row.append(part2_transfer_time)
					seqgentime_row.append(part2_seqgen_time)
					runtime_row.append(part2_run_time)
					acqtime_row.append(part2_acq_time)
					processtime_row.append(part2_process_time)

			TVD_all.append(TVD_row)
			cmdarraylength_all.append(cmdarraylength_row)
			elapsedtime_all.append(elapsedtime_row)
			transpiletime_all.append(transpiletime_row)
			transfertime_all.append(transfertime_row)
			seqgentime_all.append(seqgentime_row)
			runtime_all.append(runtime_row)
			acqtime_all.append(acqtime_row)
			processtime_all.append(processtime_row)

		TVD_filename='TVD'+'_nrc'+str(num_rc_circuits)+'_nbare'+str(num_bare_circuits)+'_K'+str('_'.join(map(str,num_K_cycles_list)))+'_nget'+str(clargs.nget)+'_'+str(''.join(map(str,clargs.qubitid_list)))+'_'+starttime+'.dat'
		numpy.savetxt(TVD_filename,TVD_all)
		cmdarraylength_filename='cmdarraylength'+'_nrc'+str(num_rc_circuits)+'_nbare'+str(num_bare_circuits)+'_K'+str('_'.join(map(str,num_K_cycles_list)))+'_nget'+str(clargs.nget)+'_'+str(''.join(map(str,clargs.qubitid_list)))+'_'+starttime+'.dat'
		numpy.savetxt(cmdarraylength_filename,cmdarraylength_all)
		elapsedtime_filename='elapsedtime'+'_nrc'+str(num_rc_circuits)+'_nbare'+str(num_bare_circuits)+'_K'+str('_'.join(map(str,num_K_cycles_list)))+'_nget'+str(clargs.nget)+'_'+str(''.join(map(str,clargs.qubitid_list)))+'_'+starttime+'.dat'
		numpy.savetxt(elapsedtime_filename,elapsedtime_all)

		transpiletime_filename='transpiletime'+'_nrc'+str(num_rc_circuits)+'_nbare'+str(num_bare_circuits)+'_K'+str('_'.join(map(str,num_K_cycles_list)))+'_nget'+str(clargs.nget)+'_'+str(''.join(map(str,clargs.qubitid_list)))+'_'+starttime+'.dat'
		numpy.savetxt(transpiletime_filename,transpiletime_all)
		transfertime_filename='transfertime'+'_nrc'+str(num_rc_circuits)+'_nbare'+str(num_bare_circuits)+'_K'+str('_'.join(map(str,num_K_cycles_list)))+'_nget'+str(clargs.nget)+'_'+str(''.join(map(str,clargs.qubitid_list)))+'_'+starttime+'.dat'
		numpy.savetxt(transfertime_filename,transfertime_all)
		seqgentime_filename='seqgentime'+'_nrc'+str(num_rc_circuits)+'_nbare'+str(num_bare_circuits)+'_K'+str('_'.join(map(str,num_K_cycles_list)))+'_nget'+str(clargs.nget)+'_'+str(''.join(map(str,clargs.qubitid_list)))+'_'+starttime+'.dat'
		numpy.savetxt(seqgentime_filename,seqgentime_all)
		runtime_filename='runtime'+'_nrc'+str(num_rc_circuits)+'_nbare'+str(num_bare_circuits)+'_K'+str('_'.join(map(str,num_K_cycles_list)))+'_nget'+str(clargs.nget)+'_'+str(''.join(map(str,clargs.qubitid_list)))+'_'+starttime+'.dat'
		numpy.savetxt(runtime_filename,runtime_all)
		acqtime_filename='acqtime'+'_nrc'+str(num_rc_circuits)+'_nbare'+str(num_bare_circuits)+'_K'+str('_'.join(map(str,num_K_cycles_list)))+'_nget'+str(clargs.nget)+'_'+str(''.join(map(str,clargs.qubitid_list)))+'_'+starttime+'.dat'
		numpy.savetxt(acqtime_filename,acqtime_all)
		processtime_filename='processtime'+'_nrc'+str(num_rc_circuits)+'_nbare'+str(num_bare_circuits)+'_K'+str('_'.join(map(str,num_K_cycles_list)))+'_nget'+str(clargs.nget)+'_'+str(''.join(map(str,clargs.qubitid_list)))+'_'+starttime+'.dat'
		numpy.savetxt(processtime_filename,processtime_all)

# python simp2q_rc_loop.py -n 1 --plot --qubitid_list Q6 Q5
