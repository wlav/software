import matplotlib
matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
from matplotlib import pyplot,patches
import datetime
import argparse
import sys
import numpy
import time
import init
import pprint
import experiment
import copy
from itertools import product

class c_rb(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid=None
		pass
	def rbseqs(self,elementlength=80,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,restart=True,el_clif_step=50,qubitid='Q7',qubitidread=['Q5','Q4','Q3']):
		self.qubitid=qubitid
		if restart:
			self.initseqs()
		rbgates=[]
		#c_list=numpy.loadtxt('rb_clist_20200529.txt',dtype=str).tolist() #python rb.py --plot -n 5 --qubitid Q6 -el 11 -nm 22 --readcorr #~/Work/bigfridge/gui/fmc120/qubic/qubitcfg.json
		c_list=self.rand_clif_1q((elementlength-1)*el_clif_step+2)  # elementlength=11, rand_clif_1q(502), 502 Clifford Pairs, 1004 Gates
		with open('rb_clist.txt','w') as file_clist:
			for listitem in c_list:
				file_clist.write('%s\n' %listitem)
		g=['I','Y90','Y180','Y270','X90','X180','X270','Z90','Z270']
		print(['%s %d'%(i,c_list.count(i)) for i in g])
		el_clif_list_pair=numpy.arange(2,2+elementlength*el_clif_step,el_clif_step)  #pair:2,52,102,152,...,452,502
		el_clif_list_gate=2*el_clif_list_pair  #gate:4,104,204,304,...,904,1004
		for i in range(elementlength):
			c_list_id=self.g_list_add_inverse(c_list[:el_clif_list_gate[i]])  #4,104,204,304,...,904,1004
			rbgates.append(c_list_id)
			print(len(c_list_id))
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		run=0
		for gates in rbgates:
			therald=run
			self.seqs.add(therald,self.qchip.gates[qubitid+'readoutdrv'])
			self.seqs.add(therald+delayread,self.qchip.gates[qubitid+'readout'])
			run=self.seqs.tend()+delay1
			tini=run
			padd=0
			for gate in gates:
				gatemodi={}
				if gate=='I':
					pass
				else:
					if 'Z' in gate:
						padd=padd-float(gate[1:])/180.*numpy.pi
					else:
						pnew=self.qchip.gates[qubitid+gate].pcalc(dt=run-tini,padd=padd)[0]
						gatemodi.update(dict(pcarrier=float(pnew)))#####?????#####gatemodi.update(dict(pcarrier=pnew))
						self.seqs.add(run,self.qchip.gates[qubitid+gate].modify(gatemodi))
					run=self.seqs.tend()
			treaddrv=self.seqs.tend()
			self.seqs.add(treaddrv,self.qchip.gates[qubitid+'readoutdrv'])
			self.seqs.add(treaddrv+delayread,self.qchip.gates[qubitid+'readout'])
			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
	def processrb(self,filename,loaddataset):
		c=self.loadjsondata(filename)
		print('c.keys()',c.keys())
		data=c[list(c.keys())[0]]
		rb_result=self.process3(data,qubitid=self.qubitid,lengthperrow=self.bufwidth_dict[self.qubitid],training=False,loaddataset=loaddataset)
		return [data,rb_result['separation'],rb_result['iqafterherald'],rb_result['population_norm']]
if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	clargs=parser.parse_args()
	rb=c_rb(**clargs.__dict__)
	el_clif_step=50
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	for nrb in range(clargs.nmeas):
		parser,cmdlinestr=experiment.cmdoptions()
		clargs=parser.parse_args()
		rb=c_rb(**clargs.__dict__)
		if clargs.sim:
			rb.setsim()
		print('measurement: %3d of %3d'%(nrb+1,clargs.nmeas))
		rb.rbseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,el_clif_step=el_clif_step,qubitid=clargs.qubitid,qubitidread=clargs.qubitidread)
		rb.run(bypass=clargs.bypass or not clargs.processfile=='')
		if clargs.processfile=='':
			data=rb.acqdata(clargs.nget)
			fprocess=rb.savejsondata(filename=clargs.filename,extype='rb',cmdlinestr=cmdlinestr,data=data)
			print('save data to ',fprocess)
			if clargs.sim:
				rb.sim()
		else:
			fprocess=clargs.processfile
		[rawdata,separation,iqafterherald,population_norm]=rb.processrb(filename=fprocess,loaddataset=clargs.dataset)
		if clargs.readcorr:
			population_norm=rb.readoutcorrection(qubitid_list=[clargs.qubitid],meas_binned=numpy.vstack((1-population_norm,population_norm)),corr_matrix=numpy.load(clargs.corrmx))[1]
			print('corrected population_norm',population_norm)
		print('dataset',clargs.dataset)
		population_norm_tmp=population_norm.reshape((1,-1))
		population_norm_array=population_norm_tmp if nrb==0 else numpy.append(population_norm_array,population_norm_tmp,axis=0)
		with open('rb_population_norm_'+timestamp+'.dat','a') as f:
			numpy.savetxt(f,population_norm_tmp)
		print('separation',separation)
		print(rb.hf.adcminmax())
	population_norm_array=population_norm_array[2:,:]
	recovery_population_array=1-population_norm_array
	recovery_population_mean=numpy.mean(recovery_population_array,axis=0)
	recovery_population_std=numpy.std(recovery_population_array,axis=0)
	el_clif_list_pair=numpy.arange(2,2+clargs.elementlength*el_clif_step,el_clif_step)  #2,52,102,152,...,452,502
	numpy.savetxt('rb_recovery_population_array'+fprocess,recovery_population_array)
	rb.plotrb(recovery_population_array,recovery_population_mean,recovery_population_std,el_clif_list_pair,figname=fprocess)
	#[rb_p,rb_a]=rb.fitrb(el_clif_list_pair,recovery_population_mean,yerr=recovery_population_std,figname=fprocess)
	[rb_p,rb_a]=rb.fitrb_violin(recovery_population_array,recovery_population_mean,recovery_population_std,el_clif_list_pair,figname=fprocess)
	print('rb_p,rb_a',rb_p,rb_a)
	if clargs.plot:
		pyplot.grid()
		pyplot.show()
	# python rb.py --plot -n 50 --qubitid Q5 -el 11 -nm 12
	# python rb.py --plot -n 50 --qubitid Q5 -el 11 -nm 12 --readcorr
