#import matplotlib
#matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
import datetime
import argparse
import sys
#from squbic import *
from matplotlib import pyplot,patches
#from qubic_t1 import cmdadd, cmdgen
import numpy
#from ether import c_ether
#from mem_gateway import c_mem_gateway
import time
from ..qubic import init
import pprint
from scipy import signal
from ..qubic import experiment
class c_vnaqubit(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid=None
	def vnaqubitseqs(self,delayread=920e-9,delaybetweenelement=600e-6,elementlength=1024,readoutdrvamp=None,readwidth=None,fstart=-500e6,fstop=500e6,rdc=0,qubitid='Q1',qubitdrvamp=None,fread=None,qubitidread=['Q2','Q1','Q0']):
		self.qubitid=qubitid
		df=1.0*(fstop-fstart)/(elementlength-1)
		self.freq=numpy.arange(fstart,fstop+df,df)[:elementlength] # avoid rounding error in python, make sure that self.freq length is elementlength
		print(len(self.freq))
		modrdrv={}
		modread={}
		modqdrv={}
		if readoutdrvamp:
			modrdrv.update(dict(amp=min(1-rdc,readoutdrvamp)))
		if qubitdrvamp:
			modqdrv.update(amp=qubitdrvamp)
		if fread:
			modrdrv.update(dict(fcarrier=fread))
			modread.update(dict(fcarrier=fread))
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		trun=0
		for frun in self.freq:
			modqdrv.update(dict(fcarrier=frun))
			#modread.update(dict(fcarrier=frun))
			self.seqs.add(trun,self.qchip.gates[qubitid+'rabi'].modify(modqdrv))
			treaddrv=self.seqs.tend()
			self.seqs.add(treaddrv,self.qchip.gates[qubitid+'read'].modify([modrdrv,modread]))
			trun=self.seqs.tend()+delaybetweenelement
		print('period',trun)
		self.seqs.setperiod(period=trun)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
	def vnaqubitdata(self,nget):
		data=self.acqdata(nget)
		return data
	def processvnaqubit(self,filename):
		#c=self.loadjsondata(filename)['accout_0']
		c=self.loadjsondata(filename)
		print(list(c.keys())[0])
		c=c[list(c.keys())[0]]
		cavr=c.reshape((-1,len(self.freq))).mean(0)
		return cavr
	def vnaqubitplot(self,c):
		pyplot.subplot(211)
		pyplot.semilogy(self.freq,abs(c))
		pyplot.grid()
		pyplot.tight_layout()
		pyplot.subplot(212)
		pyplot.plot(self.freq,signal.detrend(numpy.unwrap(numpy.angle(c))))
		pyplot.grid()
		pyplot.tight_layout()
def main():
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(delaybetweenelement=2e-6)#500e-6)
	parser.set_defaults(elementlength=1024)#128)
	clargs=parser.parse_args()
	vnaqubit=c_vnaqubit(**clargs.__dict__)
	vnaqubit.vnaqubitseqs(delayread=clargs.delayread,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,readoutdrvamp=clargs.readoutdrvamp,readwidth=None,fstart=clargs.fstart,fstop=clargs.fstop,rdc=0,qubitid=clargs.qubitid,qubitdrvamp=clargs.qubitdrvamp,fread=clargs.fread,qubitidread=clargs.qubitidread)
	if clargs.processfile=='':
		if clargs.sim:
			vnaqubit.sim()
		vnaqubit.run(bypass=clargs.bypass)
		#time.sleep(2)
		data=vnaqubit.vnaqubitdata(clargs.nget)
		fprocess=vnaqubit.savejsondata(filename=clargs.filename,extype='vnaqubit',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
	else:
		fprocess=clargs.processfile
	#[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=vnaqubit.processvnaqubit(dt=clargs.elementstep,filename=fprocess,swapdata=clargs.swapdata,swappre=clargs.swappre)
	c=vnaqubit.processvnaqubit(fprocess)
#	vnaqubit.plotrawdata(rawdata)
#	vnaqubit.plotafterheraldingtest(iqafterherald)
#	vnaqubit.plotpopulation_norm(population_norm)
	if clargs.plot:
		vnaqubit.vnaqubitplot(c)
		pyplot.savefig(fprocess+'.pdf')
		pyplot.show()
if __name__=="__main__":
	main()
