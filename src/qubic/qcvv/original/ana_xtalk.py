#!/usr/bin/env python3
'''
Goal: reads 6 measurements from meas_xtalk.py and does all analysis, generates beta, theta matrices for Adam

Primary datastorage for state sorted yields (see do_yield_stats(.))

INPUT:  yield[NY,NX,NB], where
         NB: number of N-qubit bit-strings
         NX,NY: user defined (preserved) dimensions
OUTPUT: prob[PE,NY,NX] , where
         PE=4 for: prob, probErr, yield, varYield 
Here:
  NX: 80 time bins for rabi-time scan
  NY: 6 measurements, the order is 3+3:  rdQ=drQ*(X,Y,Z), rdQ!=drQ*(X,Y,Z) 
  NB: 2 - single qubit measurements has only 2 states : 0 or 1 

'''
import os
from toolbox.Util_H5io3 import  write3_data_hdf5, read3_data_hdf5
from toolbox.Util_Qubic2 import do_yield_stats, prob_to_pauliExpectedValue
from toolbox.Util_Fitter  import  fit_data_model
from toolbox.Util_IOfunc import write_yaml
from toolbox.PlotterXtalk import Plotter
from toolbox.ModelSin   import  ModelSin
from toolbox.ModelLinear   import  ModelLinear

from pprint import pprint
import copy
import numpy as np

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],  help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',action='store_true', default=False,help="disable X-term for batch mode")

    parser.add_argument("--dataPath",  default='/home/balewski/dataAdamXtalk/xtalkMeas',help="measured data location")
    parser.add_argument( "--readQubit",  default='Q6')
    parser.add_argument( "--driveQubit", default='Q5')

    parser.add_argument("-o","--outPath", default='/home/balewski/dataAdamXtalk/xtalkSum/',help="output path for plots and tables")

    args = parser.parse_args()
    args.formatVenue='prod'
    args.dataPath+='/'
    args.outPath+='/'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#...!...!..................
def init_storage(inpMD,args):    
    pprint(inpMD)
    inpMD['qubit_drive_loc_osc']=float(os.environ['QUBIC_DRIVER_LO']) # tmp hack
    
    NX=inpMD['num_steps']
    NY=6
    NB=2
    dataY=np.zeros((NY,NX,NB),dtype=np.float32)
    print('I:',dataY.shape)
    xL=['num_shots', 'meas_date', 'chip_name','fridge_name','drive_rabi_amp','read_qubit_drvfreq','drive_qubit_drvfreq','qubit_drive_loc_osc']
    anaMD={x:inpMD[x]  for x in xL }
    anaMD['read_qubit']=args.readQubit
    anaMD['drive_qubit']=args.driveQubit
    anaMD['exp_order']=[]
    return dataY,anaMD
       

#...!...!..................
def read_xtalk_data(args):
    rdQ=args.readQubit
    drQL=[args.readQubit,args.driveQubit]
    paxL=['X','Y','Z']
    usedShots=[]
    im=0
    for drQ in drQL:      
        for pax in  paxL:            
            name="xtalk_rd%s_ax%s_dr%s"%(rdQ,pax,drQ)
            name2="ax%s_dr%s"%(pax,drQ)
            print('R:',name,name2)
            inpF=args.dataPath+'/'+name+'.h5'
            dataD,inpMD=read3_data_hdf5(inpF, verb=im==0)
            if im==0:
                dataY,anaMD=init_storage(inpMD,args)
                dataT=dataD['rabi_width']*1e9 # now in ns                
            else:
                assert rdQ==inpMD['read_qubit']
                assert drQ==inpMD['drive_qubit']
                assert pax==inpMD['meas_axis'].upper()
                assert dataT.shape==dataD['rabi_width'].shape
            anaMD['exp_order'].append(name2)
            countAr=dataD['counts'].T # want bit-string to be last index
            dataY[im]=countAr
            avrUsedShots=np.sum(countAr)/countAr.size *2 # sum 0+1 states
            usedShots.append(int(avrUsedShots))
            #print('dump ',name,dataD['counts'].shape)
            #pprint(dataD['counts'])
            im+=1
        #break #tmp
    anaMD['qpair_name']='%s_rd%s-dr%s'%(anaMD['chip_name'],args.readQubit,args.driveQubit)
    anaMD['avr_used_shots']= usedShots
    return dataY,dataT,anaMD

#...!...!..................
def dump_pop1(dataPop,dataT,anaMD):
    # shape: PE,NY,NX
    NY=6
    NX=dataPop.shape[2]
    rdQ=anaMD['read_qubit']
    drQ=anaMD['drive_qubit']
    print('tbin T/ns     rdQ=%s drQ=%s *(Z,X,Y)   %17s     dQ=%s drQ=%s *(Z,X,Y) '%(rdQ,rdQ,',',rdQ,drQ))
    for ix in range(NX):
        print('%2d: %3.0f    '%(ix,dataT[ix]),end='')
        for iy in range(NY):
            pr,er,_,_=dataPop[:,iy,ix]
            print('%.3f +-%.3f,  '%(pr,er),end='')  
        print()

       
#...!...!..................
def update_xtalk_summary(anaMD):
    rdQ=anaMD['read_qubit']
    drQ=anaMD['drive_qubit']
    QL=[rdQ,drQ]
        
    def div_freq(a,b): # divide values and propagate errors
        x=a[0]/b[0]
        rv=(a[1]/a[0])**2 +(b[1]/b[0])**2 
        e=x*np.sqrt(rv)
        return x,e

    def slope2ang(a): # computes angle  and propagates error
        # y = arctan(x) --> dy/dx=1/cos^2(y)
        ang=np.arctan2(a[0],1.) # x is 2nd !
        err=a[1]/np.cos(ang)**2        
        return ang,err


    #.... compute xtalk_strength
    freqV=[]
    for it in range(2):
        ftag="fit_axZ_dr%s"%(QL[it])
        fitMD=anaMD[ftag]
        FV=fitMD['fit_result']['fitPar']['F']
        freqV.append(FV)

    betaVal, betaErr=div_freq(freqV[1],freqV[0])
    anaMD['xtalk_strength']=[ float("%.3f"%x) for x in [betaVal, betaErr]]
    
    # .... compute xtalk_phase     
    ftag="fit_xy_dr%s"%(drQ)
    fitMD=anaMD[ftag]
    AV=fitMD['fit_result']['fitPar']['A']
    angVal,angErr=slope2ang(AV) # should be for it==1
    anaMD['xtalk_phase']=[ float("%.4f"%x) for x in [ angVal,angErr]]+['rad']
    
     
        
#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__=="__main__":

    args=get_parser()
    
    assert args.readQubit!=args.driveQubit
    dataYield,dataTime,anaMD=read_xtalk_data(args)
    # compute: prob for each bit-string (an overkill for 1 qubit)
    dataProb=do_yield_stats(dataYield) #ok
    
    print('M:dataP',dataProb.shape)
    pprint(anaMD)
    dataPop=dataProb[...,1] # use only state=1 data
    #1dump_pop1(dataPop,dataTime,anaMD)

    print('exp_ord',anaMD['exp_order'])
   
    # ...... fitting rabi-sine wave 
    plDD=copy.deepcopy(anaMD)
    oscModel=ModelSin()
    for iy in [2,5]: # ax=Z
        #break #tmp
        fitY,fitMD=fit_data_model(dataPop[:,iy],dataTime,oscModel)
        ftag='fit_'+anaMD['exp_order'][iy]
        plDD[ftag+'_pred']=fitY
        plDD[ftag]=fitMD
        anaMD[ftag]=fitMD
    if args. verb>1: pprint(fitMD)

    # ...... fitting X-Y correlation
    dataEV=prob_to_pauliExpectedValue(dataPop)
    lineModel=ModelLinear()
    for it in range(2):
        #break #tmp
        ftag='fit_xy_dr'+args.readQubit
        if it==1: ftag='fit_xy_dr'+args.driveQubit
        jt=it*3
        xBins=dataEV[0,jt] # use EV ignore errEV
        fitY,fitMD=fit_data_model(dataEV[:,jt+1],xBins,lineModel)
        plDD[ftag+'_pred']=fitY
        plDD[ftag]=fitMD
        anaMD[ftag]=fitMD
        #break

    update_xtalk_summary(anaMD)
    # save summary
    outF='%s_xtalkSum.yaml'%(anaMD['qpair_name'])
    write_yaml(anaMD,args.outPath+outF)
    print('M: MD-saved')
    #ok33

    # ....  plotting ........
    for x in ['xtalk_phase','xtalk_strength']:
        plDD[x]=anaMD[x]
    args.prjName='xtalkSum-'+anaMD['qpair_name']
    plot=Plotter(args)
        
    plot.rabi_fit(dataTime, dataPop,plDD)
    plot.expectation_xy(dataEV,plDD)
    
    viewAngs=[-45,85] # almost top view
    viewAngs=[-45,90] #  top view
    plot.bloch( dataEV,anaMD,viewAngs=viewAngs)
    plot.tomo_probs(dataTime, dataPop,plDD)
    
    plot.display_all()
    
    print('M:done')

