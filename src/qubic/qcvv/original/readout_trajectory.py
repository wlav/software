import json
import datetime
import argparse
import sys
from matplotlib import pyplot,patches
import numpy
import time
import init
import pprint
import experiment

class c_readouttrajectory(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		pass
	def readouttrajectoryseqs(self,delaybetweenelement=100e-6,readoutdrvamp=None,readwidth=None,fread=None,qubitid='Q5',qubitidread=['Q7','Q6','Q5'],prep='I'):
		print('readoutdrvamp,readwidth,fread',readoutdrvamp,readwidth,fread)
		self.seqs.add(12e-9,self.qchip.gates['M0mark'].modify(dict(twidth=8e-9)))
		modrdrv={}
		modread={}
		if readoutdrvamp is not None:
			modrdrv.update(dict(amp=readoutdrvamp))
		if readwidth is not None:
			modrdrv.update(dict(twidth=readwidth))
			modread.update(dict(twidth=readwidth))
		if fread is not None:
			modrdrv.update(dict(fcarrier=fread))
			modread.update(dict(fcarrier=fread))
		run=0e-9
		if prep=='X180':
			self.seqs.add(run,self.qchip.gates[qubitid+'X180'])
			run=self.seqs.tend()
		self.seqs.add(run,self.qchip.gates[qubitid+'read'].modify([modrdrv,modread]))
		run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
	def plotmonwithfft(self,t,buf0,buf1):
		#print(type(t),type(buf0),type(buf1))
		#print(t.shape,buf0.shape,buf1.shape)
		fmax=1./(numpy.diff(t).mean())
		df=fmax/(len(t)-1)
		length=len(t)#//2
		f=numpy.arange(length)*df
		fftval=numpy.fft.fft(buf0)[0:length]
		pyplot.figure(figsize=(8,8))
		pyplot.subplot(221)
		pyplot.plot(t,buf0,'.-')
		pyplot.subplot(222)
		pyplot.plot(f,abs(fftval))
		pyplot.subplot(223)
		pyplot.plot(t,buf1,'.-')
		pyplot.subplot(224)
		pyplot.plot(f,numpy.angle(fftval))
#		print(f[0:5],f[-5:-1],df)
#		print(t[0:5],t[-5:-1],fmax)

if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(delaybetweenelement=100e-6)
	clargs=parser.parse_args()
	loops=4
	for prep in ['I','X180']:
		if clargs.processfile=='':
			readouttrajectory=c_readouttrajectory(**clargs.__dict__)
			readouttrajectory.readouttrajectoryseqs(delaybetweenelement=clargs.delaybetweenelement,readoutdrvamp=clargs.readoutdrvamp,readwidth=clargs.readwidth,fread=clargs.fread,qubitid=clargs.qubitid,qubitidread=clargs.qubitidread,prep=prep)
			readouttrajectory.run(bypass=clargs.bypass)
			for iloop in range(loops):
				print('###iloop###',iloop)
				readouttrajectory_timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
				t,buf0,buf1=readouttrajectory.acqmonout(avrfactor=clargs.avrfactor,opsel=clargs.opsel,mon_navr=clargs.timezoom,mon_dt=clargs.timeshift,mon_sel0=clargs.mon_sel0,mon_sel1=clargs.mon_sel1)
				jsondata=dict(t=t.tolist(),buf0=buf0.tolist(),buf1=buf1.tolist())
				fprocess=readouttrajectory.savejsondata(filename=clargs.filename,extype='readouttrajectory_'+prep+'_',cmdlinestr=cmdlinestr,data=jsondata)
		else:
			fprocess=clargs.processfile
		if clargs.plot:
			with open(fprocess) as f:
				d=json.load(f)
			t=numpy.array(d['t'])
			buf0=numpy.array(d['buf0'])
			buf1=numpy.array(d['buf1'])
			buf0=buf0[:,0]
			buf1=buf1[:,0]
			readouttrajectory.plotmonwithfft(t,buf0,buf1)
			pyplot.savefig(fprocess+'.pdf')
			pyplot.show()
	# python readout_trajectory.py -o 2 -0 0 -1 1 -a 1 --qubitid Q3 --qubitidread Q3
	# Selection of DLO monitor channel: please match the DLO gate with DLO allocation in hfbridge.py
	# Example: "-1 2", 'Q0readout', 'Q0.read':4, self.lo0elementsdest=['Q0.read']
	# monseloptions='''0(xmeasin_d2),1(ymeasin_d2),2(xlo_w[0]),3(ylo_w[0]),4(xlo_w[1]),5(ylo_w[1]),6(xlo_w[2]),7(ylo_w[2]),8(dac0_in),9(dac1_in),10(dac2_in),11(dac3_in),12(dac4_in),13(dac5_in),14(dac6_in),15(dac7_in)'''
