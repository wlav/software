import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import experiment
class c_rabi(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		pass
	def rabiseqs(self,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,elementlength=80,elementstep=4e-9,readoutdrvamp=None,qubitdrvamp=None,readwidth=None,fqubit=None,fread=None,rdc=0,preadout=None):
		readoutdrvamp0,readoutdrvamp1,readoutdrvamp2=self.rdrvcalc(readoutdrvamp,dcoffset=rdc)
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		modq7rdrv={}
		modq6rdrv={}
		modq5rdrv={}
		modq7read={}
		modq6read={}
		modq5read={}
		modq7rabi={}
		if readoutdrvamp:
			modq7rdrv.update(dict(amp=readoutdrvamp0))
			modq6rdrv.update(dict(amp=0))
			modq5rdrv.update(dict(amp=0))
		if qubitdrvamp:
			modq7rabi.update(dict(amp=qubitdrvamp))
		if fqubit:
			modq7rabi.update(dict(fcarrier=fqubit))
		if readwidth:
			modq7rdrv.update(dict(twidth=readwidth))
			modq6rdrv.update(dict(twidth=readwidth))
			modq7rdrv.update(dict(twidth=readwidth))
			modq7read.update(dict(twidth=readwidth))
			modq6read.update(dict(twidth=readwidth))
			modq5read.update(dict(twidth=readwidth))
		if fread:
			modq7rdrv.update(dict(fcarrier=fread))
			modq6rdrv.update(dict(fcarrier=fread))
			modq7rdrv.update(dict(fcarrier=fread))
			modq7read.update(dict(fcarrier=fread))
			modq6read.update(dict(fcarrier=fread))
			modq5read.update(dict(fcarrier=fread))
		if preadout:
			modq7read.update(dict(pcarrier=preadout))

		run=0;
		for irun in range(elementlength):

			therald=run
			theraldread=run+delayread
			self.readall(trdrv=therald,delayread=delayread,qubitsamps={"Q7":(modq7rdrv,modq7read),"Q6":(modq6rdrv,modq6read),"Q5":(modq5rdrv,modq5read)})
                        trabi=run+delay1
			wrabi=elementstep*(irun)
			if wrabi!=0:
				modq7rabi.update(dict(twidth=wrabi))
				self.seqs.add(trabi,         	 self.qchip.gates['Q7rabi'].modify(modq7rabi))

			treaddrv=self.seqs.tend()
			treadout=treaddrv+delayread
			self.readall(trdrv=treaddrv,delayread=delayread,qubitsamps={"Q7":(modq7rdrv,modq7read),"Q6":(modq6rdrv,modq6read),"Q5":(modq5rdrv,modq5read)})
			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		self.bufwidth=[self.seqs.countdest('Q7.read'),self.seqs.countdest('Q6.read'),self.seqs.countdest('Q5.read')]
	def rabiacq(self,nget):
		data=self.acqdata(nget)
		return data
	def processrabi(self,dt,filename,dumpname,plot=False):
		c=self.loadjsondata(filename)
		return self.processrabidata(dt=dt,data=c['accout_0'],dumpname=dumpname,plot=plot)
	def processrabidata(self,dt,data,dumpname,plot=False):
#                self.rabi_result={'separation':0,'iqafterherald':numpy.zeros(3),'population_norm':numpy.zeros(3)};#self.process3(data,lengthperrow=self.bufwidth[0],training=True,dumpname=dumpname)
#                [amp,period,fiterr]=self.fitrabi(dx=dt,data=self.rabi_result['population_norm'],plot=plot)
                return [data]
	def plotrabicol(self,bufwidth):
		meas=self.rabi_result['meas'].reshape((-1,bufwidth))
		herald=self.rabi_result['herald'].reshape((-1,bufwidth))
                lim=1.1*max(max(abs(meas.reshape((-1,1)).real)),max(abs(meas.reshape((-1,1)).imag)))
		for i in range(bufwidth):
			iqafterheraldcol=numpy.extract(herald[:,i],meas[:,i])
			pyplot.clf()
                        ax=pyplot.subplot(111)
                        ax.set_aspect('equal')
                        ax.set_xlim([-lim,lim])
                        ax.set_ylim([-lim,lim])
                        pyplot.grid()
			pyplot.hexbin(iqafterheraldcol.real,iqafterheraldcol.imag,gridsize=30,cmap='Greys',bins='log')
			pyplot.savefig('animation_%02d.png'%i)


if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	print(cmdlinestr)
	clargs=parser.parse_args()
	rabi=c_rabi(**clargs.__dict__)
	parser.set_defaults(elementstep=4e-9)
        if clargs.sim:
            rabi.setsim()
	rabi.rabiseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readwidth=clargs.readwidth,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,elementlength=clargs.elementlength,elementstep=clargs.elementstep,fqubit=clargs.fqubit,preadout=clargs.preadout,fread=clargs.fread)
	if clargs.processfile=='':
		rabi.run(bypass=clargs.bypass,clear=False)
		data=rabi.rabiacq(clargs.nget)
		fprocess=rabi.savejsondata(filename=clargs.filename,extype='rabi',cmdlinestr=cmdlinestr,data=data)
		if clargs.sim:
			rabi.sim()
		print('save data to ',fprocess)
	else:
		fprocess=clargs.processfile
	[rawdata]=rabi.processrabi(dt=clargs.elementstep,filename=fprocess,dumpname=fprocess[:-4],plot=clargs.plot)
        numpy.set_printoptions(precision=None, threshold=numpy.inf, edgeitems=None, linewidth=200)
		#print rawdata
#	print 'period',period
#	print 'separation',separation
#	print 'amp',amp
	rabi.plotrawdata(d1=rawdata,figname='blobs'+fprocess,limit=True,hexbin=False)
#	rabi.plotafterheraldingtest(iqafterherald)
#	rabi.plotpopulation_norm(population_norm=population_norm,figname='population'+fprocess)
	if clargs.plot:
		pyplot.grid()
		pyplot.show()
	print(rabi.hf.adcminmax())
		#rabi.plotrabicol(bufwidth=80)   # uncomment this line and "return" line in experience.process3 when dealing with the animation
