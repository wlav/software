import matplotlib
matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
import datetime
import argparse
import sys
from squbic import *
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
from scipy import signal
import experiment
from vna import c_vna

if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(delaybetweenelement=2e-6)
	parser.set_defaults(elementlength=1024)
	clargs=parser.parse_args()
	vna=c_vna(**clargs.__dict__)
	with open(clargs.qubitcfg) as jfile:
		chipcfg=json.load(jfile)
	bandwidth=3e6
	cf=chipcfg['Qubits'][clargs.qubitid]['readfreq']
	flist=list(numpy.linspace(cf-bandwidth/2,cf+bandwidth/2,1024))
	print('qubitid',clargs.qubitid)
	#print('flist',flist)

	vna.vnaseqs(delayread=clargs.delayread,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,readoutdrvamp=clargs.readoutdrvamp,readwidth=clargs.readwidth,rdc=0,freqlist=flist,qubitidread=clargs.qubitidread)
	vna.run(bypass=clargs.bypass)
	data=vna.vnadata(clargs.nget)
	fprocess=vna.savejsondata(filename=clargs.filename,extype='vna',cmdlinestr=cmdlinestr,data=data)
	print('save data to ',fprocess)
	c=vna.processvna(fprocess)
	print('adc minmax',vna.hf.adcminmax())
	vna.vnaplot(c,figname=fprocess)
	if clargs.plot:
		pyplot.show()
