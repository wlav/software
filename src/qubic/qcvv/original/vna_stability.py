import datetime
import argparse
import sys
from squbic import *
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
from scipy import signal
import experiment
class c_vnast(experiment.c_experiment):
 	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		pass
	def vnastseqs(self,delayread=668e-9,delaybetweenelement=600e-6,elementlength=1024,readoutdrvamp=None,readwidth=None,fstart=-500e6,fstop=500e6,rdc=0):
		df=1.0*(fstop-fstart)/(elementlength-1)
		self.freq=numpy.arange(fstart,fstop+df,df)[:elementlength] # avoid rounding error in python, make sure that self.freq length is elementlength
		print(len(self.freq))
		modrdrv={}
		modread={}
		if readoutdrvamp:
			modrdrv.update(dict(amp=min(1-rdc,readoutdrvamp)))
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		trun=0
		for frun in self.freq:
			modrdrv.update(dict(fcarrier=frun))
			modread.update(dict(fcarrier=frun))
			modrdrv.update(dict(twidth=readwidth))
			modread.update(dict(twidth=readwidth))
			self.readall(trdrv=trun,qubitsamps={"Q7":(modrdrv,modread)})
			#trun=trun+delaybetweenelement
			trun=self.seqs.tend()+delaybetweenelement
		print('period',trun)
		self.seqs.setperiod(period=trun)
	def vnastdata(self,nget):
		data=self.acqdata(nget)
		return data
	def processvnast(self,filename):
		c=self.loadjsondata(filename)['accout_0']
		cavr=c.reshape((-1,len(self.freq))).mean(0)
		#add process here
		return cavr
	def vnastplot(self,c):
		pyplot.subplot(211)
		pyplot.semilogy(self.freq,abs(c))
		pyplot.tight_layout()
		pyplot.subplot(212)
		pyplot.plot(self.freq,signal.detrend(numpy.unwrap(numpy.angle(c))))
		pyplot.tight_layout()
if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(delaybetweenelement=2e-6)
	vnast_elementlength=1024
	parser.set_defaults(elementlength=vnast_elementlength)
	clargs=parser.parse_args()
	vnast=c_vnast(**clargs.__dict__)
	vnast.vnastseqs(delayread=clargs.delayread,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,readoutdrvamp=clargs.readoutdrvamp,readwidth=clargs.readwidth,fstart=clargs.fstart,fstop=clargs.fstop,rdc=0)
	vnast.run(bypass=clargs.bypass)
	vnast_timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	vnast_filename='%s.dat'%('_'.join(['vna_stability',vnast_timestamp]))
	for i in range(clargs.nmeas):
		print('measurement: %5d of %5d'%(i+1,clargs.nmeas))
		if clargs.processfile=='':
			if clargs.sim:
				vnast.sim()
			data=vnast.vnastdata(clargs.nget)
			fprocess=vnast.savejsondata(filename=clargs.filename,extype='vnast',cmdlinestr=cmdlinestr,data=data)
			print('save data to ',fprocess)
		else:
			fprocess=clargs.processfile
		#[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=vnast.processvnast(dt=clargs.elementstep,filename=fprocess,swapdata=clargs.swapdata,swappre=clargs.swappre)
		c=vnast.processvnast(fprocess)
	#	vnast.plotrawdata(rawdata)
	#	vnast.plotafterheraldingtest(iqafterherald)
	#	vnast.plotpopulation_norm(population_norm)
		if clargs.plot:
			vnast.vnastplot(c)
			pyplot.grid()
			pyplot.show()
		c=c.reshape((1,-1))
		with open(vnast_filename,'a') as f:
			numpy.savetxt(f,c)
	S21=numpy.loadtxt(vnast_filename,dtype=complex,converters=dict((k,lambda s: complex(s.decode().replace('+-', '-'))) for k in range(vnast_elementlength)))
	S21=S21[2:,:]
	amp=abs(S21)
	amp_mean=numpy.mean(amp,axis=0)
	amp_std=numpy.std(amp,axis=0)
	print(vnast.hf.adcminmax())
	pyplot.figure(1)
	pyplot.errorbar(vnast.freq,amp_mean,yerr=amp_std)
	pyplot.xlabel('fr (Hz)')
	pyplot.ylabel('amp (a.u.)')
	pyplot.figure(2)
	pyplot.plot(vnast.freq,amp_std)
	pyplot.xlabel('fr (Hz)')
	pyplot.ylabel('amp std (a.u.)')
	pyplot.figure(3)
	pyplot.plot(vnast.freq,1.0*amp_std/amp_mean)
	print('avg of std/mean',numpy.mean(1.0*amp_std/amp_mean))
	print('avg of std/mean [12:-12]',numpy.mean((1.0*amp_std/amp_mean)[12:-12]))
	pyplot.xlabel('fr (Hz)')
	pyplot.ylabel('amp std/mean')
	pyplot.figure(4)
	pyplot.plot(vnast.freq,amp.T,'.')
	pyplot.show()
