import matplotlib
matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import experiment
import glob
import os

class c_rabixtalk(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid_t=None
		self.qubitid_c=None
		self.qubitid_x=None
	def rabixtalkseqs(self,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,elementlength=80,elementstep=4e-9,restart=True,readoutdrvamp=None,qubitdrvamp=None,readwidth=None,fqubit=None,fread=None,rdc=0,preadout=None,qubitid_t='Q5',qubitid_c='Q6',qubitidread=['Q7','Q6','Q5'],amp_multiplier=0,phase_offset=0,qubitid_x='Q7'):
		self.qubitid_t=qubitid_t
		self.qubitid_c=qubitid_c
		self.qubitid_x=qubitid_x
		if restart:
			self.initseqs()
		readoutdrvamp0,readoutdrvamp1,readoutdrvamp2=self.rdrvcalc(readoutdrvamp,dcoffset=rdc)
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		modrdrv={}
		modread={}
		modrabi={}
		modrabi_simul={}
		if readoutdrvamp is not None:
			modrdrv.update(dict(amp=readoutdrvamp0))
		if qubitdrvamp is not None:
			modrabi.update(dict(amp=qubitdrvamp))
			modrabi_simul.update(dict(amp=qubitdrvamp))
		if fqubit is not None:
			modrabi.update(dict(fcarrier=fqubit))
			modrabi_simul.update(dict(fcarrier=fqubit))
		if readwidth is not None:
			modrdrv.update(dict(twidth=readwidth))
			modread.update(dict(twidth=readwidth))
		if fread is not None:
			modrdrv.update(dict(fcarrier=fread))
			modread.update(dict(fcarrier=fread))
		if preadout is not None:
			modread.update(dict(pcarrier=preadout))

		run=0
		for irun in range(elementlength):

			therald=run
			self.seqs.add(therald,self.qchip.gates[qubitid_t+'read'].modify([modrdrv,modread]))
			trabi=run+delay1
			wrabi=elementstep*irun
			if wrabi!=0:
				modrabi.update(dict(twidth=wrabi,dest=qubitid_c+'.qdrv'))
				self.seqs.add(trabi,         	 self.qchip.gates[qubitid_t+'rabi'].modify(modrabi))

				pcarrier_simul_tmp=self.qchip.gates[qubitid_t+'rabi'].pcalc(padd=phase_offset)[0]
				amp_simul_tmp=self.qchip.gates[qubitid_t+'rabi'].paralist[0]['amp']*amp_multiplier
				modrabi_simul.update(dict(twidth=wrabi,dest=qubitid_x+'.qdrv',amp=amp_simul_tmp,pcarrier=pcarrier_simul_tmp))
				self.seqs.add(trabi,         	 self.qchip.gates[qubitid_t+'rabi'].modify(modrabi_simul))

			treaddrv=self.seqs.tend()
			self.seqs.add(treaddrv,self.qchip.gates[qubitid_t+'read'].modify([modrdrv,modread]))
			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
		print('fread,fqubit,readoutdrvamp,qubitdrvamp',fread,fqubit,readoutdrvamp,qubitdrvamp)
	def rabixtalkacq(self,nget):
		data=self.acqdata(nget)
		return data
	def processrabixtalk(self,dt,filename,dumpdataset,loaddataset,plot=False):
		c=self.loadjsondata(filename)
		return self.processrabixtalkdata(dt=dt,data=c[list(c.keys())[0]],dumpdataset=dumpdataset,loaddataset=loaddataset,plot=plot,filename=filename)
	def processrabixtalkdata(self,dt,data,dumpdataset,loaddataset,plot=False,filename=''):
		self.rabixtalk_result=self.process3(data,qubitid=self.qubitid_t,lengthperrow=self.bufwidth_dict[self.qubitid_t],training=loaddataset=='',dumpdataset=dumpdataset,loaddataset=loaddataset)
		#[amp,period,fiterr]=self.fitrabi(dx=dt,data=self.rabixtalk_result['population_norm'],plot=plot,figname=filename)
		[amp,period,tdecay,fiterr]=self.fitrabidecay(dt=dt,data=self.rabixtalk_result['population_norm'],plot=plot,figname=filename)
		return [data,self.rabixtalk_result['separation'],self.rabixtalk_result['iqafterherald'],self.rabixtalk_result['population_norm'],amp,period,fiterr]

if __name__=="__main__":
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	rabi_rate=[]
	#amp_multiplier_map=numpy.arange(0.0,2.0,0.125)
	amp_multiplier_map=numpy.arange(1.5,2.0,0.05)#C6T5X7
	#phase_offset_map=numpy.arange(0.0,numpy.pi*2,numpy.pi/8)
	phase_offset_map=numpy.arange(2.5,3.0,0.05)#C6T5X7

	for amp_multiplier in amp_multiplier_map:
		rabi_rate_tmp=[]
		for phase_offset in phase_offset_map:
			parser,cmdlinestr=experiment.cmdoptions()
			print(cmdlinestr)
			parser.set_defaults(elementstep=4e-9)
			clargs=parser.parse_args()
			rabixtalk=c_rabixtalk(**clargs.__dict__)
			if clargs.sim:
				rabixtalk.setsim()
			rabixtalk.rabixtalkseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readwidth=clargs.readwidth,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,elementlength=clargs.elementlength,elementstep=clargs.elementstep,fqubit=clargs.fqubit,preadout=clargs.preadout,fread=clargs.fread,qubitid_t=clargs.qubitid_t,qubitid_c=clargs.qubitid_c,qubitidread=clargs.qubitidread,amp_multiplier=amp_multiplier,phase_offset=phase_offset,qubitid_x=clargs.qubitid_x)
			if clargs.processfile=='':
				rabixtalk.run(bypass=clargs.bypass)
				data=rabixtalk.rabixtalkacq(clargs.nget)
				fprocess=rabixtalk.savejsondata(filename=clargs.filename+'_scanamp'+str(amp_multiplier)+'_scanphase'+str(phase_offset),extype='rabixtalk',cmdlinestr=cmdlinestr,data=data)
				print('save data to ',fprocess)
				if clargs.sim:
					rabixtalk.sim()
			else:
				fprocess=clargs.processfile
			[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=rabixtalk.processrabixtalk(dt=clargs.elementstep,filename=fprocess,dumpdataset=fprocess[:-4],loaddataset=clargs.dataset,plot=clargs.plot)
			rabi_rate_tmp.append(1/period)
			print('period',period)
			print('separation',separation)
			print('amp',amp)
			#rabixtalk.plotrawdata(d1=rawdata,figname='blobs'+fprocess)
			rabixtalk.plotpopulation_norm(population_norm=population_norm,figname='population'+fprocess)
			print(rabixtalk.hf.adcminmax())
			if clargs.plot:
				pyplot.grid()
				#pyplot.show()
		rabi_rate.append(rabi_rate_tmp)
		with open(clargs.filename+'_rabixtalk'+'_qubitid_c'+clargs.qubitid_c+'_qubitid_t'+clargs.qubitid_t+'_'+timestamp+'.dat','a') as f:
			numpy.savetxt(f,numpy.array(rabi_rate_tmp).reshape((1,-1)))
	pyplot.figure()
	pyplot.pcolormesh(rabixtalk.repack1D_cent2edge(phase_offset_map),rabixtalk.repack1D_cent2edge(amp_multiplier_map),rabi_rate,cmap='RdBu_r')
	pyplot.legend()
	pyplot.xlabel('Phase Offset (rad)')
	pyplot.ylabel('Amplitude Multiplier')
	pyplot.colorbar(label='Rabi Rate (Hz)')
	pyplot.savefig(clargs.filename+'_rabixtalk'+'_qubitid_c'+clargs.qubitid_c+'_qubitid_t'+clargs.qubitid_t+'_'+timestamp+'.pdf')
	#python rabi_xtalk_addldrv.py --plot -n 20 --qubitid_c Q6 --qubitid_t Q5 --qubitid_x Q7 -es 8e-9 -el 40
