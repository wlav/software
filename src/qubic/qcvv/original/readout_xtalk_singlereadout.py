import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import copy
import experiment

class c_readoutxtalk(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid_c=None
		self.qubitid_t=None
		pass
	def readoutxtalkseqs(self,delayread=712e-9,delay1=12e-6,delaybetweenelement=600e-6,qubitid_c='Q5',qubitid_t='Q4',ctrl='I',tgt='I',qubitidread=['Q5','Q4','Q3'],readoutdrvamp=None,fread=None):
		self.qubitid_c=qubitid_c
		self.qubitid_t=qubitid_t
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		modrdrv={}
		modread={}
		modtgt={}
		run=0
		if readoutdrvamp is not None:
			modrdrv.update(dict(amp=readoutdrvamp))
		if fread is not None:
			modrdrv.update(dict(fcarrier=fread))
			modread.update(dict(fcarrier=fread))
		for ctrl in ['I','X180']:
			for tgt in ['I','X180']:
				# Readout
				therald=run
				self.seqs.add(therald,self.qchip.gates[qubitid_t+'read'].modify([modrdrv,modread]))
	
				# Control state (control qubit: I or X180)
				tini=run+delay1
				if ctrl=='X180':
					self.seqs.add(tini,         	 self.qchip.gates[qubitid_c+'X180'])
					#self.seqs.add(tini,              self.qchip.gates[qubitid_c+'X180'].modify({"dest":qubitid_t+".qdrv","amp":0.6,"pcarrier":4.08}))
					ttgt=self.seqs.tend()
				else:
					ttgt=tini
				ti180=ttgt-tini
	
				# Target state (target qubit: I or X180)
				if tgt=='X180':
					modtgt.update(dict(pcarrier=self.qchip.gates[qubitid_t+'X180'].pcalc(dt=ti180)[0]))
					self.seqs.add(ttgt,              self.qchip.gates[qubitid_t+'X180'].modify(modtgt))
				else:
					pass
	
				# Readout
				treaddrv=self.seqs.tend()
				self.seqs.add(treaddrv,self.qchip.gates[qubitid_t+'read'].modify([modrdrv,modread]))
	
				run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
		accout_list=['accout_0','accout_1','accout_2']
		self.accout_dict=dict(zip(qubitidread,accout_list))
	def processreadoutxtalk(self,filename,loaddataset):
		c=self.loadjsondata(filename)
		print 'self.bufwidth_dict',self.bufwidth_dict,'   self.accout_dict',self.accout_dict
		print 'accout in use: ',self.accout_dict[self.qubitid_t]
		data=c[self.accout_dict[self.qubitid_t]]
		tgt_result=self.process3(data,qubitid=self.qubitid_t,lengthperrow=self.bufwidth_dict[self.qubitid_t],training=False,loaddataset=loaddataset)
		return [data,tgt_result['separation'],tgt_result['iqafterherald'],tgt_result['population_norm']]


if __name__=="__main__":
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	parser,cmdlinestr=experiment.cmdoptions()
	clargs=parser.parse_args()
	readoutxtalk=c_readoutxtalk(**clargs.__dict__)
	if clargs.sim:
		readoutxtalk.setsim()
	readoutxtalk.readoutxtalkseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,qubitid_c=clargs.qubitid_c,qubitid_t=clargs.qubitid_t,qubitidread=clargs.qubitidread,readoutdrvamp=clargs.readoutdrvamp,fread=clargs.fread)
	readoutxtalk.run()
	data=readoutxtalk.acqdata(clargs.nget)
	fprocess=readoutxtalk.savejsondata(filename=clargs.filename,extype='readoutxtalk',cmdlinestr=cmdlinestr,data=data)
	print 'save data to ',fprocess
	if clargs.sim:
		readoutxtalk.sim()
	[rawdata_t,separation_t,iqafterherald_t,population_norm_t]=readoutxtalk.processreadoutxtalk(filename=fprocess,loaddataset=clargs.dataset)
	readoutxtalk.plotpopulation_norm(population_norm=population_norm_t,figname='target'+fprocess)
	if clargs.plot:
		#pyplot.grid()
		pyplot.show()
