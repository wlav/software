import datetime
import argparse
import sys
from matplotlib import pyplot,patches
import numpy
import time
import init
import experiment

class c_rcali(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid=None
		pass
	def rcaliseqs(self,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,readoutdrvamp=None,qubitdrvamp=None,readwidth=None,fqubit=None,fread=None,rdc=0,qubitid='Q7',qubitidread=['Q7','Q6','Q5']):
		self.qubitid=qubitid
		readoutdrvamp0,readoutdrvamp1,readoutdrvamp2=self.rdrvcalc(readoutdrvamp,dcoffset=rdc)
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		modrdrv={}
		modread={}
		modqdrv={}
		if readoutdrvamp is not None:
			modrdrv.update(dict(amp=readoutdrvamp0))
		if fread is not None:
			modrdrv.update(dict(fcarrier=fread))
			modread.update(dict(fcarrier=fread))
		if readwidth is not None:
			modrdrv.update(dict(twidth=readwidth))
			modread.update(dict(twidth=readwidth))
		if qubitdrvamp is not None:
			modqdrv.update(dict(amp=qubitdrvamp))
		if fqubit is not None:
			modqdrv.update(dict(fcarrier=fqubit))
		run=0
		for gate in ['I','X180']:
			therald=run
			self.seqs.add(therald,self.qchip.gates[qubitid+'read'].modify([modrdrv,modread]))
			tgate=run+delay1
			if gate=='X180':
				self.seqs.add(tgate,self.qchip.gates[qubitid+'X180'].modify(modqdrv))
				treaddrv=self.seqs.tend()
			else:
				treaddrv=tgate
			self.seqs.add(treaddrv,self.qchip.gates[qubitid+'read'].modify([modrdrv,modread]))
			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
	def acqrcali(self,nget):
		data=self.acqdata(nget)
		return data
	def processrcali(self,filename,loaddataset):
		c=self.loadjsondata(filename)
		print('c.keys()',c.keys())
		data=c[list(c.keys())[0]]
		rcali_result=self.process3(data,qubitid=self.qubitid,lengthperrow=self.bufwidth_dict[self.qubitid],training=False,loaddataset=loaddataset)
		return [data,rcali_result['separation'],rcali_result['iqafterherald'],rcali_result['population_norm']]
if __name__=="__main__":
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	parser,cmdlinestr=experiment.cmdoptions()
	clargs=parser.parse_args()
	rcali=c_rcali(**clargs.__dict__)
	if clargs.sim:
		rcali.setsim()
	rcali.rcaliseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,readwidth=clargs.readwidth,fqubit=clargs.fqubit,fread=clargs.fread,qubitid=clargs.qubitid,qubitidread=clargs.qubitidread)
	if clargs.processfile=='':
		rcali.run(bypass=clargs.bypass)
		data=rcali.acqrcali(clargs.nget)
		fprocess=rcali.savejsondata(filename=clargs.filename,extype='rcali',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
		if clargs.sim:
			rcali.sim()
	else:
		fprocess=clargs.processfile
	[rawdata,separation,iqafterherald,population_norm]=rcali.processrcali(filename=fprocess,loaddataset=clargs.dataset)
	print('population_norm',population_norm)
	print('corr_matrix %s'%clargs.qubitid,numpy.vstack((1-population_norm,population_norm)))
	print(rcali.hf.adcminmax())
	corr_matrix=dict(numpy.load('corrmatrix.npz'))
	corr_matrix[clargs.qubitid]=numpy.vstack((1-population_norm,population_norm))
	numpy.savez('corrmatrix.npz',**corr_matrix)
	numpy.savez('corrmatrix_'+timestamp+'.npz',**corr_matrix)
	rcali.plotrawdata(d1=rawdata,figname='blobs'+fprocess)
	#rcali.plotafterheraldingtest(iqafterherald)
	rcali.plotpopulation_norm(population_norm=population_norm,figname='population'+fprocess)
	if clargs.plot:
		pyplot.grid()
		pyplot.show()
	# python rcali.py --plot -n 100 --qubitid Q6 --qubitidread Q6
