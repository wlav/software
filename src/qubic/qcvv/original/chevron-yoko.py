import matplotlib
matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import experiment
from rabi_w import c_rabi
from yokogawa import yokogawa

if __name__=="__main__":

        yoko=yokogawa()
        print(yoko.idn())
        print(yoko.readstatus())
        yoko.setoutput('off')
        yoko.setfunc('CURR')
        print(yoko.readstatus())
#	import random
#	l=random.random()
        yoko.setoutput('on')
        yoko.setrange(1.0e-3)
#	for l in numpy.arange(-1e-3,1e-3,0.1e-3):
        #for l in [1e-3]:
        print(len(sys.argv))
        print(yoko.readstatus())

        timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
        parser,cmdlinestr=experiment.cmdoptions()

        parser.add_argument('--fqubit_center',help='qubit drive frequency center',dest='fqubit_center',type=float,default=00e6)
        parser.add_argument('--yoko_center',help='yoko amp level center',dest='yoko_center',type=float,default=0e6)
        parser.add_argument('--yoko_span',help='yoko amp level span',dest='yoko_span',type=float,default=2000e-6)
        parser.add_argument('--yoko_step',help='yoko amp level step',dest='yoko_step',type=float,default=10e-6)
#	parser.add_argument('--fqubit_span',help='qubit drive frequency span',dest='fqubit_span',type=float,default=1000e6)
#	parser.add_argument('--fqubit_step',help='qubit drive frequency step',dest='fqubit_step',type=float,default=10e6)
        clargs=parser.parse_args()
        rabi=c_rabi(**clargs.__dict__)
        fqubit_center=clargs.fqubit_center#100e6#-71e6
#	fqubit_span=clargs.fqubit_span#20e6#10e6
#	fqubit_step=clargs.fqubit_step#1e6#2e6

        yoko_center=clargs.yoko_center
        yoko_span=clargs.yoko_span
        yoko_step=clargs.yoko_step
        yoko_sweep=numpy.arange(yoko_center-yoko_span/2.0,yoko_center+yoko_span/2.0+yoko_step,yoko_step)
        chev_data=[]

        rabi.rabiseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readwidth=clargs.readwidth,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,elementlength=clargs.elementlength,elementstep=clargs.elementstep,preadout=clargs.preadout,fqubit=None,fread=clargs.fread,qubitid=clargs.qubitid,qubitidread=clargs.qubitidread)
        rabi.run()
        for yoko_l in yoko_sweep:
            print('update freq',yoko)
            #rabi.hf.cmd127all=[]
            #rabi.hf.cmdgen1(destfqdrv={clargs.qubitid+'.qdrv':fqubit_center})
            #rabi.hf.run(seqs=rabi.seqs,memclear=False,cmdclear=False)
            yoko.setlevel(yoko_l)
            data=rabi.rabiacq(clargs.nget)
            cmdlinestr='fqubit'+str()
            fprocess=rabi.savejsondata(filename=clargs.filename,extype='rabi_chevron',cmdlinestr=cmdlinestr,data=data)
            print('save data to ',fprocess)
            c=rabi.loadjsondata(fprocess)
            #rabi.rabi_result=rabi.process3(c[list(c.keys())[0]],lengthperrow=max(rabi.bufwidth),training=True,dumpname=fprocess[:-4])
            rabi.rabi_result=rabi.process3(c[list(c.keys())[0]],qubitid=rabi.qubitid,lengthperrow=rabi.bufwidth_dict[rabi.qubitid],training=clargs.dataset=='',dumpdataset=fprocess[:-4],loaddataset=clargs.dataset)
            population_norm=rabi.rabi_result['population_norm']
            chev_data.append(population_norm)
            with open('processed_chevron_'+timestamp+'.dat','a') as f:
                 numpy.savetxt(f,population_norm.reshape((1,-1)))

        x_axis=clargs.elementstep*1e9*numpy.arange(len(chev_data[0]))  # clargs.elementstep=4e-9
        pyplot.pcolormesh(x_axis,yoko_sweep/1e-6,chev_data,cmap='RdBu_r')
        pyplot.clim(0,1)
        #pyplot.axhline(fqubit_center/1e6,c='black',ls='dashed',label='Current Frequency')
        #pyplot.legend()
        pyplot.xlabel('t (ns)')
        pyplot.ylabel('Yoko amp (microAmp)')
        pyplot.colorbar(label='P(|1>)')
        #pyplot.title('Qubit %s'%q)
        pyplot.tight_layout()
        #pyplot.savefig('chevron'+'_fcenter'+str(fqubit_center)+'_fspan'+str(fqubit_span)+'_fstep'+str(fqubit_step)+'_'+timestamp+'.png')
        pyplot.savefig('chevron_yoko_'+clargs.qubitid+'_readoutdrvamp'+str(clargs.readoutdrvamp)+'_fread'+str(clargs.fread)+'_qubitdrvamp'+str(clargs.qubitdrvamp)+'_yokocenter'+str(yoko_center)+'_yokospan'+str(yoko_span)+'_yokostep'+str(yoko_step)+'_'+timestamp+'.pdf')
        #pyplot.show()

        #python chevron.py --plot -n 50 -r 0.69 -fr " 103.3e6" -q 0.7 --qubitid Q6
