import cProfile
#import matplotlib
#matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
from matplotlib import pyplot,patches
import datetime
import argparse
import sys
import numpy
import time
import init
import pprint
import experiment
import copy
from itertools import product
import json

class c_rb1q(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid=None
		pass
	def rb1qseqs(self,circuits_file,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,restart=True,qubitidread=['Q7','Q6','Q5']):
		header=open(circuits_file,'r').read().splitlines()
		self.qubitid=header[0]
		circuit_length=[int(x) for x in header[1].split(' ')]
		n_circuits=int(header[2])
		print('qubitid:',self.qubitid,'   circuit_length:',circuit_length,'   n_circuits:',n_circuits)
		circuits=header[3:]

		if restart:
			self.initseqs()
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		run=0

		counter_circuits=0
		for index in range(len(circuits)):
			gate=circuits[index]
			lastgate=circuits[index-1] if index>=1 else ''
			if counter_circuits==0 or 'meas' in lastgate:
				therald=run
				theraldread=run+delayread
				self.seqs.add(therald,self.qchip.gates[self.qubitid+'readoutdrv'])
				self.seqs.add(theraldread,self.qchip.gates[self.qubitid+'readout'])
				run=self.seqs.tend()+delay1
				tini=run
				padd=0
			if 'meas' in gate:
				treaddrv=self.seqs.tend()
				treadout=treaddrv+delayread
				self.seqs.add(treaddrv,self.qchip.gates[self.qubitid+'readoutdrv'])
				self.seqs.add(treaddrv+delayread,self.qchip.gates[self.qubitid+'readout'])
				run=self.seqs.tend()+delaybetweenelement
			else:
				gatemodi={}
				if 'pass' in gate:
					pass
				elif 'Z' in gate:
					padd=padd-float(gate[3:])/180.*numpy.pi
				else:
					pnew=self.qchip.gates[gate].pcalc(dt=run-tini,padd=padd)[0]
					gatemodi.update(dict(pcarrier=float(pnew)))
					self.seqs.add(run,self.qchip.gates[gate].modify(gatemodi))
				run=self.seqs.tend()
			counter_circuits=counter_circuits+1
			if(counter_circuits%1000==0):
				print('counter',counter_circuits)
#		idx_list=[idx+1 for idx,val in enumerate(circuits) if val==self.qubitid+'meas']
#		rbgates=[circuits[i: j] for i, j in zip([0]+idx_list, idx_list+([len(circuits)] if idx_list[-1]!=len(circuits) else []))]
#		for gates in rbgates:
#			therald=run
#			theraldread=run+delayread
#			self.seqs.add(therald,self.qchip.gates[self.qubitid+'readoutdrv'])
#			self.seqs.add(theraldread,self.qchip.gates[self.qubitid+'readout'])
#			run=self.seqs.tend()+delay1
#			tini=run
#			padd=0
#			for gate in gates:
#				if 'meas' in gate:
#					treaddrv=self.seqs.tend()
#					treadout=treaddrv+delayread
#					self.seqs.add(treaddrv,self.qchip.gates[self.qubitid+'readoutdrv'])
#					self.seqs.add(treaddrv+delayread,self.qchip.gates[self.qubitid+'readout'])
#					run=self.seqs.tend()+delaybetweenelement
#				else:
#					gatemodi={}
#					if 'pass' in gate:
#						pass
#					else:
#						if 'Z' in gate:
#							padd=padd-float(gate[3:])/180.*numpy.pi
#						else:
#							pnew=self.qchip.gates[gate].pcalc(dt=run-tini,padd=padd)[0]
#							gatemodi.update(dict(pcarrier=pnew))
#							self.seqs.add(run,self.qchip.gates[gate].modify(gatemodi))
#					run=self.seqs.tend()
		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
	def processrb1q(self,filename,loaddataset):
		c=self.loadjsondata(filename)
		print('c.keys()',c.keys())
		data=c[list(c.keys())[0]]
		rb1q_result=self.process_trueq(data,qubitid=self.qubitid,lengthperrow=self.bufwidth_dict[self.qubitid],loaddataset=loaddataset)
		return [data,rb1q_result['separation'],rb1q_result['population_norm'],rb1q_result['0'],rb1q_result['1']]


if __name__=="__main__":
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	parser,cmdlinestr=experiment.cmdoptions()
	parser.add_argument('--qubic_file',help='qubic_file name from trueq ',dest='qubic_file',type=str)
	clargs=parser.parse_args()
	rb1q=c_rb1q(**clargs.__dict__)
	circuits_file=clargs.qubic_file
	print('circuits_file %s'%circuits_file)
	if clargs.sim:
		rb1q.setsim()
	#pr=cProfile.Profile()
	#pr.enable()
	rb1q.rb1qseqs(circuits_file=circuits_file,delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,qubitidread=clargs.qubitidread)
	#pr.disable()
	#pr.print_stats()
	rb1q.run()
	data=rb1q.acqdata(clargs.nget)
	fprocess=rb1q.savejsondata(filename=clargs.filename,extype='rb1q',cmdlinestr=cmdlinestr,data=data)
	print('save data to ',fprocess)
	if clargs.sim:
		rb1q.sim()
	[rawdata,separation,population_norm,count_0,count_1]=rb1q.processrb1q(filename=fprocess,loaddataset=clargs.dataset)
	trueq_result=[{'0':int(count_0[i]),'1':int(count_1[i])} for i in range(len(count_0))]
	print('dataset',clargs.dataset)
	print('separation',separation)
	print(rb1q.hf.adcminmax())
	print('count_0',count_0)
	print('count_1',count_1)
	print('population_norm',population_norm)
	print('trueq_result',trueq_result)
	resultfilename=circuits_file[:-4]+'_'+timestamp+'.dat'
	with open(resultfilename,'w') as f_trueqresult:
		json.dump(trueq_result,f_trueqresult)
	print('resultfile %s'%resultfilename)

#	rbgates_full=[]
#	for i_circ in range(n_circuits):
#		rbgates_full.append(rbgates_list[i_circ*len(circuit_length):(i_circ+1)*len(circuit_length)])
#	for i_circ in range(n_circuits):
#		print 'measurement: %3d of %3d'%(i_circ+1,n_circuits)
#		rb1q=c_rb1q()
#		parser,cmdlinestr=experiment.cmdoptions()
#		clargs=parser.parse_args()
#		if clargs.sim:
#			rb1q.setsim()
#		rb1q.rb1qseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,qubitid=qubitid,qubitidread=clargs.qubitidread,rbgates=rbgates_full[i_circ])
#		rb1q.run()
#		data=rb1q.acqdata(clargs.nget)
#		fprocess=rb1q.savejsondata(filename=clargs.filename,extype='rb1q',cmdlinestr=cmdlinestr,data=data)
#		print 'save data to ',fprocess
#		if clargs.sim:
#			rb1q.sim()
#		[rawdata,separation,iqafterherald,population_norm]=rb1q.processrb1q(filename=fprocess,loaddataset=clargs.dataset)
