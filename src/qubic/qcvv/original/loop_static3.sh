#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable

if [[ ! -v QUBIC_CHIP_NAME ]]; then
    echo QUBIC_CHIP_NAME is undefined, source your chip first, aborting.
    exit 66
else
    echo monitor chip named: ${QUBIC_CHIP_NAME}
fi

# Disable canvas from popping up for:  rabi_w.py 
export QUBICnoXTERM=1

# set defaults
dataPath=/tmp/cohMonSnowball

numIter=1000

while [ "$#" -gt 0 ]; do
  case "$1" in
    -n) numIter="$2"; shift 2;;
    -d) dataPath="$2"; shift 2;;

    --numIter=*) numIter="${1#*=}"; shift 1;;
    --dataPath=*) dataPath="${1#*=}"; shift 1;;
    --numIter|--dataPath) echo "$1 requires an argument" >&2; exit 1;;

    -*) echo "unknown option: $1" >&2; exit 1;;
    *) handle_argument "$1"; shift 1;;
  esac
done
passFile=$dataPath/run.pass

echo numIter=$numIter
echo dataPath=$dataPath
echo passFile=$passFile

if [ ! \( -d "$dataPath" \) ]; then
    echo S:create $dataPath
    mkdir -p $dataPath
fi

echo start `date`  > $passFile

datetime=`date '+%Y%m%d_%H%M%S'`
logFile=$dataPath/log_static3_$datetime
echo $logFile  

declare -A switch_map
switch_map['Q0']=6
switch_map['Q1']=0
switch_map['Q2']=2
switch_map['Q3']=3
switch_map['Q4']=5
switch_map['Q5']=7
switch_map['Q6']=1
switch_map['Q7']=4
declare -A framsey_map
framsey_map['Q0']=40e3
framsey_map['Q1']=40e3
framsey_map['Q2']=40e3
framsey_map['Q3']=40e3
framsey_map['Q4']=40e3
framsey_map['Q5']=40e3
framsey_map['Q6']=40e3
framsey_map['Q7']=40e3


function statistics_meas {
    echo time now : `date '+%Y%m%d_%H%M%S'`
    echo "================================"  >> $logFile    
    echo time now : `date '+%Y%m%d_%H%M%S'`  >> $logFile    
    qubitid=$1
    port=$2
    framsey=$3
    echo qubit id : $qubitid;
    echo switch port : $port;
    echo framsey : $framsey;
    # Jan setup:
    ssh -i /home/balewski/.ssh/qubic.pem campbell109.dyn.berkeley.edu -p 8843 " hostname; date; cd test1; python switch.py $port " >> $logFile;
    # Yilun setup:
    #python switch.py $port >> $logFile;
    sleep 10;
    echo "++++++++  rabi_w.py  --qubitid $qubitid  start">> $logFile
    python rabi_w.py -n 50 --qubitid $qubitid --qubitidread $qubitid --removefile >> $logFile;
    #python vnastatic.py -n 20 --qubitid $qubitid >> $logFile;
    echo "++++++++  static3.py  --qubitid $qubitid  start">> $logFile
    python static3.py -n 50 --qubitid $qubitid --qubitidread $qubitid --framsey " $framsey" --filestarttime $datetime --removefile >> $logFile;
    #python simp.py -n 1 --plot --qubitid_list $qubitid --filestarttime $datetime >> $logFile;
    echo "done iter: $i qubit: $qubitid  date:`date '+%Y%m%d_%H%M%S'` " > $logFile
}


function confirm_runpass {
    # check if run.pass-file exist to continue
    if [ ! \( -e "${passFile}" \) ] ;then
	echo "S: early finished ,  file=${passFile}= does not exist!"
	echo time now : `date '+%Y%m%d_%H%M%S'`  >> $logFile
	echo "S: early finish scan at i=$i " >> $logFile
	exit 1
    fi
    echo "started iter $i of $numIter for $qid  date: `date '+%Y%m%d_%H%M%S'` " >> $passFile
}

#=================================
#  M A I N 
#=================================

for i in `seq $numIter`; do
	echo "*****************"
	echo loop index : $i of $numIter
	#for qid in Q0 Q1 Q2 Q3 Q4 Q5 Q6 Q7; do
	for qid in  Q5 Q6 ; do  # tmp, testing
	    confirm_runpass
	 
	    statistics_meas $qid ${switch_map["$qid"]} ${framsey_map["$qid"]}
	    # hack - move all produced files to dataPath
	    mv statics_ramsey*dat statics_spinecho*dat statics_t1meas*dat  *.pdf *.joblib  $dataPath
	done

	# regenerate summary page
	time ./statics_timeplot.py --startDate $datetime --outPath /tmp/cohMonWeb/ --noXterm
	echo "###########  completed iteration $i of  $numIter date:"`date`
done
