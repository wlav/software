import sys
sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot
import matplotlib
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import argparse
def signvalue(vin,width=16):
	return vin-2**width if vin>>(width-1) else vin

if __name__=="__main__":
	interface=c_ether('192.168.1.122',port=3000)
	mg=c_mem_gateway(interface, min3=True,memgatewaybug=False)
	alist=[]
	dlist=[]
	cmds=[]
        parser = argparse.ArgumentParser()
        parser.add_argument('-r','--readoutdrvamp',help='readout drv amp',dest='readoutdrvamp',type=float,default=0.17)
        parser.add_argument('-q','--qubitdrvamp',help='qubit drv amp',dest='qubitdrvamp',type=float,default=0.3)
        parser.add_argument('-n','--filename',help='filename ',dest='filename',type=str,default='')
        clargs=parser.parse_args()

	opsel=0 # 0 for navg; 1 for min; 2 for max;
	alist.append(0x5000f)
	dlist.append(opsel)
	mon_navr=0
	alist.append(0x50006)
	dlist.append(mon_navr)
	mon_slice=0
	alist.append(0x50008)
	dlist.append(mon_slice)
	mon_sel0=0
	alist.append(0x50009)
	dlist.append(mon_sel0)
	mon_sel1=2#4#13
	alist.append(0x5000a)
	dlist.append(mon_sel1)

	test=0
	alist.append(0x5000c)
	dlist.append(test)
	alist.append(0x5000b)
	dlist.append(1)
	alist.append(0x50002)
	dlist.append(1)
	result=mg.readwrite(alist=alist,dlist=dlist,write=1)
	numpy.set_printoptions(precision=None, threshold=None, edgeitems=None, linewidth=200)
	comp_list=[]
	addr_start=[0x42000,0x4a000,0x4b000]
	alist=[]
	dlist=[]
	reseta=[0x50002]
	resetd=[1]
	for k in range(3):
		alist.extend(range(addr_start[k],addr_start[k]+0x1000))
	dlist=numpy.zeros(len(alist))
	for index in range(1000):
		result=mg.readwrite(alist=alist,dlist=dlist,write=0)
		mg.readwrite(alist=reseta,dlist=resetd,write=1)
		time.sleep(0.1)
		newdata=numpy.array([signvalue(i&0xffffffff,32) for i in mg.parse_readvalue(result)])
		xy8=newdata if index==0 else numpy.vstack((xy8,newdata))
	buf0=xy8[:,0:4096]
	buf1=xy8[:,4096:8192]
	buf2=xy8[:,8192:12288]
	#print 'xy8',xy8,xy8.shape,buf0.shape,buf1.shape,buf2.shape
	for buf in [buf0,buf1,buf2]:
		xy82=buf.reshape([-1,2])
		xy8comp=xy82[:,0]+1j*xy82[:,1]
#	#print 'xy8comp',xy8comp
#	#print 'abs(xy8comp.mean())',abs(xy8comp.mean())
		comp_list.append(xy8comp)
#
	fig1=pyplot.figure(1)
#	pyplot.plot(abs(comp_list[1]),'.-')
#	pyplot.xlabel('index')
#	pyplot.ylabel('accumulated output (a.u.)')
	for i in range(3):
		pyplot.subplot(3,4,4*i+1)
		#pyplot.semilogy(abs(comp_list[i]),'.-')
		pyplot.plot(abs(comp_list[i]),'.-')
		pyplot.subplot(3,4,4*i+2)
		pyplot.plot(comp_list[i].real,'.')
		pyplot.subplot(3,4,4*i+3)
		pyplot.plot(comp_list[i].imag,'.')
		pyplot.subplot(3,4,4*i+4)
#		pyplot.plot(comp_list[i].real,comp_list[i].imag,'.')
		#pyplot.hexbin(comp_list[i].real,comp_list[i].imag,gridsize=30,cmap='Greys',norm=matplotlib.colors.LogNorm())
		pyplot.hexbin(comp_list[i].real,comp_list[i].imag,gridsize=30,cmap='Greys',bins='log')#norm=matplotlib.colors.LogNorm())

	#fig1.suptitle('readoutdrvamp %8.2f qubitdrvamp %8.2f'%(clargs.readoutdrvamp,clargs.qubitdrvamp))
	#pyplot.savefig('%s_r%f_q%f.png'%(clargs.filename,clargs.readoutdrvamp,clargs.qubitdrvamp))
	pyplot.show()
	rabiperiod=80
	trash=2048%(rabiperiod*2) # Herald and data
	comprow=comp_list[0].reshape((-1,2048))[:,0:-trash].reshape((-1,2))
	xlim=max(abs(comprow[:,0]))*1.2
	ylim=max(abs(comprow[:,1]))*1.2
	lim=max(xlim,ylim)	
	x,y=comprow.shape
	index=numpy.arange(x).reshape(-1,rabiperiod)
	x,y=index.shape
	result=[]
	for i in range(y):
		result.append(comprow[index[:,i],:])
	filename='%s.dat'%clargs.filename
	print(result,len(result))
	numpy.savetxt(filename,comprow)
	fig1=pyplot.figure(1)

	pyplot.ion()
	for row in range(rabiperiod):
		#pyplot.plot(comprow[0,row,:].real,comprow[0,row,:].imag)
		pyplot.clf()
		pyplot.hexbin(result[row][:,1].real,result[row][:,1].imag,gridsize=30,cmap='Greys',bins='log')#norm=matplotlib.colors.LogNorm())a
		pyplot.xlim([-lim,lim])
		pyplot.ylim([-lim,lim])
#		print comprow[1,row,:][0:10]
		pyplot.title('row %d'%row)
		pyplot.savefig('%s_rabi_w_%02d.png'%(clargs.filename,row))
		pyplot.draw()
	#	pyplot.close()
#		pyplot.show()

