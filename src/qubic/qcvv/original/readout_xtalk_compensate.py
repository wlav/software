import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import copy
import experiment

class c_readoutxtalk(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid_c=None
		self.qubitid_t=None
		pass
	def readoutxtalkseqs(self,delayread=712e-9,delay1=12e-6,delaybetweenelement=600e-6,qubitid_c='Q5',qubitid_t='Q4',ctrl='I',tgt='I',qubitidread=['Q5','Q4','Q3'],readoutdrvamp=None,fread=None,phase_offset_ctrl=0,amp_multiplier_ctrl=0,phase_offset_tgt=0,amp_multiplier_tgt=0):
		self.qubitid_c=qubitid_c
		self.qubitid_t=qubitid_t
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		modrdrv={}
		modread={}
		modctrl_simul={}
		modtgt={}
		modtgt_simul={}
		run=0
		if readoutdrvamp is not None:
			modrdrv.update(dict(amp=readoutdrvamp))
		if fread is not None:
			modrdrv.update(dict(fcarrier=fread))
			modread.update(dict(fcarrier=fread))
		for ctrl in ['I','X180']:
			for tgt in ['I','X180']:
				# Readout
				therald=run
				self.seqs.add(therald,self.qchip.gates[qubitid_c+'read'])
				self.seqs.add(therald,self.qchip.gates[qubitid_t+'read'].modify([modrdrv,modread]))
	
				# Control state (control qubit: I or X180)
				tini=run+delay1
				if ctrl=='X180':
					self.seqs.add(tini,         	 self.qchip.gates[qubitid_c+'X180'])

					pcarrier_simul_tmp=self.qchip.gates[qubitid_c+'X180'].pcalc(padd=phase_offset_ctrl)[0]
					amp_simul_tmp=self.qchip.gates[qubitid_c+'X180'].paralist[0]['amp']*amp_multiplier_ctrl
					modctrl_simul.update(dict(dest=qubitid_t+'.qdrv',amp=amp_simul_tmp,pcarrier=pcarrier_simul_tmp))
					self.seqs.add(tini,              self.qchip.gates[qubitid_c+'X180'].modify(modctrl_simul))

					ttgt=self.seqs.tend()
				else:
					ttgt=tini
				ti180=ttgt-tini
	
				# Target state (target qubit: I or X180)
				if tgt=='X180':
					modtgt.update(dict(pcarrier=self.qchip.gates[qubitid_t+'X180'].pcalc(dt=ti180)[0]))
					self.seqs.add(ttgt,              self.qchip.gates[qubitid_t+'X180'].modify(modtgt))

					pcarrier_simul_tmp=self.qchip.gates[qubitid_t+'X180'].pcalc(dt=ti180,padd=phase_offset_tgt)[0]
					amp_simul_tmp=self.qchip.gates[qubitid_t+'X180'].paralist[0]['amp']*amp_multiplier_tgt
					modtgt_simul.update(dict(dest=qubitid_c+'.qdrv',amp=amp_simul_tmp,pcarrier=pcarrier_simul_tmp))
					self.seqs.add(ttgt,              self.qchip.gates[qubitid_t+'X180'].modify(modtgt_simul))
				else:
					pass
	
				# Readout
				treaddrv=self.seqs.tend()
				self.seqs.add(treaddrv,self.qchip.gates[qubitid_c+'read'])
				self.seqs.add(treaddrv,self.qchip.gates[qubitid_t+'read'].modify([modrdrv,modread]))
	
				run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
		accout_list=['accout_0','accout_1','accout_2']
		self.accout_dict=dict(zip(qubitidread,accout_list))
	def processreadoutxtalk(self,filename,loaddataset):
		c=self.loadjsondata(filename)
		print 'self.bufwidth_dict',self.bufwidth_dict,'   self.accout_dict',self.accout_dict
		print 'accout in use: ',self.accout_dict[self.qubitid_c],self.accout_dict[self.qubitid_t]
		data_list=[c[self.accout_dict[self.qubitid_c]],c[self.accout_dict[self.qubitid_t]]]
		ctrl_result=self.process3(data_list[0],qubitid=self.qubitid_c,lengthperrow=self.bufwidth_dict[self.qubitid_c],training=False,loaddataset=loaddataset)
		tgt_result=self.process3(data_list[1],qubitid=self.qubitid_t,lengthperrow=self.bufwidth_dict[self.qubitid_t],training=False,loaddataset=loaddataset)
		return [data_list[0],ctrl_result['separation'],ctrl_result['iqafterherald'],ctrl_result['population_norm'],data_list[1],tgt_result['separation'],tgt_result['iqafterherald'],tgt_result['population_norm']]


if __name__=="__main__":
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	#amp_multiplier_ctrl_map=numpy.arange(0.0,2.0,0.1)
	#phase_offset_ctrl_map=numpy.arange(0.0,numpy.pi*2,numpy.pi/10)
	#amp_multiplier_tgt_map=numpy.arange(0.0,2.0,0.1)
	#phase_offset_tgt_map=numpy.arange(0.0,numpy.pi*2,numpy.pi/10)
	phase_offset_ctrl=4.925-numpy.pi
	amp_multiplier_ctrl=1.4
	phase_offset_tgt=0.85+numpy.pi
	amp_multiplier_tgt=0.63
	parser,cmdlinestr=experiment.cmdoptions()
	clargs=parser.parse_args()
	readoutxtalk=c_readoutxtalk(**clargs.__dict__)
	if clargs.sim:
		readoutxtalk.setsim()
	readoutxtalk.readoutxtalkseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,qubitid_c=clargs.qubitid_c,qubitid_t=clargs.qubitid_t,qubitidread=clargs.qubitidread,readoutdrvamp=clargs.readoutdrvamp,fread=clargs.fread,phase_offset_ctrl=phase_offset_ctrl,amp_multiplier_ctrl=amp_multiplier_ctrl,phase_offset_tgt=phase_offset_tgt,amp_multiplier_tgt=amp_multiplier_tgt)
	readoutxtalk.run()
	data=readoutxtalk.acqdata(clargs.nget)
	fprocess=readoutxtalk.savejsondata(filename=clargs.filename,extype='readoutxtalk',cmdlinestr=cmdlinestr,data=data)
	print 'save data to ',fprocess
	if clargs.sim:
		readoutxtalk.sim()
	[rawdata_c,separation_c,iqafterherald_c,population_norm_c,rawdata_t,separation_t,iqafterherald_t,population_norm_t]=readoutxtalk.processreadoutxtalk(filename=fprocess,loaddataset=clargs.dataset)
	readoutxtalk.plotpopulation_norm(population_norm=population_norm_c,figname='control'+fprocess)
	readoutxtalk.plotpopulation_norm(population_norm=population_norm_t,figname='target'+fprocess)
	if clargs.plot:
		#pyplot.grid()
		pyplot.show()
