import numpy
import functools
import itertools
from collections import OrderedDict
from matplotlib import pyplot
from matplotlib.font_manager import FontProperties


def I(n=2):
	return numpy.eye(n, dtype='complex')

def rot_x(theta):
	c, s = numpy.cos(theta / 2), numpy.sin(theta / 2)
	return numpy.matrix([[c, -1j * s], [-1j * s, c]])

def rot_y(theta):
	c, s = numpy.cos(theta / 2), numpy.sin(theta / 2)
	return numpy.matrix([[c, -s], [s, c]])

def rot_z(theta):
	exp_z = numpy.exp(1j * theta / 2.)
	exp_z_inv = numpy.exp(-1j * theta / 2.)
	return numpy.matrix([[exp_z_inv, 0], [0, exp_z]])


X90 = rot_x(numpy.pi/2)
Y90 = rot_y(numpy.pi/2)
Z90 = rot_z(numpy.pi/2)
X180 = rot_x(numpy.pi)
Y180 = rot_y(numpy.pi)
Z180 = rot_z(numpy.pi)
X270 = rot_x(3*numpy.pi/2)
Y270 = rot_y(3*numpy.pi/2)
Z270 = rot_z(3*numpy.pi/2)

full_set = {'I': I(), 'X180': X180, 'Y180': Y180, 'Z180': Z180, 'X90': X90, 'Y90': Y90, 'Z90': Z90, 'X270': X270, 'Y270': Y270, 'Z270': Z270}
pulse_to_op = {'I': I(), 'X90': X90, 'X180': X180, 'X270': X270, 'Y90': Y90, 'Y270': Y270}

sigmax = numpy.matrix([[0, 1], [1, 0]], dtype='complex')
sigmay = numpy.matrix([[0, -1j], [1j, 0]], dtype='complex')
sigmaz = numpy.matrix([[1, 0], [0, -1]], dtype='complex')
paulis = OrderedDict([('I', numpy.matrix(numpy.eye(2, dtype=complex))), ('X', sigmax), ('Y', sigmay), ('Z', sigmaz)])


def base_convert(i, b, n):
	result = []
	while i > 0:
		result.insert(0, i % b)
		i = i // b
	if len(result) < n:
		padding = [0] * (n - (len(result)))
		padding.extend(result)
		return padding
	return result

def nkron(oplist): #takes a kronecker product viewing the ketlist like a binary string: |qN,qN-1,...,q1>
	if len(oplist) <= 1:
		return numpy.array(oplist)
	kronop = numpy.kron(oplist[0],oplist[1])
	if len(oplist) > 2:
		for op in (oplist[2:]):
			kronop = numpy.kron(kronop,op)
	return kronop

def comp_basis_str(n):
	ind_list = [[str(ii) for ii in base_convert(i,2,n)] for i in numpy.arange(2**n)]
	op_string_list = list(numpy.array([['|{}><{}|'.format(a,b) for b in [''.join(l)for l in ind_list]] for a in [''.join(l)for l in ind_list]]).flatten())
	return op_string_list

def trace(x, real=True, rounding=6):
	result = numpy.round(numpy.trace(x), rounding)
	if real:
		result = numpy.float(numpy.real(result))
	return result

def tensor(*t_list):
	result = numpy.matrix([1], dtype='complex')
	for t in t_list:
		result = numpy.kron(result, t)
	return result

def meas_decomp(meas):
	vals, vecs = numpy.linalg.eig(meas)
	projectors = []
	e_vals = []
	for val in numpy.unique(vals):
		out = numpy.zeros_like(meas)
		out[:, vals == val] = vecs[:, vals == val]
		projectors.append(out*numpy.conjugate(out.T))
		e_vals.append(val)
	return projectors, e_vals

def create_projectors(meas_operators):
	return [functools.reduce(numpy.dot, x) for x in list(itertools.product(*[meas_decomp(x)[0] for x in meas_operators]))[::-1]]

def generate_tomo_pulse_list(n_qubits=1, tomography_list=None):
	if tomography_list is None:
		tomography_list = ['I', 'X90', 'Y90']
	if n_qubits == 1:
		return tomography_list
	else:
		return list(itertools.product(tomography_list, repeat=n_qubits))

def binary_arrays_to_bins(x, system_levels=2):
	# convert a list of bit-strings to integers computed in big-endian form e.g. 01 = 2
	x = numpy.array(x)
	return numpy.sum(numpy.einsum('i..., i->i...', x, system_levels ** numpy.arange(x.shape[0])), 0)

def construct_meas_from_list(meas_list, projectors, mapping=full_set):
	measurements = []
	meas_names = []
	for c in projectors:
		for x in meas_list:
			if isinstance(x, tuple):
				rot = numpy.conj(tensor(*[mapping[y] for y in x])).T
			else:
				rot = numpy.matrix(numpy.conj(tensor(*[mapping[x]])).T)
			measurements.append(rot*c*rot.H)
			meas_names.append("{}  {}".format(numpy.argmax(numpy.diag(c)), x))
	return measurements, meas_names

def standard_tomography(measurement_avgs, n_qubits=1, meas_operator=sigmaz, tomography_list=None):
	meas_ops = []
	for q in range(n_qubits):
		ops = [I() for _ in range(q)]
		ops.append(meas_operator)
		ops.extend([I() for _ in range(n_qubits-q-1)])
		meas_ops.append(tensor(*ops))
	projectors = create_projectors(meas_ops)
	pre_rotation_list = generate_tomo_pulse_list(n_qubits=n_qubits,tomography_list=tomography_list)
	meas_matrices, names = construct_meas_from_list(pre_rotation_list, projectors)
	N = len(meas_matrices)
	d = meas_matrices[0].shape[0]
	A = numpy.zeros((N, d ** 2), dtype=complex)
	for i, v in enumerate(meas_matrices):
		A[i, :] = numpy.reshape(numpy.conj(numpy.array(v)), (1, -1))
	rho_uncons = numpy.linalg.lstsq(A, numpy.array(measurement_avgs), rcond=-1)[0]
	rho_uncons = numpy.reshape(rho_uncons, (d, d))
	return rho_uncons

def vec_to_dm(vec):
	if 1 in numpy.shape(vec) or len(numpy.shape(vec))==1:
		if type(vec) == list:
			dm = numpy.dot(numpy.array(vec).reshape(-1, 1), numpy.conjugate(numpy.array(vec).reshape(1, -1)))
			return dm
		else:
			dm = numpy.dot(vec.reshape(-1, 1), numpy.conjugate(vec.reshape(1, -1)))
			return dm
	return vec

def plot_dm(dm, rounding=3, display='Full', show_colorbar=False, show_ticks=True, x_labels=None, y_labels=None, axes=None, title=None,scale=None, text_color='Grey'):
	dm = vec_to_dm(dm)
	x_dim = numpy.shape(dm)[1]
	y_dim = numpy.shape(dm)[0]
	if scale is None:
		scale = numpy.max([x_dim/1.-1, 1.3])
	if axes is None:
		fig = pyplot.figure(figsize=(scale, scale))
	display = display.lower()
	if display == 'real':
		dm_show = numpy.real(dm)
		display = "Real Component"
	elif display == 'full':
		dm_show = dm
		display = "Full"
	elif display == 'imag' or display == 'complex':
		dm_show = numpy.imag(dm)
		display = "Imaginary Component"
	else:
		display = 'Magnitude'
		dm_show = numpy.abs(dm)
	if axes:
		im = axes.imshow(numpy.real(dm_show), interpolation='none', cmap='RdBu', vmin=-1, vmax=1)
	else:
		im = pyplot.imshow(numpy.real(dm_show), interpolation='none', cmap='RdBu', vmin=-1, vmax=1)
	small_font = FontProperties()#size='small')
	if text_color is None:
		text_color = pyplot.get_cmap("RdBu")(0.5)
	for x in range(dm.shape[1]):
		for y in range(dm.shape[0]):
			#if numpy.abs(numpy.round(dm_show[y, x], 2)) > 0:
			if numpy.abs(numpy.round(dm_show[y, x], rounding)) > 0:
				#val = numpy.round(dm_show[y, x], 2)
				val = numpy.round(dm_show[y, x], rounding)
				if not numpy.isclose(numpy.imag(val), 0):
					val_str = "{}\n{}".format(numpy.real(val), numpy.imag(val))
				else:
					val_str = "{}".format(numpy.real(val))
				if axes:
					axes.text(x, y, val_str, ha='center', va='center', color=text_color, fontproperties=small_font)
				else:
					pyplot.text(x, y, val_str, ha='center', va='center', color=text_color, fontproperties=small_font)
	if show_ticks:
		if x_labels is None:
			fmt_str = "{0:0" + str(int(numpy.log2(dm.shape[1]))) + "b}"
			pyplot.xticks(numpy.arange(dm.shape[1]),[fmt_str.format(x) for x in numpy.arange(dm.shape[1])],rotation='vertical',fontproperties=small_font)
		else:
			pyplot.xticks(numpy.arange(dm.shape[1]),x_labels,fontproperties=small_font)
		if y_labels is None:
			fmt_str = "{0:0" + str(int(numpy.log2(dm.shape[0]))) + "b}"
			pyplot.yticks(numpy.arange(dm.shape[0]),[fmt_str.format(x) for x in numpy.arange(dm.shape[0])],fontproperties=small_font)
		else:
			pyplot.yticks(numpy.arange(dm.shape[0]),y_labels,fontproperties=small_font)
	if title:
		pyplot.title(title)
	if show_colorbar:
		pyplot.colorbar(im, fraction=0.03, pad=0.04)
	if not axes:
		return fig
	else:
		return axes

def project_and_normalize_density_matrix(rho_uncons):
	rho_uncons = rho_uncons / trace(rho_uncons)
	d = rho_uncons.shape[0]
	[eigvals_un, eigvecs_un] = numpy.linalg.eigh(rho_uncons)
	if numpy.min(eigvals_un) >= 0:
		return rho_uncons
	eigvals_un = list(eigvals_un)
	eigvals_un.reverse()
	eigvals_new = [0.0] * len(eigvals_un)
	i = d
	a = 0.0
	while eigvals_un[i - 1] + a / float(i) < 0:
		a += eigvals_un[i - 1]
		i -= 1
	for j in range(i):
		eigvals_new[j] = eigvals_un[j] + a / float(i)
	eigvals_new.reverse()
	rho_cons = numpy.dot(eigvecs_un, numpy.dot(numpy.diag(eigvals_new), numpy.conj(eigvecs_un.T)))
	return rho_cons

def pulses_to_matrix(pulse_list):
	dim = int(2 ** (len(pulse_list)))
	gs = numpy.matrix(numpy.zeros([dim, dim], dtype=complex))
	gs[0, 0] = 1.0
	oplist = [pulse_to_op[p] for p in pulse_list]
	return numpy.matrix(tensor(*oplist)) * gs * numpy.matrix(tensor(*oplist)).H

def unitary_transform(unitary, state):
	assert isinstance(unitary, numpy.matrix)
	return unitary*state*unitary.H

def pauli_basis(n,vectorize=False,plot=True):
	P_SQ = [paulis[k] for k in paulis.keys()]
	P_strings = list(paulis.keys())
	ind_list = [base_convert(i,4,n) for i in numpy.arange(4**n)]
	op_list = []
	op_string_list = []
	if len(ind_list) <= 4: # single qubit
		if vectorize:
			for l in ind_list:
				op_list.append(nkron([P_SQ[k] for k in l]).flatten())
				op_string_list.append(''.join([P_strings[k] for k in l]))
			op_list = numpy.matrix(numpy.array(op_list),dtype='complex')
			if plot:
				plot_comp_to_pauli_matrix(op_list,op_string_list)
			return numpy.matrix(numpy.array(op_list), dtype='complex'),op_string_list
		else:
			for l in ind_list:
				op_list.append(numpy.matrix(nkron([P_SQ[k] for k in l])))
				op_string_list.append(''.join([P_strings[k] for k in l]))
			return numpy.array(op_list),op_string_list
	else:
		if vectorize:
			for l in ind_list:
				op_list.append(nkron([P_SQ[k] for k in l]).flatten())
				op_string_list.append(''.join([P_strings[k] for k in l]))
			op_list = numpy.matrix(numpy.array(op_list),dtype='complex')
			if plot:
				plot_comp_to_pauli_matrix(op_list,op_string_list)
			return op_list,op_string_list
		else:
			for l in ind_list:
				op_list.append(nkron([P_SQ[k] for k in l]))
				op_string_list.append(''.join([P_strings[k] for k in l]))
			return numpy.array(op_list),op_string_list

def plot_comp_to_pauli_matrix(CtoPmat,pauli_str):
	angles = numpy.arange(-numpy.pi, numpy.pi, numpy.pi / 4.)
	h = (angles + numpy.pi) / (2 * numpy.pi) + 0.5
	r = numpy.array([1.0] * 8)
	l = 1.0 / (1.0 + r ** 0.3)
	s = 1.0
	c = numpy.vectorize(hls_to_rgb)(h, l, s)  # --> tuple
	c = numpy.array(c)  # -->  array of (3,n,m) shape, but need (n,m,3)
	c = c.swapaxes(0, 1)
	phase_cm = LinearSegmentedColormap.from_list('HuePhase', c, N=50)
	def colorize(z):
		r = numpy.abs(z)
		arg = numpy.angle(z)
		h = (arg + numpy.pi) / (2 * numpy.pi) + 0.5
		l = 1.0 / (1.0 + r ** 0.3)
		s = 0.8
		c = numpy.vectorize(hls_to_rgb)(h, l, s)  # --> tuple hls = hue, luminosity, saturation
		c = numpy.array(c)  # -->  array of (3,n,m) shape, but need (n,m,3)
		c = c.swapaxes(0, 2)
		return c
	n = int(numpy.log2(numpy.sqrt(len(CtoPmat)))) #number of qubits
	comp_str = comp_basis_str(2 ** (n - 1))
	pyplot.figure(figsize=(5*n,5*n))
	pyplot.imshow(colorize(numpy.array(CtoPmat).T),cmap=phase_cm)
	cbar = pyplot.colorbar(ticks=[0.1,0.4,0.6,0.8, 1.0])
	cbar.ax.set_yticklabels(['-$\pi$','-$\pi/2$', '0', '$\pi/2$','$\pi$'])
	cbar.set_label('Arg[z]')
	pyplot.yticks(numpy.arange(0,len(pauli_str),1.0),pauli_str,rotation=0)
	pyplot.xlabel('Input Comp dm element')
	pyplot.xticks(numpy.arange(0,len(comp_str),1.0),comp_str,rotation=90)
	pyplot.ylabel('Output Pauli Basis')
	pyplot.grid(ls='--')
	pyplot.tight_layout()

def plot_matrix(dm, show_colorbar=False, show_ticks=True, x_labels=None, y_labels=None, axes=None,
		title=None, scale=None, text_color=None, rounding=2, plot_settings=None):
	"""Plot a density matrix"""
	dm = vec_to_dm(dm)
	x_dim = numpy.shape(dm)[1]
	if scale is None:
		scale = numpy.max([x_dim / 1. - 1, 1.3])
	if plot_settings is not None:
		if 'title' in plot_settings:
			title = plot_settings.pop('title')
	if plot_settings is None:
		pyplot.rc('font', **{'size': 18, 'family': 'Arial'})
	else:
		pyplot.rc(**plot_settings)
	if axes is None:
		fig = pyplot.figure(figsize=(scale, scale))
	dm_show = dm
	if axes:
		im = axes.imshow(numpy.real(dm_show), interpolation='none', cmap='RdBu', vmin=-1, vmax=1)
	else:
		im = pyplot.imshow(numpy.real(dm_show), interpolation='none', cmap='RdBu', vmin=-1, vmax=1)
	small_font = FontProperties()
	for x in range(dm.shape[1]):
		for y in range(dm.shape[0]):
			if numpy.abs(numpy.round(dm_show[y, x], rounding)) > 0:
				if text_color is None:
					if abs(numpy.real(numpy.round(dm_show[y, x], rounding))) > 0.5:
						current_text_color = 'w'
					else:
						current_text_color = 'k'
				else:
					current_text_color = text_color
				val = numpy.round(dm_show[y, x], rounding)
				if not numpy.isclose(numpy.imag(val), 0):
					val_str = "{}\n{}".format(numpy.real(val), numpy.imag(val))
				else:
					val_str = "{}".format(numpy.real(val))
				if axes:
					axes.text(x, y, val_str, ha='center', va='center', color=current_text_color)
				else:
					pyplot.text(x, y, val_str, ha='center', va='center', color=current_text_color)
	if show_ticks:
		if x_labels is None:
			fmt_str = "{0:0" + str(int(numpy.log2(dm.shape[1]))) + "b}"
			pyplot.xticks(numpy.arange(dm.shape[1]),[fmt_str.format(x) for x in numpy.arange(dm.shape[1])],rotation='vertical')
		else:
			pyplot.xticks(numpy.arange(dm.shape[1]),x_labels)
		if y_labels is None:
			fmt_str = "{0:0" + str(int(numpy.log2(dm.shape[0]))) + "b}"
			pyplot.yticks(numpy.arange(dm.shape[0]),[fmt_str.format(x) for x in numpy.arange(dm.shape[0])])
		else:
			pyplot.yticks(numpy.arange(dm.shape[0]),y_labels)
		from matplotlib.ticker import AutoMinorLocator
		pyplot.gca().yaxis.set_minor_locator(AutoMinorLocator(2))
		pyplot.gca().xaxis.set_minor_locator(AutoMinorLocator(2))
		pyplot.tick_params(which='minor', length=0)
		pyplot.grid(which='minor')
	if title:
		pyplot.title(title)
	if show_colorbar:
		pyplot.colorbar(im, fraction=0.03, pad=0.04)
	if not axes:
		return fig
	else:
		return axes

def get_process_matrix(states_in, states_out, physical=True):
	"""
	Extract Pauli Transfer Matrix from measurements, described by input density matrices and output density-matrices
	:param states_in:   array-like, list of input density matrices
	:param states_out:  array-like, list of input density matrices
	:param physical     bool, whether to enforce physicality constraints on the Pauli Transfer Matrix
	:return:            matrix-like, Pauli transfer matrix based on input to output state mapping
	"""
	n = int(numpy.log2(numpy.sqrt(len(states_in))))
	basis_str = list(map(''.join, itertools.product(*[["I", 'X', 'Y', 'Z']] * n)))
	orth_norm_herm_basis = {'I': I(),
			'X': sigmax,
			'Y': sigmay,
			'Z': sigmaz}
	n_qubit_basis = []
	for base in basis_str:
		op_list = [orth_norm_herm_basis[k] for k in list(base)]
		n_qubit_basis.append(tensor(*op_list))
	n_qubit_basis = numpy.array(n_qubit_basis)
	p_in = numpy.array([numpy.linalg.solve(n_qubit_basis.T.reshape(4 ** n, 4 ** n), x.reshape(-1)) for x in states_in])
	p_out = numpy.array([numpy.linalg.solve(n_qubit_basis.T.reshape(4 ** n, 4 ** n), x.reshape(-1)) for x in states_out])
	pm = numpy.linalg.lstsq(p_out, p_in)[0]
	#if physical: # whether to enforce physicality constraints on the Pauli Transfer Matrix
	if False:
		choi_matrix = ptm_to_choi(pm)
		choi_physical = project_and_normalize_density_matrix(choi_matrix)
		pm_physical = choi_to_ptm(choi_physical)
		pm = pm_physical
	return pm

#def ptm_to_choi(ptm):
#	n = int(numpy.log2(numpy.sqrt(len(ptm))))  # ptm dimension is 4**n by 4**n
#	c2pmat, c2pstr = pauli_basis(n, vectorize=True, plot=False)
#	choi_elements = []
#	for basis_element in numpy.arange(len(ptm)):  # 0 = II, 1 = IX, 2 = IZ, 3 = XI, etc...
#		pauli_vec_in = numpy.matrix(numpy.zeros(len(ptm))).T
#		pauli_vec_in[basis_element] = 1.0
#		pauli_vec_out = ptm @ pauli_vec_in
#		pauli_in_mat_basis = (c2pmat.T @ pauli_vec_in).reshape(2 ** n, 2 ** n).T
#		pauli_out_mat_basis = (c2pmat.T @ pauli_vec_out).reshape(2 ** n, 2 ** n)
#		choi_elements.append(tensor(pauli_in_mat_basis, pauli_out_mat_basis))
#	choi_matrix = numpy.matrix(numpy.sum(choi_elements, axis=0))
#	choi_matrix /= numpy.trace(choi_matrix)
#	return choi_matrix
#
#def choi_to_ptm(choi):
#	n = int(numpy.log2(numpy.sqrt(len(choi))))  # ptm dimension is 4**n by 4**n
#	c2pmat, c2pstr = pauli_basis(n, vectorize=True, plot=False)
#	ptm = numpy.matrix(numpy.zeros_like(choi), dtype='complex128')
#	for i in range(ptm.shape[0]):
#		choi_elements_row = []
#		pauli_basis_element_in = (c2pmat[i]).reshape(2 ** n, 2 ** n).T  # input Pauli element
#		for j in range(ptm.shape[1]):
#			pauli_basis_element_out = (c2pmat[j]).reshape(2 ** n, 2 ** n)  # output Pauli element
#			ptm[i, j] = numpy.trace(choi @ tensor(pauli_basis_element_out, pauli_basis_element_in).astype('complex128'))
#	return ptm
#
#def process_fidelity(ptm_ideal, ptm_exp):
#	assert ptm_ideal.shape == ptm_exp.shape, "PTMs must be the same shape!"
#	d = int(numpy.sqrt(len(ptm_ideal)))
#	return (numpy.trace(ptm_ideal.T @ ptm_exp/numpy.max(ptm_exp)) / d**2).real



#ubits=['Q5','Q4']
#n_qubits=len(qubits)
#meas_result=numpy.array([[[1,1,0,0,1,1,0,1,1],[1,0,1,0,1,0,0,0,1]],[[0,0,1,0,1,1,0,1,0],[0,1,1,1,0,1,1,0,0]]])
#print meas_result
#unbinned_data=binary_arrays_to_bins(meas_result)
#print 'unbinned_data',unbinned_data.shape,unbinned_data
#binned_data=numpy.mean([unbinned_data==i for i in range(2**n_qubits)],1)
#print 'binned_data',binned_data.shape,binned_data
#
#measurement_avgs=numpy.arange(0,1,1./36)
#rho_uncons=standard_tomography(measurement_avgs=measurement_avgs,n_qubits=n_qubits)
#
#print rho_uncons.shape,rho_uncons
#
#plot_dm(rho_uncons, rounding=3, scale=5)
##plot_dm(rho_uncons, scale=5)
#pyplot.show()
