import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import copy
from experiment import c_experiment
from rabi_w import c_rabi
import qutip

class c_cnot(c_rabi):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit'):
		c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg)
		self.qubitid_c=None
		self.qubitid_t=None
		pass
	def cnotseqs(self,delayread=712e-9,delay1=12e-6,delaybetweenelement=600e-6,tcr=None,qubitid_c='Q5',qubitid_t='Q4',pcr=0,IZphase=0,IXamp_deg=0,qubitidread=['Q5','Q4','Q3'],qubitdrvamp=None):
		self.qubitid_c=qubitid_c
		self.qubitid_t=qubitid_t
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		modcr={}
		modxgate={}
		tcr=tcr if tcr else self.qchip.gates['CR('+qubitid_c+qubitid_t+')'].paralist[0]['twidth']
		IXamp_deg=(IXamp_deg+180)%360-180     #wrap IXamp_deg to (-180,180)
		amp_tmp=IXamp_deg/90*self.qchip.gates[qubitid_t+'X90'].paralist[0]['amp'] if IXamp_deg>=0 else (IXamp_deg+360)/180*self.qchip.gates[qubitid_t+'X180'].paralist[0]['amp']
		twidth_tmp=self.qchip.gates[qubitid_t+'X90'].paralist[0]['twidth'] if IXamp_deg>=0 else round(2*self.qchip.gates[qubitid_t+'X90'].paralist[0]['twidth'],15)
		if qubitdrvamp is not None:
			modcr.update(dict(amp=qubitdrvamp))
		run=0
		for prep in ['I','X180']:
			for ctrl in ['I','X180']:
				for meas in ['Y-90','X90','I']:
					# Readout
					therald=run
					self.seqs.add(therald,self.qchip.gates[qubitid_t+'read'])
					run=self.seqs.tend()+delay1
					tini=run

					# Target state preparation (target qubit: I or X180)
					if prep=='X180':
						self.seqs.add(run,self.qchip.gates[qubitid_t+'X180'])
						run=self.seqs.tend()

					# Control state preparation (control qubit: I or X180)
					if ctrl=='X180':
						pctrl=self.qchip.gates[qubitid_c+'X180'].pcalc(dt=run-tini)[0]
						self.seqs.add(run,self.qchip.gates[qubitid_c+'X180'].modify(dict(pcarrier=pctrl)))
						run=self.seqs.tend()

					# CR gate (drive the control qubit at the frequency of the target frequency)
					modcr.update(dict(twidth=tcr,pcarrier=self.qchip.gates['CR('+qubitid_c+qubitid_t+')'].pcalc(dt=run-tini,padd=pcr)[0]))
					self.seqs.add(run,self.qchip.gates['CR('+qubitid_c+qubitid_t+')'].modify(modcr))
					run=self.seqs.tend()

					# CNOT X gate (target qubit)
					pcarrier_tmp=self.qchip.gates['CNOT('+qubitid_c+qubitid_t+').'+qubitid_t+'X'].pcalc(dt=run-tini,padd=pcr)[0]
					modxgate.update(dict(twidth=twidth_tmp,amp=amp_tmp,pcarrier=pcarrier_tmp))
					self.seqs.add(run,self.qchip.gates['CNOT('+qubitid_c+qubitid_t+').'+qubitid_t+'X'].modify(modxgate))
					run=self.seqs.tend()

					# Projection (target qubit: Y-90, X90 or I)
					if meas=='I':
						pmeas=0
					else:
						pmeas=self.qchip.gates[qubitid_t+meas].pcalc(dt=run-tini)[0]
						self.seqs.add(run,self.qchip.gates[qubitid_t+meas].modify(dict(pcarrier=pmeas)))

					# Readout
					treaddrv=self.seqs.tend()
					self.seqs.add(treaddrv,self.qchip.gates[qubitid_t+'read'])

					run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]	
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
	def processcnot(self,filename,loaddataset):
		c=self.loadjsondata(filename)
		print('c.keys()',c.keys())
		data=c[list(c.keys())[0]]
		cnot_result=self.process3(data,qubitid=self.qubitid_t,lengthperrow=self.bufwidth_dict[self.qubitid_t],training=False,loaddataset=loaddataset)
		return [data,cnot_result['separation'],cnot_result['iqafterherald'],cnot_result['population_norm']]


if __name__=="__main__":
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	cnot=c_cnot()
	parser,cmdlinestr=cnot.cmdoptions()
	parser.add_argument('--tcr',help='pulse length for CR gate',dest='tcr',type=float,default=None)
	parser.add_argument('--pcr',help='starting phase for CR gate (ZXphase)',dest='pcr',type=float,default=0)
	parser.add_argument('--IZphase',help='IZ phase after CR gate',dest='IZphase',type=float,default=0) # May not need this
	parser.add_argument('--IXamp_deg',help='IX amp (in unit of degree) after CR gate',dest='IXamp_deg',type=float,default=0)
	clargs=parser.parse_args()
	cnot.initseqs()
	cnot.cnotseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,tcr=clargs.tcr,qubitid_c=clargs.qubitid_c,qubitid_t=clargs.qubitid_t,pcr=clargs.pcr,IZphase=clargs.IZphase,IXamp_deg=clargs.IXamp_deg,qubitidread=clargs.qubitidread,qubitdrvamp=clargs.qubitdrvamp)
	cnot.run()
	data=cnot.acqdata(clargs.nget)
	fprocess=cnot.savejsondata(filename=clargs.filename,extype='cnot',cmdlinestr=cmdlinestr,data=data)
	print('save data to ',fprocess)
	[rawdata,separation,iqafterherald,population_norm]=cnot.processcnot(filename=fprocess,loaddataset=clargs.dataset)
	if clargs.readcorr:
		population_norm=cnot.readoutcorrection(qubitid_list=[clargs.qubitid_t],meas_binned=numpy.vstack((1-population_norm,population_norm)),corr_matrix=numpy.load(clargs.corrmx))[1]
		print('corrected population_norm',population_norm)
	pnts=1-2*population_norm
	pnt_c0t0=pnts[0:3]
	pnt_c1t0=pnts[3:6]
	pnt_c0t1=pnts[6:9]
	pnt_c1t1=pnts[9:12]
	print('pnts',pnts)
	print('pnt_c0t0',pnt_c0t0)
	print('pnt_c1t0',pnt_c1t0)
	print('pnt_c0t1',pnt_c0t1)
	print('pnt_c1t1',pnt_c1t1)

	result=list(pnts.reshape((1,-1))[0])
	print(result)
	with open(clargs.filename+'_cnot'+'_tcr'+str(clargs.tcr)+'_pcr'+str(clargs.pcr)+'_IZphase'+str(clargs.IZphase)+'_IXampdeg'+str(clargs.IXamp_deg)+'_'+timestamp+'.dat','a') as f:
		numpy.savetxt(f,numpy.array(result).reshape((1,-1)))

	bloch=qutip.Bloch()
	bloch.add_points(pnt_c0t0)
	bloch.add_points(pnt_c1t0)
	bloch.add_points(pnt_c0t1)
	bloch.add_points(pnt_c1t1)
	bloch.show()
	pyplot.show()
	bloch.save(clargs.filename+'_cnot'+'_tcr'+str(clargs.tcr)+'_pcr'+str(clargs.pcr)+'_IZphase'+str(clargs.IZphase)+'_IXampdeg'+str(clargs.IXamp_deg)+'_'+timestamp+'.png')

# python cnot2.py -n 50 --qubitid_c Q6 --qubitid_t Q5 --readcorr
