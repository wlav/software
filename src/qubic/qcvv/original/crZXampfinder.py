import matplotlib
matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
import datetime
import sys
from squbic import *
from matplotlib import pyplot
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
from experiment import fullsin
import experiment
from crZXamprepeat import c_crZXamprepeat
from scipy.optimize import curve_fit,fminbound

def crZXampfinder(acr_center,acr_halfspan,n_repeat,tcr,delayread,delaybetweenelement,nget,qubitid_c,qubitid_t,qubitidread=['Q7','Q6','Q5'],pcr=0,acr_num=81,fixed_ramp=True,readcorr=True):
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	acr_list=acr_center+numpy.linspace(-acr_halfspan,acr_halfspan,acr_num)
	result_filename='crZXamprepeat'+'_tcr'+str(tcr)+'_pcr'+str(pcr)+'_nrepeat'+str(n_repeat)+'_'+timestamp
	for ctrl in ['I','X180']:
		for meas in ['Y-90','X90','I']:
			parser,cmdlinestr=experiment.cmdoptions()
			clargs=parser.parse_args()
			crZXamprepeat=c_crZXamprepeat(**clargs.__dict__)
			crZXamprepeat.crZXamprepeatseqs(delayread=delayread,delay1=12e-6,delaybetweenelement=delaybetweenelement,qubitid_c=qubitid_c,qubitid_t=qubitid_t,ctrl=ctrl,meas=meas,pcr=pcr,acr_list=acr_list,tcr=tcr,qubitidread=qubitidread,fixed_ramp=fixed_ramp,n_repeat=n_repeat)
			crZXamprepeat.run()
			data=crZXamprepeat.rabiacq(nget)
			fprocess=crZXamprepeat.savejsondata(filename=clargs.filename,extype='crZXamprepeat',cmdlinestr=cmdlinestr,data=data)
			print('save data to ',fprocess)
			[rawdata,separation,iqafterherald,population_norm]=crZXamprepeat.processcrZXamprepeat(filename=fprocess,loaddataset=clargs.dataset)
			if readcorr:
				population_norm=crZXamprepeat.readoutcorrection(qubitid_list=[qubitid_t],meas_binned=numpy.vstack((1-population_norm,population_norm)),corr_matrix=numpy.load(clargs.corrmx))[1]
				print('corrected population_norm',population_norm)
			pnt_traj=1-2*population_norm
			with open(result_filename+'.dat','a') as f:
				numpy.savetxt(f,[pnt_traj])
	with open(result_filename+'.dat','a') as f:
		numpy.savetxt(f,[numpy.array(acr_list)])
	crdata=numpy.loadtxt(result_filename+'.dat')
	adata=crdata[6,:]
	for index,target_projection in enumerate(['Target <X>','Target <Y>','Target <Z>']):
		pyplot.figure()
		pyplot.plot(adata,crdata[index,:],label='Control |0>',color='b')
		pyplot.plot(adata,crdata[index+3,:],label='Control |1>',color='r')
		pyplot.legend()
		pyplot.xlabel('CR ZX Amplitude')
		pyplot.ylabel(target_projection)
		pyplot.ylim(-1.1,1.1)
		pyplot.savefig(result_filename+'_'+str(index)+'.png')
		pyplot.close()
	rdata=numpy.sqrt((crdata[0,:]-crdata[3,:])**2+(crdata[1,:]-crdata[4,:])**2+(crdata[2,:]-crdata[5,:])**2)/2.0
	dx=adata[1]-adata[0]
	spec=abs(numpy.fft.rfft(rdata-rdata.mean()))
	fest=numpy.argmax(spec)*1.0/(dx*len(rdata))
	aest=(rdata.max()-rdata.min())/2.0
	oest=rdata.mean()
	fmin=0.3*fest
	fmax=1.2*fest
	popt,pcov=curve_fit(fullsin,adata,rdata,bounds=(([0.2*aest,fmin,-2*numpy.pi,oest*0.3],[1.2*aest,fmax,2*numpy.pi,oest*1.2])))
	pyplot.figure()
	pyplot.plot(adata,fullsin(adata,*popt),label='fit')
	pyplot.plot(adata,rdata,'*',label='raw')
	pyplot.xlabel('CR ZX Amplitude')
	pyplot.ylabel(r'$|\vec{R}|$')
	pyplot.ylim(-0.05,1.05)
	pyplot.legend()
	pyplot.savefig(result_filename+'_R'+'.png')
	pyplot.close()
	acr=fminbound(func=lambda x,*popt:-fullsin(x,*popt),x1=adata[0],x2=adata[-1],xtol=0.001,disp=True,args=(popt))
	sys.stdout.flush()
	return acr

if __name__=="__main__":
	acr_center=0.6203327603406689
	n_repeat=3
	span_shrink=3
	acr_halfspan=0.48/span_shrink
	tcr=168e-9
	delayread=680e-9
	delaybetweenelement=600e-6
	nget=50
	qubitid_c='Q6'
	qubitid_t='Q5'
	acr=crZXampfinder(acr_center,acr_halfspan,n_repeat,tcr,delayread,delaybetweenelement,nget,qubitid_c,qubitid_t,qubitidread=['Q7','Q6','Q5'],pcr=0,acr_num=81,fixed_ramp=True,readcorr=True)
	print('acr',acr)
