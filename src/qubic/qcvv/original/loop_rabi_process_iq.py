from matplotlib import pyplot,dates
import datetime
import numpy

qubitid_list=['Q5','Q6','Q7']
#filenames=['looprabidata_20200214_1539']
#filenames=['looprabidata_20200214_1732']
#filenames=['looprabidata_20200214_1747']
#filenames=['looprabidata_20200214_1759']
#filenames=['looprabidata_20200214_2158','looprabidata_20200214_2321']   # twpa on, fixed attn, fixed attn, low iq value
#filenames=['looprabidata_20200215_1130','looprabidata_20200215_1302','looprabidata_20200215_1436']  # twpa off, fixed attn, fixed attn, low iq value
#filenames=['looprabidata_20200214_2158','looprabidata_20200214_2321','looprabidata_20200215_1130','looprabidata_20200215_1302','looprabidata_20200215_1436']
#filenames=['looprabidata_20200215_2206','looprabidata_20200215_2339']   # loopback at panel, fixed attn, amp, fixed attn
#filenames=['looprabidata_20200216_1603']                                # attn1 19dB, fixed attn, amp, twpa on
#filenames=['looprabidata_20200216_2237','looprabidata_20200217_0046']   # attn1 5dB, fixed attn, amp, twpa on
#filenames=['looprabidata_20200217_0046']   # attn1 5dB, fixed attn, amp, twpa on
#filenames=['looprabidata_20200217_1030']                                # attn1 5dB, fixed attn, amp, twpa on
#filenames=['looprabidata_20200217_1307']                                # attn1 10dB, fixed attn, amp, twpa on
#filenames=['looprabidata_20200217_0724']                                # attn1 5dB, fixed attn, amp, twpa off
#filenames=['looprabidata_20200216_2237','looprabidata_20200217_0046','looprabidata_20200217_0724']
qubitid_list=['Q0','Q6','Q1']
#filenames=['looprabidata_20200219_0057']  # BPF fixedattn8   amp fixedattn3   twpa on
filenames=['looprabidata_20200219_1009']  # BPF fixedattn8   amp fixedattn3   twpa off

for qubitid in qubitid_list:
	time=[]
	rabi_i=[]
	rabi_q=[]
	for filename in filenames:
		data=open(filename,'r').read().splitlines()
		for i in range(len(data)):
			if 'save data to  _'+qubitid in data[i]:
				time_tmp=dates.date2num(datetime.datetime.strptime(data[i][-26:-4],'%Y%m%d_%H%M%S_%f'))
				time.append(time_tmp)
			if ' qubitid: '+qubitid in data[i]:
				rabi_i.append(float(data[i].split(' ')[-5]))
				rabi_q.append(float(data[i].split(' ')[-1]))
	if len(time)!=len(rabi_i):
		time=time[:len(rabi_i)]

	fig=pyplot.figure()
	ax=fig.add_subplot(211)
	ax.xaxis.set_major_formatter(dates.DateFormatter('%m/%d/%Y %H:%M'))
	ax.plot_date(time,rabi_i,ls='-',marker='.')
	ax.set_title(qubitid)
	ax.set_ylabel('Rabi I')
	fig.autofmt_xdate(rotation=30)
	fig.tight_layout()
	bx=fig.add_subplot(212)
	bx.xaxis.set_major_formatter(dates.DateFormatter('%m/%d/%Y %H:%M'))
	bx.plot_date(time,rabi_q,ls='-',marker='.')
	bx.set_ylabel('Rabi Q')
	fig.autofmt_xdate(rotation=30)
	fig.tight_layout()

	color=[str(1.0*item/len(rabi_i)) for item in range(len(rabi_i))]
	pyplot.figure()
	cx=pyplot.subplot()
	cx.set_aspect('equal')
	#cx.plot(rabi_i,rabi_q,'.')
	cx.scatter(rabi_i,rabi_q,s=10,color=color)
	cx.set_title(qubitid)
	cx.set_xlabel('I')
	cx.set_ylabel('Q')
	pyplot.tight_layout()

	meas=numpy.array(rabi_i)+1j*numpy.array(rabi_q)
#	fig3=pyplot.figure()
#	dx=fig3.add_subplot(211)
#	dx.xaxis.set_major_formatter(dates.DateFormatter('%m/%d/%Y %H:%M'))
#	dx.plot_date(time,abs(meas),ls='-',marker='.')
#	dx.set_title(qubitid)
#	dx.set_ylabel('Magnitude')
#	fig3.autofmt_xdate(rotation=30)
#	fig3.tight_layout()
#	ex=fig3.add_subplot(212)
#	ex.xaxis.set_major_formatter(dates.DateFormatter('%m/%d/%Y %H:%M'))
#	ex.plot_date(time,numpy.unwrap(numpy.angle(meas)),ls='-',marker='.')
#	ex.set_ylabel('Phase')
#	fig3.autofmt_xdate(rotation=30)
#	fig3.tight_layout()

	fig4=pyplot.figure()
	fx=fig4.add_subplot(111)
	fx.xaxis.set_major_formatter(dates.DateFormatter('%m/%d/%Y %H:%M'))
	fx.plot_date(time,1.0*abs(meas)/numpy.mean(abs(meas)),ls='-',marker='.')
	fx.set_title(qubitid)
	fx.set_ylabel('Normalized Magnitude')
	fig4.autofmt_xdate(rotation=30)
	fig4.tight_layout()

	pyplot.figure()
	gx=pyplot.subplot()
	gx.set_aspect('equal')
	#cx.plot(rabi_i,rabi_q,'.')
	gx.scatter(rabi_i,rabi_q,s=10,color=color)
	xylim=1.05*max([max(abs(numpy.array(rabi_i))),max(abs(numpy.array(rabi_q)))])
	gx.set_xlim([-xylim,xylim])
	gx.set_ylim([-xylim,xylim])
	gx.set_title(qubitid)
	gx.set_xlabel('I')
	gx.set_ylabel('Q')
	pyplot.tight_layout()

pyplot.show()
