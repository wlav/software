import datetime
import argparse
import sys
from squbic import *
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
from scipy import signal
from experiment import c_experiment
from vna import c_vna
import vxi11

if __name__=="__main__":
	vna=c_vna()
	parser,cmdlinestr=vna.cmdoptions()
	parser.set_defaults(delaybetweenelement=2e-6)
	parser.set_defaults(elementlength=1024)
	clargs=parser.parse_args()
	vna.vnaseqs(delayread=clargs.delayread,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,readoutdrvamp=clargs.readoutdrvamp,readwidth=None,fstart=clargs.fstart,fstop=clargs.fstop,rdc=0,qubitidread=clargs.qubitidread)
	vna.run(bypass=clargs.bypass)

	vna_df=1.0*(clargs.fstop-clargs.fstart)/(clargs.elementlength-1)
	print('elementlength',clargs.elementlength)
	vna_freq=numpy.arange(clargs.fstart,clargs.fstop+vna_df,vna_df)[:clargs.elementlength]
	twpa_fstart=6.763e9
	twpa_fstop=6.803e9+2e6
	twpa_fstep=2e6
	twpa_pstart=-2.0
	twpa_pstop=5.0+0.2
	twpa_pstep=0.2
	twpa_timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	twpa_on_filename='%s.dat'%('_'.join(['twpa','pumpf',str(twpa_fstart),str(twpa_fstop),str(twpa_fstep),'pumpp',str(twpa_pstart),str(twpa_pstop),str(twpa_pstep),twpa_timestamp]))
	twpa_off_filename='%s.dat'%('_'.join(['twpa','pumpoff',twpa_timestamp]))

	instr=vxi11.Instrument("192.168.1.23")
	print(instr.ask("*IDN?\n"))
	for twpa_freq in numpy.arange(twpa_fstart,twpa_fstop,twpa_fstep):
		instr.write("FREQ %s %s\r\n"%(twpa_freq,'Hz'))
		for twpa_power in numpy.arange(twpa_pstart,twpa_pstop,twpa_pstep):
			instr.write("POW %s %s\r\n"%(twpa_power,'dBm'))
			print('Pump Freq: ',instr.ask("FREQ?\n"))
			print('Pump Power: ',instr.ask("POW?\n"))
			for twpa_status in ['ON','OFF']:
				instr.write("OUTP %s \r\n"%(twpa_status))
				print('BNC845 status',instr.ask("OUTP?\n"))
				time.sleep(1)
				if clargs.processfile=='':
					if clargs.sim:
						vna.sim()
				#	vna.run(bypass=clargs.bypass)  # no need to run here
					data=vna.vnadata(clargs.nget)
					fprocess=vna.savejsondata(filename='pump'+twpa_status+'_pumpf'+str(twpa_freq)+'_pumpp'+str(twpa_power)+clargs.filename,extype='twpa',cmdlinestr=cmdlinestr,data=data)
				else:
					fprocess=clargs.processfile
				c=vna.processvna(fprocess)
				if clargs.plot:
					vna.vnaplot(c)
					pyplot.grid()
					pyplot.show()
				twpa_filename=twpa_on_filename if twpa_status=='ON' else twpa_off_filename
				with open(twpa_filename,'a') as f:
					numpy.savetxt(f,(vna_freq,c))
				time.sleep(1)
