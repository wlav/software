#import matplotlib
#matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import copy
import experiment
from rabi_w import c_rabi
import qutip
import math

class c_crrepeat(c_rabi):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid_c=None
		self.qubitid_t=None
		pass
	def crrepeatseqs(self,delayread=712e-9,delay1=12e-6,delaybetweenelement=600e-6,elementlength=80,elementstep=4e-9,qubitid_c='Q5',qubitid_t='Q4',ctrl='I',meas='I',pcr=0,txgate=0e-9,qubitidread=['Q5','Q4','Q3'],amp_multiplier=0,phase_offset=0,fixed_ramp=False,n_repeat=1):
		self.qubitid_c=qubitid_c
		self.qubitid_t=qubitid_t
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		modini_simul={}
		modcr_simul={}
		modcr={}
		modxgate={}
		self.tcr_list=[]
		run=0
		for irun in range(elementlength):
			# Readout
			therald=run
			self.seqs.add(therald,self.qchip.gates[qubitid_t+'read'])
			run=self.seqs.tend()+delay1
			tini=run

			# Control state preparation (control qubit: I or X180)
			if ctrl=='X180':
				self.seqs.add(run,         	 self.qchip.gates[qubitid_c+'X180'])
				run=self.seqs.tend()

			for i_repeat in range(n_repeat):
				# CR gate (drive the control qubit at the frequency of the target frequency)
				if fixed_ramp:
					tcr=elementstep*(irun+math.ceil(64e-9/elementstep))
					modcr.update(dict(twidth=tcr,pcarrier=self.qchip.gates['CR('+qubitid_c+qubitid_t+')'].pcalc(dt=run-tini,padd=pcr)[0]))
				else:
					tcr=elementstep*irun
					modcr.update(dict(twidth=tcr,pcarrier=self.qchip.gates['CR('+qubitid_c+qubitid_t+')'].pcalc(dt=run-tini,padd=pcr)[0],env=[{'env_func': 'cos_edge_square', 'paradict': {'ramp_fraction': 0.25,'ramp_length':None}}]))
				if i_repeat==0:
					self.tcr_list.append(tcr)
				if tcr>1e-14:
					self.seqs.add(run,self.qchip.gates['CR('+qubitid_c+qubitid_t+')'].modify(modcr))
					run=self.seqs.tend()

				# CNOT X gate (target qubit)
				if txgate>1e-14:
					modxgate.update(dict(twidth=txgate,pcarrier=self.qchip.gates['CNOT('+qubitid_c+qubitid_t+').'+qubitid_t+'X'].pcalc(dt=run-tini)[0]))
					self.seqs.add(run,self.qchip.gates['CNOT('+qubitid_c+qubitid_t+').'+qubitid_t+'X'].modify(modxgate))
					run=self.seqs.tend()

			# Projection (target qubit: Y-90, X90 or I)
			if meas=='I':
				pmeas=0
			else:
				pmeas=self.qchip.gates[qubitid_t+meas].pcalc(dt=run-tini)[0]
				self.seqs.add(self.seqs.tend(),self.qchip.gates[qubitid_t+meas].modify(dict(pcarrier=pmeas)))

			# Readout
			treaddrv=self.seqs.tend()
			self.seqs.add(treaddrv,self.qchip.gates[qubitid_t+'read'])

			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
	def processcrrepeat(self,dt,filename,loaddataset):
		c=self.loadjsondata(filename)
		print('c.keys()',c.keys())
		data=c[list(c.keys())[0]]
		crrepeat_result=self.process3(data,qubitid=self.qubitid_t,lengthperrow=self.bufwidth_dict[self.qubitid_t],training=False,loaddataset=loaddataset)
		return [data,crrepeat_result['separation'],crrepeat_result['iqafterherald'],crrepeat_result['population_norm']]


if __name__=="__main__":
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	parser,cmdlinestr=experiment.cmdoptions()
	clargs=parser.parse_args()
	pcr_map=numpy.array([3.925])#3.925
	txgate_map=numpy.array([0e-9])
	#pcr_map=numpy.arange(3.8,4.1,0.01)
	#pcr_map=numpy.arange(0.68+numpy.pi,0.78+numpy.pi,0.01)
	#txgate_map=numpy.arange(0,32e-9+16e-9,16e-9)
	amp_multiplier=0#1.40
	phase_offset=0#4.925-numpy.pi
	n_repeat=3
	for txgate in txgate_map:
		print('*****txgate*****',txgate)
		for pcr in pcr_map:
			print('*****pcr*****',pcr)
			if clargs.processfile=='':
				for ctrl in ['I','X180']:
					for meas in ['Y-90','X90','I']:
						crrepeat=c_crrepeat(**clargs.__dict__)
						#crrepeat.initseqs()
						if clargs.sim:
							crrepeat.setsim()
						crrepeat.crrepeatseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,elementstep=clargs.elementstep,qubitid_c=clargs.qubitid_c,qubitid_t=clargs.qubitid_t,ctrl=ctrl,meas=meas,pcr=pcr,txgate=txgate,qubitidread=clargs.qubitidread,amp_multiplier=amp_multiplier,phase_offset=phase_offset,fixed_ramp=clargs.fixed_ramp,n_repeat=n_repeat)
						crrepeat.run()
						data=crrepeat.rabiacq(clargs.nget)
						fprocess=crrepeat.savejsondata(filename=clargs.filename,extype='crrepeat',cmdlinestr=cmdlinestr,data=data)
						print('save data to ',fprocess)
						if clargs.sim:
							crrepeat.sim()
						#[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=crrepeat.processrabi(dt=clargs.elementstep,filename=fprocess,dumpname=fprocess[:-4],plot=clargs.plot)
						[rawdata,separation,iqafterherald,population_norm]=crrepeat.processcrrepeat(dt=clargs.elementstep,filename=fprocess,loaddataset=clargs.dataset)
						if clargs.readcorr:
							population_norm=crrepeat.readoutcorrection(qubitid_list=[clargs.qubitid_t],meas_binned=numpy.vstack((1-population_norm,population_norm)),corr_matrix=numpy.load(clargs.corrmx))[1]
							print('corrected population_norm',population_norm)
						#[amp_cr,period_cr,fiterr_cr]=crrepeat.fitrabi(dx=clargs.elementstep,data=population_norm,figname=meas)
						#print 'amp:%6e    period:%6e'%(amp_cr,period_cr)
						#crrepeat.plotrawdata(d1=rawdata,figname='blobs'+fprocess)
						#crrepeat.plotpopulation_norm(population_norm=population_norm,figname='population'+fprocess)
						if clargs.plot:
							pyplot.grid()
							pyplot.show()
						pnt_traj=1-2*population_norm
						with open(clargs.filename+'_cross_resonance'+'_pcr'+str(pcr)+'_txgate'+str(txgate)+'_'+timestamp+'.dat','a') as f:
							numpy.savetxt(f,[pnt_traj])
				with open(clargs.filename+'_cross_resonance'+'_pcr'+str(pcr)+'_txgate'+str(txgate)+'_'+timestamp+'.dat','a') as f:
					#numpy.savetxt(f,[numpy.arange(0,round(clargs.elementlength*clargs.elementstep,15),round(clargs.elementstep,15))])
					numpy.savetxt(f,[numpy.array(crrepeat.tcr_list)])

				crdata=numpy.loadtxt(clargs.filename+'_cross_resonance'+'_pcr'+str(pcr)+'_txgate'+str(txgate)+'_'+timestamp+'.dat')
			else:
				crdata=numpy.loadtxt(clargs.processfile)
			for index,target_projection in enumerate(['Target <X>','Target <Y>','Target <Z>']):
				pyplot.figure()
				pyplot.plot(crdata[6,:],crdata[index,:],label='Control |0>',color='b')
				pyplot.plot(crdata[6,:],crdata[index+3,:],label='Control |1>',color='r')
				pyplot.legend()
				pyplot.xlabel('Pulse Length (ns)')
				pyplot.ylabel(target_projection)
				#pyplot.ylim(-1,1)
				pyplot.ylim(-1.1,1.1)
				pyplot.savefig(clargs.filename+'_cross_resonance_'+str(index)+'_pcr'+str(pcr)+'_txgate'+str(txgate)+'_'+timestamp+'.png')
			distance=numpy.sqrt((crdata[0,:]-crdata[3,:])**2+(crdata[1,:]-crdata[4,:])**2+(crdata[2,:]-crdata[5,:])**2)/2.0
			pyplot.figure()
			pyplot.plot(crdata[6,:],distance)
			pyplot.xlabel('Pulse Length (ns)')
			pyplot.ylabel(r'$|\vec{R}|$')
			#pyplot.ylim(0,1)
			pyplot.ylim(-0.05,1.05)
			pyplot.savefig(clargs.filename+'_cross_resonance_R'+'_pcr'+str(pcr)+'_txgate'+str(txgate)+'_'+timestamp+'.png')
			print('distance',distance)
			bloch=qutip.Bloch()
			bloch.add_points(crdata[0:3,:])
			bloch.add_points(crdata[3:6,:])
			bloch.show()
			pyplot.show()
			bloch.save(clargs.filename+'_cross_resonance_T'+'_pcr'+str(pcr)+'_txgate'+str(txgate)+'_'+timestamp+'.png')

# python crrepeat.py -n 50 --qubitid_c Q6 --qubitid_t Q5 --readcorr
# python crrepeat.py -n 50 --qubitid_c Q6 --qubitid_t Q5 --readcorr -el 40 -es 8e-9
# python crrepeat.py -n 50 --qubitid_c Q6 --qubitid_t Q5 --readcorr -el 40 -es 8e-9 --fixed_ramp
