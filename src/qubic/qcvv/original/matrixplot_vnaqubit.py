import numpy
import os
import glob
#readoutdrvamp_map=numpy.array([0.345]*8)
#fread_map=numpy.array([-182.5e6,-130.6e6,-73.6e6,-12.7e6,44.3e6,103324180.18406232,160524828.70687464,217.5e6])
#readoutdrvamp_map=numpy.array([0.2]*8)
#fread_map=numpy.array([-181.4e6,-127.5e6,-71.4e6,-12.0e6,44.8e6,104.3e6,160.7e6,217.8e6])
readoutdrvamp_map=numpy.array([0.15,0.2,0.2,0.15,0.15,0.3,0.3,0.2])
fread_map=numpy.array([-181.0e6,-127.5e6,-71.4e6,-11.9e6,45.0e6,104.1e6,160.5e6,217.8e6])
qubitdrvamp_map=numpy.arange(0.1,0.6,0.1)
switch_map=numpy.array([1,2,3,4])
for iqubitdrvamp in range(len(qubitdrvamp_map)):
	for iswitch in range(len(switch_map)):
		for ifread in range(len(fread_map)):
			cmdlinestr='rdrv'+str(readoutdrvamp_map[ifread])+'_qdrv'+str(qubitdrvamp_map[iqubitdrvamp])+'_fr'+str(fread_map[ifread])+'_sw'+str(switch_map[iswitch])
			filename='_vnaqubitscan_'+cmdlinestr
			matchfilename=glob.glob(filename+'*.png')
			if len(matchfilename)==1:
				fname=matchfilename[0][0:-4]
				print(r'\includegraphics[width=0.125\textwidth,height=0.22\textheight]{{./vnaqubitscan/%s}.png}'%fname)
				pass
			else:
				print(filename)
		print(r'\\')
	print(r'\newpage')
	




