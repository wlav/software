import json
import sys
sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
from squbic import c_hfbridge,c_qchip,c_seq,c_seqs
import time
import argparse
def signvalue(vin,width=16):
	return vin-2**width if vin>>(width-1) else vin

if __name__=="__main__":
	chan=sys.argv[1]
	print(len(sys.argv))
	#val=int(sys.argv[2])
	reg="dac%d_dc"%(int(sys.argv[1]))
	print(reg)
	hf=c_hfbridge()
	hf.read([reg,])
	val=hf.getregval(reg)[0]
	if len(sys.argv)<3:
		val=val+10
	else:
		val=int(sys.argv[2])
	hf.write([(reg,val),])
	hf.read([reg,])
	print(hf.getregval(reg)[0])

