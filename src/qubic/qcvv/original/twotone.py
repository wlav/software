#import matplotlib
#matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
import datetime
import argparse
import sys
from squbic import *
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
from scipy import signal
import experiment
class c_twotone(experiment.c_experiment):
	def __init__(self,**kwargs):
		experiment.c_experiment.__init__(self,**kwargs)
		for k,v in kwargs.items():
			setattr(self,k,v)
	def twotoneseqs(self,rdc=0,**kw):
		for k,v in kw.items():
			setattr(self,k,v)
		df=1.0*(self.fstop-self.fstart)/(self.elementlength-1)
		self.freq=numpy.arange(self.fstart,self.fstop+df,df)[:self.elementlength] # avoid rounding error in python, make sure that self.freq length is elementlength
		print(len(self.freq))
		modrdrv={}
		modread={}
		modqdrv={}
		if self.readoutdrvamp:
			modrdrv.update(dict(amp=min(1-rdc,self.readoutdrvamp)))
		if self.qubitdrvamp:
			modqdrv.update(amp=self.qubitdrvamp)
		if self.fread:
			modrdrv.update(dict(fcarrier=self.fread))
			modread.update(dict(fcarrier=self.fread))
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done','qubitid',self.qubitid)
		trun=0
		for fro in self.freq:
			modrdrv.update(dict(fcarrier=fro))
			modread.update(dict(fcarrier=fro))
			#modread.update(dict(fcarrier=fro))
			self.seqs.add(trun,self.qchip.gates[self.qubitid+'rabi'].modify(modqdrv))
			treaddrv=self.seqs.tend()
			self.seqs.add(treaddrv,self.qchip.gates[self.qubitid+'read'].modify([modrdrv,modread]))
			trun=self.seqs.tend()+self.delaybetweenelement
		print('period',trun)
		self.seqs.setperiod(period=trun)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in self.qubitidread]
		self.bufwidth_dict=dict(zip(self.qubitidread,bufwidth))
	def twotonedata(self,nget=None):
		nget = self.nget if nget==None else nget
		data=self.acqdata(nget)
		return data
	def processtwotonefile(self,filename):
		#c=self.loadjsondata(filename)['accout_0']
		c=self.loadjsondata(filename)
		self.processtwotone(c)
	def processtwotone(self,c):
		print(list(c.keys())[0])
		c=numpy.array(c[list(c.keys())[0]])
		c=c[:,0]+1j*c[:,1]
		cavr=c.reshape((-1,len(self.freq))).mean(0)
		return cavr
	def twotoneplot(self,c):
		pyplot.subplot(211)
		pyplot.semilogy(self.freq,abs(c))
		pyplot.grid()
		pyplot.tight_layout()
		pyplot.subplot(212)
		pyplot.plot(self.freq,signal.detrend(numpy.unwrap(numpy.angle(c))))
		pyplot.grid()
		pyplot.tight_layout()
#
#	
if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(delaybetweenelement=2e-6)#500e-6)
	parser.set_defaults(elementlength=1024)#128)
	clargs=parser.parse_args()
	print(clargs.__dict__)
	twotone=c_twotone(**clargs.__dict__)
	twotone.twotoneseqs()
	twotone.run()
	fqubit_sweep=numpy.arange(140e6,200e6,2e6)
	data=numpy.zeros((len(fqubit_sweep),clargs.elementlength)).astype('Complex64')
	if clargs.processfile=='':
		if clargs.sim:
			twotone.sim()
		for index,fqubit in enumerate(fqubit_sweep):
			twotone.hf.cmd128all=[]
			twotone.hf.cmdgen2(destfqdrv={clargs.qubitid+'.qdrv':fqubit})
			twotone.hf.run(seqs=twotone.seqs,memclear=False,cmdclear=False)
			d=twotone.twotonedata(clargs.nget)
			c=twotone.processtwotone(d)
			print(c,c.shape,data.shape)
			data[index]=c
			print(data[index])
		fprocess=twotone.savejsondata(filename=clargs.filename,extype='twotone',cmdlinestr=cmdlinestr,data={'datare':data.real.tolist(),'dataim':data.imag.tolist(),'fqubit':fqubit_sweep.tolist(),'fro':twotone.freq.tolist()})
		print('save data to ',fprocess)
	else:
		fprocess=clargs.processfile
#	if clargs.plot:
#		twotone.twotoneplot(c)
#		pyplot.savefig(fprocess+'.pdf')
#		pyplot.show()
