from matplotlib import pyplot,cm
import numpy

vna_elementlength=1024
vna_fstart=91e6#148e6#-500e6
vna_fstop=101e6#158e6#500e6
vna_df=1.0*(vna_fstop-vna_fstart)/(vna_elementlength-1)
vna_freq=numpy.arange(vna_fstart,vna_fstop+vna_df,vna_df)[:vna_elementlength]
vna_flength=len(vna_freq)

twpa_fstart=6.763e9
twpa_fstop=6.803e9+2e6
twpa_fstep=2e6
twpa_pstart=-2.0
twpa_pstop=5.0+0.2
twpa_pstep=0.2
twpa_flength=len(numpy.arange(twpa_fstart,twpa_fstop,twpa_fstep))
twpa_plength=len(numpy.arange(twpa_pstart,twpa_pstop,twpa_pstep))

twpa_on_filename='twpa_pumpf_6763000000.0_6805000000.0_2000000.0_pumpp_-2.0_5.2_0.2_20200212_081932_287815.dat'
twpa_off_filename='twpa_pumpoff_20200212_081932_287815.dat'
data=numpy.loadtxt(twpa_on_filename,dtype=complex,converters=dict((k,lambda s: complex(s.decode().replace('+-', '-'))) for k in range(vna_elementlength)))
data0=numpy.loadtxt(twpa_off_filename,dtype=complex,converters=dict((k,lambda s: complex(s.decode().replace('+-', '-'))) for k in range(vna_elementlength)))
S21=data[1::2,:]
Soff=data0[1::2,:]
amp_on=abs(S21)
amp_off=abs(Soff)
amp=20*numpy.log10(amp_on/amp_off)
for i in range(twpa_flength):
	print '%3d of %3d'%(i+1,twpa_flength)
	amp_frame=amp[i*twpa_plength:(i+1)*twpa_plength,:]
	Z_amp=amp_frame.reshape((twpa_plength,vna_flength))
	pyplot.figure()
	#im_amp=pyplot.imshow(Z_amp,aspect='auto',extent=[vna_fstart-vna_df,vna_fstop,twpa_pstop-twpa_pstep,twpa_pstart-twpa_pstep],vmax=amp.max(),vmin=amp.min())
	im_amp=pyplot.imshow(Z_amp,aspect='auto',extent=[vna_fstart-vna_df,vna_fstop,twpa_pstop-twpa_pstep,twpa_pstart-twpa_pstep],vmax=20,vmin=-10)
	#im_amp=pyplot.imshow(Z_amp,aspect='auto',extent=[vna_fstart-vna_df,vna_fstop,twpa_pstop-twpa_pstep,twpa_pstart-twpa_pstep],vmax=30,vmin=0)
	pyplot.xlabel('fr (Hz)')
	pyplot.ylabel('pumpp (dBm)')
	pyplot.title('pumpf %.9e Hz'%(twpa_fstart+i*twpa_fstep))
	cb=pyplot.colorbar(im_amp)
	cb.set_label('TWPA gain (dB)')
	#pyplot.savefig('twpa_'+twpa_filename[-26:-4]+'_%03d.png'%i,dpi=600)  # high resolution
	pyplot.savefig('twpa_'+twpa_on_filename[-26:-4]+'_%03d.png'%i)
print 'convert -delay 100 -loop 0 twpa_%s*.png twpa_%s.gif'%(twpa_on_filename[-26:-4],twpa_on_filename[-26:-4])
pyplot.show()
