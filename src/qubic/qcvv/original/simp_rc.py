import datetime
from matplotlib import pyplot
import numpy
import experiment
import json
import trueq
import qubic_trueq
from simp import c_simp

if __name__=="__main__":
	starttime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	config = trueq.Config("""
        Name: MyDevice
        N_Systems: 2
        Mode: ZXZXZ
        Dimension: 2
        Gate Z(phi):
            Hamiltonian:
                - ["Z", phi]
            Involving: {'(5,)': (), '(6,)': ()}
        Gate X(phi):
            Hamiltonian:
                - ["X", phi]
            Involving: {'(5,)': (), '(6,)': ()}
        Gate CNOT:
            Matrix:
                - [1, 0, 0, 0]
                - [0, 1, 0, 0]
                - [0, 0, 0, 1]
                - [0, 0, 1, 0]
            Involving:
                (6, 5) : ()
        """
	)	
	
	n_random_cycles = [4]
	n_circuits = 1

	simp=c_simp()
	parser,cmdlinestr=simp.cmdoptions()
	clargs=parser.parse_args()

	gates=[simp.qchip.gates['M0mark'],simp.qchip.gates['Q6X90'],simp.qchip.gates['Q6read'],simp.qchip.gates['Q5X90'],simp.qchip.gates['Q5read']]

	elementlist={'Q6.qdrv':2,'Q5.qdrv':3,'Q7.qdrv':1,'Q6.rdrv':5,'Q5.rdrv':6,'Q7.rdrv':4,'Q6.read':9,'Q5.read':10,'M0.mark':12}
	destlist=   {'Q6.qdrv':2,'Q5.qdrv':3,'Q7.qdrv':1,'Q6.rdrv':0,'Q5.rdrv':0,'Q7.rdrv':0,'Q6.read':5,'Q5.read':6,'M0.mark':13}
	patchlist={'Q6.rdrv':0,'Q6.read':0,'Q6.qdrv':0,'Q5.rdrv':8e-9,'Q5.read':8e-9,'Q5.qdrv':8e-9,'M0.mark':0}
	delayafterheralding=12e-6
	delaybetweenelement=600e-6
	firsttime=True

	if clargs.processfile=='':
		if clargs.sim:
			simp.setsim()
		timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		datetimeformat='%Y%m%d_%H%M%S_%f'
		print('timestamp to start',(datetime.datetime.strptime(timestamp,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).seconds)
		circuits=qubic_trueq.trueq_srb(timestamp=timestamp,qubitid_list=clargs.qubitid_list,n_random_cycles=n_random_cycles,n_circuits=n_circuits,independent=True)#,twirling_group="SU")

		bare_circuit=circuits[0]
		num_RC=1000
		RC_circuits=trueq.randomly_compile(bare_circuit,n_compilations=num_RC)
		circuits=RC_circuits

		cirgentime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		transpiler = trueq.compilation.get_transpiler(config)
		transpiled_circuit = transpiler.compile(circuits)
		print('cirgentime to start',(datetime.datetime.strptime(cirgentime,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).seconds)
		trueq_result=[]
		index=0
		lastcmdarraylength=0
		for circuit in transpiled_circuit:
			print('index',index)
			index=index+1
			[qubitid,cmdarray]=qubic_trueq.trueq_circ_to_qubic2(circuit,heralding=delayafterheralding>0)
			#print('cmdarray len',len(cmdarray))
			cmdarraylength=simp.simpseqsgen(delayafterheralding=delayafterheralding,delaybetweenelement=delaybetweenelement,cmdarray=cmdarray,gates=gates if firsttime else None,elementlist=elementlist,destlist=destlist,patchlist=patchlist,qubitid=qubitid)
			simp.run(memwrite=firsttime,memclear=firsttime,cmdclear=cmdarraylength<lastcmdarraylength,preread=firsttime,cmdclearlength=lastcmdarraylength*4)
			lastcmdarraylength=cmdarraylength
			print('cmdarray this',cmdarraylength,'last',lastcmdarraylength)
			data=simp.simpacq(clargs.nget)
			fprocess=simp.savejsondata(filename=clargs.filename,extype='simp',cmdlinestr=cmdlinestr,data=data)
			print('save data to ',fprocess)
			if clargs.sim:
				simp.sim()
			trueq_return=simp.processsimp(filename=fprocess,loaddataset=clargs.dataset)
			print(trueq_return)
			trueq_result.extend(trueq_return)
			firsttime=False

		print(trueq_result)
		s=trueq.Simulator()
		s.run(circuits)
		print(circuits.results)
		timestampresult=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		resultfilename='circuits_'+timestamp+'_'+timestampresult+'.dat'


		with open(resultfilename,'w') as f_trueqresult:
			json.dump(trueq_result,f_trueqresult)
##		print('resultfile %s'%resultfilename)
		beforerb1q_process=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		print('beforerb1q_process to start',(datetime.datetime.strptime(beforerb1q_process,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).seconds)
	else:
		resultfilename=clargs.processfile


	with open(resultfilename) as file_read:
		result_plot=json.loads(file_read.read())
	population=[]
	population_0=[]
	count_0=[]
	count_1=[]
	total=[]
	for item in result_plot:
		totalitem=item['0']+item['1']
		total.append(totalitem)
		population.append(item['1']/totalitem)
		population_0.append(item['0']/totalitem)
		count_0.append(item['0'])
		count_1.append(item['1'])
	pyplot.figure(1)
	pyplot.hist(population,histtype='bar',alpha=0.3)
	#pyplot.axvline(0.2,linestyle='--',linewidth=1,label='Source Circuit')
	pyplot.xlabel('Population')
	pyplot.ylabel('Counts')
	#pyplot.legend()
	pyplot.savefig(resultfilename+'_population.pdf')
	pyplot.figure(2)
	pyplot.hist(count_0,histtype='bar',alpha=0.3,label='|0>')
	pyplot.hist(count_1,histtype='bar',alpha=0.3,label='|1>')
	pyplot.xlabel('Result Raw Counts')
	pyplot.ylabel('Counts')
	pyplot.legend()
	pyplot.savefig(resultfilename+'_rawcounts.pdf')
	pyplot.figure(3)
	width=0.5
	ind=numpy.arange(len(count_0))
	rect_bar=[0.1,0.1,0.65,0.80]
	rect_his=[0.755,0.1,0.2,0.80]
	ax_bar3=pyplot.axes(rect_bar)
	ax_his3=pyplot.axes(rect_his)
	p1 = ax_bar3.bar(ind, count_1, width, color='b')
	p2 = ax_bar3.bar(ind, count_0, width,bottom=count_1, color='#ff7f0e')
	ax_bar3.legend((p1[0],p2[0]),('|1>','|0>'),loc='center')
	ax_bar3.set_xlabel('random circuits')
	ax_bar3.set_ylabel('count')
	ax_bar3.set_title('randomized compliling')
	bins=numpy.arange(0,1024,10)
	p3=ax_his3.hist(total,bins=bins,orientation='horizontal',color='r')
	ax_his3.legend(['total'],loc='center')
	ax_bar3.set_ylim((0,1024))
	ax_his3.set_ylim((0,1024))
	pyplot.savefig(resultfilename+'_counts.pdf')
	pyplot.savefig(resultfilename+'_counts.png')
	pyplot.figure(4)
	ax_bar4=pyplot.axes(rect_bar)
	ax_his4=pyplot.axes(rect_his)
	p1 = ax_bar4.bar(ind, population, width, color='b')
	p2 = ax_bar4.bar(ind, population_0, width,bottom=population, color='#ff7f0e')
	bins=numpy.arange(0,1,0.01)

	p3=ax_his4.hist(population,bins=bins,orientation='horizontal',color='b')
	ax_bar4.legend((p1[0],p2[0]),('|1>','|0>'),loc='center')
	ax_his4.legend(['|1> population'],loc='center')
	ax_bar4.set_xlabel('random circuits')
	ax_bar4.set_ylabel('nomizalized population')
	ax_bar4.set_title('randomized compliling')
	pyplot.savefig(resultfilename+'_population_hist.pdf')
	pyplot.savefig(resultfilename+'_population_hist.png')
	ax_bar4.set_ylim((0,1))
	ax_his4.set_ylim((0,1))
	pyplot.show()
