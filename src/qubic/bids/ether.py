#!/usr/bin/python
import sys
import socket
import struct
import time
import sys,getopt
import os
import random
import numpy
import datetime
from time import gmtime, strftime

class c_ether:
    " Ethernet IO class for PSPEPS local bus access through mem_gateway "
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        if ip is not None and port is not None:
            print('connect ',ip,'at port ',port)
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
            self.connect()
        else:
            print('Warning: not connect to any hardware chassis, ok for simulation')

    def connect(self):
        self.socket.connect((self.ip, self.port))

    def __del__(self):
        if self.ip is None or self.port is None:
            pass
        else:
            self.socket.close()

def usage():
    print('usage: mbtest.py [commands]')
    print('-t, --target <ip address>')
    print('-h, --help')
    print('-a, --address <address in hex>')

if __name__ == "__main__":
    ip_addr = '192.168.21.11'
    port = 50006
    target = c_ether(ip_addr, port)
    print(dir(target.socket))
'''
    if len(argv)==0:
        usage()
        sys.exit()
    try:
        opts, args = getopt.getopt(argv, 'hoa:t:s:',['help','target=','address=','size='])
    except getopt.GetoptError as err:
        print(str(err))
        sys.exit(2)

    ip_addr = '192.168.21.21'
    port = 3000
    reg_address = 0x100
    reg_size = 0x100

    for opt,arg in opts:
        if opt in ('-h', '--help'):
            usage()
            sys.exit()
        elif opt in ('-t', '--target'):
            ip_addr = arg
        elif opt in ('-a', '--address'):
            reg_address = arg
        elif opt in ('-s', '--size'):
            reg_size = arg

'''
