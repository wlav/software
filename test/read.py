import sys
import numpy
import StringIO
from matplotlib import pyplot
def readcomplex(filename,nchan=3):
	f=open(filename)
	s=f.read()
	f.close()
	#d=numpy.genfromtxt(StringIO.StringIO(s),dtype=(complex,complex,complex),delimiter='  ', converters={0: lambda s: complex(s.decode().replace('+-', '-')),1: lambda s: complex(s.decode().replace('+-', '-')),2: lambda s: complex(s.decode().replace('+-', '-'))})
	d=numpy.genfromtxt(StringIO.StringIO(s),dtype=nchan*(complex,),delimiter='  ', converters=dict((k,lambda s: complex(s.decode().replace('+-', '-'))) for k in range(nchan)))
#	d1=numpy.array([i[0] for i in d])
#	d2=numpy.array([i[1] for i in d])
#	d3=numpy.array([i[2] for i in d])
#	return [d1,d2,d3]
	return d
if __name__=="__main__":
	d=numpy.array(readrabi(sys.argv[1]))
	print(d[0].shape)
	print(d[1].shape)
	print(d[2].shape)
	pyplot.plot(d[0].real,d[0].imag)

#	print type(d),d.shape,type(d[0])# d[:,0]
