import sys
import re
filename=sys.argv[1]
f=open(filename)
s=f.read()
f.close()

lines=[]
printpat='(\s*)print\s+([\s\S]*)\s*'
for l in s.split('\n'):
	m=re.match(printpat,l)
	if m:
		g=m.groups()
		content=g[1]
		if (content[0]=='(') and content[-1]==')':
			lines.append(l)
		else:
			if (content[-1]==','):
				content=''.join([content,"end=''"])
			line='%sprint(%s)'%(g[0],content)
			lines.append(line)
			print(l,line)
	else:
		lines.append(l)
new='\n'.join(lines)

f=open(filename,'w')
f.write(new)
f.close()

