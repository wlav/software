import datetime
from matplotlib import pyplot
import numpy
import experiment
import json
import re
import importlib


if __name__=="__main__":
	import trueq
	import qubic_trueq
	starttime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	datetimeformat='%Y%m%d_%H%M%S_%f'
	
	from simp2q import c_simp2q
	parser,cmdlinestr=experiment.cmdoptions()
	parser.add_argument('--rbprotocol',help='RB protocol',dest='rbprotocol',type=str,default='SRB')
	clargs=parser.parse_args()
	simp2q=c_simp2q(**clargs.__dict__)

	config=trueq.Config.from_yaml('trueq_gate_'+clargs.qubitcfg[9:-5]+'.yaml')
	print('trueq_gatecfg',config)

	wiremap=importlib.import_module(clargs.wiremapmodule)
	gatesall=wiremap.gatesall
	elementlistall=wiremap.elementlistall
	destlistall=wiremap.destlistall
	patchlistall=wiremap.patchlistall

	if clargs.processfile=='':
		[qid_c,qid_t]=[int(clargs.qubitid_list[0][1:]),int(clargs.qubitid_list[1][1:])]
		delayafterheralding=12e-6
		delaybetweenelement=600e-6
		firsttime=True
		tt_list=[]
		tt_list.append(trueq.Cycle({(qid_t,):config.Z(0),(qid_c,):config.Z(0)}))
		#tt_list.append(trueq.Cycle({(qid_t,):config.X(90),(qid_c,):config.X(90)}))
		#tt_list.append(trueq.Cycle({(qid_t,):config.X(90)}))
		#tt_list.append(trueq.Cycle({(qid_c,):config.X(90)}))
		tt_list.append(trueq.Cycle({(qid_t,):config.Z(0),(qid_c,):config.Z(0)}))
		#tt_list.append(trueq.Cycle({(qid_t,):config.X(90),(qid_c,):config.X(90)}))
		#tt_list.append(trueq.Cycle({(qid_t,):config.X(90)}))
		#tt_list.append(trueq.Cycle({(qid_c,):config.X(90)}))
		tt_list.append(trueq.Cycle({(qid_t,):config.Z(0),(qid_c,):config.Z(0)}))
		tt_list.append(trueq.Cycle({(qid_c,qid_t):config.CNOT()}))
		tt_list.append(trueq.Cycle({(qid_c,qid_t):config.CNOT()}))
		tt_list.append(trueq.Cycle({(qid_t,):trueq.Meas(),(qid_c,):trueq.Meas()}))
		circuits=trueq.Circuit(tt_list)
		transpiled_circuit=trueq.CircuitCollection(circuits)
		timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')

		trueq_result=[]
		measbinned_result=[]
		index=0
		lastcmdarraylength=0
		for circuit in transpiled_circuit:
			print('index',index)
			index=index+1
			[qubitid,cmdarray]=qubic_trueq.trueq_circ_to_qubic2(circuit,heralding=delayafterheralding>0)
			for l in cmdarray:
				print(l)
			gates=[simp2q.qchip.gates[clargs.qubitid_list[0]+clargs.qubitid_list[1]+'CNOT']]+[simp2q.qchip.gates[g] for g in gatesall if any([qid in g for qid in qubitid+['M0']]) and any([x in g for x in ['X90','read','mark']])]
			elementlist={k:elementlistall[k] for k in elementlistall.keys() if any([qid in k for qid in qubitid+['M0']])}
			destlist={k:destlistall[k] for k in destlistall.keys() if any([qid in k for qid in qubitid+['M0']])}
			patchlist={k:patchlistall[k] for k in patchlistall.keys() if any([qid in k for qid in qubitid+['M0']])}
			cmdarraylength=simp2q.simp2qseqsgen(delayafterheralding=delayafterheralding,delaybetweenelement=delaybetweenelement,cmdarray=cmdarray,gates=gates if firsttime else None,elementlist=elementlist,destlist=destlist,patchlist=patchlist,qubitid=qubitid)
			simp2q.run(memwrite=firsttime,memclear=firsttime,cmdclear=cmdarraylength<lastcmdarraylength,preread=firsttime,cmdclearlength=lastcmdarraylength*4)
			lastcmdarraylength=cmdarraylength
			print('cmdarray this',cmdarraylength,'last',lastcmdarraylength)
			data=simp2q.simp2qacq(clargs.nget)
			fprocess=simp2q.savejsondata(filename=clargs.filename,extype='simp2q',cmdlinestr=cmdlinestr,data=data)
			print('save data to ',fprocess)
			trueq_return,meas_binned=simp2q.processsimp2q(filename=fprocess,loaddataset=clargs.dataset)
			print(trueq_return)
			measbinned_result.extend([{'00':meas_binned[0][0],'01':meas_binned[1][0],'10':meas_binned[2][0],'11':meas_binned[3][0]}])
			trueq_result.extend(trueq_return)
			firsttime=False
			print('measbinned_result',measbinned_result)
		print(trueq_result)
	else:
		resultfilename=clargs.processfile


#python testtrueq.py -n 1 --plot --qubitid_list Q2 Q1
