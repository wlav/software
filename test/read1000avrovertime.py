#from matplotlib import pyplot
import numpy
import sys
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
def sign16(vin):
	return vin-65536 if vin>>15 else vin
if __name__=="__main__":
	interface=c_ether('192.168.1.122',port=3000)
	mg=c_mem_gateway(interface,min3=True,memgatewaybug=False)
#	alist=[0]
#	dlist=[4096]
#	result=mg.readwrite(alist=alist,dlist=dlist,write=1)
	xy8=numpy.zeros(2048)
	xall=[]
	yall=[]
	avr=3000
	for rep in range(avr):
		alist=[]
		dlist=[]
		alist.extend(range(0x60000,0x60000+1024))
		alist.extend(range(0x61000,0x61000+1024))
		dlist=numpy.zeros(len(alist))
		result=mg.readwrite(alist=alist,dlist=dlist,write=0)
	#	numpy.set_printoptions(precision=None, threshold=None, edgeitems=None, linewidth=200)
		xy8=numpy.array([sign16(i&0xffff) for i in mg.parse_readvalue(result)])
		x=xy8[0:1024]
		y=xy8[1024:2048]
		xall.append(x)
		yall.append(y)
		if rep%500==0:
			print(rep)
	#pyplot.plot(xy8[0:1024],xy8[1024:2048],'.')
	#print xall,yall
	#pyplot.plot(x,y,'.')
	#pyplot.hexbin(xall,yall)
	numpy.save('dat_demod_'+sys.argv[1],numpy.array([xall,yall]))
#	pyplot.plot(x/avr,'r')
#	pyplot.plot(y/avr,'g')
#	pyplot.show()

