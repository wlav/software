import numpy
from matplotlib import pyplot
import sys
import re
f=open(sys.argv[1])
s=f.read()
f.close()
args=re.findall('argline([\s\S]*?)\n',s,re.MULTILINE)
amps=re.findall('\namp([\s\S]*?)\n',s,re.MULTILINE)
seps=re.findall('\nseparation([\s\S]*?)\n',s,re.MULTILINE)
amp=[float(i) for i in amps]
sep=[float(i) for i in seps]
x=numpy.arange(0.1,4.01,0.1)
print(len(x))
pyplot.figure(1)
pyplot.plot(x,amp)
pyplot.xlabel('Readout Pulse Length ($\mu$s)')
pyplot.ylabel('Rabi Oscillation Amplitude')
pyplot.grid()
pyplot.figure(2)
pyplot.plot(x,sep)
pyplot.xlabel('Readout Pulse Length ($\mu$s)')
pyplot.ylabel('Blobs Separation')
pyplot.grid()
print(amp)
print(args)
print(sep)
pyplot.show()
