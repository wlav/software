import sys
from squbic import *
sys.path.append('../../firmware/laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
if __name__=="__main__":
	interface=c_ether('192.168.1.122',port=3000)
	mg = c_mem_gateway(interface, min3=True,memgatewaybug=False)

	with open('qubitcfg.json') as jfile:
		chipcfg=json.load(jfile)
	qchip=c_qchip(chipcfg)
	seq=[]
	trun=numpy.arange(0,50*1150e-9,1150e-9)
	for irun in range(50):
		run=trun[irun]+1e-9
		wrun=7e-9*(irun+1)
		arun=0.2*(irun+1)
		seq.extend([(run+0,qchip.gates['Q1readout'])
				,(run+0,qchip.gates['Q0readout'])
				,(run+0,qchip.gates['Q0readoutdrv'].modify({"amp":0.5}))
				,(run+0,qchip.gates['Q1readoutdrv'].modify({"amp":0.5}))
				,(run+2e-7,qchip.gates['Q1rabi'].modify({"twidth":wrun,"amp":0.5}))
				,(None,qchip.gates['Q1readoutdrv'].modify({"amp":0.5}))
			,(None,qchip.gates['Q0rabi_ef'])
			,(None,qchip.gates['Q1rabi_gh'].modify({"amp":0.5}))
			,(None,qchip.gates['Q0readoutdrv'].modify({"amp":0.5}))
			])
		#	   seq.extend([(run+0,qchip.gates['Q1rabi_gh'])
		#   ,(None,qchip.gates['Q1rabi_gh'].modify({"twidth":wrun}))
		#   ,(None,qchip.gates['Q1rabi_gh1'])
		#   ,(None,qchip.gates['Q1rabi_gh2'])
		#   ])
	seqs=c_seqs(seq)
	hf=c_hfbridge()
	((cmda,cmdv),ops)=hf.cmdgen(seqs)
#	print cmdv
#	cmdv=numpy.array(cmdv).astype('Complex64')
	alist=list(cmda)
	dlist=[((int(v.real*19894)<<16)+int(v.imag*19894)) for v in cmdv]
#	list((int(numpy.round((cmdv*19800).real))<<16)+(int(numpy.round((cmdv*19800).imag))))
	print([hex(i) for i in alist])
	print([hex(i) for i in dlist])
	result=mg.readwrite(alist=alist,dlist=dlist,write=1)
	alist=[]
	dlist=[]
	cmddict={}
	for cmd in ops:
		cmdadd(cmddict,cmdgen(trig_t=int(ops[cmd]['trig_t'])
			,element=int(ops[cmd]['element'])
			,dest=int(ops[cmd]['dest'])
			,start=int(ops[cmd]['start'])
			,length=int(ops[cmd]['length'])
			,phini=ops[cmd]['phini']/2/numpy.pi*360
			,freq=ops[cmd]['freq']
			))
	cmdlist=[v for k,v in sorted(cmddict.items())]
	for i in range(len(cmdlist)):
		alist.extend([0x40000+2*i,0x40400+2*i,0x40400+2*i+1])
		dlist.extend([(cmdlist[i]>>64)&0xffffffff,cmdlist[i]&0xffffffff,(cmdlist[i]>>32)&0xffffffff])
	result=mg.readwrite(alist=alist,dlist=dlist,write=1)
	print([hex(i) for i in alist])
	print([hex(i) for i in dlist])

	tend=6*500e-9
	from cmdsim import cmdsim
	sim1=cmdsim((cmda,cmdv),ops)
	for chan in range(7):
		tv=sim1.getiqmod(chan)
		#tv=sim1.getiq(chan)
		pyplot.subplot(7,2,chan*2+1)
		pyplot.plot(tv[:,0],tv[:,1].real)
		pyplot.xlim([0,tend/1e-9])
		pyplot.subplot(7,2,chan*2+2)
		pyplot.plot(tv[:,0],tv[:,1].imag)
		pyplot.xlim([0,tend/1e-9])
		print('chan %d max %8.2f'%(chan,max(abs(tv[:,1]))))
	pyplot.show()
