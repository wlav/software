import sys
sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import argparse
def signvalue(vin,width=16):
	return vin-2**width if vin>>(width-1) else vin

if __name__=="__main__":
	interface=c_ether('192.168.1.122',port=3000)
	mg=c_mem_gateway(interface, min3=True,memgatewaybug=False)
	alist=[]
	dlist=[]
	cmds=[]
        parser = argparse.ArgumentParser()
        parser.add_argument('-c','--calibration',help='calibration file',dest='califile',type=str,default='')
        parser.add_argument('-s','--shift',help='timeshift',dest='timeshift',type=int,default=0)
        parser.add_argument('-z','--zoom',help='timezoom',dest='timezoom',type=int,default=0)
        parser.add_argument('-i','--iqplot',help='iqplot',dest='iqplot',type=bool,default=True)
        parser.add_argument('-r','--readoutdrvamp',help='readout drv amp',dest='readoutdrvamp',type=float,default=0.17)
        parser.add_argument('-q','--qubitdrvamp',help='qubit drv amp',dest='qubitdrvamp',type=float,default=0.3)
        clargs=parser.parse_args()
        if not clargs.califile=='':
            caliri1=numpy.loadtxt('na_cali1.txt')
            cali1=caliri1[:,0]+1j*caliri1[:,1]

	opsel=2 # 0 for navg; 1 for min; 2 for max;
	alist.append(0x5000f)
	dlist.append(opsel)
	mon_navr=int(clargs.timezoom)
	alist.append(0x50006)
	dlist.append(mon_navr)
	mon_dt=int(clargs.timeshift)
	alist.append(0x50007)
	dlist.append(mon_dt)
	mon_slice=0
	alist.append(0x50008)
	dlist.append(mon_slice)
	mon_sel0=0
	alist.append(0x50009)
	dlist.append(mon_sel0)
	mon_sel1=2#4#13
	alist.append(0x5000a)
	dlist.append(mon_sel1)

	test=0
	alist.append(0x5000c)
	dlist.append(test)
	alist.append(0x5000b)
	dlist.append(1)
	alist.append(0x50002)
	dlist.append(1)
	result=mg.readwrite(alist=alist,dlist=dlist,write=1)
	for i in range(len(alist)):
		cmds.append("lb_write_task(20'h%x,32'h%x);"%(alist[i],dlist[i]))
	time.sleep(2)
	comp_list=[]
	addr_start=[0x42000,0x4a000,0x4b000]
	for k in range(3):
		alist=[]
		dlist=[]
		alist.extend(range(addr_start[k],addr_start[k]+0x1000))
		dlist=numpy.zeros(len(alist))
		result=mg.readwrite(alist=alist,dlist=dlist,write=0)
		for i in range(len(alist)):
			cmds.append("lb_write_task(20'h%x,32'h%x);"%(alist[i],dlist[i]))
		numpy.set_printoptions(precision=None, threshold=None, edgeitems=None, linewidth=200)
		xy8=numpy.array([signvalue(i&0xffffffff,32) for i in mg.parse_readvalue(result)])
		xy82=xy8.reshape([-1,2])
		xy8comp=xy82[:,0]+1j*xy82[:,1]
		#print 'xy8comp',xy8comp
		#print 'abs(xy8comp.mean())',abs(xy8comp.mean())
		comp_list.append(xy8comp)

	#addr_start=[0x70000,0x71000]
	addr_minmax=[0x5000d,0x5000e]
	alist=[]
	dlist=[]
	alist.extend(addr_minmax)
	dlist=numpy.zeros(len(alist))
	result=mg.readwrite(alist=alist,dlist=dlist,write=0)
	for i in range(len(addr_minmax)):
		cmds.append("lb_write_task(20'h%x,32'h%x);"%(alist[i],dlist[i]))
	numpy.set_printoptions(precision=None, threshold=None, edgeitems=None, linewidth=200)
	adc_min=numpy.array([signvalue((i&0xffff0000)>>16,16) for i in mg.parse_readvalue(result)])
	adc_max=numpy.array([signvalue(i&0xffff,16) for i in mg.parse_readvalue(result)])
	print('adc_min,adc_max',adc_min,adc_max)

	monout_list=[]
	addr_start=[0x72000,0x73000]
	MEMAW=10
	numpy.set_printoptions(precision=None, threshold=None, edgeitems=None, linewidth=200)
	for k in range(2):
		for mon_slice in range(4):
			alist=[]
			dlist=[]
			alist.append(0x50008)
			dlist.append(mon_slice)
			alist.append(0x5000b)
			dlist.append(1)
			result=mg.readwrite(alist=alist,dlist=dlist,write=1)
			time.sleep(0.5)
			alist=[]
			dlist=[]
			alist.extend(range(addr_start[k],addr_start[k]+2**MEMAW))
			dlist=list(numpy.zeros(len(alist)))
			for i in range(len(alist)):
				cmds.append("lb_write_task(20'h%x,32'h%x);"%(alist[i],dlist[i]))
			result=mg.readwrite(alist=alist,dlist=dlist,write=0)
			monout=numpy.array([signvalue(i&0xffff,16) for i in mg.parse_readvalue(result)])
			#print 'monout',monout[0:10],monout[-10:],len(monout)
			monout_list.append(monout)

	f=open('cmdsimlist.vh','a+')
	f.write('\n'.join(cmds))
	f.close()
        if not clargs.califile=='':
            comp_list[1]=comp_list[1]/cali1;
	fig1=pyplot.figure(1)
#	pyplot.plot(abs(comp_list[1]),'.-')
#	pyplot.xlabel('index')
#	pyplot.ylabel('accumulated output (a.u.)')
	#if (clargs.iqplot):
	if (0):
		for i in range(3):
			pyplot.subplot(3,4,4*i+1)
			#pyplot.semilogy(abs(comp_list[i]),'.-')
			pyplot.plot(abs(comp_list[i]),'.-')
			pyplot.subplot(3,4,4*i+2)
			pyplot.plot(comp_list[i].real,'.')
			pyplot.subplot(3,4,4*i+3)
			pyplot.plot(comp_list[i].imag,'.')
			pyplot.subplot(3,4,4*i+4)
			pyplot.plot(comp_list[i].real,comp_list[i].imag,'.')
			maxval=max(max(abs(comp_list[i].real)),max(abs(comp_list[i].imag)))
			pyplot.xlim([-1.2*maxval,1.2*maxval])
			pyplot.ylim([-1.2*maxval,1.2*maxval])
		#fig1.suptitle('readoutdrvamp %8.2f qubitdrvamp %8.2f'%(clargs.readoutdrvamp,clargs.qubitdrvamp))
		#pyplot.savefig('iqh_r%f_q%f.png'%(clargs.readoutdrvamp,clargs.qubitdrvamp))
			#pyplot.hexbin(comp_list[i].real,comp_list[i].imag,gridsize=50)
	else:
		nrun=2048
		#pyplot.semilogy(7000.+1000./nrun+1000./nrun*numpy.arange(nrun),abs(comp_list[1]),'.-')
		pyplot.semilogy(1000./nrun+1000./nrun*numpy.arange(nrun),abs(comp_list[0]),'.-')
		#pyplot.plot(1000./nrun+1000./nrun*numpy.arange(nrun),abs(comp_list[1]),'.-')
		#pyplot.xlabel('MHz')
		#pyplot.ylabel('a.u.')
		#pyplot.ylim(0,1.2)

        if clargs.califile=='':
            numpy.savetxt('na_cali0.txt',[(cali.real,cali.imag) for cali in comp_list[0]])
            numpy.savetxt('na_cali1.txt',[(cali.real,cali.imag) for cali in comp_list[1]])
            numpy.savetxt('na_cali2.txt',[(cali.real,cali.imag) for cali in comp_list[2]])

	pyplot.figure(2)
	pyplot.plot(4*numpy.arange(2**MEMAW),monout_list[0],'.-')
	pyplot.xlabel('time (ns)')
	pyplot.ylabel('ADC raw readout (count)')
	for i in range(2):
		for j in range(4):
			pyplot.subplot(2,1,i+1)
			pyplot.plot(range(2**MEMAW),monout_list[4*i+j],'.-')
#		print 'debug',clargs.timeshift,monout_list[0].min(),monout_list[0].max()
	pyplot.show()
